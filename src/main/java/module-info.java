module org.refcodes.textual {
	requires transitive org.refcodes.exception;
	requires org.refcodes.numerical;
	requires org.refcodes.runtime;
	requires transitive java.desktop;
	requires transitive org.refcodes.data;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.generator;
	requires transitive org.refcodes.graphical;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;

	exports org.refcodes.textual;
}
