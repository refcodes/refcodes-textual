// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * The {@link TableStatus} provides the status of the table, e.g. whether it has
 * begin printing the header, whether it has continued to print a row or whether
 * it has printed the tail. The {@link TableStatus} is required by a
 * {@link TablePrinter} ({@link TableBuilder}) to determine whether to do
 * additional table decoration for a print operation or to fail that operation
 * as of an illegal state exception. As far as possible an illegal state
 * exception is to be tried to be prevented by decorating the table
 * automatically by an operation as of the current {@link TableStatus}.
 */
public enum TableStatus {
	/**
	 * Nothing as been printed yet.
	 */
	NONE,
	/** The header's top line has been printed. */
	HEADER_BEGIN,
	/**
	 * A header columns line has been printed.
	 */
	HEADER_CONTINUE,
	/** The header's bottom line has been printed. */
	HEADER_END,
	/** A row's top line has been printed. */
	ROW_BEGIN,
	/**
	 * A row columns line has been printed.
	 */
	ROW_CONTINUE,
	/**
	 * The row's bottom line has been printed, usually when joining the table
	 * with another table.
	 */
	ROW_END,

	TAIL
	/**
	 * The table's tail (bottom) line has been printed finishing off the table.
	 */
}
