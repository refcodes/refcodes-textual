// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Overwrites a text with another one to the the right or to the left.
 */
public class OverwriteTextBuilder extends AbstractText<OverwriteTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _overwritingText = null;

	private OverwriteTextMode _overwriteTextMode = OverwriteTextMode.RIGHT;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the overwriting text from the overwriting text property.
	 * 
	 * @return The overwriting text stored by the overwriting text property.
	 */
	public String getOverwritingText() {
		return _overwritingText;
	}

	/**
	 * Sets the overwriting text for the overwriting text property.
	 * 
	 * @param aOverwritingText The overwriting text to be stored by the
	 *        overwriting text property.
	 */
	public void setOverwritingText( String aOverwritingText ) {
		_overwritingText = aOverwritingText;

	}

	/**
	 * Retrieves the overwrite text mode from the overwrite text mode property.
	 * 
	 * @return The overwrite text mode stored by the overwrite text mode
	 *         property.
	 */
	public OverwriteTextMode getOverwriteTextMode() {
		return _overwriteTextMode;
	}

	/**
	 * Sets the overwrite text mode for the overwrite text mode property.
	 * 
	 * @param aTextOverwriteMode The overwrite text mode to be stored by the
	 *        overwrite text mode property.
	 */
	public void setTextOverwriteMode( OverwriteTextMode aTextOverwriteMode ) {
		_overwriteTextMode = aTextOverwriteMode;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return toStrings( getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		final String[] theResult = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theResult[i] = asOverwriteText( aText[i], _overwritingText, _overwriteTextMode );
		}
		return theResult;
	}

	/**
	 * Sets the overwriting text for the overwriting text property.
	 * 
	 * @param aOverwritingText The overwriting text to be stored by the
	 *        overwriting text property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public OverwriteTextBuilder withOverwritingText( String aOverwritingText ) {
		setOverwritingText( aOverwritingText );
		return this;
	}

	/**
	 * Sets the overwrite text mode for the overwrite text mode property.
	 * 
	 * @param aOverwriteTextMode The overwrite text mode to be stored by the
	 *        overwrite text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public OverwriteTextBuilder withOverwriteTextMode( OverwriteTextMode aOverwriteTextMode ) {
		setTextOverwriteMode( aOverwriteTextMode );
		return this;
	}

	/**
	 * To overwrite.
	 *
	 * @param aText the text
	 * @param aWriteOverText the write over text
	 * @param aOverwriteTextMode the overwrite text mode
	 * 
	 * @return the string
	 */
	public static String asOverwriteText( String aText, String aWriteOverText, OverwriteTextMode aOverwriteTextMode ) {
		switch ( aOverwriteTextMode ) {
		case LEFT -> {
			return asOverwriteLeft( aText, aWriteOverText );
		}
		case RIGHT -> {
			return asOverwriteRight( aText, aWriteOverText );
		}
		}
		throw new IllegalArgumentException( "You must pass a valid overwrite text mode, though you actually passed <" + aOverwriteTextMode + ">!" );
	}

	/**
	 * Overwrites a text starting at the left hand side with the given
	 * "overwriting" text.
	 * 
	 * @param aText The text to be overwritten.
	 * @param aWriteOverText The text used for overwriting.
	 * 
	 * @return A {@link String} overwritten on the left hand side with the given
	 *         "overwriting" text.
	 */
	public static String asOverwriteLeft( String aText, String aWriteOverText ) {
		if ( aWriteOverText.length() > aText.length() ) {
			return aWriteOverText.substring( 0, aText.length() );
		}
		if ( aWriteOverText.length() == aText.length() ) {
			return aWriteOverText;
		}
		final String theText;
		theText = aWriteOverText + aText.substring( aWriteOverText.length() );
		return theText;
	}

	/**
	 * Overwrites a text starting at the right hand side with the given
	 * "overwriting" text.
	 * 
	 * @param aText The text to be overwritten.
	 * @param aWriteOverText The text used for overwriting.
	 * 
	 * @return A {@link String} overwritten on the right hand side with the
	 *         given "overwriting" text.
	 */
	public static String asOverwriteRight( String aText, String aWriteOverText ) {
		if ( aWriteOverText.length() > aText.length() ) {
			return aWriteOverText.substring( aWriteOverText.length() - aText.length() );
		}
		if ( aWriteOverText.length() == aText.length() ) {
			return aWriteOverText;
		}
		final String theText;
		theText = aText.substring( 0, aText.length() - aWriteOverText.length() ) + aWriteOverText;
		return theText;
	}
}
