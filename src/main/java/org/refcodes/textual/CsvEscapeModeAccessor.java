// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a {@link CsvEscapeMode} property.
 */
public interface CsvEscapeModeAccessor {

	/**
	 * Gets the currently set {@link CsvEscapeMode} being used.
	 * 
	 * @return The currently configured {@link CsvEscapeMode}s.
	 */
	CsvEscapeMode getCsvEscapeMode();

	/**
	 * Provides a mutator for an {@link CsvEscapeMode} property.
	 * 
	 * @param <B> The builder which implements the {@link CsvEscapeModeBuilder}.
	 */
	public interface CsvEscapeModeBuilder<B extends CsvEscapeModeBuilder<?>> {

		/**
		 * Sets the rows {@link CsvEscapeMode} to use returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aCsvEscapeMode The {@link CsvEscapeMode} to be used when
		 *        printing a row or the header.
		 * 
		 * @return This {@link CsvEscapeModeBuilder} instance to continue
		 *         configuration.
		 */
		B withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode );
	}

	/**
	 * Provides a mutator for an {@link CsvEscapeMode} property.
	 */
	public interface CsvEscapeModeMutator {

		/**
		 * Sets the {@link CsvEscapeMode} to be used.
		 * 
		 * @param aCsvEscapeMode The {@link CsvEscapeMode} to be stored by the
		 *        {@link CsvEscapeMode} property.
		 */
		void setCsvEscapeMode( CsvEscapeMode aCsvEscapeMode );
	}

	/**
	 * Provides a {@link CsvEscapeMode} property.
	 */
	public interface CsvEscapeModeProperty extends CsvEscapeModeAccessor, CsvEscapeModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link CsvEscapeMode}
		 * (setter) as of {@link #setCsvEscapeMode(CsvEscapeMode)} and returns
		 * the very same value (getter).
		 * 
		 * @param aCsvEscapeMode The {@link CsvEscapeMode} to set (via
		 *        {@link #setCsvEscapeMode(CsvEscapeMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default CsvEscapeMode letCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
			setCsvEscapeMode( aCsvEscapeMode );
			return aCsvEscapeMode;
		}
	}
}