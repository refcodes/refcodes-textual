// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a horizontal align text mode property.
 */
public interface HorizAlignTextModeAccessor {

	/**
	 * Retrieves the horizontal align text mode from the horizontal align text
	 * mode property.
	 * 
	 * @return The horizontal align text mode stored by the horizontal align
	 *         text mode property.
	 */
	HorizAlignTextMode getHorizAlignTextMode();

	/**
	 * Provides a mutator for a horizontal align text mode property.
	 */
	public interface HorizAlignTextModeMutator {

		/**
		 * Sets the horizontal align text mode for the horizontal align text
		 * mode property.
		 * 
		 * @param aHorizAlignTextMode The horizontal align text mode to be
		 *        stored by the font style property.
		 */
		void setHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode );
	}

	/**
	 * Provides a builder method for a horizontal align text mode property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface HorizAlignTextModeBuilder<B extends HorizAlignTextModeBuilder<B>> {

		/**
		 * Sets the horizontal align text mode for the horizontal align text
		 * mode property.
		 * 
		 * @param aHorizAlignTextMode The horizontal align text mode to be
		 *        stored by the font style property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode );
	}

	/**
	 * Provides a horizontal align text mode property.
	 */
	public interface HorizAlignTextModeProperty extends HorizAlignTextModeAccessor, HorizAlignTextModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link HorizAlignTextMode} (setter) as of
		 * {@link #setHorizAlignTextMode(HorizAlignTextMode)} and returns the
		 * very same value (getter).
		 * 
		 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to set (via
		 *        {@link #setHorizAlignTextMode(HorizAlignTextMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default HorizAlignTextMode letHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
			setHorizAlignTextMode( aHorizAlignTextMode );
			return aHorizAlignTextMode;
		}
	}
}
