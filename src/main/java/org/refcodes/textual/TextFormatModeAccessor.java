// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a text-format mode property.
 */
public interface TextFormatModeAccessor {

	/**
	 * Retrieves the text-format mode from the text-format mode property.
	 * 
	 * @return The text-format mode stored by the text-format mode property.
	 */
	TextFormatMode getTextFormatMode();

	/**
	 * Provides a mutator for a text-format mode property.
	 */
	public interface TextFormatModeMutator {

		/**
		 * Sets the text-format mode for the text-format mode property.
		 * 
		 * @param aTextFormatMode The text-format mode to be stored by the
		 *        text-format mode property.
		 */
		void setTextFormatMode( TextFormatMode aTextFormatMode );
	}

	/**
	 * Provides a builder method for a text-format mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TextFormatModeBuilder<B extends TextFormatModeBuilder<B>> {

		/**
		 * Sets the text-format mode for the text-format mode property.
		 * 
		 * @param aTextFormatMode The text-format mode to be stored by the
		 *        text-format mode property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTextFormatMode( TextFormatMode aTextFormatMode );
	}

	/**
	 * Provides a text-format mode property.
	 */
	public interface TextFormatModeProperty extends TextFormatModeAccessor, TextFormatModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TextFormatMode}
		 * (setter) as of {@link #setTextFormatMode(TextFormatMode)} and returns
		 * the very same value (getter).
		 * 
		 * @param aTextFormatMode The {@link TextFormatMode} to set (via
		 *        {@link #setTextFormatMode(TextFormatMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TextFormatMode letTextFormatMode( TextFormatMode aTextFormatMode ) {
			setTextFormatMode( aTextFormatMode );
			return aTextFormatMode;
		}
	}
}
