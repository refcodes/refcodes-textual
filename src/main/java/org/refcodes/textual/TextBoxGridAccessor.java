// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a {@link TextBoxGrid} property.
 */
public interface TextBoxGridAccessor {

	/**
	 * Gets the currently set {@link TextBoxGrid} being used.
	 * 
	 * @return The currently configured {@link TextBoxGrid}s.
	 */
	TextBoxGrid getTextBoxGrid();

	/**
	 * Provides a mutator for an {@link TextBoxGrid} property.
	 * 
	 * @param <B> The builder which implements the {@link TextBoxGridBuilder}.
	 */
	public interface TextBoxGridBuilder<B extends TextBoxGridBuilder<?>> {

		/**
		 * Sets the rows {@link TextBoxGrid} to use returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aTextBoxGrid The {@link TextBoxGrid} to be used.
		 * 
		 * @return This {@link TextBoxGridBuilder} instance to continue
		 *         configuration.
		 */
		B withTextBoxGrid( TextBoxGrid aTextBoxGrid );

		/**
		 * Sets the rows {@link TextBoxGrid} to use returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aTextBoxStyle The {@link TextBoxStyle} to be used.
		 * 
		 * @return This {@link TextBoxGridBuilder} instance to continue
		 *         configuration.
		 */
		default B withTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
			return withTextBoxGrid( (TextBoxGrid) aTextBoxStyle );
		}
	}

	/**
	 * Provides a mutator for an {@link TextBoxGrid} property.
	 */
	public interface TextBoxGridMutator {

		/**
		 * Sets the {@link TextBoxGrid} to be used.
		 * 
		 * @param aTextBoxGrid The {@link TextBoxGrid} to be stored by the
		 *        {@link TextBoxGrid} property.
		 */
		void setTextBoxGrid( TextBoxGrid aTextBoxGrid );

		/**
		 * Sets the {@link TextBoxGrid} to be used.
		 * 
		 * @param aTextBoxStyle The {@link TextBoxStyle} to be stored by the
		 *        {@link TextBoxGrid} property.
		 */
		default void setTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
			setTextBoxGrid( (TextBoxGrid) aTextBoxStyle );
		}
	}

	/**
	 * Provides a {@link TextBoxGrid} property.
	 */
	public interface TextBoxGridProperty extends TextBoxGridAccessor, TextBoxGridMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TextBoxGrid}
		 * (setter) as of {@link #setTextBoxGrid(TextBoxGrid)} and returns the
		 * very same value (getter).
		 * 
		 * @param aTextBoxGrid The {@link TextBoxGrid} to set (via
		 *        {@link #setTextBoxGrid(TextBoxGrid)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TextBoxGrid letTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
			setTextBoxGrid( aTextBoxGrid );
			return aTextBoxGrid;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TextBoxGrid}
		 * (setter) as of {@link #setTextBoxGrid(TextBoxGrid)} and returns the
		 * very same value (getter).
		 * 
		 * @param aTextBoxStyle The {@link TextBoxStyle} to set (via
		 *        {@link #setTextBoxGrid(TextBoxGrid)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TextBoxGrid letTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
			setTextBoxGrid( aTextBoxStyle );
			return aTextBoxStyle;
		}
	}
}