// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;
import org.refcodes.runtime.Terminal;

/**
 * The Class TextLineBuilderImpl.
 *
 * @author steiner
 */
public class TextLineBuilder implements ColumnWidthBuilder<TextLineBuilder>, ColumnWidthProperty {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _columnWidth = Terminal.toHeuristicWidth();

	private char _lineChar = '-';

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextLineBuilder withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnWidth;
	}

	/**
	 * Retrieves the line char from the line char property.
	 * 
	 * @return The line char stored by the line char property.
	 */
	public char getLineChar() {
		return _lineChar;
	}

	/**
	 * Sets the line char for the line char property.
	 * 
	 * @param aLineChar The line char to be stored by the line char property.
	 */
	public void setLineChar( char aLineChar ) {
		_lineChar = aLineChar;
	}

	/**
	 * Sets the line char for the line char property.
	 * 
	 * @param aLineChar The line char to be stored by the line char property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public TextLineBuilder withLineChar( char aLineChar ) {
		setLineChar( aLineChar );
		return this;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String}
	 */
	@Override
	public String toString() {
		return asString( _columnWidth, _lineChar );
	}

	/**
	 * Returns a {@link String} with the given length and containing only the
	 * provided fill character.
	 * 
	 * @param aLength The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @return The {@link String} filled with the fill character till the
	 *         provided length.
	 */
	public static String asString( int aLength, char aFillChar ) {
		if ( aLength <= 0 ) {
			return "";
		}
		final StringBuilder theBuffer = new StringBuilder();
		while ( theBuffer.length() < aLength ) {
			theBuffer.append( aFillChar );
		}
		return theBuffer.toString();
	}
}
