// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Determines the type of width being specified, either relative where the sum
 * of all relative widths represents 100%; or absolute in number of chars.
 * Specifying relative widths instead of percent (%) does not require various
 * widths to add up to 100%. This is ehttps://www.metacodes.proly useful when
 * the number of relative widths may vary; recalculation of all the relative
 * widths to add up to 100% is obsolete as having relative widths, and not
 * percent (%)..
 *
 * @author steiner
 */
public enum ColumnWidthType {
	/**
	 * The width is being relative; The total sum of all of relative widths
	 * represents 100%.
	 */
	RELATIVE,
	/**
	 * The width is being specified absolute in number of chars.
	 */
	ABSOLUTE
}