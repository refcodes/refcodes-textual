// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Implementation of the {@link ColumnWidthMetrics} interface.
 */
public class ColumnWidthMetricsImpl implements ColumnWidthMetrics {

	// /////////////////////////////////////////////////////////////////////
	// ENUM:
	// /////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////

	private int _width = 0;

	private ColumnWidthType _widthType = ColumnWidthType.ABSOLUTE;

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new column width metrics impl.
	 */
	public ColumnWidthMetricsImpl() {}

	/**
	 * Constructs a column's width, either in percent (%) or in number of chars.
	 * 
	 * @param aWidth The width for the column, either in percent (%) or in
	 *        number of chars, depending on the provided {@link ColumnWidthType}
	 *        .
	 * @param aWidthType The type of the width being provided, either percent
	 *        (%) or number of chars.
	 */
	public ColumnWidthMetricsImpl( int aWidth, ColumnWidthType aWidthType ) {
		_width = aWidth;
		_widthType = aWidthType;
	}

	// /////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _width;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _width + ( _widthType == ColumnWidthType.RELATIVE ? "%" : "" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_width = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnWidthMetrics withColumnWidth( int aColumnWidth ) {
		_width = aColumnWidth;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnWidthType getColumnWidthType() {
		return _widthType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidthType( ColumnWidthType aColumnWidthType ) {
		_widthType = aColumnWidthType;
	}
}