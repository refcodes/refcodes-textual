// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * The Enum Case.
 */
public enum Case {

	UPPER, LOWER;

	/**
	 * Converts the given {@link String} according to the case represented by
	 * this enumeration.
	 * 
	 * @param aString The {@link String} to be converted.
	 * 
	 * @return The accordingly converted {@link String}.
	 */
	public String toCase( String aString ) {
		if ( aString == null ) {
			return aString;
		}
		switch ( this ) {
		case UPPER -> {
			return aString.toUpperCase();
		}
		case LOWER -> {
			return aString.toLowerCase();
		}
		}
		return aString;
	}
}
