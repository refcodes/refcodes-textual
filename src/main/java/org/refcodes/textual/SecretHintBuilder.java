// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.runtime.Terminal;

/**
 * Dangerous builder for printing secret text such as passwords or pass-phrases
 * or keys without unveiling too much of the secret information with still being
 * able to say if the correct secret is being displayed. Useful in log-files, do
 * not use any production log level! USE WITH CAUTION!
 */
public class SecretHintBuilder extends AbstractText<SecretHintBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setText( String... aText ) {
		final String[] theSecrets = new String[aText.length];
		for ( int i = 0; i < theSecrets.length; i++ ) {
			theSecrets[i] = toString( aText[i] );
		}
		super.setText( theSecrets );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return getText();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return SecretHintBuilder.asStrings( aText );
	}

	/**
	 * Convenience method for directly invoking
	 * {@link SecretHintBuilder#toString(String...)} without leaving any state.
	 * 
	 * @param aText The text to be obscured.
	 * 
	 * @return The now obscured text.
	 */
	public static String asString( String aText ) {
		return SecretHintBuilder.toString( aText );
	}

	/**
	 * Convenience method for directly invoking {@link #toString(String...)}
	 * without leaving any state.
	 * 
	 * @param aText The text to be obscured.
	 * 
	 * @return The now obscured text.
	 */
	public static String asString( String... aText ) {
		String theResult = "";
		final String[] theStrings = asStrings( aText );
		for ( int i = 0; i < theStrings.length; i++ ) {
			theResult += theStrings[i];
			if ( i < theStrings.length - 1 ) {
				theResult += Terminal.getLineBreak();
			}
		}
		return theResult;
	}

	/**
	 * Convenience method for directly invoking {@link #toString(String...)}
	 * without leaving any state.
	 * 
	 * @param aText array The text to be obscured.
	 * 
	 * @return The now obscured text array.
	 */
	public static String[] asStrings( String... aText ) {
		final String[] theSecrets = new String[aText.length];
		for ( int i = 0; i < theSecrets.length; i++ ) {
			theSecrets[i] = asString( aText[i] );
		}
		return theSecrets;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This method provides a password hint by keeping the first and the last
	 * characters of the password and exchanging the rest by an asterisk "*". In
	 * case the password length is smaller than 10 characters, then the complete
	 * password is replaced by asterisk "*" characters.
	 * -------------------------------------------------------------------------
	 * CAUTION: This method must only be used when being in development
	 * environments and a password hint is required in debug log methods to see
	 * whether the correct password is being configured.
	 * 
	 * @param aSecret The secret to be printed
	 * 
	 * @return The printable secret
	 */
	protected static String toString( String aSecret ) {
		if ( aSecret == null || aSecret.isEmpty() ) {
			return "****";
		}
		final StringBuilder theBuffer = new StringBuilder();
		if ( aSecret.length() >= 10 ) {
			theBuffer.append( aSecret.charAt( 0 ) );
			for ( int i = 0; i < aSecret.length() - 2; i++ ) {
				theBuffer.append( '*' );
			}
			theBuffer.append( aSecret.charAt( aSecret.length() - 1 ) );
		}
		else {
			for ( int i = 0; i < aSecret.length(); i++ ) {
				theBuffer.append( '*' );
			}
		}
		return theBuffer.toString();
	}
}
