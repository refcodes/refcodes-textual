// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.mixin.NameAccessor;

/**
 * An enumeration with commonly used font categories.
 */
public enum FontFamily implements NameAccessor {

	// @formatter:off
	DIALOG("Dialog"), 
	
	DIALOG_INPUT("DialogInput"), 
	
	MONOSPACED("Monospaced"), 
	
	SERIF("Serif"), 
	
	SANS_SERIF("SansSerif");
	// @formatter:on

	private String _name;

	/**
	 * Constructor of the enumeration element with the given font name.
	 * 
	 * @param aFontName The enumeration element#s font name.
	 */
	private FontFamily( String aFontName ) {
		_name = aFontName;
	}

	/**
	 * Returns the font name of the given enumeration element.
	 * 
	 * @return The according font name.
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _name;
	}

	/**
	 * Retrieves the enumeration element representing the given font name
	 * (ignoring the case) or null if none was found.
	 * 
	 * @param aCategoryName The font category name for which to get the
	 *        enumeration element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static FontFamily fromName( String aCategoryName ) {
		for ( FontFamily eFontName : values() ) {
			if ( eFontName.getName().equalsIgnoreCase( aCategoryName ) ) {
				return eFontName;
			}
		}
		return null;
	}
}
