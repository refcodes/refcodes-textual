// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.Literal;
import org.refcodes.data.MoreText;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;
import org.refcodes.runtime.Terminal;

/**
 * A builder for processing a text with the given width applied and the given
 * {@link MoreTextMode} applied.
 */
public class MoreTextBuilder extends AbstractText<MoreTextBuilder> implements ColumnWidthProperty, ColumnWidthBuilder<MoreTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _columnWidth = Terminal.toHeuristicWidth();

	private MoreTextMode _moreTextMode = MoreTextMode.RIGHT;

	private String _moreText = "" + MoreText.MORE_TEXT_BEFORE.getChar();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the more text from the more text property.
	 * 
	 * @return The more text stored by the more text property.
	 */
	public String getMoreText() {
		return _moreText;
	}

	/**
	 * Sets the more text for the more text property.
	 * 
	 * @param aMoreText The more text to be stored by the more text mode
	 *        property.
	 */
	public void setMoreText( String aMoreText ) {
		_moreText = aMoreText;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoreTextBuilder withColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
		return this;
	}

	/**
	 * Retrieves the more text mode from the more text mode property.
	 * 
	 * @return The more text mode stored by the more text mode property.
	 */
	public MoreTextMode getMoreTextMode() {
		return _moreTextMode;
	}

	/**
	 * Sets the more text mode for the more text mode property.
	 * 
	 * @param aMoreTextMode The more text mode to be stored by the more text
	 *        mode property.
	 */
	public void setMoreTextMode( MoreTextMode aMoreTextMode ) {
		_moreTextMode = aMoreTextMode;

	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String} array
	 */
	@Override
	public String[] toStrings() {
		return MoreTextBuilder.asMoreText( getText(), _columnWidth, _moreText, _moreTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return MoreTextBuilder.asMoreText( aText, _columnWidth, _moreText, _moreTextMode );
	}

	/**
	 * Sets the more text mode for the more text mode property.
	 *
	 * @param aMoreTextMode the more text mode
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public MoreTextBuilder withMoreTextMode( MoreTextMode aMoreTextMode ) {
		setMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets the more text for the more text property.
	 *
	 * @param aMoreText the more text
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public MoreTextBuilder withMoreText( String aMoreText ) {
		setMoreText( aMoreText );
		return this;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String}
	 * 
	 * @throws IllegalStateException Thrown in case more than one text line has
	 *         been set via the {@link #withText(String...)} or
	 *         {@link #setText(String...)} methods.
	 */
	@Override
	public String toString() {
		return super.toString();
	}

	/**
	 * The text array is "mored" depending on the provided {@link MoreTextMode}.
	 * It is assumed that the text does not contain any ANSI escape codes, else
	 * {@link #asMoreText(String[], int, String, MoreTextMode, boolean)} In case
	 * it is {@link MoreTextMode#NONE}, then the text is returned untouched. In
	 * case it is {@link MoreTextMode#LEFT}, then the text is "mored" to the
	 * left. In case it is {@link MoreTextMode#RIGHT}, then the text is "mored"
	 * the the right.
	 * 
	 * @param aText The text array to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param aMoreTextMode The {@link MoreTextMode} specifying on how to
	 *        truncate the text.
	 * 
	 * @return The accordingly "mored" text array.
	 */
	public static String[] asMoreText( String aText[], int aLength, String aMore, MoreTextMode aMoreTextMode ) {
		return asMoreText( aText, aLength, aMore, aMoreTextMode, false );
	}

	/**
	 * The text array is "mored" depending on the provided {@link MoreTextMode}.
	 * In case it is {@link MoreTextMode#NONE}, then the text is returned
	 * untouched. In case it is {@link MoreTextMode#LEFT}, then the text is
	 * "mored" to the left. In case it is {@link MoreTextMode#RIGHT}, then the
	 * text is "mored" the the right.
	 * 
	 * @param aText The text array to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param aMoreTextMode The {@link MoreTextMode} specifying on how to
	 *        truncate the text.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return The accordingly "mored" text array.
	 */
	public static String[] asMoreText( String aText[], int aLength, String aMore, MoreTextMode aMoreTextMode, boolean hasAnsiEscapeCodes ) {
		final String[] theResult = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theResult[i] = asMoreText( aText[i], aLength, aMore, aMoreTextMode, hasAnsiEscapeCodes );
		}
		return theResult;
	}

	/**
	 * The text is "mored" depending on the provided {@link MoreTextMode}. It is
	 * assumed that the text does not contain any ANSI escape codes, else
	 * {@link #asMoreText(String, int, String, MoreTextMode, boolean)} In case
	 * it is {@link MoreTextMode#NONE}, then the text is returned untouched. In
	 * case it is {@link MoreTextMode#LEFT}, then the text is "mored" to the
	 * left. In case it is {@link MoreTextMode#RIGHT}, then the text is "mored"
	 * the the right.
	 * 
	 * @param aText The text array to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param aMoreTextMode The {@link MoreTextMode} specifying on how to
	 *        truncate the text.
	 * 
	 * @return The accordingly "mored" text array.
	 */
	public static String asMoreText( String aText, int aLength, String aMore, MoreTextMode aMoreTextMode ) {
		return asMoreText( aText, aLength, aMore, aMoreTextMode, false );
	}

	/**
	 * The text is "mored" depending on the provided {@link MoreTextMode}. In
	 * case it is {@link MoreTextMode#NONE}, then the text is returned
	 * untouched. In case it is {@link MoreTextMode#LEFT}, then the text is
	 * "mored" to the left. In case it is {@link MoreTextMode#RIGHT}, then the
	 * text is "mored" the the right.
	 * 
	 * @param aText The text to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param aMoreTextMode The {@link MoreTextMode} specifying on how to
	 *        truncate the text.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return The accordingly "mored" text.
	 */
	public static String asMoreText( String aText, int aLength, String aMore, MoreTextMode aMoreTextMode, boolean hasAnsiEscapeCodes ) {
		switch ( aMoreTextMode ) {
		case LEFT:
			return toMoreLeft( aText, aLength, aMore, hasAnsiEscapeCodes );
		case CENTER:
			return toMoreCenter( aText, aLength, aMore, hasAnsiEscapeCodes );
		case RIGHT:
			return toMoreRight( aText, aLength, aMore, hasAnsiEscapeCodes );
		case NONE:
		default:
			return aText;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Truncates a text by cutting the text from left to the right to reach the
	 * given length. If the text is shorter, then the text is returned. In case
	 * the text is longer than the required length, then the more text (e.g.
	 * "...") is prepended to indicate that text has been cut off, altogether
	 * being of the required length.
	 * 
	 * @param aText The text to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String}, when being longer than the provided length,
	 *         which is being truncated and prepended with the given more (e.g.
	 *         "...") text, altogether being of the required length.
	 */
	private static String toMoreLeft( String aText, int aLength, String aMore, boolean hasAnsiEscapeCodes ) {
		if ( aText == null ) {
			aText = Literal.NULL.getValue();
		}
		if ( ( toLength( aMore, hasAnsiEscapeCodes ) > aLength ) && ( toLength( aText, hasAnsiEscapeCodes ) > aLength ) ) {
			return aMore.substring( 0, aLength );
		}

		if ( toLength( aText, hasAnsiEscapeCodes ) <= aLength ) {
			return aText;
		}
		aText = aText.substring( ( toLength( aText, hasAnsiEscapeCodes ) - aLength ) + toLength( aMore, hasAnsiEscapeCodes ) );
		return aMore + aText;
	}

	/**
	 * Truncates a text by cutting the text from left to the right to reach the
	 * given length. If the text is shorter, then the text is returned. In case
	 * the text is longer than the required length, then the more text (e.g.
	 * "...") is prepended to indicate that text has been cut off, altogether
	 * being of the required length.
	 * 
	 * @param aText The text to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String}, when being longer than the provided length,
	 *         which is being truncated and prepended with the given more (e.g.
	 *         "...") text, altogether being of the required length.
	 */
	private static String toMoreRight( String aText, int aLength, String aMore, boolean hasAnsiEscapeCodes ) {
		if ( ( toLength( aMore, hasAnsiEscapeCodes ) > aLength ) && ( toLength( aText, hasAnsiEscapeCodes ) > aLength ) ) {
			return aMore.substring( 0, aLength );
		}

		if ( toLength( aText, hasAnsiEscapeCodes ) <= aLength ) {
			return aText;
		}
		aText = aText.substring( 0, aLength - toLength( aMore, hasAnsiEscapeCodes ) );
		return aText + aMore;
	}

	/**
	 * Truncates a text by cutting the text from left to the right and from
	 * right to left one by one to reach the given length. If the text is
	 * shorter, then the text is returned. In case the text is longer than the
	 * required length, then the more text (e.g. "...") is prepended to indicate
	 * that text has been cut off, altogether being of the required length.
	 * 
	 * @param aText The text to be truncated.
	 * @param aLength The length to be reached.
	 * @param aMore The text to be prepended to the left in case the text was
	 *        longer than the required length.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String}, when being longer than the provided length,
	 *         which is being truncated and prepended with the given more (e.g.
	 *         "...") text, altogether being of the required length.
	 */
	private static String toMoreCenter( String aText, int aLength, String aMore, boolean hasAnsiEscapeCodes ) {
		if ( ( toLength( aMore, hasAnsiEscapeCodes ) > aLength ) && ( toLength( aText, hasAnsiEscapeCodes ) > aLength ) ) {
			return aMore.substring( 0, aLength );
		}

		if ( toLength( aText, hasAnsiEscapeCodes ) <= aLength ) {
			return aText;
		}

		aText = aText.substring( 0, toLength( aText, hasAnsiEscapeCodes ) - toLength( aMore, hasAnsiEscapeCodes ) );
		if ( toLength( aText, hasAnsiEscapeCodes ) - toLength( aMore, hasAnsiEscapeCodes ) == aLength ) {
			return aText + aMore;
		}
		while ( toLength( aText, hasAnsiEscapeCodes ) + ( toLength( aMore, hasAnsiEscapeCodes ) * 2 ) > aLength ) {
			aText = aText.substring( toLength( aMore, hasAnsiEscapeCodes ) );
			if ( toLength( aText, hasAnsiEscapeCodes ) + ( toLength( aMore, hasAnsiEscapeCodes ) * 2 ) == aLength ) {
				return aMore + aText + aMore;
			}
			aText = aText.substring( 0, toLength( aText, hasAnsiEscapeCodes ) - toLength( aMore, hasAnsiEscapeCodes ) );
			if ( toLength( aText, hasAnsiEscapeCodes ) + ( toLength( aMore, hasAnsiEscapeCodes ) * 2 ) == aLength ) {
				return aMore + aText + aMore;
			}
		}
		return aMore + aText + aMore;
	}

	/**
	 * Returns the length of the text, considering whether the text is expected
	 * to have ANSI escape codes not considered to be part of the length.
	 * 
	 * @param aText The text for which to get the length.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return The according print length.
	 */
	private static int toLength( String aText, boolean hasAnsiEscapeCodes ) {
		if ( hasAnsiEscapeCodes ) {
			return AnsiEscapeCode.toUnescapedLength( aText );
		}
		return aText.length();
	}
}
