// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a {@link TableStyle} property.
 */
public interface TableStyleAccessor {

	/**
	 * Gets the currently set {@link TableStyle} being used.
	 * 
	 * @return The currently configured {@link TableStyle}s.
	 */
	TableStyle getTableStyle();

	/**
	 * Provides a mutator for an {@link TableStyle} property.
	 * 
	 * @param <B> The builder which implements the {@link TableStyleBuilder}.
	 */
	public interface TableStyleBuilder<B extends TableStyleBuilder<?>> {

		/**
		 * Sets the rows {@link TableStyle} to use returns this builder as of
		 * the Builder-Pattern.
		 * 
		 * @param aTableStyle The {@link TableStyle} to be used when printing a
		 *        row or the header.
		 * 
		 * @return This {@link TableBuilder} instance to continue configuration.
		 */
		B withTableStyle( TableStyle aTableStyle );
	}

	/**
	 * Provides a mutator for an {@link TableStyle} property.
	 */
	public interface TableStyleMutator {

		/**
		 * Sets the {@link TableStyle} to be used.
		 * 
		 * @param aTableStyle The {@link TableStyle} to be stored by the
		 *        {@link TableStyle} property.
		 */
		void setTableStyle( TableStyle aTableStyle );
	}

	/**
	 * Provides a {@link TableStyle} property.
	 */
	public interface TableStyleProperty extends TableStyleAccessor, TableStyleMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link TableStyle}
		 * (setter) as of {@link #setTableStyle(TableStyle)} and returns the
		 * very same value (getter).
		 * 
		 * @param aTableStyle The {@link TableStyle} to set (via
		 *        {@link #setTableStyle(TableStyle)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default TableStyle letTableStyle( TableStyle aTableStyle ) {
			setTableStyle( aTableStyle );
			return aTableStyle;
		}
	}
}