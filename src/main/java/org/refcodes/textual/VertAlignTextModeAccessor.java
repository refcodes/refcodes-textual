// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a vertical align text mode property.
 */
public interface VertAlignTextModeAccessor {

	/**
	 * Retrieves the vertical align text mode from the vertical align text mode
	 * property.
	 * 
	 * @return The vertical align text mode stored by the vertical align text
	 *         mode property.
	 */
	VertAlignTextMode getVertAlignTextMode();

	/**
	 * Provides a mutator for a vertical align text mode property.
	 */
	public interface VertAlignTextModeMutator {

		/**
		 * Sets the vertical align text mode for the vertical align text mode
		 * property.
		 * 
		 * @param aVertAlignTextMode The vertical align text mode to be stored
		 *        by the font style property.
		 */
		void setVertAlignTextMode( VertAlignTextMode aVertAlignTextMode );
	}

	/**
	 * Provides a builder method for a vertical align text mode property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface VertAlignTextModeBuilder<B extends VertAlignTextModeBuilder<B>> {

		/**
		 * Sets the vertical align text mode for the vertical align text mode
		 * property.
		 * 
		 * @param aVertAlignTextMode The vertical align text mode to be stored
		 *        by the font style property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withVertAlignTextMode( VertAlignTextMode aVertAlignTextMode );
	}

	/**
	 * Provides a vertical align text mode property.
	 */
	public interface VertAlignTextModeProperty extends VertAlignTextModeAccessor, VertAlignTextModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link VertAlignTextMode} (setter) as of
		 * {@link #setVertAlignTextMode(VertAlignTextMode)} and returns the very
		 * same value (getter).
		 * 
		 * @param aVertAlignTextMode The {@link VertAlignTextMode} to set (via
		 *        {@link #setVertAlignTextMode(VertAlignTextMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default VertAlignTextMode letVertAlignTextMode( VertAlignTextMode aVertAlignTextMode ) {
			setVertAlignTextMode( aVertAlignTextMode );
			return aVertAlignTextMode;
		}
	}
}
