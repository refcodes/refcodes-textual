// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeBuilder;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeMutator;
import org.refcodes.textual.MoreTextModeAccessor.MoreTextModeBuilder;
import org.refcodes.textual.MoreTextModeAccessor.MoreTextModeMutator;
import org.refcodes.textual.SplitTextModeAccessor.SplitTextModeBuilder;
import org.refcodes.textual.SplitTextModeAccessor.SplitTextModeMutator;
import org.refcodes.textual.TextFormatModeAccessor.TextFormatModeBuilder;
import org.refcodes.textual.TextFormatModeAccessor.TextFormatModeMutator;

/**
 * The Interface ColumnFormatMetrics.
 */
public interface ColumnFormatMetrics extends ColumnWidthMetrics, SplitTextModeMutator, SplitTextModeBuilder<ColumnFormatMetrics>, TextFormatModeMutator, TextFormatModeBuilder<ColumnFormatMetrics>, HorizAlignTextModeMutator, HorizAlignTextModeBuilder<ColumnFormatMetrics>, MoreTextModeMutator, MoreTextModeBuilder<ColumnFormatMetrics> {

	/**
	 * Sets an ANSI Escape-Code for the header and the rows and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern. In case
	 * an ANSI Escape-Code is set, then the ANSI Escape-Code is prepended and an
	 * ANSI Reset-Code is appended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link ColumnFormatMetrics} instance to continue
	 *         configuration.
	 */
	default ColumnFormatMetrics withEscapeCode( String aEscapeCode ) {
		setEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * Sets an ANSI Escape-Code factory for the header and the rows and returns
	 * this {@link ColumnFormatMetrics} instance as of the Builder-Pattern. In
	 * case an ANSI Escape-Code has been determined by the
	 * {@link EscapeCodeFactory}, then the ANSI Escape-Code is prepended and an
	 * ANSI Reset-Code is appended to the according text being printed.
	 * 
	 * @param aEscapeCodeFactory The {@link EscapeCodeFactory} to be used for
	 *        determining ANSI escaping.
	 * 
	 * @return This {@link ColumnFormatMetrics} instance to continue
	 *         configuration.
	 */
	default ColumnFormatMetrics withEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setEscapeCodeFactory( aEscapeCodeFactory );
		return this;
	}

	/**
	 * Sets an ANSI Escape-Code factory for the header and the rows. In case an
	 * ANSI Escape-Code has been determined by the {@link EscapeCodeFactory},
	 * then the ANSI Escape-Code is prepended and an ANSI Reset-Code is appended
	 * to the according text being printed.
	 * 
	 * @param aEscapeCodeFactory The {@link EscapeCodeFactory} to be used for
	 *        determining ANSI escaping.
	 */
	default void setEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setHeaderEscapeCodeFactory( aEscapeCodeFactory );
		setRowEscapeCodeFactory( aEscapeCodeFactory );
	}

	/**
	 * Sets an ANSI Escape-Code factory for the header and the rows and returns
	 * this {@link ColumnFormatMetrics} instance as of the Builder-Pattern. In
	 * case an ANSI Escape-Code has been determined by the
	 * {@link EscapeCodeFactory}, then the ANSI Escape-Code is prepended and an
	 * ANSI Reset-Code is appended to the according text being printed.
	 *
	 * @param aEscapeCodeFactory The {@link EscapeCodeFactory} to be used for
	 *        determining ANSI escaping.
	 * 
	 * @return the column format metrics
	 */
	default ColumnFormatMetrics withHeaderEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setHeaderEscapeCodeFactory( aEscapeCodeFactory );
		return this;
	}

	/**
	 * Sets an ANSI Escape-Code factory for the header. In case an ANSI
	 * Escape-Code is set, then the ANSI Escape-Code is prepended and an ANSI
	 * Reset-Code is appended to the according text being printed.
	 * 
	 * @param aEscapeCodeFactory The {@link EscapeCodeFactory} to be used for
	 *        determining ANSI escaping.
	 */
	void setHeaderEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory );

	/**
	 * Gets the ANSI Escape-Code factory for the header. In case an ANSI
	 * Escape-Code is set, then the ANSI Escape-Code is prepended and an ANSI
	 * Reset-Code is appended to the according text being printed.
	 * 
	 * @return The {@link EscapeCodeFactory} to be used for determining ANSI
	 *         escaping.
	 */
	EscapeCodeFactory getHeaderEscapeCodeFactory();

	/**
	 * Determines the ANSI Escape-Code by evaluating the provided TID: If an
	 * {@link EscapeCodeFactory} has been set via
	 * {@link #setEscapeCodeFactory(EscapeCodeFactory)} and the factory returns
	 * a value different to null, then this value is returned. Else the value
	 * returned by {@link #getHeaderEscapeCode()} is returned as fallback.
	 * 
	 * @param aIdentifier The TID for which to lookup an Escape-Code.
	 * 
	 * @return The identified Escape-Code or null if none was identifiable.
	 */
	String toHeaderEscapeCode( Object aIdentifier );

	/**
	 * Sets an ANSI Escape-Code factory for the row and the rows and returns
	 * this {@link ColumnFormatMetrics} instance as of the Builder-Pattern. In
	 * case an ANSI Escape-Code has been determined by the
	 * {@link EscapeCodeFactory}, then the ANSI Escape-Code is prepended and an
	 * ANSI Reset-Code is appended to the according text being printed.
	 *
	 * @param aEscapeCodeFactory The {@link EscapeCodeFactory} to be used for
	 *        determining ANSI escaping.
	 * 
	 * @return the column format metrics
	 */
	default ColumnFormatMetrics withRowEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setRowEscapeCodeFactory( aEscapeCodeFactory );
		return this;
	}

	/**
	 * Sets an ANSI Escape-Code factory for the row. In case an ANSI Escape-Code
	 * is set, then the ANSI Escape-Code is prepended and an ANSI Reset-Code is
	 * appended to the according text being printed.
	 * 
	 * @param aEscapeCodeFactory The {@link EscapeCodeFactory} to be used for
	 *        determining ANSI escaping. configuration.
	 */
	void setRowEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory );

	/**
	 * Gets the ANSI Escape-Code factory for the row. In case an ANSI
	 * Escape-Code is set, then the ANSI Escape-Code is prepended and an ANSI
	 * Reset-Code is appended to the according text being printed.
	 * 
	 * @return The {@link EscapeCodeFactory} to be used for determining ANSI
	 *         escaping.
	 */
	EscapeCodeFactory getRowEscapeCodeFactory();

	/**
	 * Determines the ANSI Escape-Code by evaluating the provided TID: If an
	 * {@link EscapeCodeFactory} has been set via
	 * {@link #setEscapeCodeFactory(EscapeCodeFactory)} and the factory returns
	 * a value different to null, then this value is returned. Else the value
	 * returned by {@link #getRowEscapeCode()} is returned as fallback.
	 * 
	 * @param aIdentifier The TID for which to lookup an Escape-Code.
	 * 
	 * @return The identified Escape-Code or null if none was identifiable.
	 */
	String toRowEscapeCode( Object aIdentifier );

	/**
	 * Sets an ANSI Escape-Code for the header and the rows. In case an ANSI
	 * Escape-Code is set, then the ANSI Escape-Code is prepended and an ANSI
	 * Reset-Code is appended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 */
	default void setEscapeCode( String aEscapeCode ) {
		setHeaderEscapeCode( aEscapeCode );
		setRowEscapeCode( aEscapeCode );
	}

	/**
	 * Sets an ANSI Escape-Code for the header and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern. In case
	 * an ANSI Escape-Code is set, then the ANSI Escape-Code is prepended and an
	 * ANSI Reset-Code is appended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link ColumnFormatMetrics} instance to continue
	 *         configuration.
	 */
	default ColumnFormatMetrics withHeaderEscapeCode( String aEscapeCode ) {
		setHeaderEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * Sets an ANSI Escape-Code for the header. In case an ANSI Escape-Code is
	 * set, then the ANSI Escape-Code is prepended and an ANSI Reset-Code is
	 * appended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 */
	void setHeaderEscapeCode( String aEscapeCode );

	/**
	 * Returns the ANSI Escape-Code for the header. In case an ANSI Escape-Code
	 * is set, then the ANSI Escape-Code is prepended and an ANSI Reset-Code is
	 * appended to the according text being printed.
	 * 
	 * @return The {@link String} to be used for ANSI escaping.
	 */
	String getHeaderEscapeCode();

	/**
	 * Sets an ANSI Escape-Code for the rows and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern. The
	 * latest ANSI Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link ColumnFormatMetrics} instance to continue
	 *         configuration.
	 */
	default ColumnFormatMetrics withRowEscapeCode( String aEscapeCode ) {
		setRowEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * Sets an ANSI Escape-Code for the rows and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern. The
	 * latest ANSI Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 */
	void setRowEscapeCode( String aEscapeCode );

	/**
	 * Returns the ANSI Escape-Code for the rows and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern. The
	 * latest ANSI Escape-Code being set wins.
	 * 
	 * @return The {@link String} to be used for ANSI escaping.
	 */
	String getRowEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnFormatMetrics withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnFormatMetrics withColumnWidthType( ColumnWidthType aColumnWidthType ) {
		setColumnWidthType( aColumnWidthType );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnFormatMetrics withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void setHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHeaderHorizAlignTextMode( aHorizAlignTextMode );
		setRowHorizAlignTextMode( aHorizAlignTextMode );
	}

	/**
	 * Sets an alignment mode for the header and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHeaderHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets an alignment mode for the header.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 */
	void setHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode );

	/**
	 * Gets the alignment mode for the header.
	 * 
	 * @return The {@link HorizAlignTextMode} to be used for aligning the text.
	 */
	HorizAlignTextMode getHeaderHorizAlignTextMode();

	/**
	 * Sets an alignment mode for the row and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setRowHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets an alignment mode for the row.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 */
	void setRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode );

	/**
	 * Gets the alignment mode for the row.
	 * 
	 * @return The {@link HorizAlignTextMode} to be used for aligning the text.
	 */
	HorizAlignTextMode getRowHorizAlignTextMode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnFormatMetrics withMoreTextMode( MoreTextMode aMoreTextMode ) {
		setMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void setMoreTextMode( MoreTextMode aMoreTextMode ) {
		setHeaderMoreTextMode( aMoreTextMode );
		setRowMoreTextMode( aMoreTextMode );

	}

	/**
	 * Sets a more-text mode for the header and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for truncating
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withHeaderMoreTextMode( MoreTextMode aMoreTextMode ) {
		setHeaderMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets a more-text mode for the header.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for truncating
	 *        the text.
	 */
	void setHeaderMoreTextMode( MoreTextMode aMoreTextMode );

	/**
	 * Gets the alignment mode for the header.
	 * 
	 * @return The {@link MoreTextMode} to be used for truncating the text.
	 */
	MoreTextMode getHeaderMoreTextMode();

	/**
	 * Sets a more-text mode for the row and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for truncating
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withRowMoreTextMode( MoreTextMode aMoreTextMode ) {
		setRowMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets a more-text mode for the row.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for truncating
	 *        the text.
	 */
	void setRowMoreTextMode( MoreTextMode aMoreTextMode );

	/**
	 * Gets the alignment mode for the row.
	 * 
	 * @return The {@link MoreTextMode} to be used for truncating the text.
	 */
	MoreTextMode getRowMoreTextMode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnFormatMetrics withTextFormatMode( TextFormatMode aTextFormatMode ) {
		setTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void setTextFormatMode( TextFormatMode aTextFormatMode ) {
		setHeaderTextFormatMode( aTextFormatMode );
		setRowTextFormatMode( aTextFormatMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnFormatMetrics withSplitTextMode( SplitTextMode aSplitTextMode ) {
		setSplitTextMode( aSplitTextMode );
		return this;
	}

	@Override
	default void setSplitTextMode( SplitTextMode aSplitTextMode ) {
		setHeaderSplitTextMode( aSplitTextMode );
		setRowSplitTextMode( aSplitTextMode );
	}

	/**
	 * Sets the {@link SplitTextMode} for the header.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be set for the header;
	 */
	void setHeaderSplitTextMode( SplitTextMode aSplitTextMode );

	/**
	 * Returns the {@link SplitTextMode} for the header.
	 * 
	 * @return The {@link SplitTextMode} to be set for the header;
	 */
	SplitTextMode getHeaderSplitTextMode();

	/**
	 * Sets the {@link SplitTextMode} for the header and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be set for the header;
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withHeaderSplitTextMode( SplitTextMode aSplitTextMode ) {
		setHeaderSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets the {@link SplitTextMode} for the row.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be set for the row;
	 */
	void setRowSplitTextMode( SplitTextMode aSplitTextMode );

	/**
	 * Returns the {@link SplitTextMode} for the row.
	 * 
	 * @return The {@link SplitTextMode} to be set for the row;
	 */
	SplitTextMode getRowSplitTextMode();

	/**
	 * Sets the {@link SplitTextMode} for the row and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be set for the row;
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withRowSplitTextMode( SplitTextMode aSplitTextMode ) {
		setRowSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets the {@link TextFormatMode} for the header.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be set for the
	 *        header;
	 */
	void setHeaderTextFormatMode( TextFormatMode aTextFormatMode );

	/**
	 * Returns the {@link TextFormatMode} for the header.
	 * 
	 * @return The {@link TextFormatMode} to be set for the header;
	 */
	TextFormatMode getHeaderTextFormatMode();

	/**
	 * Sets the {@link TextFormatMode} for the header and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be set for the
	 *        header;
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withHeaderTextFormatMode( TextFormatMode aTextFormatMode ) {
		setHeaderTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * Sets the {@link TextFormatMode} for the row.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be set for the row;
	 */
	void setRowTextFormatMode( TextFormatMode aTextFormatMode );

	/**
	 * Returns the {@link TextFormatMode} for the row.
	 * 
	 * @return The {@link TextFormatMode} to be set for the row;
	 */
	TextFormatMode getRowTextFormatMode();

	/**
	 * Sets the {@link TextFormatMode} for the row and returns this
	 * {@link ColumnFormatMetrics} instance as of the Builder-Pattern.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be set for the row;
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	default ColumnFormatMetrics withRowTextFormatMode( TextFormatMode aTextFormatMode ) {
		setRowTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * From column format metrics.
	 *
	 * @param aColumnFormatMetrics the column format metrics
	 */
	default void fromColumnFormatMetrics( ColumnFormatMetrics aColumnFormatMetrics ) {
		fromColumnWidthMetrics( aColumnFormatMetrics );
		setHeaderEscapeCode( aColumnFormatMetrics.getHeaderEscapeCode() );
		setRowEscapeCode( aColumnFormatMetrics.getRowEscapeCode() );
		setHeaderHorizAlignTextMode( aColumnFormatMetrics.getHeaderHorizAlignTextMode() );
		setRowHorizAlignTextMode( aColumnFormatMetrics.getRowHorizAlignTextMode() );
		setHeaderMoreTextMode( aColumnFormatMetrics.getHeaderMoreTextMode() );
		setRowMoreTextMode( aColumnFormatMetrics.getRowMoreTextMode() );
		setHeaderSplitTextMode( aColumnFormatMetrics.getHeaderSplitTextMode() );
		setRowSplitTextMode( aColumnFormatMetrics.getRowSplitTextMode() );
		setHeaderMoreTextMode( aColumnFormatMetrics.getHeaderMoreTextMode() );
		setRowMoreTextMode( aColumnFormatMetrics.getRowMoreTextMode() );
		setHeaderTextFormatMode( aColumnFormatMetrics.getHeaderTextFormatMode() );
		setRowTextFormatMode( aColumnFormatMetrics.getRowTextFormatMode() );
		setHeaderEscapeCodeFactory( aColumnFormatMetrics.getHeaderEscapeCodeFactory() );
		setRowEscapeCodeFactory( aColumnFormatMetrics.getRowEscapeCodeFactory() );
	}
}