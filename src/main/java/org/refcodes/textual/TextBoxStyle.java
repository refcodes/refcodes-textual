// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.runtime.Terminal;

/**
 * The {@link TextBoxStyle} enumeration defines character sets for various box
 * grid representations. For example Box Drawing (Unicode block) characters are
 * used, which are not available on every system or terminal: <code>
 * ╔══════════════════════════════════════════════════════════════════════════╗
 * ║ U+250x 	─ 	━ 	│ 	┃ 	┄ 	┅ 	┆ 	┇ 	┈ 	┉ 	┊ 	┋ 	┌ 	┍ 	┎ 	┏ ║
 * ║ U+251x 	┐ 	┑ 	┒ 	┓ 	└ 	┕ 	┖ 	┗ 	┘ 	┙ 	┚ 	┛ 	├ 	┝ 	┞ 	┟ ║
 * ║ U+252x 	┠ 	┡ 	┢ 	┣ 	┤ 	┥ 	┦ 	┧ 	┨ 	┩ 	┪ 	┫ 	┬ 	┭ 	┮ 	┯ ║
 * ║ U+253x 	┰ 	┱ 	┲ 	┳ 	┴ 	┵ 	┶ 	┷ 	┸ 	┹ 	┺ 	┻ 	┼ 	┽ 	┾ 	┿ ║
 * ║ U+254x 	╀ 	╁ 	╂ 	╃ 	╄ 	╅ 	╆ 	╇ 	╈ 	╉ 	╊ 	╋ 	╌ 	╍ 	╎ 	╏ ║
 * ║ U+255x 	═ 	║ 	╒ 	╓ 	╔ 	╕ 	╖ 	╗ 	╘ 	╙ 	╚ 	╛ 	╜ 	╝ 	╞ 	╟ ║
 * ║ U+256x 	╠ 	╡ 	╢ 	╣ 	╤ 	╥ 	╦ 	╧ 	╨ 	╩ 	╪ 	╫ 	╬ 	╭ 	╮ 	╯ ║
 * ║ U+257x 	╰ 	╱ 	╲ 	╳ 	╴ 	╵ 	╶ 	╷ 	╸ 	╹ 	╺ 	╻ 	╼ 	╽ 	╾ 	╿ ║
 * ╚══════════════════════════════════════════════════════════════════════════╝
 * </code> Therefore plain ASCII character sets such as {@link #ASCII} are
 * provided as well.
 */
public enum TextBoxStyle implements TextBoxGrid {

	/**
	 * Representation of the following text box grid: <code>
	 * ┌─┬─┐
	 * │ │ │
	 * ├─┼─┤
	 * │ │ │
	 * └─┴─┘
	 * </code>
	 */
	SINGLE(
	// @formatter:off
		new String[] {
			"┌─┬─┐",
			"│ │ │",
			"├─┼─┤",
			"│ │ │",
			"└─┴─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┌─┬─┐
	 * │ │ │
	 * ├─┼─┤
	 * │ │ │
	 * └─┴─┘
	 * </code>
	 */
	SINGLE_DASHED(
	// @formatter:off
		new String[] {
			"┌─┬─┐",
			"│ │ │",
			"├─┼─┤",
			"│ │ │",
			"└─┴─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ╒═╤═╕
	 * │ │ │
	 * ╞═╪═╡
	 * │ │ │
	 * ╘═╧═╛
	 * </code>
	 */
	SINGLE_DOUBLE(
	// @formatter:off
		new String[] {
			"╒═╤═╕",
			"│ │ │",
			"╞═╪═╡",
			"│ │ │",
			"╘═╧═╛"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ╓─╥─╖
	 * ║ ║ ║
	 * ╟─╫─╢
	 * ║ ║ ║
	 * ╙─╨─╜
	 * </code>
	 */
	DOUBLE_SINGLE(
	// @formatter:off
		new String[] {
			"╓─╥─╖",
			"║ ║ ║",
			"╟─╫─╢",
			"║ ║ ║",
			"╙─╨─╜"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ╔═╦═╗
	 * ║ ║ ║
	 * ╠═╬═╣
	 * ║ ║ ║
	 * ╚═╩═╝
	 * </code>
	 */
	DOUBLE(
	// @formatter:off
		new String[] {
			"╔═╦═╗",
			"║ ║ ║",
			"╠═╬═╣",
			"║ ║ ║",
			"╚═╩═╝"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┏━┳━┓
	 * ┃ ┃ ┃
	 * ┣━╋━┫
	 * ┃ ┃ ┃
	 * ┗━┻━┛
	 * </code>
	 */
	BOLD(
	// @formatter:off
		new String[] {
			"┏━┳━┓",
			"┃ ┃ ┃",
			"┣━╋━┫",
			"┃ ┃ ┃",
			"┗━┻━┛"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┏━┳━┓
	 * ┃ ┃ ┃
	 * ┠─╂─┨
	 * ┃ ┃ ┃
	 * ┗━┻━┛
	 * </code>
	 */
	HYBRID_BOLD_SINGLE(
	// @formatter:off
		new String[] {
			"┏━┯━┓",
			"┃ │ ┃",
			"┠─┼─┨",
			"┃ │ ┃",
			"┗━┷━┛"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┌─┰─┐
	 * │ ┃ │
	 * ┝━╋━┥
	 * │ ┃ │
	 * └─┸─┘
	 * </code>
	 */
	HYBRID_SINGLE_BOLD(
	// @formatter:off
		new String[] {
			"┌─┰─┐",
			"│ ┃ │",
			"┝━╋━┥",
			"│ ┃ │",
			"└─┸─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┏━┳━┓
	 * ┃ ┃ ┃
	 * ┡━╇━┩
	 * │ │ │
	 * └─┴─┘
	 * </code>
	 */
	BOLD_HEADER_SINGLE_BODY(
	// @formatter:off
		new String[] {
			"┏━┳━┓",
			"┃ ┃ ┃",
			"┡━╇━┩",
			"│ │ │",
			"└─┴─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┌─┬─┐
	 * │ │ │
	 * ┢━╈━┪
	 * ┃ ┃ ┃
	 * ┗━┻━┛
	 * </code>
	 */
	SINGLE_HEADER_BOLD_BODY(
	// @formatter:off
		new String[] {
			"┌─┬─┐",
			"│ │ │",
			"┢━╈━┪",
			"┃ ┃ ┃",
			"┗━┻━┛"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┏━┯━┓
	 * ┃ │ ┃
	 * ┡━┿━┩
	 * │ │ │
	 * └─┴─┘
	 * </code>
	 */
	HYBRID_BOLD_HEADER_SINGLE_BODY(
	// @formatter:off
		new String[] {
			"┏━┯━┓",
			"┃ │ ┃",
			"┡━┿━┩",
			"│ │ │",
			"└─┴─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┌─┬─┐
	 * │ │ │
	 * ┢━┿━┪
	 * ┃ │ ┃
	 * ┗━┷━┛
	 * </code>
	 */
	HYBRID_SINGLE_HEADER_BOLD_BODY(
	// @formatter:off
		new String[] {
			"┌─┬─┐",
			"│ │ │",
			"┢━┿━┪",
			"┃ │ ┃",
			"┗━┷━┛"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ╔═╤═╗
	 * ║ │ ║
	 * ╠═╪═╣
	 * ║ │ ║
	 * ╚═╧═╝
	 * </code>
	 */
	HYBRID_DOUBLE_SINGLE(
	// @formatter:off
		new String[] {
			"╔═╤═╗",
			"║ │ ║",
			"╠═╪═╣",
			"║ │ ║",
			"╚═╧═╝"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┌─╥─┐
	 * │ ║ │
	 * ├─╫─┤
	 * │ ║ │
	 * └─╨─┘
	 * </code>
	 */
	HYBRID_SINGLE_DOUBLE(
	// @formatter:off
		new String[] {
			"┌─╥─┐",
			"│ ║ │",
			"├─╫─┤",
			"│ ║ │",
			"└─╨─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ╔═╤═╗
	 * ║ │ ║
	 * ╟─┼─╢
	 * ║ │ ║
	 * ╚═╧═╝
	 * </code>
	 */
	DOUBLE_BORDER_SINGLE_CONTENT(
	// @formatter:off
		new String[] {
			"╔═╤═╗",
			"║ │ ║",
			"╟─┼─╢",
			"║ │ ║",
			"╚═╧═╝"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * ┌─╥─┐
	 * │ ║ │
	 * ╞═╬═╡
	 * │ ║ │
	 * └─╨─┘
	 * </code>
	 */
	SINGLE_BORDER_DOUBLE_CONTENT(
	// @formatter:off
		new String[] {
			"┌─╥─┐",
			"│ ║ │",
			"╞═╬═╡",
			"│ ║ │",
			"└─╨─┘"
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * /-+-\
	 * | | |
	 * +-+-+
	 * | | |
	 * \-+-/
	 * </code>
	 */
	ASCII(
	// @formatter:off
		new String[] {
			 "/-+-\\",
			 "| | |",
			 "+-+-+",
			 "| | |",
			"\\-+-/"
		}
			// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * "     "
	 * "     "
	 * "     "
	 * "     "
	 * "     "
	 * </code>
	 */
	BLANK(
	// @formatter:off
		new String[] {
			"     ",
			"     ",
			"     ",
			"     ",
			"     "
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * " ─ ─ "
	 * "     "
	 * " ─ ─ "
	 * "     "
	 * " ─ ─ "
	 * </code>
	 */
	SINGLE_BLANK(
	// @formatter:off
		new String[] {
			" ─ ─ ",
			"     ",
			" ─ ─ ",
			"     ",
			" ─ ─ "
		}
		// @formatter:on
	),
	/**
	 * Representation of the following text box grid: <code>
	 * " - - "
	 * "     "
	 * " - - "
	 * "     "
	 * " - - "
	 * </code>
	 */
	ASCII_BLANK(
	// @formatter:off
		new String[] {
			" - - ",
			"     ", 
			" - - ", 
			"     ", 
			" - - "  
		}
		// @formatter:on
	);

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Style supported Unicode capable terminals (when not being under test).
	 */
	public static final TextBoxStyle UNICODE_TEXT_BOX_STYLE = TextBoxStyle.HYBRID_BOLD_SINGLE;

	/**
	 * Style displayed for sure when being on a Windows machine.
	 */
	public static final TextBoxStyle WINDOWS_TEXT_BOX_STYLE = TextBoxStyle.SINGLE;

	/**
	 * Style displayed for sure when being under test (e.g. JUnit) or in a
	 * Cp1252 or a plain "xterm".
	 */
	public static final TextBoxStyle ASCII_TEXT_BOX_STYLE = TextBoxStyle.ASCII;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TextBoxGrid _textBoxGrid;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link TextBoxStyle} element with the according
	 * {@link String} array representing the box grid characters to be used. The
	 * array may look as follows: <code>
	 * new String[] {
	 *   "┌─┬─┐",
	 *   "│ │ │",
	 *   "├─┼─┤",
	 *   "│ │ │",
	 *   "└─┴─┘"
	 * }
	 *</code>
	 * 
	 * @param aTextBoxGrid The {@link String} array "painting" the grid.
	 */
	private TextBoxStyle( String[] aTextBoxStyle ) {
		_textBoxGrid = new TextBoxGridImpl( aTextBoxStyle );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getLeftEdge() {
		return _textBoxGrid.getLeftEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getTopLeftEdge() {
		return _textBoxGrid.getTopLeftEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getDividerEdge() {
		return _textBoxGrid.getDividerEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getTopDividerEdge() {
		return _textBoxGrid.getTopDividerEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getTopRightEdge() {
		return _textBoxGrid.getTopRightEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getRightEdge() {
		return _textBoxGrid.getRightEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getLeftLine() {
		return _textBoxGrid.getLeftLine();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getDividerLine() {
		return _textBoxGrid.getDividerLine();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getRightLine() {
		return _textBoxGrid.getRightLine();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getTopLine() {
		return _textBoxGrid.getTopLine();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getBottomRightEdge() {
		return _textBoxGrid.getBottomRightEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getBottomDividerEdge() {
		return _textBoxGrid.getBottomDividerEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getBottomLeftEdge() {
		return _textBoxGrid.getBottomLeftEdge();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getBottomLine() {
		return _textBoxGrid.getBottomLine();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getInnerLine() {
		return _textBoxGrid.getInnerLine();
	}

	/**
	 * Retrieves the best fitting {@link TextBoxStyle} for the given value by
	 * ignoring the characters' case as well as all "-" and "_" characters in
	 * the provided value as well as in the enumeration's name..
	 *
	 * @param aValue The value for which to determine the {@link TextBoxStyle}.
	 * 
	 * @return The according {@link TextBoxStyle} or null if none was found.
	 */
	public static TextBoxGrid toTextBoxGrid( String aValue ) {
		aValue = aValue != null ? aValue.replaceAll( "-", "" ).replaceAll( "_", "" ) : aValue;
		for ( TextBoxStyle eValue : values() ) {
			if ( eValue.name().replaceAll( "-", "" ).replaceAll( "_", "" ).equalsIgnoreCase( aValue ) ) {
				return eValue;
			}
		}
		return null;
	}

	/**
	 * Depending on the runtime environment (Windows, Linux, JUnit) we prefer
	 * different text box styles as depending on the runtime environment not all
	 * characters used by the various text box styles may be available.
	 * 
	 * @return The {@link TextBoxStyle} by default fitting best for the current
	 *         environment.
	 */
	public static TextBoxGrid toExecutionTextBoxGrid() {
		return switch ( Terminal.toCharSetCapability() ) {
		case UNICODE -> UNICODE_TEXT_BOX_STYLE;
		case WINDOWS -> WINDOWS_TEXT_BOX_STYLE;
		case ASCII -> ASCII_TEXT_BOX_STYLE;
		};
	}
}
