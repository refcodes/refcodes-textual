// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.Collection;

import org.refcodes.runtime.Terminal;

/**
 * The Class AbstractText.
 *
 * @author steiner
 * 
 * @param <B> the generic type
 */
abstract class AbstractText<B extends Text<B>> implements Text<B> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _textLines[] = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getText() {
		return _textLines;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setText( String... aText ) {
		_textLines = aText;
	}

	/**
	 * With text.
	 *
	 * @param aText the text
	 * 
	 * @return the b
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withText( String... aText ) {
		setText( aText );
		return (B) this;
	}

	/**
	 * With text.
	 *
	 * @param aText the text
	 * 
	 * @return the b
	 */
	@SuppressWarnings("unchecked")
	@Override
	public B withText( Collection<String> aText ) {
		setText( aText );
		return (B) this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		String theResult = "";
		final String[] theStrings = toStrings();
		for ( int i = 0; i < theStrings.length; i++ ) {
			theResult += theStrings[i];
			if ( i < theStrings.length - 1 ) {
				theResult += Terminal.getLineBreak();
			}
		}
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString( String... aText ) {
		String theResult = "";
		final String[] theStrings = toStrings( aText );
		for ( int i = 0; i < theStrings.length; i++ ) {
			theResult += theStrings[i];
			if ( i < theStrings.length - 1 ) {
				theResult += Terminal.getLineBreak();
			}
		}
		return theResult;
	}
}
