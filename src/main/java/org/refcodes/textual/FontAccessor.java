// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a font property.
 */
public interface FontAccessor {

	/**
	 * Retrieves the font from the font property.
	 * 
	 * @return The font stored by the font property.
	 */
	Font getFont();

	/**
	 * Provides a mutator for a font property.
	 */
	public interface FontMutator {

		/**
		 * Sets the font for the font property.
		 * 
		 * @param aFont The font to be stored by the font property.
		 */
		void setFont( Font aFont );
	}

	/**
	 * Provides a builder method for a font property returning the builder for
	 * applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FontBuilder<B extends FontBuilder<B>> {

		/**
		 * Sets the font for the font property.
		 * 
		 * @param aFont The font to be stored by the font property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFont( Font aFont );
	}

	/**
	 * Provides a font property.
	 */
	public interface FontProperty extends FontAccessor, FontMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Font} (setter) as
		 * of {@link #setFont(Font)} and returns the very same value (getter).
		 * 
		 * @param aFont The {@link Font} to set (via {@link #setFont(Font)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Font letFont( Font aFont ) {
			setFont( aFont );
			return aFont;
		}
	}
}
