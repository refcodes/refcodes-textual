// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.mixin.CodeAccessor;
import org.refcodes.mixin.NameAccessor;

/**
 * An enumeration with commonly used font styles.
 */
public enum FontStyle implements CodeAccessor<Integer>, NameAccessor {

	PLAIN("Plain", java.awt.Font.PLAIN),

	BOLD("Bold", java.awt.Font.BOLD),

	ITALIC("Italic", java.awt.Font.ITALIC),

	BOLD_ITALIC("Bold_Italic", java.awt.Font.BOLD + java.awt.Font.ITALIC);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _fontStyleCode;

	private String _fontStyleName;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new font style.
	 *
	 * @param aFontStyleName the font style name
	 * @param aFontStyleCode the font style code
	 */
	private FontStyle( String aFontStyleName, int aFontStyleCode ) {
		_fontStyleName = aFontStyleName;
		_fontStyleCode = aFontStyleCode;

	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the font style code of the given enumeration element.
	 * 
	 * @return The according style code.
	 */
	@Override
	public Integer getCode() {
		return _fontStyleCode;
	}

	/**
	 * Returns the font style name of the given enumeration element.
	 * 
	 * @return The according font style name.
	 */
	@Override
	public String getName() {
		return _fontStyleName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _fontStyleName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the enumeration element representing the given font name
	 * (ignoring the case) or null if none was found.
	 * 
	 * @param aStyleName The font name for which to get the enumeration element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static FontStyle fromFontStyleName( String aStyleName ) {
		for ( FontStyle eFontStyle : values() ) {
			if ( eFontStyle.getName().equalsIgnoreCase( aStyleName ) ) {
				return eFontStyle;
			}
		}
		return null;
	}

	/**
	 * Retrieves the enumeration element representing the given font style code
	 * (ignoring the case) or null if none was found.
	 * 
	 * @param aStyleCode The font style code for which to get the enumeration
	 *        element.
	 * 
	 * @return The enumeration element determined or null if none matching was
	 *         found.
	 */
	public static FontStyle fromFontStyleCode( int aStyleCode ) {
		for ( FontStyle eFontStyle : values() ) {
			if ( eFontStyle.getCode() == aStyleCode ) {
				return eFontStyle;
			}
		}
		return null;
	}
}
