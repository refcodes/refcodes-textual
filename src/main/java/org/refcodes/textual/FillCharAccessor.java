// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a fill char property.
 */
public interface FillCharAccessor {

	/**
	 * Retrieves the fill char from the fill char property.
	 * 
	 * @return The fill char stored by the fill char property.
	 */
	char getFillChar();

	/**
	 * Provides a mutator for a fill char property.
	 */
	public interface FillCharMutator {

		/**
		 * Sets the fill char for the fill char property.
		 * 
		 * @param aFillChar The fill char to be stored by the fill char
		 *        property.
		 */
		void setFillChar( char aFillChar );
	}

	/**
	 * Provides a builder method for a fill char property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FillCharBuilder<B extends FillCharBuilder<B>> {

		/**
		 * Sets the fill char for the fill char property.
		 * 
		 * @param aFillChar The fill char to be stored by the fill char
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFillChar( char aFillChar );
	}

	/**
	 * Provides a fill char property.
	 */
	public interface FillCharProperty extends FillCharAccessor, FillCharMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setFillChar(char)} and returns the very same value (getter).
		 * 
		 * @param aFillChar The character to set (via
		 *        {@link #setFillChar(char)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char letFillChar( char aFillChar ) {
			setFillChar( aFillChar );
			return aFillChar;
		}
	}
}
