// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.graphical.VisibleAccessor.VisibleBuilder;
import org.refcodes.graphical.VisibleAccessor.VisibleProperty;
import org.refcodes.mixin.NameAccessor.NameBuilder;
import org.refcodes.mixin.NameAccessor.NameProperty;

/**
 * The Interface ColumnSetupMetrics.
 */
public interface ColumnSetupMetrics extends ColumnFormatMetrics, VisibleProperty, VisibleBuilder<ColumnSetupMetrics>, NameProperty, NameBuilder<ColumnSetupMetrics> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withName( String aName ) {
		setName( aName );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withShow() {
		setVisible( true );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHide() {
		setVisible( false );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withEscapeCode( String aEscapeCode ) {
		setEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHeaderEscapeCode( String aEscapeCode ) {
		setHeaderEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withRowEscapeCode( String aEscapeCode ) {
		setRowEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withColumnWidthType( ColumnWidthType aColumnWidthType ) {
		setColumnWidthType( aColumnWidthType );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHeaderHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setRowHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withMoreTextMode( MoreTextMode aMoreTextMode ) {
		setMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHeaderMoreTextMode( MoreTextMode aMoreTextMode ) {
		setHeaderMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withRowMoreTextMode( MoreTextMode aMoreTextMode ) {
		setRowMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withTextFormatMode( TextFormatMode aTextFormatMode ) {
		setTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withSplitTextMode( SplitTextMode aSplitTextMode ) {
		setSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHeaderSplitTextMode( SplitTextMode aSplitTextMode ) {
		setHeaderSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withRowSplitTextMode( SplitTextMode aSplitTextMode ) {
		setRowSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHeaderTextFormatMode( TextFormatMode aTextFormatMode ) {
		setHeaderTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withRowTextFormatMode( TextFormatMode aTextFormatMode ) {
		setRowTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setEscapeCodeFactory( aEscapeCodeFactory );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withHeaderEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setHeaderEscapeCodeFactory( aEscapeCodeFactory );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ColumnSetupMetrics withRowEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		setRowEscapeCodeFactory( aEscapeCodeFactory );
		return this;
	}

	/**
	 * From column setup metrics.
	 *
	 * @param aColumnSetupMetrics the column setup metrics
	 */
	default void fromColumnSetupMetrics( ColumnSetupMetrics aColumnSetupMetrics ) {
		fromColumnFormatMetrics( aColumnSetupMetrics );
		setName( aColumnSetupMetrics.getName() );
		setVisible( aColumnSetupMetrics.isVisible() );
	}
}