// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.runtime.Terminal;

/**
 * The Enum TableStyle defines character sets for various box border styles.
 */
public enum TableStyle {

	/**
	 * Same as {@link #SINGLE_HEADER_SINGLE_BODY} as of convenience purposes.
	 */
	SINGLE(TextBoxStyle.SINGLE, TextBoxStyle.SINGLE, TextBoxStyle.SINGLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#SINGLE} for the
	 * header and {@link TextBoxStyle#SINGLE} for the body.
	 */
	SINGLE_HEADER_SINGLE_BODY(TextBoxStyle.SINGLE, TextBoxStyle.SINGLE, TextBoxStyle.SINGLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#DOUBLE_SINGLE} for
	 * the header and {@link TextBoxStyle#SINGLE} for the body.
	 */
	DOUBLE_SINGLE_HEADER_SINGLE_BODY(TextBoxStyle.DOUBLE_SINGLE, TextBoxStyle.SINGLE, TextBoxStyle.SINGLE),

	/**
	 * Same as {@link #DOUBLE_HEADER_DOUBLE_BODY} as of convenience purposes.
	 */
	DOUBLE(TextBoxStyle.DOUBLE, TextBoxStyle.DOUBLE, TextBoxStyle.DOUBLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#DOUBLE} for the
	 * header and {@link TextBoxStyle#DOUBLE} for the body.
	 */
	DOUBLE_HEADER_DOUBLE_BODY(TextBoxStyle.DOUBLE, TextBoxStyle.DOUBLE, TextBoxStyle.DOUBLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#DOUBLE_SINGLE} for
	 * the header and {@link TextBoxStyle#DOUBLE_SINGLE} for the body.
	 */
	DOUBLE_SINGLE_HEADER_DOUBLE_SINGLE_BODY(TextBoxStyle.DOUBLE_SINGLE, TextBoxStyle.DOUBLE_SINGLE, TextBoxStyle.DOUBLE_SINGLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#DOUBLE_SINGLE} for
	 * the header and {@link TextBoxStyle#DOUBLE_SINGLE} for the body.
	 */
	SINGLE_DOUBLE_HEADER_SINGLE_BODY(TextBoxStyle.SINGLE_DOUBLE, TextBoxStyle.SINGLE, TextBoxStyle.SINGLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#SINGLE_DOUBLE} for
	 * the header and {@link TextBoxStyle#SINGLE_DOUBLE} for the body.
	 */
	SINGLE_DOUBLE_HEADER_SINGLE_DOUBLE_BODY(TextBoxStyle.SINGLE_DOUBLE, TextBoxStyle.SINGLE_DOUBLE, TextBoxStyle.SINGLE_DOUBLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#DOUBLE} for the
	 * header and {@link TextBoxStyle#DOUBLE_SINGLE} for the body.
	 */
	DOUBLE_HEADER_DOUBLE_SINGLE_BODY(TextBoxStyle.DOUBLE, TextBoxStyle.DOUBLE_SINGLE, TextBoxStyle.DOUBLE_SINGLE),

	/**
	 * Representing a table style using {@link TextBoxStyle#DOUBLE_SINGLE} for
	 * the header and {@link TextBoxStyle#DOUBLE_SINGLE} for the body.
	 */
	DOUBLE_SINGLE_HEADER_SINGLE_DASHED_BODY(TextBoxStyle.DOUBLE_SINGLE, TextBoxStyle.SINGLE_DASHED, TextBoxStyle.SINGLE_DASHED),

	/**
	 * Same as {@link #BOLD_HEADER_BOLD_BODY} as of convenience purposes.
	 */
	BOLD(TextBoxStyle.BOLD, TextBoxStyle.BOLD, TextBoxStyle.BOLD),

	/**
	 * Representing a table style using {@link TextBoxStyle#BOLD} for the header
	 * and {@link TextBoxStyle#BOLD} for the body.
	 */
	BOLD_HEADER_BOLD_BODY(TextBoxStyle.BOLD, TextBoxStyle.BOLD, TextBoxStyle.BOLD),

	/**
	 * Representing a table style using
	 * {@link TextBoxStyle#BOLD_HEADER_SINGLE_BODY} for the header and
	 * {@link TextBoxStyle#SINGLE} for the body.
	 */
	BOLD_HEADER_SINGLE_BODY(TextBoxStyle.BOLD_HEADER_SINGLE_BODY, TextBoxStyle.SINGLE, TextBoxStyle.SINGLE),
	/**
	 * Representing a table style using
	 * {@link TextBoxStyle#HYBRID_BOLD_HEADER_SINGLE_BODY} for the header and
	 * {@link TextBoxStyle#SINGLE} for the body.
	 */
	HYBRID_BOLD_HEADER_SINGLE_BODY(TextBoxStyle.HYBRID_BOLD_HEADER_SINGLE_BODY, TextBoxStyle.SINGLE, TextBoxStyle.SINGLE),

	/**
	 * Same as {@link #ASCII_HEADER_ASCII_BODY} as of convenience purposes.
	 */
	ASCII(TextBoxStyle.ASCII, TextBoxStyle.ASCII, TextBoxStyle.ASCII),

	/**
	 * Representing a table style using {@link TextBoxStyle#ASCII} for the
	 * header and {@link TextBoxStyle#ASCII} for the body.
	 */
	ASCII_HEADER_ASCII_BODY(TextBoxStyle.ASCII, TextBoxStyle.ASCII, TextBoxStyle.ASCII),

	/**
	 * Same as {@link #BLANK_HEADER_BLANK_BODY} as of convenience purposes.
	 */
	BLANK(TextBoxStyle.BLANK, TextBoxStyle.BLANK, TextBoxStyle.BLANK),

	/**
	 * Representing a table style using {@link TextBoxStyle#BLANK} for the
	 * header and {@link TextBoxStyle#BLANK} for the body.
	 */
	BLANK_HEADER_BLANK_BODY(TextBoxStyle.BLANK, TextBoxStyle.BLANK, TextBoxStyle.BLANK),

	/**
	 * Representing a table style using {@link TextBoxStyle#SINGLE_BLANK} for
	 * the header and {@link TextBoxStyle#SINGLE_BLANK} for the body.
	 */
	SINGLE_BLANK_HEADER_SINGLE_BLANK_BODY(TextBoxStyle.SINGLE_BLANK, TextBoxStyle.SINGLE_BLANK, TextBoxStyle.SINGLE_BLANK),

	/**
	 * Representing a table style using {@link TextBoxStyle#ASCII_BLANK} for the
	 * header and {@link TextBoxStyle#ASCII_BLANK} for the body.
	 */
	ASCII_BLANK_HEADER_ASCII_BLANK_BODY(TextBoxStyle.ASCII_BLANK, TextBoxStyle.ASCII_BLANK, TextBoxStyle.ASCII_BLANK);

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Style supported Unicode capable terminals (when not being under test).
	 */
	public static final TableStyle UNICODE_TABLE_STYLE = TableStyle.HYBRID_BOLD_HEADER_SINGLE_BODY;

	/**
	 * Style displayed for sure when being on a Windows machine.
	 */
	public static final TableStyle WINDOWS_TABLE_STYLE = TableStyle.SINGLE_HEADER_SINGLE_BODY;

	/**
	 * Style displayed for sure when being under test (e.g. JUnit) or in a
	 * Cp1252 or a plain "xterm".
	 */
	public static final TableStyle ASCII_TABLE_STYLE = TableStyle.ASCII_HEADER_ASCII_BODY;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TextBoxGrid _tableHeader;
	private TextBoxGrid _tableBody;
	private TextBoxGrid _tableTail;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the table style with the according predefined characters for
	 * the table elements.
	 *
	 * @param aTableHeader The {@link TextBoxGrid} to be used when printing the
	 *        header.
	 * @param aTableBody The {@link TextBoxGrid} to be used when printing the
	 *        body.
	 * @param aTableTail The {@link TextBoxGrid} to be used when printing the
	 *        tail.
	 */
	private TableStyle( TextBoxStyle aTableHeader, TextBoxStyle aTableBody, TextBoxStyle aTableTail ) {
		_tableHeader = aTableHeader;
		_tableBody = aTableBody;
		_tableTail = aTableTail;
	};

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the table header's characters.
	 * 
	 * @return The table header characters.
	 */
	public TextBoxGrid getHeader() {
		return _tableHeader;
	}

	/**
	 * Returns the table body's characters.
	 * 
	 * @return The table body characters.
	 */
	public TextBoxGrid getBody() {
		return _tableBody;
	}

	/**
	 * Returns the table tail's characters.
	 * 
	 * @return The table header characters.
	 */
	public TextBoxGrid getTail() {
		return _tableTail;
	}

	/**
	 * Retrieves a {@link TableStyle} depending on the given string, ignoring
	 * the case as well as being graceful regarding "-" and "_",
	 *
	 * @param aValue The name of the {@link TableStyle} to be interpreted
	 *        graceful.
	 * 
	 * @return The {@link TableStyle} being determined or null if none was
	 *         found.
	 */
	public static TableStyle toTableStyle( String aValue ) {
		aValue = aValue != null ? aValue.replaceAll( "-", "" ).replaceAll( "_", "" ) : aValue;
		for ( TableStyle eValue : values() ) {
			if ( eValue.name().replaceAll( "-", "" ).replaceAll( "_", "" ).equalsIgnoreCase( aValue ) ) {
				return eValue;
			}
		}
		return null;
	}

	/**
	 * Depending on the runtime environment (Windows, Linux, JUnit) we prefer
	 * different table styles as depending on the runtime environment not all
	 * characters used by the various table styles may be available.
	 * 
	 * @return The {@link TableStyle} by default fitting best for the current
	 *         environment.
	 */
	public static TableStyle toExecutionTableStyle() {
		return switch ( Terminal.toCharSetCapability() ) {
		case UNICODE -> UNICODE_TABLE_STYLE;
		case WINDOWS -> WINDOWS_TABLE_STYLE;
		case ASCII -> ASCII_TABLE_STYLE;
		};
	}
}
