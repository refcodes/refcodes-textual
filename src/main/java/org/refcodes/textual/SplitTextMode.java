// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.CharSet;

/**
 * The {@link SplitTextMode} specifies for according algorithms on how a line is
 * to be split apart.
 */
public enum SplitTextMode {

	/**
	 * A line is to be split at an end of a line, best as it gets, identified by
	 * the {@link CharSet#END_OF_LINE} characters.
	 */
	AT_END_OF_LINE,
	/**
	 * A line is to be split at the first end of a line detected, identified by
	 * the {@link CharSet#END_OF_LINE} characters.
	 */
	AT_FIRST_END_OF_LINE,
	/**
	 * A line is to be split at space characters, best as it gets, identified by
	 * the space " " character.
	 */
	AT_SPACE,
	/**
	 * A line is to be split exactly at the given width not reckoning the end of
	 * lines or spaces.
	 */
	AT_FIXED_WIDTH
}
