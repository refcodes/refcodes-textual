// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.ConsoleDimension;
import org.refcodes.mixin.EscapeCodesStatusAccessor.EscapeCodeStatusBuilder;
import org.refcodes.mixin.EscapeCodesStatusAccessor.EscapeCodeStatusProperty;
import org.refcodes.mixin.RowHeightAccessor.RowHeightBuilder;
import org.refcodes.mixin.RowHeightAccessor.RowHeightProperty;
import org.refcodes.textual.VertAlignTextModeAccessor.VertAlignTextModeBuilder;
import org.refcodes.textual.VertAlignTextModeAccessor.VertAlignTextModeProperty;

/**
 * Fills a text up on by appending the given char to the left or to the the
 * right or inbetween till the given length is reached. How the text is aligned
 * depends on the setting of the {@link VertAlignTextMode} attribute.
 */
public class VertAlignTextBuilder extends AbstractText<VertAlignTextBuilder> implements RowHeightBuilder<VertAlignTextBuilder>, RowHeightProperty, VertAlignTextModeProperty, VertAlignTextModeBuilder<VertAlignTextBuilder>, EscapeCodeStatusProperty, EscapeCodeStatusBuilder<VertAlignTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _rowHeight = ConsoleDimension.MIN_HEIGHT.getValue();
	private VertAlignTextMode _alignTextMode = VertAlignTextMode.TOP;
	private char _fillChar = ' ';
	private boolean _isEscapeCodesEnabled = false;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @return an instance (using a default implementation) of this builder
	 */
	public VertAlignTextBuilder build() {
		return new VertAlignTextBuilder();
	}

	/**
	 * Retrieves the fill char from the fill char property.
	 * 
	 * @return The fill char stored by the fill char property.
	 */
	public char getFillChar() {
		return _fillChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRowHeight() {
		return _rowHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VertAlignTextMode getVertAlignTextMode() {
		return _alignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEscapeCodesEnabled() {
		return _isEscapeCodesEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCodesEnabled( boolean isEscCodesEnabled ) {
		_isEscapeCodesEnabled = isEscCodesEnabled;

	}

	/**
	 * Sets the fill char for the fill char property.
	 * 
	 * @param aFillChar The fill char to be stored by the align text mode
	 *        property.
	 */
	public void setFillChar( char aFillChar ) {
		_fillChar = aFillChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowHeight( int aRowHeight ) {
		_rowHeight = aRowHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVertAlignTextMode( VertAlignTextMode aVertAlignTextMode ) {
		_alignTextMode = aVertAlignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return asAligned( getText(), _rowHeight, _fillChar, _alignTextMode, _isEscapeCodesEnabled );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return asAligned( aText, _rowHeight, _fillChar, _alignTextMode, _isEscapeCodesEnabled );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VertAlignTextBuilder withEscapeCodesEnabled( boolean isEscCodesEnabled ) {
		setEscapeCodesEnabled( isEscCodesEnabled );
		return this;
	}

	/**
	 * Sets the fill char for the fill char property.
	 * 
	 * @param aFillChar The fill char to be stored by the align text mode
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VertAlignTextBuilder withFillChar( char aFillChar ) {
		setFillChar( aFillChar );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VertAlignTextBuilder withRowHeight( int aRowHeight ) {
		setRowHeight( aRowHeight );
		return this;
	}

	/**
	 * Sets the align text mode for the align text mode property.
	 * 
	 * @param aVertAlignTextMode The align text mode to be stored by the align
	 *        text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	@Override
	public VertAlignTextBuilder withVertAlignTextMode( VertAlignTextMode aVertAlignTextMode ) {
		setVertAlignTextMode( aVertAlignTextMode );
		return this;
	}

	/**
	 * Fills up or truncates a given text block to the provided height; filling
	 * up or truncating depends on the {@link VertAlignTextMode} specified, .
	 * When filling up, the provided fill char is used.
	 *
	 * @param aTextBlock The text block to be filled up / truncated.
	 * @param aHeight The height of the resulting text block.
	 * @param aFillChar the fill char to use when filling up.
	 * @param aVertAlignTextMode The mode on how to fill up the text block. The
	 *        mode can be {@link VertAlignTextMode#BOTTOM},
	 *        {@link VertAlignTextMode#MIDDLE} or {@link VertAlignTextMode#TOP}
	 * 
	 * @return The text block filled up / truncated to the required height.
	 */
	public static String[] asAligned( String[] aTextBlock, int aHeight, char aFillChar, VertAlignTextMode aVertAlignTextMode ) {
		return asAligned( aTextBlock, aHeight, aFillChar, aVertAlignTextMode, false );
	}

	/**
	 * Fills up or truncates a given text block to the provided height; filling
	 * up or truncating depends on the {@link VertAlignTextMode} specified. When
	 * filling up, a space (" ") character as fill char is used.
	 *
	 * @param aTextBlock The text block to be filled up / truncated.
	 * @param aHeight The height of the resulting text block.
	 * @param aVertAlignTextMode The mode on how to fill up the text block. The
	 *        mode can be {@link VertAlignTextMode#BOTTOM},
	 *        {@link VertAlignTextMode#MIDDLE} or {@link VertAlignTextMode#TOP}
	 * 
	 * @return The text block filled up / truncated to the required height.
	 */
	public static String[] asAligned( String[] aTextBlock, int aHeight, VertAlignTextMode aVertAlignTextMode ) {
		return asAligned( aTextBlock, aHeight, ' ', aVertAlignTextMode, false );
	}

	/**
	 * Fills up or truncates a given text block to the provided height; filling
	 * up or truncating depends on the {@link VertAlignTextMode} specified. When
	 * filling up, a space (" ") character as fill char is used.
	 *
	 * @param aTextBlock The text block to be filled up / truncated.
	 * @param aHeight The height of the resulting text block.
	 * @param aVertAlignTextMode The mode on how to fill up the text block. The
	 *        mode can be {@link VertAlignTextMode#BOTTOM},
	 *        {@link VertAlignTextMode#MIDDLE} or {@link VertAlignTextMode#TOP}
	 * @param isEscCodesEnabled True in case ANSI escape codes are to be
	 *        regarded when calculating text lengths (they do not count).
	 * 
	 * @return The text block filled up / truncated to the required height.
	 */
	public static String[] asAligned( String[] aTextBlock, int aHeight, VertAlignTextMode aVertAlignTextMode, boolean isEscCodesEnabled ) {
		return asAligned( aTextBlock, aHeight, ' ', aVertAlignTextMode, isEscCodesEnabled );
	}

	/**
	 * Fills up or truncates a given text block to the provided height; filling
	 * up or truncating depends on the {@link VertAlignTextMode} specified, .
	 * When filling up, the provided fill char is used.
	 *
	 * @param aTextBlock The text block to be filled up / truncated.
	 * @param aHeight The height of the resulting text block.
	 * @param aFillChar the fill char to use when filling up.
	 * @param aVertAlignTextMode The mode on how to fill up the text block. The
	 *        mode can be {@link VertAlignTextMode#BOTTOM},
	 *        {@link VertAlignTextMode#MIDDLE} or {@link VertAlignTextMode#TOP}
	 * @param isEscCodesEnabled True in case ANSI escape codes are to be
	 *        regarded when calculating text lengths (they do not count).
	 * 
	 * @return The text block filled up / truncated to the required height.
	 */
	public static String[] asAligned( String[] aTextBlock, int aHeight, char aFillChar, VertAlignTextMode aVertAlignTextMode, boolean isEscCodesEnabled ) {
		if ( aTextBlock == null || aHeight == aTextBlock.length ) {
			return aTextBlock;
		}
		String theLine = null;
		final String[] theLines = new String[aHeight];
		final int theWidth = AnsiEscapeCode.toLength( aTextBlock[0], isEscCodesEnabled );
		switch ( aVertAlignTextMode ) {
		case BOTTOM:
			throw new RuntimeException( org.refcodes.data.Text.NOT_YET_IMPLEMENTED.getText() );
		case MIDDLE:
			throw new RuntimeException( org.refcodes.data.Text.NOT_YET_IMPLEMENTED.getText() );
		case TOP:
			for ( int i = 0; i < theLines.length; i++ ) {
				if ( i < aTextBlock.length ) {
					theLines[i] = aTextBlock[i];
				}
				else {
					if ( theLine == null && aTextBlock != null && aTextBlock.length > 0 ) {
						theLine = new TextLineBuilder().withColumnWidth( theWidth ).withLineChar( aFillChar ).toString();
					}
					theLines[i] = theLine;
				}

			}
			return theLines;
		}
		throw new IllegalArgumentException( "You must provided a supported value for the vertical align mode, though you provided <" + aVertAlignTextMode + ">. The code seems to be out of date, please participate in getting the code even better. See <http://www.refcodes.org> and <https://birbucket.org/refcodes>." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
