// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.Trap;
import org.refcodes.graphical.BoxBorderMode;
import org.refcodes.graphical.GraphicalUtility;
import org.refcodes.graphical.Pixmap;
import org.refcodes.graphical.RgbPixmap;
import org.refcodes.graphical.RgbPixmapBuilderImpl;
import org.refcodes.graphical.RgbPixmapImageBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.Terminal;
import org.refcodes.textual.FontAccessor.FontBuilder;
import org.refcodes.textual.FontAccessor.FontProperty;
import org.refcodes.textual.FontFamilyAccessor.FontFamilyBuilder;
import org.refcodes.textual.FontFamilyAccessor.FontFamilyProperty;
import org.refcodes.textual.FontNameAccessor.FontNameBuilder;
import org.refcodes.textual.FontNameAccessor.FontNameProperty;
import org.refcodes.textual.FontSizeAccessor.FontSizeBuilder;
import org.refcodes.textual.FontSizeAccessor.FontSizeProperty;
import org.refcodes.textual.FontStyleAccessor.FontStyleBuilder;
import org.refcodes.textual.FontStyleAccessor.FontStyleProperty;

/**
 * ASCII-Art as of the {@link AsciiArtBuilder} is represented by an array of
 * {@link String} instances "visualizing" a given text (rendered with the given
 * {@link Font}) or image as so called ASCII_HEADER_ASCII_BODY art.
 * ASCII_HEADER_ASCII_BODY characters are used as "pixels" when "paining" the
 * text or image into the {@link String} array. Different
 * ASCII_HEADER_ASCII_BODY characters represent different levels of brightness
 * for a "pixel".
 */
public class AsciiArtBuilder extends AbstractText<AsciiArtBuilder> implements FontProperty, FontBuilder<AsciiArtBuilder>, FontFamilyProperty, FontFamilyBuilder<AsciiArtBuilder>, FontStyleProperty, FontStyleBuilder<AsciiArtBuilder>, FontSizeProperty, FontSizeBuilder<AsciiArtBuilder>, FontNameProperty, FontNameBuilder<AsciiArtBuilder>, ColumnWidthBuilder<AsciiArtBuilder>, ColumnWidthProperty {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( AsciiArtBuilder.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Font _font = new Font( FontFamily.SANS_SERIF, FontStyle.PLAIN, 16 );
	private RgbPixmap _pixmap = null;
	private AsciiArtMode _asciiArtMode = AsciiArtMode.NORMAL;
	private PixmapRatioMode _pixmapRatioMode = PixmapRatioMode.NONE;
	private AsciiColorPalette _asciiColorPalette = AsciiColorPalette.HALFTONE_GRAY;
	private char[] _asciiColors = null;
	private int _columnWidth = -1;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the {@link Pixmap} ratio mode from the {@link Pixmap} ratio
	 * mode property.
	 * 
	 * @return The {@link Pixmap} ratio mode stored by the {@link Pixmap} ratio
	 *         mode property.
	 */
	public PixmapRatioMode getPixmapRatioMode() {
		return _pixmapRatioMode;
	}

	/**
	 * Sets the {@link Pixmap} ratio mode for the {@link Pixmap} ratio mode
	 * property.
	 * 
	 * @param aPixmapRatioMode The {@link Pixmap} ratio mode to be stored by the
	 *        text strip mode property.
	 */
	public void setPixmapRatioMode( PixmapRatioMode aPixmapRatioMode ) {
		_pixmapRatioMode = aPixmapRatioMode;
	}

	/**
	 * Retrieves the {@link Pixmap} from the {@link Pixmap} property.
	 * 
	 * @return The {@link Pixmap} stored by the {@link Pixmap} property.
	 */
	public RgbPixmap getRgbPixmap() {
		return _pixmap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AsciiArtBuilder withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Font getFont() {
		return _font;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFont( Font aFont ) {
		_font = aFont;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AsciiArtBuilder withFont( Font aFont ) {
		setFont( aFont );
		return this;
	}

	/**
	 * Retrieves the ASCII-Art mode from the ASCII-Art mode property.
	 * 
	 * @return The ASCII-Art mode stored by the ASCII-Art mode property.
	 */
	public AsciiArtMode getAsciiArtMode() {
		return _asciiArtMode;
	}

	/**
	 * Sets the ASCII-Art mode for the ASCII_HEADER_ASCII_BODY art mode
	 * property.
	 * 
	 * @param aAsciiArtMode The ASCII-Art mode to be stored by the text strip
	 *        mode property.
	 */
	public void setAsciiArtMode( AsciiArtMode aAsciiArtMode ) {
		_asciiArtMode = aAsciiArtMode;
	}

	/**
	 * Retrieves the ASCII_HEADER_ASCII_BODY colors from the
	 * ASCII_HEADER_ASCII_BODY colors property.
	 * 
	 * @return The ASCII_HEADER_ASCII_BODY colors stored by the
	 *         ASCII_HEADER_ASCII_BODY colors property.
	 */
	public char[] getAsciiColors() {
		return _asciiColors != null ? _asciiColors : ( _asciiColorPalette != null ? _asciiColorPalette.getPalette() : null );
	}

	/**
	 * Sets the ASCII_HEADER_ASCII_BODY colors for the ASCII_HEADER_ASCII_BODY
	 * colors property.
	 * 
	 * @param aAsciiColors The ASCII_HEADER_ASCII_BODY colors to be stored by
	 *        the ASCII_HEADER_ASCII_BODY colors property.
	 */
	public void setAsciiColors( char... aAsciiColors ) {
		_asciiColors = aAsciiColors;
		_asciiColorPalette = null;
	}

	/**
	 * Retrieves the ASCII_HEADER_ASCII_BODY color palette from the
	 * ASCII_HEADER_ASCII_BODY color palette property.
	 * 
	 * @return The ASCII_HEADER_ASCII_BODY color palette stored by the
	 *         ASCII_HEADER_ASCII_BODY color palette property.
	 */
	public AsciiColorPalette getAsciiColorPalette() {
		return _asciiColorPalette;
	}

	/**
	 * Sets the ASCII_HEADER_ASCII_BODY color palette for the
	 * ASCII_HEADER_ASCII_BODY color palette property.
	 * 
	 * @param aAsciiColorPalette The ASCII_HEADER_ASCII_BODY color palette to be
	 *        stored by the ASCII_HEADER_ASCII_BODY color palette property.
	 */
	public void setAsciiColorPalette( AsciiColorPalette aAsciiColorPalette ) {
		_asciiColorPalette = aAsciiColorPalette;
		_asciiColors = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FontFamily getFontFamily() {
		return _font != null ? _font.getFamily() : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFontFamily( FontFamily aFontName ) {
		if ( _font == null ) {
			_font = new Font();
		}
		_font.setFamily( aFontName );
		setFont( _font );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AsciiArtBuilder withFontFamily( FontFamily aFontName ) {
		setFontFamily( aFontName );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FontStyle getFontStyle() {
		return _font != null ? _font.getStyle() : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFontStyle( FontStyle aFontStyle ) {
		if ( _font == null ) {
			_font = new Font();
		}
		_font.setStyle( aFontStyle );
		setFont( _font );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AsciiArtBuilder withFontStyle( FontStyle aFontStyle ) {
		setFontStyle( aFontStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFontSize() {
		return _font != null ? _font.getSize() : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFontSize( int aFontSize ) {
		if ( _font == null ) {
			_font = new Font();
		}
		_font.setSize( aFontSize );
		setFont( _font );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AsciiArtBuilder withFontSize( int aFontSize ) {
		setFontSize( aFontSize );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFontName() {
		return _font != null ? _font.getName() : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFontName( String aFontName ) {
		if ( _font == null ) {
			_font = new Font();
		}
		_font.setName( aFontName );
		setFont( _font );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AsciiArtBuilder withFontName( String aFontName ) {
		setFontName( aFontName );
		return this;
	}

	/**
	 * Sets the {@link Pixmap} for the {@link Pixmap} property.
	 * 
	 * @param aPixmap The {@link Pixmap} to be stored by the {@link Pixmap}
	 *        property.
	 */
	public void setRgbPixmap( RgbPixmap aPixmap ) {
		_pixmap = aPixmap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		if ( _pixmap != null ) {
			return asAsciiArt( _pixmap, getColumnWidth(), getAsciiArtMode(), getPixmapRatioMode(), getAsciiColors() );
		}
		else if ( getText() != null ) {
			return asAsciiArt( _columnWidth, _font, _asciiArtMode, getAsciiColors(), getText() );
		}
		else {
			throw new IllegalStateException( "Cannot invoke the ASCII-Art generation as there is neither a text nor a pixmap being set." );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return asAsciiArt( _columnWidth, _font, _asciiArtMode, getAsciiColors(), aText );
	}

	/**
	 * Race condition safe shortcut for using {@link #withRgbPixmap(RgbPixmap)}
	 * followed by {@link #toString()}. Implementation requirements: This method
	 * must not(!) be implemented by calling {@link #withRgbPixmap(RgbPixmap)}
	 * followed by {@link #toString()} (do not change the {@link Pixmap}
	 * property) as this would not be thread safe!
	 * 
	 * @param aPixmap The {@link Pixmap} to be converted to ASCII-Art.
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( RgbPixmap aPixmap ) {
		String theResult = "";
		final String[] theStrings = toStrings( aPixmap );
		for ( int i = 0; i < theStrings.length; i++ ) {
			theResult += theStrings[i];
			if ( i < theStrings.length - 1 ) {
				theResult += Terminal.getLineBreak();
			}
		}
		return theResult;
	}

	/**
	 * Race condition safe shortcut for using {@link #withRgbPixmap(RgbPixmap)}
	 * followed by {@link #toStrings()}. Implementation requirements: This
	 * method must not(!) be implemented by calling
	 * {@link #withRgbPixmap(RgbPixmap)} followed by {@link #toStrings()} (do
	 * not change the {@link Pixmap} property) as this would not be thread safe!
	 * 
	 * @param aPixmap The {@link Pixmap} to be converted to ASCII-Art.
	 * 
	 * @return The according resulting {@link String} array
	 */
	public String[] toStrings( RgbPixmap aPixmap ) {
		return asAsciiArt( aPixmap, getColumnWidth(), getAsciiArtMode(), getPixmapRatioMode(), getAsciiColors() );
	}

	/**
	 * Sets the ASCII-Art mode for the ASCII_HEADER_ASCII_BODY art mode
	 * property.
	 * 
	 * @param aAsciiArtMode The ASCII-Art mode to be stored by the text strip
	 *        mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public AsciiArtBuilder withAsciiArtMode( AsciiArtMode aAsciiArtMode ) {
		setAsciiArtMode( aAsciiArtMode );
		return this;
	}

	/**
	 * Sets the {@link Pixmap} ratio mode for the {@link Pixmap} ratio mode
	 * property.
	 * 
	 * @param aPixmapRatioMode The {@link Pixmap} ratio mode to be stored by the
	 *        text strip mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public AsciiArtBuilder withPixmapRatioMode( PixmapRatioMode aPixmapRatioMode ) {
		setPixmapRatioMode( aPixmapRatioMode );
		return this;
	}

	/**
	 * Sets the ASCII_HEADER_ASCII_BODY colors for the ASCII_HEADER_ASCII_BODY
	 * colors property.
	 * 
	 * @param aAsciiColors The ASCII_HEADER_ASCII_BODY colors to be stored by
	 *        the ASCII_HEADER_ASCII_BODY colors property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public AsciiArtBuilder withAsciiColors( char... aAsciiColors ) {
		setAsciiColors( aAsciiColors );
		return this;
	}

	/**
	 * Sets the ASCII_HEADER_ASCII_BODY colors for the ASCII_HEADER_ASCII_BODY
	 * colors property.
	 * 
	 * @param aAsciiColors The ASCII_HEADER_ASCII_BODY colors to be stored by
	 *        the ASCII_HEADER_ASCII_BODY colors property.
	 */
	public void setAsciiColors( String aAsciiColors ) {
		final char[] theChars = new char[aAsciiColors.length()];
		aAsciiColors.getChars( 0, aAsciiColors.length(), theChars, 0 );
		setAsciiColors( theChars );
	}

	/**
	 * Sets the ASCII_HEADER_ASCII_BODY colors for the ASCII_HEADER_ASCII_BODY
	 * colors property.
	 * 
	 * @param aAsciiColors The ASCII_HEADER_ASCII_BODY colors to be stored by
	 *        the ASCII_HEADER_ASCII_BODY colors property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public AsciiArtBuilder withAsciiColors( String aAsciiColors ) {
		setAsciiColors( aAsciiColors );
		return this;
	}

	/**
	 * Sets the ASCII_HEADER_ASCII_BODY color palette for the
	 * ASCII_HEADER_ASCII_BODY color palette property.
	 * 
	 * @param aAsciiColorPalette The ASCII_HEADER_ASCII_BODY color palette to be
	 *        stored by the ASCII_HEADER_ASCII_BODY color palette property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public AsciiArtBuilder withAsciiColorPalette( AsciiColorPalette aAsciiColorPalette ) {
		setAsciiColorPalette( aAsciiColorPalette );
		return this;
	}

	/**
	 * Sets the {@link Pixmap} for the {@link Pixmap} property.
	 * 
	 * @param aPixmap The {@link Pixmap} to be stored by the {@link Pixmap}
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public AsciiArtBuilder withRgbPixmap( RgbPixmap aPixmap ) {
		setRgbPixmap( aPixmap );
		return this;
	}

	/**
	 * Sets the image file for the {@link Pixmap} property.
	 * 
	 * @param aImageFile The image file from which to retrieve the
	 *        {@link Pixmap} which is to be stored by the {@link Pixmap}
	 *        property.
	 * 
	 * @throws IOException in case the file was not found.
	 */
	public void setImageFile( File aImageFile ) throws IOException {
		setRgbPixmap( new RgbPixmapImageBuilder().withImageFile( aImageFile ).toPixmap() );
	}

	/**
	 * Sets the image file for the {@link Pixmap} property.
	 * 
	 * @param aImageFile The image file for the {@link Pixmap} to be stored by
	 *        the {@link Pixmap} property.
	 * 
	 * @return The builder for applying multiple build operations.
	 * 
	 * @throws IOException in case the file was not found.
	 */
	public AsciiArtBuilder withImageFile( File aImageFile ) throws IOException {
		setImageFile( aImageFile );
		return this;
	}

	/**
	 * Sets the image's {@link InputStream} for the {@link Pixmap} property.
	 * 
	 * @param aImageStream The image's {@link InputStream} from which to
	 *        retrieve the {@link Pixmap} which is to be stored by the
	 *        {@link Pixmap} property.
	 * 
	 * @throws IOException in case the file was not found.
	 */
	public void setImageInputStream( InputStream aImageStream ) throws IOException {
		setRgbPixmap( new RgbPixmapImageBuilder().withImageInputStream( aImageStream ).toPixmap() );
	}

	/**
	 * Sets the image's {@link InputStream} for the {@link Pixmap} property.
	 * 
	 * @param aImageStream The image's {@link InputStream} for the
	 *        {@link Pixmap} to be stored by the {@link Pixmap} property.
	 * 
	 * @return The builder for applying multiple build operations.
	 * 
	 * @throws IOException in case the file was not found.
	 */
	public AsciiArtBuilder withImageInputStream( InputStream aImageStream ) throws IOException {
		setImageInputStream( aImageStream );
		return this;
	}

	/**
	 * Race condition safe shortcut for using {@link #withImageFile(File)}
	 * followed by {@link #toString()}. Implementation requirements: This method
	 * must not(!) be implemented by calling {@link #withImageFile(File)}
	 * followed by {@link #toString()} (do not change the {@link Pixmap}
	 * property) as this would not be thread safe!
	 * 
	 * @param aImageStream The image's {@link InputStream} for the
	 *        {@link Pixmap} to be stored by the {@link Pixmap} property.
	 * 
	 * @return The according resulting {@link String}
	 * 
	 * @throws IOException in case the file was not found
	 */
	public String toString( InputStream aImageStream ) throws IOException {
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder().withWidth( getColumnWidth() );
		return toString( theBuilder.toPixmap( aImageStream ) );
	}

	/**
	 * Race condition safe shortcut for using {@link #withImageFile(File)}
	 * followed by {@link #toStrings()}. Implementation requirements: This
	 * method must not(!) be implemented by calling {@link #withImageFile(File)}
	 * followed by {@link #toStrings()} (do not change the {@link Pixmap}
	 * property) as this would not be thread safe!
	 * 
	 * @param aImageStream The image's {@link InputStream} for the
	 *        {@link Pixmap} to be stored by the {@link Pixmap} property.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException in case the file was not found or the like.
	 */
	public String[] toStrings( InputStream aImageStream ) throws IOException {
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder().withWidth( getColumnWidth() );
		return toStrings( theBuilder.toPixmap( aImageStream ) );
	}

	/**
	 * Race condition safe shortcut for using {@link #withImageFile(File)}
	 * followed by {@link #toString()}. Implementation requirements: This method
	 * must not(!) be implemented by calling {@link #withImageFile(File)}
	 * followed by {@link #toString()} (do not change the {@link Pixmap}
	 * property) as this would not be thread safe!
	 * 
	 * @param aImageFile The image file to be converted to ASCII-Art.
	 * 
	 * @return The according resulting {@link String}
	 * 
	 * @throws IOException in case the file was not found
	 */
	public String toString( File aImageFile ) throws IOException {
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder().withWidth( getColumnWidth() );
		return toString( theBuilder.toPixmap( aImageFile ) );
	}

	/**
	 * Race condition safe shortcut for using {@link #withImageFile(File)}
	 * followed by {@link #toStrings()}. Implementation requirements: This
	 * method must not(!) be implemented by calling {@link #withImageFile(File)}
	 * followed by {@link #toStrings()} (do not change the {@link Pixmap}
	 * property) as this would not be thread safe!
	 * 
	 * @param aImageFile The image file to be converted to ASCII-Art.
	 * 
	 * @return The according resulting {@link String} array
	 * 
	 * @throws IOException in case the file was not found or the like.
	 */
	public String[] toStrings( File aImageFile ) throws IOException {
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder().withWidth( getColumnWidth() );
		return toStrings( theBuilder.toPixmap( aImageFile ) );
	}

	/**
	 * Produces ASCII-Art from the provided arguments.
	 *
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aFont The {@link Font} to use for printing ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aLines The text lines to convert to ASCII-Art.
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * 
	 * @return The resulting ASCII-Art.
	 */
	public static String[] asAsciiArt( int aWidth, Font aFont, AsciiArtMode aAsciiArtMode, String[] aLines, char... aPalette ) {
		return asAsciiArt( aWidth, aFont, aAsciiArtMode, aPalette, aLines );
	}

	/**
	 * Produces ASCII-Art from the provided arguments.
	 * 
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aFont The {@link Font} to use for printing ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * @param aLines The text lines to convert to ASCII-Art.
	 * 
	 * @return The resulting ASCII-Art.
	 */
	public static String[] asAsciiArt( int aWidth, Font aFont, AsciiArtMode aAsciiArtMode, char[] aPalette, String... aLines ) {
		final List<String> theText = new ArrayList<>();
		String eLine;
		for ( int i = 0; i < aLines.length; i++ ) {
			eLine = aLines[i];
			Collections.addAll( theText, asAsciiArt( eLine, aWidth, aFont, aAsciiArtMode, aPalette ) );
			if ( i < aLines.length - 1 ) {
				theText.add( new TextLineBuilder().withLineChar( ' ' ).withColumnWidth( theText.get( 0 ).length() ).toString() );
			}
		}
		return theText.toArray( new String[theText.size()] );
	}

	/**
	 * Produces ASCII-Art from the provided arguments.
	 * 
	 * @param aText The text to convert to ASCII-Art.
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aFont The {@link Font} to use for printing ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * 
	 * @return The resulting ASCII-Art.
	 */
	public static String[] asAsciiArt( String aText, int aWidth, Font aFont, AsciiArtMode aAsciiArtMode, char... aPalette ) {
		try {
			switch ( aAsciiArtMode ) {
			case NORMAL -> {
				if ( aWidth != -1 ) {
					return toAsciiArt( aText, aWidth, aFont, aPalette );
				}
				if ( aWidth == -1 && aFont.getSize() == -1 ) {
					return toAsciiArt( aText, Terminal.toHeuristicWidth(), aFont, aPalette );
				}
				return toAsciiArt( aText, aFont, aPalette );
			}
			case INVERSE -> {
				if ( aWidth != -1 ) {
					return toInverseAsciiArt( aText, aWidth, aFont, aPalette );
				}
				if ( aWidth == -1 && aFont.getSize() == -1 ) {
					return toInverseAsciiArt( aText, Terminal.toHeuristicWidth(), aFont, aPalette );
				}
				return toInverseAsciiArt( aText, aFont, aPalette );
			}
			default -> throw new IllegalArgumentException( "You must pass a valid ASCII-Art mode, though you actually passed <" + aAsciiArtMode + ">!" );
			}
		}
		catch ( Error | Exception e ) {
			if ( Execution.isUnderTest() ) {
				LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
			}
		}
		return asSimpleBanner( aText, aWidth, aAsciiArtMode, aPalette );
	}

	/**
	 * Fallback in case creating the ASCII-Art banner failed (can happen in the
	 * GraalVM with poor AWT support for rendering fonts.
	 * 
	 * @param aText The text to convert to ASCII-Art.
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * 
	 * @return The resulting simple banner.
	 */
	public static String[] asSimpleBanner( String aText, int aWidth, AsciiArtMode aAsciiArtMode, char... aPalette ) {

		if ( aText == null || aText.isEmpty() ) {
			return new String[] {};
		}

		if ( aAsciiArtMode == AsciiArtMode.INVERSE ) {
			final char[] thePalette = new char[aPalette.length];
			System.arraycopy( aPalette, 0, thePalette, 0, aPalette.length );
			reverseArray( thePalette );
			aPalette = thePalette;
		}

		// Fallback in case GraalVM cannot handle AWT calls |-->
		final int theInnerBorderWidth = 1;
		final int theTotalBorderChars = theInnerBorderWidth * 2;

		aText = CaseStyleBuilder.asKebabCase( aText );

		final TextBorderBuilder theBorderBuilder = new TextBorderBuilder();
		final StringBuilder theString = new StringBuilder();
		final char theFillChar = aPalette[0];
		// if ( theFillChar == ' ' && aPalette.length >= 2 ) theFillChar = aPalette[1];
		if ( theFillChar != ' ' ) {
			theString.append( ' ' );
		}
		for ( int i = 0; i < aText.length(); i++ ) {
			theString.append( aText.charAt( i ) );
			if ( i < aText.length() - 1 ) {
				theString.append( ' ' );
			}
		}
		if ( theFillChar != ' ' ) {
			theString.append( ' ' );
		}

		aText = theString.toString().toUpperCase();
		final HorizAlignTextBuilder theHorizAlignTextBuilder = new HorizAlignTextBuilder();
		final int theHorizWidth = aText.length() + theTotalBorderChars <= aWidth ? aWidth - theTotalBorderChars : aText.length();
		theHorizAlignTextBuilder.setColumnWidth( theHorizWidth );
		theHorizAlignTextBuilder.setFillChar( theFillChar );
		theHorizAlignTextBuilder.setText( aText );
		theHorizAlignTextBuilder.setHorizAlignTextMode( HorizAlignTextMode.CENTER );
		aText = theHorizAlignTextBuilder.toString();
		theBorderBuilder.setText( aText );
		theBorderBuilder.setBorderChar( theFillChar );
		theBorderBuilder.setBorderWidth( theInnerBorderWidth );
		theBorderBuilder.setBoxBorderMode( BoxBorderMode.TOP_RIGHT_BOTTOM_LEFT );
		return theBorderBuilder.toStrings();
		// Fallback in case GraalVM cannot handle AWT calls <--|
	}

	/**
	 * Produces ASCII-Art from the provided arguments.
	 *
	 * @param aImageFile The image file to be converted to ASCII-Art.
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aPixmapRatioMode The {@link PixmapRatioMode} describes how to
	 *        scale the {@link Pixmap} ratio for ASCII-Art as a
	 *        {@link Character} on the screen does not have a ratio of 1:1 (in
	 *        comparison to a pixel which usually has a ration of 1:1).
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * 
	 * @return The resulting ASCII-Art.
	 * 
	 * @throws IOException in case the file was not found or the like.
	 */
	public static String[] asAsciiArt( File aImageFile, int aWidth, AsciiArtMode aAsciiArtMode, PixmapRatioMode aPixmapRatioMode, char... aPalette ) throws IOException {
		final RgbPixmapImageBuilder thePixmap = new RgbPixmapImageBuilder().withWidth( aWidth );
		return asAsciiArt( thePixmap.toPixmap( aImageFile ), aWidth, aAsciiArtMode, aPixmapRatioMode, aPalette );
	}

	/**
	 * Produces ASCII-Art from the provided arguments.
	 *
	 * @param aImageStream The image's {@link InputStream} to be converted to
	 *        ASCII-Art.
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aPixmapRatioMode The {@link PixmapRatioMode} describes how to
	 *        scale the {@link Pixmap} ratio for ASCII-Art as a
	 *        {@link Character} on the screen does not have a ratio of 1:1 (in
	 *        comparison to a pixel which usually has a ration of 1:1).
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * 
	 * @return The resulting ASCII-Art.
	 * 
	 * @throws IOException in case the file was not found or the like.
	 */
	public static String[] asAsciiArt( InputStream aImageStream, int aWidth, AsciiArtMode aAsciiArtMode, PixmapRatioMode aPixmapRatioMode, char... aPalette ) throws IOException {
		final RgbPixmapImageBuilder thePixmap = new RgbPixmapImageBuilder().withWidth( aWidth );
		return asAsciiArt( thePixmap.toPixmap( aImageStream ), aWidth, aAsciiArtMode, aPixmapRatioMode, aPalette );
	}

	/**
	 * Produces ASCII-Art from the provided arguments.
	 *
	 * @param aPixmap The {@link Pixmap} which to convert to ASCII-Art.
	 * @param aWidth The width for the resulting ASCII-Art.
	 * @param aAsciiArtMode The {@link AsciiArtMode} specifies whether to invert
	 *        the resulting ASCII-Art or not.
	 * @param aPixmapRatioMode The {@link PixmapRatioMode} describes how to
	 *        scale the {@link Pixmap} ratio for ASCII-Art as a
	 *        {@link Character} on the screen does not have a ratio of 1:1 (in
	 *        comparison to a pixel which usually has a ration of 1:1).
	 * @param aPalette The {@link Character} set used as ASCII-Art color
	 *        palette.
	 * 
	 * @return The resulting ASCII-Art.
	 */
	public static String[] asAsciiArt( RgbPixmap aPixmap, int aWidth, AsciiArtMode aAsciiArtMode, PixmapRatioMode aPixmapRatioMode, char... aPalette ) {
		RgbPixmap thePixmap = aPixmap;
		final int theWidth = aWidth == -1 ? aPixmap.getWidth() : aWidth;
		if ( theWidth != aPixmap.getWidth() || aPixmapRatioMode != PixmapRatioMode.NONE ) {
			java.awt.image.BufferedImage theBufferedImage = new java.awt.image.BufferedImage( aPixmap.getWidth(), aPixmap.getHeight(), java.awt.image.BufferedImage.TYPE_4BYTE_ABGR );
			for ( int x = 0; x < aPixmap.getWidth(); x++ ) {
				for ( int y = 0; y < aPixmap.getHeight(); y++ ) {
					theBufferedImage.setRGB( x, y, aPixmap.getPixelAt( x, y ).toRgbValue() );
				}
			}
			int theHeight = aPixmap.getHeight();
			if ( aPixmapRatioMode != PixmapRatioMode.NONE ) {
				theHeight = Math.round( (float) theWidth / (float) aPixmap.getWidth() * aPixmap.getHeight() * aPixmapRatioMode.getRatio() );
			}

			final java.awt.Image theScaledImage = theBufferedImage.getScaledInstance( theWidth, theHeight, java.awt.Image.SCALE_SMOOTH );
			theBufferedImage = new java.awt.image.BufferedImage( theWidth, theHeight, java.awt.image.BufferedImage.TYPE_4BYTE_ABGR );
			theBufferedImage.getGraphics().drawImage( theScaledImage, 0, 0, new java.awt.Color( 0, 0, 0 ), null );
			final RgbPixmapBuilderImpl theScaledPixmap = new RgbPixmapBuilderImpl( theWidth, theHeight );
			for ( int x = 0; x < theScaledPixmap.getWidth(); x++ ) {
				for ( int y = 0; y < theScaledPixmap.getHeight(); y++ ) {
					theScaledPixmap.setRgbAt( theBufferedImage.getRGB( x, y ), x, y );
				}
			}
			thePixmap = theScaledPixmap;
		}
		switch ( aAsciiArtMode ) {
		case NORMAL -> {
			return toAsciiArt( thePixmap, aPalette );
		}
		case INVERSE -> {
			return toInverseAsciiArt( thePixmap, aPalette );
		}
		}
		throw new IllegalArgumentException( "You must pass a valid ASCII-Art mode, though you actually passed <" + aAsciiArtMode + ">!" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To ascii art.
	 *
	 * @param aPixmap the pixmap
	 * @param aPalette the palette
	 * 
	 * @return the string[]
	 */
	protected static String[] toAsciiArt( RgbPixmap aPixmap, char[] aPalette ) {
		final String[] theAsciiArt = new String[aPixmap.getHeight()];
		for ( int y = 0; y < aPixmap.getHeight(); y++ ) {
			theAsciiArt[y] = "";
			for ( int x = 0; x < aPixmap.getWidth(); x++ ) {
				theAsciiArt[y] += toAscii( aPixmap.getPixelAt( x, y ).toRgbValue(), aPalette );
			}
		}
		return theAsciiArt;
	}

	/**
	 * To inverse ascii art.
	 *
	 * @param aPixmap the pixmap
	 * @param aPalette the palette
	 * 
	 * @return the string[]
	 */
	protected static String[] toInverseAsciiArt( RgbPixmap aPixmap, char[] aPalette ) {
		final char[] thePalette = new char[aPalette.length];
		System.arraycopy( aPalette, 0, thePalette, 0, aPalette.length );
		reverseArray( thePalette );
		return toAsciiArt( aPixmap, thePalette );
	}

	/**
	 * This method creates an array of {@link String} instances containing the
	 * given text as so called ASCII-Art rendered with the given {@link Font}
	 * name and {@link Font} to fit into the given width.
	 *
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aWidth the width
	 * @param aFontName The {@link Font} name to be used when rendering to
	 *        ASCII-Art.
	 * @param aFontStyle The {@link Font} style to be used when rendering to
	 *        ASCII-Art.
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toAsciiArt( String aText, int aWidth, String aFontName, int aFontStyle, char[] aPalette ) {
		try {
			final String[] theLines = toAsciiArt( aText, new Font( GraphicalUtility.getAwtFont( aText, aWidth, aFontName, aFontStyle ) ), aPalette );
			for ( int i = 0; i < theLines.length; i++ ) {
				if ( theLines[i].length() != aWidth ) {
					theLines[i] = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( theLines[i] ).withColumnWidth( aWidth ).withFillChar( aPalette[0] ).toString();
				}
			}
			return theLines;
		}
		catch ( Exception | Error e ) {
			if ( Execution.isUnderTest() ) {
				LOGGER.log( Level.WARNING, Trap.asMessage( e ), e );
			}
		}
		return asSimpleBanner( aText, aWidth, AsciiArtMode.NORMAL, aPalette );
	}

	/**
	 * This method creates an array of {@link String} instances containing the
	 * given text as so called ASCII-Art rendered with the given {@link Font}
	 * name and {@link Font} style to fit into the given width.
	 *
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aWidth the width
	 * @param aFont the font
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toAsciiArt( String aText, int aWidth, Font aFont, char[] aPalette ) {

		final String theFontName = aFont.getName();
		final int theFontSyle = aFont.getStyle().getCode();

		final String[] theLines = toAsciiArt( aText, aWidth, theFontName, theFontSyle, aPalette );
		for ( int i = 0; i < theLines.length; i++ ) {
			if ( theLines[i].length() != aWidth ) {
				theLines[i] = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( theLines[i] ).withColumnWidth( aWidth ).withFillChar( aPalette[0] ).toString();
			}
		}
		return theLines;
	}

	/**
	 * Same as {@link toAsciiArt} with inverted colors (black = white, dark =
	 * bright, bright = dark, white = black).
	 *
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aWidth the width
	 * @param aFontName The {@link Font} name to be used when rendering to
	 *        ASCII-Art.
	 * @param aFontStyle The {@link Font} style to be used when rendering to
	 *        ASCII-Art.
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toInverseAsciiArt( String aText, int aWidth, String aFontName, int aFontStyle, char[] aPalette ) {
		final char[] thePalette = new char[aPalette.length];
		System.arraycopy( aPalette, 0, thePalette, 0, aPalette.length );
		reverseArray( thePalette );
		return toAsciiArt( aText, aWidth, aFontName, aFontStyle, thePalette );
	}

	/**
	 * Same as {@link toAsciiArt} with inverted colors (black = white, dark =
	 * bright, bright = dark, white = black).
	 *
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aWidth the width
	 * @param aFont The {@link Font} to be used when rendering to ASCII-Art.
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toInverseAsciiArt( String aText, int aWidth, Font aFont, char[] aPalette ) {
		final char[] thePalette = new char[aPalette.length];
		System.arraycopy( aPalette, 0, thePalette, 0, aPalette.length );
		reverseArray( thePalette );
		return toAsciiArt( aText, aWidth, aFont, thePalette );
	}

	/**
	 * This method creates an array of {@link String} instances containing the
	 * given text as so called ASCII-Art rendered with the given {@link Font}
	 * name and {@link Font} style to fit into the given width.
	 *
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aWidth the width
	 * @param aFontName The {@link Font} name to be used when rendering to
	 *        ASCII-Art.
	 * @param aFontStyle The {@link Font} style to be used when rendering to
	 *        ASCII-Art.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toAsciiArt( String aText, int aWidth, String aFontName, int aFontStyle ) {
		return toAsciiArt( aText, aWidth, aFontName, aFontStyle, AsciiColorPalette.MAX_LEVEL_GRAY.getPalette() );
	}

	/**
	 * Same as {@link toAsciiArt} with inverted colors (black = white, dark =
	 * bright, bright = dark, white = black).
	 *
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aWidth the width
	 * @param aFontName The {@link Font} name to be used when rendering to
	 *        ASCII-Art.
	 * @param aFontStyle The {@link Font} style to be used when rendering to
	 *        ASCII-Art.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toInverseAsciiArt( String aText, int aWidth, String aFontName, int aFontStyle ) {
		final char[] thePalette = new char[AsciiColorPalette.MAX_LEVEL_GRAY.getPalette().length];
		System.arraycopy( AsciiColorPalette.MAX_LEVEL_GRAY.getPalette(), 0, thePalette, 0, AsciiColorPalette.MAX_LEVEL_GRAY.getPalette().length );
		reverseArray( thePalette );
		return toAsciiArt( aText, aWidth, aFontName, aFontStyle, thePalette );
	}

	/**
	 * This method creates an array of {@link String} instances containing the
	 * given text rendered with the given {@link Font} as so called ASCII-Art.
	 * 
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aFont The {@link Font} to render the ASCII-Art.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toAsciiArt( String aText, Font aFont ) {
		return toAsciiArt( aText, aFont, AsciiColorPalette.MAX_LEVEL_GRAY.getPalette() );
	}

	/**
	 * Same as {@link toAsciiArt} with inverted colors (black = white, dark =
	 * bright, bright = dark, white = black).
	 * 
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aFont The {@link Font} to render the ASCII-Art.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toInverseAsciiArt( String aText, Font aFont ) {
		final char[] thePalette = new char[AsciiColorPalette.MAX_LEVEL_GRAY.getPalette().length];
		System.arraycopy( AsciiColorPalette.MAX_LEVEL_GRAY.getPalette(), 0, thePalette, 0, AsciiColorPalette.MAX_LEVEL_GRAY.getPalette().length );
		reverseArray( thePalette );
		return toAsciiArt( aText, aFont, thePalette );
	}

	/**
	 * This method creates an array of {@link String} instances containing the
	 * given text rendered with the given {@link Font} as so called ASCII-Art.
	 * 
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aFont The {@link Font} to render the ASCII-Art.
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toAsciiArt( String aText, Font aFont, char[] aPalette ) {
		final int theImageType = java.awt.image.BufferedImage.TYPE_BYTE_GRAY;

		// -------------------------------------------------------------------
		// First get metrics for determining the dimensions of what we create:
		// -------------------------------------------------------------------

		java.awt.image.BufferedImage theImage = new java.awt.image.BufferedImage( 1, 1, theImageType );
		java.awt.Graphics theGraphics = theImage.getGraphics();
		theGraphics.setFont( aFont.toAwtFont() );
		final java.awt.FontMetrics theFontMetrics = theGraphics.getFontMetrics();

		final int theWidth = theFontMetrics.stringWidth( aText );
		final int theHeight = theFontMetrics.getAscent() + theFontMetrics.getHeight() + theFontMetrics.getDescent();

		// -------------------------------------------------------------------
		// Now we know the actual size for the image:
		// -------------------------------------------------------------------
		theImage = new java.awt.image.BufferedImage( theWidth, theHeight, theImageType );
		theGraphics = theImage.getGraphics();
		theGraphics.setFont( aFont.toAwtFont() );
		final java.awt.Graphics2D theGraphics2d = (java.awt.Graphics2D) theGraphics;
		theGraphics2d.setRenderingHint( java.awt.RenderingHints.KEY_TEXT_ANTIALIASING, java.awt.RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
		theGraphics2d.drawString( aText, 0, theHeight - theFontMetrics.getDescent() );

		final List<String> theLines = new ArrayList<>();
		String eLine;
		char eAsciiChar;

		int theTrimBegin = theWidth;
		int theTrimEnd = 0;

		for ( int y = 0; y < theHeight; y++ ) {
			final StringBuilder theBuffer = new StringBuilder();
			for ( int x = 0; x < theWidth; x++ ) {
				eAsciiChar = toAscii( theImage.getRGB( x, y ), aPalette );
				theBuffer.append( eAsciiChar );
			}
			eLine = theBuffer.toString();
			if ( eLine.trim().isEmpty() ) {
				continue;
			}

			if ( isObsolete( eLine ) ) {
				continue;
			}

			theLines.add( eLine );

			for ( int i = 0; i < eLine.length(); i++ ) {
				if ( ( eLine.charAt( i ) != ' ' ) && ( i < theTrimBegin ) ) {
					theTrimBegin = i;
					break;
				}
			}

			for ( int i = eLine.length() - 1; i >= 0; i-- ) {
				if ( ( eLine.charAt( i ) != ' ' ) && ( i > theTrimEnd ) ) {
					theTrimEnd = i;
					break;
				}
			}

		}

		// ---------------------------------------------------------------------
		// Nothing to crop:
		// ---------------------------------------------------------------------
		if ( ( theTrimBegin == 0 ) && ( theTrimEnd == theWidth - 1 ) ) {
			return theLines.toArray( new String[theLines.size()] );
		}
		// ---------------------------------------------------------------------
		// Crop the lines:
		// ---------------------------------------------------------------------

		final String[] theResult = new String[theLines.size()];
		for ( int i = 0; i < theLines.size(); i++ ) {
			theResult[i] = theLines.get( i ).substring( theTrimBegin, theTrimEnd + 1 );

		}
		return theResult;
	}

	/**
	 * Same as {@link toAsciiArt} with inverted colors (black = white, dark =
	 * bright, bright = dark, white = black).
	 * 
	 * @param aText The text to be rendered as ASCII-Art.
	 * @param aFont The {@link Font} to render the ASCII-Art.
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return An array of {@link String} instances, when printed out in the
	 *         order as returned, visualize the provided text in the given
	 *         {@link Font} as ASCII-Art.
	 */
	protected static String[] toInverseAsciiArt( String aText, Font aFont, char[] aPalette ) {
		final char[] thePalette = new char[aPalette.length];
		System.arraycopy( aPalette, 0, thePalette, 0, aPalette.length );
		reverseArray( thePalette );
		return toAsciiArt( aText, aFont, thePalette );
	}

	/**
	 * Converts an RGB value to an ASCII_HEADER_ASCII_BODY character from a
	 * palette if characters as provided.
	 *
	 * @param aRgbValue The RGB value to be converted to an
	 *        ASCII_HEADER_ASCII_BODY character.
	 * @param aPalette The character palate to use when converting an RGB value
	 *        to an ASCII_HEADER_ASCII_BODY character. The first character
	 *        represents white, the last character represents black, the
	 *        characters in between are gradients from white to black.
	 * 
	 * @return the char
	 */
	protected static char toAscii( int aRgbValue, char[] aPalette ) {
		final int eGrayIndex;
		final float theFactor = (float) 256 / (float) aPalette.length;
		final float theIndex = GraphicalUtility.toGray( aRgbValue ) / theFactor;
		eGrayIndex = (int) theIndex;
		final char theAsciiChar = aPalette[eGrayIndex];
		return theAsciiChar;
	}

	/**
	 * Converts an RGB value to an ASCII_HEADER_ASCII_BODY character from a
	 * palette if characters as provided.
	 *
	 * @param aRgbValue The RGB value to be converted to an
	 *        ASCII_HEADER_ASCII_BODY character.
	 * 
	 * @return the char
	 */
	protected static char toAscii( int aRgbValue ) {
		return toAscii( aRgbValue, AsciiColorPalette.MAX_LEVEL_GRAY.getPalette() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Helper method for testing whether a {@link String} just consists of all
	 * the same character.
	 * 
	 * @param aLine The line to see whether it is full of a dedicated character.
	 * 
	 * @return True if being full of one character.
	 */
	private static boolean isObsolete( String aLine ) {
		if ( aLine == null || aLine.isEmpty() ) {
			return true;
		}
		final char theChar = aLine.charAt( 0 );
		for ( int i = 1; i < aLine.length(); i++ ) {
			if ( aLine.charAt( i ) != theChar ) {
				return false;
			}
		}
		return true;
	}

	private static void reverseArray( char[] aArray ) {
		char eChar;
		for ( int i = 0; i < aArray.length / 2; i++ ) {
			eChar = aArray[i];
			aArray[i] = aArray[aArray.length - i - 1];
			aArray[aArray.length - i - 1] = eChar;
		}
	}
}
