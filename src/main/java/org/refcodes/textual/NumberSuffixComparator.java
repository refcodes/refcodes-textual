// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.Comparator;

/**
 * Comparator for strings with prefixed numbers taking into account the value(!)
 * of the prefixed number:
 * 
 * Eg. "foo1" &lt; "foo10"
 */
public class NumberSuffixComparator implements Comparator<String> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compare( String o1, String o2 ) {
		if ( o1 != null && o2 != null ) {
			String thePrefix = "";
			for ( int i = 0; i < o1.length() & i < o2.length(); i++ ) {
				if ( o1.charAt( i ) == o2.charAt( i ) ) {
					thePrefix += o1.charAt( i );
				}
				else {
					break;
				}
			}
			o1 = o1.substring( thePrefix.length() );
			o2 = o2.substring( thePrefix.length() );
			try {
				final int thisIndex = Integer.valueOf( o1 );
				final int thatIndex = Integer.valueOf( o2 );
				if ( thisIndex == thatIndex ) {
					return 0;
				}
				if ( thisIndex < thatIndex ) {
					return -1;
				}
				if ( thisIndex > thatIndex ) {
					return 1;
				}
			}
			catch ( NumberFormatException ignore ) {}
		}
		return o1.compareTo( o2 );
	}
}
