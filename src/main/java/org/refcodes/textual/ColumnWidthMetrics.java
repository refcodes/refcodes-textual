// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;

/**
 * This interface specifies means to define a width either as percentage or
 * absolute, e.g. in the number of chars.
 */
public interface ColumnWidthMetrics extends ColumnWidthProperty, ColumnWidthBuilder<ColumnWidthMetrics> {

	/**
	 * The type of the width being provided, either percent (%) or number of
	 * chars.
	 * 
	 * @return The column's width type, either percent (%) or number of chars.
	 */
	ColumnWidthType getColumnWidthType();

	/**
	 * Sets the column width type.
	 *
	 * @param aColumnWidthType the new column width type
	 */
	void setColumnWidthType( ColumnWidthType aColumnWidthType );

	/**
	 * With column width type.
	 *
	 * @param aColumnWidthType the column width type
	 * 
	 * @return the column width metrics
	 */
	default ColumnWidthMetrics withColumnWidthType( ColumnWidthType aColumnWidthType ) {
		setColumnWidthType( aColumnWidthType );
		return this;
	}

	/**
	 * From column width metrics.
	 *
	 * @param aColumnWidthMetrics the column width metrics
	 */
	default void fromColumnWidthMetrics( ColumnWidthMetrics aColumnWidthMetrics ) {
		setColumnWidth( aColumnWidthMetrics.getColumnWidth() );
		setColumnWidthType( aColumnWidthMetrics.getColumnWidthType() );
	}
}