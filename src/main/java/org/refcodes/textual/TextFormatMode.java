// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Determines how to format a text (in a table), e.g. do not format anything,
 * format the text only or format the whole cell containing the text, even blank
 * lines (important for the background).
 */
public enum TextFormatMode {
	/**
	 * Noting is to be formatted.
	 */
	NONE,
	/**
	 * Just the text portions are to be formatted.
	 */
	TEXT,
	/**
	 * The whole cell including text and blank lines is formatted
	 * (ehttps://www.metacodes.proly important in case of a formatted
	 * background).
	 */
	CELL
}