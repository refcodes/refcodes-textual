// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.Prefix;
import org.refcodes.mixin.DelimiterAccessor.DelimiterBuilder;
import org.refcodes.mixin.DelimiterAccessor.DelimiterProperty;
import org.refcodes.mixin.TrimAccessor.TrimBuilder;
import org.refcodes.mixin.TrimAccessor.TrimProperty;
import org.refcodes.textual.CsvEscapeModeAccessor.CsvEscapeModeBuilder;
import org.refcodes.textual.CsvEscapeModeAccessor.CsvEscapeModeProperty;

/**
 * This interface defines common functionality for implementations capable of
 * CSV (file) handling.
 */
public interface CsvMixin extends CsvEscapeModeProperty, CsvEscapeModeBuilder<CsvMixin>, TrimProperty, TrimBuilder<CsvMixin>, DelimiterProperty, DelimiterBuilder<CsvMixin> {

	/**
	 * Sets the prefixes used to identify CSV lines to be ignored.
	 * 
	 * @param aCommentPrefixes The prefixes used to identify CSV lines to be
	 *        ignored.
	 */
	void setCommentPrefixes( String... aCommentPrefixes );

	/**
	 * Gets the prefixes used to identify CSV lines to be ignored.
	 * 
	 * @return return The prefixes used to identify CSV lines to be ignored.
	 */
	String[] getCommentPrefixes();

	/**
	 * Clears the prefixes used to identify CSV lines to be ignored. No CSV
	 * lines will be ignored any more.
	 */
	void clearCommentPrefixes();

	/**
	 * Sets the prefixes used to identify CSV lines to be ignored.
	 * 
	 * @param aCommentPrefixes The prefixes used to identify CSV lines to be
	 *        ignored.
	 * 
	 * @return The instance on which this method has been invoked as of the
	 *         builder pattern.
	 */
	CsvMixin withCommentPrefixes( String... aCommentPrefixes );

	/**
	 * Tests whether the line starts with a comment prefix.
	 * 
	 * @param aLine The line to be tested.
	 * 
	 * @return True in case the line starts with a comment prefix, else false.
	 */
	default boolean isComment( String aLine ) {
		if ( getCommentPrefixes() != null && aLine != null ) {
			for ( String ePrefix : getCommentPrefixes() ) {
				if ( aLine.startsWith( ePrefix ) ) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Uncomment the given line in case it is a comment as of
	 * {@link #isComment(String)}.
	 * 
	 * @param aLine The line to be uncommented.
	 * 
	 * @return The uncommented line.
	 */
	default String uncomment( String aLine ) {
		if ( !isComment( aLine ) ) {
			return aLine;
		}
		for ( String ePrefix : getCommentPrefixes() ) {
			if ( aLine.startsWith( ePrefix ) ) {
				aLine = aLine.substring( ePrefix.length() );
				while ( aLine.startsWith( " " ) ) {
					aLine = aLine.substring( 1 );
				}
				break;
			}
		}
		return aLine;
	}

	/**
	 * Converts a text to a comment by using the fist defined comment prefix as
	 * of {@link #getCommentPrefixes()} suffixed with a space (" ") followed by
	 * the line of text. If no prefix has been set via
	 * {@link #setCommentPrefixes(String...)} or
	 * {@link #withCommentPrefixes(String...)}, then the
	 * {@link Prefix#CSV_COMMENT} is used.
	 * 
	 * @param aLine The line to be converted to a comment line.
	 * 
	 * @return The comment accordingly prefixed.
	 */
	default String toComment( String aLine ) {
		final String theCommentPrefix;
		if ( getCommentPrefixes() == null || getCommentPrefixes().length == 0 || getCommentPrefixes()[0] == null || getCommentPrefixes()[0].isEmpty() ) {
			theCommentPrefix = Prefix.CSV_COMMENT.getPrefix();
		}
		else {
			theCommentPrefix = getCommentPrefixes()[0];
		}
		return theCommentPrefix + " " + aLine;
	}
}
