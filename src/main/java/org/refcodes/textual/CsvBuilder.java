// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.refcodes.data.Delimiter;

/**
 * This class implements CSV processing functionality.
 */
public class CsvBuilder implements CsvMixin {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _record = null;
	private Collection<Object> _fields = null;
	private char _delimiterChar = Delimiter.CSV.getChar();
	private CsvEscapeMode _csvEscapeMode = CsvEscapeMode.ESCAPED;
	private boolean _isTrim = true;
	private String[] _commentPrefixes = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrim( boolean isTrim ) {
		_isTrim = isTrim;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTrim() {
		return _isTrim;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvEscapeMode getCsvEscapeMode() {
		return _csvEscapeMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvEscapeMode = aCsvEscapeMode;
	}

	/**
	 * Sets the CSV line for the CSV line property.
	 * 
	 * @param aRecord The CSV line to be stored by the CSV line property.
	 */
	public void setRecord( String aRecord ) {
		_record = aRecord;
	}

	/**
	 * Retrieves the CSV line from the CSV line property.
	 * 
	 * @return The CSV line stored by the CSV line property.
	 */
	public String getRecord() {
		return _record;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Object... aFields ) {
		_fields = toCollection( aFields );
	}

	/**
	 * Retrieves the CSV elements from the CSV elements property.
	 * 
	 * @return The CSV elements stored by the CSV elements property.
	 */
	public Collection<Object> getFields() {
		return _fields;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Collection<Object> aFields ) {
		_fields = aFields;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _delimiterChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDelimiter( char aDelimiterChar ) {
		_delimiterChar = aDelimiterChar;
	}

	/**
	 * Creates a list of {@link String} elements from the CSV line being set via
	 * {@link #setRecord(String)} or {@link #withRecord(String)}.
	 * 
	 * @return The elements created from the CSV line.
	 * 
	 * @throws IllegalStateException Thrown in case no elements have been set
	 *         via the {@link #withFields(String[])} or
	 *         {@link #setFields(String[])} methods.
	 */
	public List<String> toFields() {
		if ( !isComment( _record ) ) {
			return CsvBuilder.asFields( _record, _delimiterChar, _csvEscapeMode, _isTrim );
		}
		return null;
	}

	/**
	 * Creates a CSV line from the elements being set via
	 * {@link #setFields(String[])} or {@link #withFields(String[])}.
	 * 
	 * @return The CSV line created from the CSV line.
	 * 
	 * @throws IllegalStateException Thrown in case no CSV line has been set via
	 *         the {@link #withRecord(String)} or {@link #setRecord(String)}
	 *         methods.
	 */
	public String toRecord() {
		return CsvBuilder.asRecord( _fields, _delimiterChar, _csvEscapeMode );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Object... aFields ) {
		return CsvBuilder.asRecord( toCollection( aFields ), _delimiterChar, _csvEscapeMode );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Collection<Object> aFields ) {
		return CsvBuilder.asRecord( aFields, _delimiterChar, _csvEscapeMode );
	}

	/**
	 * Creates an array of {@link String}s from the CSV line being set via
	 * {@link #setRecord(String)} or {@link #withRecord(String)}. It should
	 * return the same result as an array as a call to {@link #toFields()}.
	 * Returns null if the record is a comment as of {@link #isComment(String)}.
	 * 
	 * @return The elements created from the CSV line or null if the record is a
	 *         comment.
	 */
	public String[] toStrings() {
		final List<String> theCsvElements = toFields();
		if ( theCsvElements != null ) {
			return theCsvElements.toArray( new String[theCsvElements.size()] );
		}
		else {
			return null;
		}
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( String... aFields ) {
		return CsvBuilder.asRecord( toCollection( aFields ), _delimiterChar, _csvEscapeMode );
	}

	/**
	 * Convenience method for the {@link #toFields(String)} method.
	 * 
	 * @param aRecord The CSV line to be stored by the CSV line property.
	 * 
	 * @return The elements created from the CSV line or null if the record is a
	 *         comment.
	 */
	public String[] toStrings( String aRecord ) {
		return CsvBuilder.asStrings( aRecord, _delimiterChar, _csvEscapeMode, _isTrim, getCommentPrefixes() );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes. It should return the same result as a call to
	 * {@link #toRecord()}.
	 * 
	 * @return The according resulting {@link String}
	 */
	@Override
	public String toString() {
		return toRecord();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCommentPrefixes( String... aCommentPrefixes ) {
		_commentPrefixes = aCommentPrefixes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getCommentPrefixes() {
		return _commentPrefixes;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearCommentPrefixes() {
		_commentPrefixes = null;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvBuilder withTrim( boolean isTrim ) {
		setTrim( isTrim );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvBuilder withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		setCsvEscapeMode( aCsvEscapeMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvBuilder withDelimiter( char aDelimiterChar ) {
		setDelimiter( aDelimiterChar );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvBuilder withCommentPrefixes( String... aCommentPrefixes ) {
		setCommentPrefixes( aCommentPrefixes );
		return this;
	}

	/**
	 * Sets the CSV line for the CSV line property.
	 * 
	 * @param aRecord The CSV line to be stored by the CSV line property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withRecord( String aRecord ) {
		setRecord( aRecord );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( boolean... aFields ) {
		final Boolean[] theFields = new Boolean[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( byte... aFields ) {
		final Byte[] theFields = new Byte[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( short... aFields ) {
		final Short[] theFields = new Short[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( int... aFields ) {
		final Integer[] theFields = new Integer[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( long... aFields ) {
		final Long[] theFields = new Long[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( float... aFields ) {
		final Float[] theFields = new Float[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( double... aFields ) {
		final Double[] theFields = new Double[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( char... aFields ) {
		final Character[] theFields = new Character[aFields.length];
		for ( int i = 0; i < theFields.length; i++ ) {
			theFields[i] = aFields[i];
		}
		setFields( (Object[]) theFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( boolean... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( byte... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( short... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( int... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( long... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( float... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( double... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( char... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Boolean... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Byte... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Short... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Integer... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Long... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Float... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Double... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( Character... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setFields( String... aFields ) {
		setFields( (Object[]) aFields );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Object... aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Boolean... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Byte... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Short... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Integer... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Long... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Float... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Double... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Character... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( String... aFields ) {
		setFields( (Object[]) aFields );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aFields The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public CsvBuilder withFields( Collection<Object> aFields ) {
		setFields( aFields );
		return this;
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( boolean... aFields ) {
		final Boolean[] theBoxed = new Boolean[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( byte... aFields ) {
		final Byte[] theBoxed = new Byte[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( short... aFields ) {
		final Short[] theBoxed = new Short[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( int... aFields ) {
		final Integer[] theBoxed = new Integer[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( long... aFields ) {
		final Long[] theBoxed = new Long[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( float... aFields ) {
		final Float[] theBoxed = new Float[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( double... aFields ) {
		final Double[] theBoxed = new Double[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( char... aFields ) {
		final Character[] theBoxed = new Character[aFields.length];
		Arrays.setAll( theBoxed, n -> aFields[n] );
		return toRecord( theBoxed );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toString( Object... aFields ) {
		return toRecord( aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Boolean... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Byte... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * To record.
	 *
	 * @param aFields the fields
	 * 
	 * @return the string
	 */
	public String toRecord( Short... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Integer... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Long... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Float... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Double... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toRecord( Character... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toString( String... aFields ) {
		return toRecord( (Object[]) aFields );
	}

	/**
	 * Creates a CSV line from the fields being provided This method is to be
	 * side effect free in terms of the fields (and the resulting record) is not
	 * part of the state for this instance (from the point of view of this
	 * method). Still changing for example the delimiter via
	 * {@link #withDelimiter(char)} can cause side effects! For avoiding thread
	 * race conditions / side effects regarding the fields (and the resulting
	 * record), use this method instead of the combination of
	 * {@link #withFields(String...)} with {@link #toRecord()}.
	 * 
	 * @param aFields The CSV elements to be converted into a record.
	 * 
	 * @return The CSV line created from the fields.
	 */
	public String toString( Collection<Object> aFields ) {
		return toRecord( aFields.toArray( new Object[aFields.size()] ) );
	}

	/**
	 * Creates an array of {@link String}s from the CSV line being set via
	 * {@link #setRecord(String)} or {@link #withRecord(String)}. It should
	 * return the same result as an array as a call to {@link #toFields()}.
	 * Returns null if the record is a comment as of {@link #isComment(String)}.
	 * This method is to be side effect free in terms of the record (and the
	 * resulting fields) is not part of the state for this instance (from the
	 * point of view of this method). Still changing for example the delimiter
	 * via {@link #withDelimiter(char)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the record (and the
	 * resulting fields), use this method instead of the combination of
	 * {@link #withRecord(String)} with {@link #toStrings()}.
	 * 
	 * @param aRecord The CSV line to be stored by the CSV line property.
	 * 
	 * @return The elements created from the CSV line or null if the record is a
	 *         comment.
	 */
	public String[] toFields( String aRecord ) {
		return toStrings( aRecord );
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @return an instance (using a default implementation) of this builder
	 */
	static CsvBuilder build() {
		return new CsvBuilder();
	}

	/**
	 * Creates fields from the given record.
	 *
	 * @param aRecord The record to use.
	 * @param aDelimiter The delimiter to use.
	 * @param aCsvEscapeMode The {@link CsvEscapeMode} to use.
	 * @param isTrim True in case the record and the resulting fields are to be
	 *        trimmed.
	 * 
	 * @return The according record.
	 */
	public static List<String> asFields( String aRecord, char aDelimiter, CsvEscapeMode aCsvEscapeMode, boolean isTrim ) {

		if ( isTrim && aRecord != null && aRecord.length() != 0 ) {
			aRecord = aRecord.trim();
		}

		switch ( aCsvEscapeMode ) {
		case ESCAPED: {
			final List<String> theFields = fromSeparatedValues( aRecord, aDelimiter );
			if ( !isTrim ) {
				return theFields;
			}
			else {
				final List<String> theTrimmedFields = new ArrayList<>();
				for ( String eField : theFields ) {
					if ( eField != null ) {
						eField = eField.trim();
					}
					theTrimmedFields.add( eField );
				}
				return theTrimmedFields;
			}
		}
		case NONE: {
			final List<String> theFields = fromSplitValues( aRecord, aDelimiter );
			if ( !isTrim ) {
				return theFields;
			}
			else {
				final List<String> theTrimmedValues = new ArrayList<>();
				for ( String eField : theFields ) {
					if ( eField != null ) {
						eField = eField.trim();
					}
					theTrimmedValues.add( eField );
				}
				return theTrimmedValues;
			}
		}
		default:
			break;
		}
		throw new IllegalArgumentException( "You must pass a valid text overwrite mode, though you actually passed <" + aCsvEscapeMode + ">!" );
	}

	/**
	 * Creates a record from the given fields.
	 *
	 * @param aFields The fields to use.
	 * @param aDelimiter The delimiter to use.
	 * @param aCsvEscapeMode The {@link CsvEscapeMode} to use.
	 * 
	 * @return The according record.
	 */
	public static String asRecord( Collection<Object> aFields, char aDelimiter, CsvEscapeMode aCsvEscapeMode ) {
		switch ( aCsvEscapeMode ) {
		case ESCAPED -> {
			return toSeparatedValues( aFields, aDelimiter );
		}
		case NONE -> {
			return toSplitValues( aFields, aDelimiter );
		}
		default -> {
		}
		}
		throw new IllegalArgumentException( "You must pass a valid text overwrite mode, though you actually passed <" + aCsvEscapeMode + ">!" );
	}

	/**
	 * Splits a record into dedicated fields.
	 * 
	 * @param aRecord The record which to split into dedicated fields.
	 * @param aDelimiterChar The delimiter denoting the end of a value.
	 * @param aCsvEscapeMode The {@link CsvEscapeMode} to use.
	 * @param isTrim Whether to trim the record and the resulting fields.
	 * @param aCommentPrefixes The prefixes used to identify comments.
	 * 
	 * @return The according fields from the record.
	 */
	public static String[] asStrings( String aRecord, char aDelimiterChar, CsvEscapeMode aCsvEscapeMode, boolean isTrim, String[] aCommentPrefixes ) {
		if ( !isComment( aRecord, aCommentPrefixes ) ) {
			final List<String> theCsvElements = asFields( aRecord, aDelimiterChar, aCsvEscapeMode, isTrim );
			return theCsvElements.toArray( new String[theCsvElements.size()] );
		}
		else {
			return null;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static String toEscapedRegexp( char aDelimiter ) {
		return ( "" + aDelimiter ).replaceAll( "\\.", "\\\\." ).replaceAll( "\\|", "\\\\|" );
	}

	private static boolean isComment( String aLine, String[] aCommentPrefixes ) {
		if ( aCommentPrefixes != null && aLine != null ) {
			for ( String ePrefix : aCommentPrefixes ) {
				if ( aLine.startsWith( ePrefix ) ) {
					return true;
				}
			}
		}
		return false;
	}

	private static List<String> fromSeparatedValues( String aSeparatedValues, char aDelimiter ) {

		final List<String> theOutputList = new ArrayList<>();

		if ( aSeparatedValues != null && aSeparatedValues.length() != 0 ) {
			final List<String> theInputList = new ArrayList<>();
			final String[] theItems = aSeparatedValues.split( toEscapedRegexp( aDelimiter ) );
			final List<String> theTokens = new ArrayList<>();
			//while ( aSeparatedValues.startsWith( "" + aDelimiter ) ) {
			//theTokens.add( "" );
			//aSeparatedValues = aSeparatedValues.substring( 1 );
			//}
			for ( String theItem : theItems ) {
				if ( "\"\"".equals( theItem ) ) {
					theTokens.add( "" );
				}
				else {
					theTokens.add( theItem.replaceAll( "\"\"", "\"" ) );
				}
			}
			while ( aSeparatedValues.endsWith( "" + aDelimiter ) ) {
				theTokens.add( "" );
				aSeparatedValues = aSeparatedValues.substring( 0, aSeparatedValues.length() - 1 );
			}

			String ePreviousItem = null;
			for ( String eToken : theTokens ) {
				if ( !theInputList.isEmpty() ) {
					ePreviousItem = theInputList.get( theInputList.size() - 1 );
					if ( ( ( ePreviousItem.startsWith( "\"" ) ) ) && ( !ePreviousItem.endsWith( "\"" ) ) ) {
						theInputList.remove( theInputList.size() - 1 );
						theInputList.add( ePreviousItem + aDelimiter + eToken );
					}
					else {
						theInputList.add( eToken );
					}
				}
				else {
					theInputList.add( eToken );
				}
			}

			for ( String eItem : theInputList ) {
				if ( eItem.length() > 1 && eItem.startsWith( "\"" ) && eItem.endsWith( "\"" ) ) {
					eItem = eItem.substring( 1, eItem.length() - 1 );
				}
				eItem.replaceAll( "\"\"", "\"" );
				theOutputList.add( eItem.isEmpty() ? null : eItem );
			}
		}
		return theOutputList;
	}

	private static String toSeparatedValues( Object[] aArray, char aDelimiter ) {
		final List<Object> theList = new ArrayList<>();
		Collections.addAll( theList, aArray );
		return toSeparatedValues( theList, aDelimiter );
	}

	private static String toSplitValue( Object aValue, char aDelimiter ) {
		final Object[] eItems;
		final String[] eStringItems;
		final char aInnerDelimeter;
		if ( aDelimiter != Delimiter.ARRAY.getChar() ) {
			aInnerDelimeter = Delimiter.ARRAY.getChar();
		}
		else {
			aInnerDelimeter = Delimiter.LIST.getChar();
		}
		if ( aValue instanceof String[] ) {
			aValue = toSplitValues( (String[]) aValue, aInnerDelimeter );
		}
		else if ( aValue instanceof Object[] ) {
			eItems = (Object[]) aValue;
			eStringItems = new String[eItems.length];
			for ( int i = 0; i < eItems.length; i++ ) {
				eStringItems[i] = eItems[i] != null ? eItems[i].toString() : null;
			}
			aValue = toSplitValues( eStringItems, aInnerDelimeter );
		}

		return aValue.toString();
	}

	private static List<String> fromSplitValues( String aSplitValues, char aDelimiter ) {
		return Arrays.asList( aSplitValues.split( toEscapedRegexp( aDelimiter ) ) );
	}

	private static String toSplitValues( Object[] aArray, char aDelimiter ) {
		final List<Object> theList = new ArrayList<>();
		Collections.addAll( theList, aArray );
		return toSplitValues( theList, aDelimiter );
	}

	private static String toSplitValues( Collection<?> aCollection, char aDelimiter ) {
		final StringBuilder theBuffer = new StringBuilder();
		String eItemString = null;
		Object eItem = null;
		final Iterator<?> e = aCollection.iterator();
		while ( e.hasNext() ) {
			eItem = e.next();
			if ( eItem != null ) {

				eItemString = toSplitValue( eItem, aDelimiter );
				theBuffer.append( eItemString );
			}
			if ( e.hasNext() ) {
				theBuffer.append( aDelimiter );
			}
		}
		return theBuffer.toString();
	}

	private static String toSeparatedValues( Collection<?> aCollection, char aDelimiter ) {
		final StringBuilder theBuffer = new StringBuilder();
		String eItemString = null;
		Object eItem = null;
		final Iterator<?> e = aCollection.iterator();
		while ( e.hasNext() ) {
			eItem = e.next();
			if ( eItem != null ) {
				eItemString = toSeparatedValue( eItem, aDelimiter );
				theBuffer.append( eItemString );
			}
			if ( e.hasNext() ) {
				theBuffer.append( aDelimiter );
			}
		}
		return theBuffer.toString();
	}

	private static String toSeparatedValue( Object aValue, char aDelimiter ) {
		String eItemString;
		final Object[] eItems;
		final String[] eStringItems;
		final char aInnerDelimeter;
		if ( aDelimiter != Delimiter.ARRAY.getChar() ) {
			aInnerDelimeter = Delimiter.ARRAY.getChar();
		}
		else {
			aInnerDelimeter = Delimiter.LIST.getChar();
		}
		if ( aValue instanceof String[] ) {
			aValue = toSeparatedValues( (String[]) aValue, aInnerDelimeter );
		}
		else if ( aValue instanceof Object[] ) {
			eItems = (Object[]) aValue;
			eStringItems = new String[eItems.length];
			for ( int i = 0; i < eItems.length; i++ ) {
				eStringItems[i] = eItems[i] != null ? eItems[i].toString() : null;
			}
			aValue = toSeparatedValues( eStringItems, aInnerDelimeter );
		}

		eItemString = aValue.toString();
		// ---------------------------------------------------------------------
		// Do we have the delimiter in the {@link String}? Then we must
		// enclose the {@link String} in double quotes. To do that, we must
		// 'escape' any double quote in the {@link String} with a double quote.
		// ---------------------------------------------------------------------
		if ( ( eItemString.indexOf( aDelimiter ) != -1 ) || ( eItemString.contains( "\"" ) ) ) {
			// ---------------------------------------
			// Escape all double quotes in the {@link String}:
			// ---------------------------------------
			eItemString = eItemString.replaceAll( "\"", "\"\"" );
			// ------------------------------------
			// Enclose the {@link String} in double quotes:
			// ------------------------------------
			eItemString = "\"" + eItemString + "\"";
		}
		return eItemString;
	}

	@SuppressWarnings("unchecked")
	private static Collection<Object> toCollection( Object[] aFields ) {
		if ( aFields.length == 1 && aFields[0] instanceof Collection<?> ) {
			return ( (Collection<Object>) aFields[0] );
		}
		else {
			return Arrays.asList( aFields );
		}
	}
}
