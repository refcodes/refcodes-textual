// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a font name property.
 */
public interface FontNameAccessor {

	/**
	 * Retrieves the font name from the font name property.
	 * 
	 * @return The font name stored by the font name property.
	 */
	String getFontName();

	/**
	 * Provides a mutator for a font name property.
	 */
	public interface FontNameMutator {

		/**
		 * Sets the font name for the font name property.
		 * 
		 * @param aFontName The font name to be stored by the font name
		 *        property.
		 */
		void setFontName( String aFontName );
	}

	/**
	 * Provides a builder method for a font name property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FontNameBuilder<B extends FontNameBuilder<B>> {

		/**
		 * Sets the font name for the font name property.
		 * 
		 * @param aFontName The font name to be stored by the font name
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFontName( String aFontName );
	}

	/**
	 * Provides a font name property.
	 */
	public interface FontNameProperty extends FontNameAccessor, FontNameMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setFontName(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aFontName The {@link String} to set (via
		 *        {@link #setFontName(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letFontName( String aFontName ) {
			setFontName( aFontName );
			return aFontName;
		}
	}
}
