// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Implementation of the {@link ColumnFormatMetrics} interface.
 */
public class ColumnSetupMetricsImpl extends ColumnFormatMetricsImpl implements ColumnSetupMetrics {

	// /////////////////////////////////////////////////////////////////////
	// ENUM:
	// /////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////

	private boolean _isVisible = true;

	private String _name = null;

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new column setup metrics impl.
	 */
	public ColumnSetupMetricsImpl() {}

	/**
	 * Constructs a column's width, either in percent (%) or in number of chars.
	 * 
	 * @param aWidth The width for the column, either in percent (%) or in
	 *        number of chars, depending on the provided {@link ColumnWidthType}
	 *        .
	 * @param aWidthType The type of the width being provided, either percent
	 *        (%) or number of chars.
	 */
	public ColumnSetupMetricsImpl( int aWidth, ColumnWidthType aWidthType ) {
		super( aWidth, aWidthType );
	}

	// /////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVisible() {
		return _isVisible;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVisible( boolean isVisible ) {
		_isVisible = isVisible;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setName( String aName ) {
		_name = aName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnSetupMetrics withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}
}