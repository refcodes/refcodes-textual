// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

/**
 * In comparison to the {@link TableBuilder}, the {@link TablePrinter} just
 * provides the methods required to print a table. This is most useful when you
 * want to create a {@link TablePrinter} composite (which encapsulates many
 * {@link TablePrinter} instances) which only requires the printing methods as
 * the builder methods would make no sense: Think of a {@link TablePrinter}
 * which is used for logging text with a log priority. For each log priority
 * there is a dedicated {@link TablePrinter} instance configured slightly
 * different, e.g using different text colors for the printed row depending on
 * the log priority. Those "internal" {@link TablePrinter} instances would be
 * preconfigured so that a {@link TableBuilder} composite would actually
 * overload the composite with Builder-Pattern functionality which must not be
 * applied to the internal {@link TablePrinter} instances. Actually the internal
 * {@link TablePrinter} instances would be part (implementation secret) of a
 * {@link TableBuilder} instance as them are configured each individually using
 * the Builder-Pattern.
 */
public interface TablePrinter {

	/**
	 * Begins a header, for convenience reasons, this is being encapsulated by
	 * the {@link #toHeader(String...)} method. Use this method in case you need
	 * more control on the header construction than {@link #toHeader(String...)}
	 * can provide you.
	 * 
	 * @return The header's begin {@link String} including the line breaks.
	 */
	String toHeaderBegin();

	/**
	 * For more semantic clearness this method returns an end-of-header /
	 * begin-of-row line, though is equivalent to the method
	 * {@link #toHeaderBegin()}.
	 * 
	 * @return The header's end / row's begin {@link String} including the line
	 *         breaks.
	 */
	default String toHeaderComplete() {
		return toHeaderBegin();
	}

	/**
	 * Continues a begun header, for convenience reasons, this is being
	 * encapsulated by the {@link #toHeader(String...)} method. Use this method
	 * in case you need more control on the header construction than
	 * {@link #toHeader(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 * 
	 * @return The begun header continued {@link String} including the line
	 *         breaks.
	 */
	String toHeaderContinue( String... aColumns );

	/**
	 * Continues a begun header, for convenience reasons, this is being
	 * encapsulated by the {@link #toHeader(String...)} method. Use this method
	 * in case you need more control on the header construction than
	 * {@link #toHeader(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 * 
	 * @return The begun header continued {@link String} including the line
	 *         breaks.
	 */
	default String toHeaderContinue( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		return toHeaderContinue( theRow );
	}

	/**
	 * Ends the headers of the provided {@link TableBuilder} for this table
	 * printer to continue; different header widths and column widths are taken
	 * care of, so it is a pleasure to mix different {@link TableBuilder}s,
	 * ehttps://www.metacodes.proly when empty columns may result in better
	 * using another column layout making better use of the available width.
	 * 
	 * @param aTablePrinter The {@link TableBuilder} to which to append this
	 *        {@link TableBuilder}'s headers.
	 * 
	 * @return The divider header joining the provided (top)
	 *         {@link TableBuilder}'s layout with this (bottom)
	 *         {@link TableBuilder}'s layout.
	 */
	String toHeaderEnd( TableBuilder aTablePrinter );

	/**
	 * Ends a header, for convenience reasons, this is being encapsulated by the
	 * {@link #toHeader(String...)} method. Use this method in case you need
	 * more control on the header construction than {@link #toHeader(String...)}
	 * can provide you.
	 * 
	 * @return The header's end {@link String} including the line breaks.
	 */
	String toHeaderEnd();

	/**
	 * Begins a row, for convenience reasons, this is being encapsulated by the
	 * {@link #toRow(String...)} method. Use this method in case you need more
	 * control on the row construction than {@link #toRow(String...)} can
	 * provide you.
	 * 
	 * @return The row's begin {@link String} including the line breaks.
	 */
	String toRowBegin();

	/**
	 * Ends a row, actually the same as {@link #toRowBegin()} (which begins a
	 * new row thereby ending the current row).
	 */
	default String toRowEnd() {
		return toRowBegin();
	}

	/**
	 * Continues a begun row, for convenience reasons, this is being
	 * encapsulated by the {@link #toRow(String...)} method. Use this method in
	 * case you need more control on the header construction than
	 * {@link #toRow(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 * 
	 * @return The begun row continued {@link String} including the line breaks.
	 */
	String toRowContinue( String... aColumns );

	/**
	 * Continues a begun row, for convenience reasons, this is being
	 * encapsulated by the {@link #toRow(String...)} method. Use this method in
	 * case you need more control on the header construction than
	 * {@link #toRow(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 * 
	 * @return The begun row continued {@link String} including the line breaks.
	 */
	default String toRowContinue( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		return toRowContinue( theRow );
	}

	/**
	 * Ends a row, for convenience reasons, this is being encapsulated by the
	 * {@link #toRow(String...)} method. Use this method in case you need more
	 * control on the header construction than {@link #toRow(String...)} can
	 * provide you.
	 *
	 * @param aTablePrinter the table printer
	 * 
	 * @return The row's end {@link String} including the line breaks.
	 */
	// String toRowEnd();

	/**
	 * Ends the rows of the provided {@link TableBuilder} for this table printer
	 * to continue; different row widths and column widths are taken care of, so
	 * it is a pleasure to mix different {@link TableBuilder}s,
	 * ehttps://www.metacodes.proly when empty columns may result in better
	 * using another column layout making better use of the available width.
	 * 
	 * @param aTablePrinter The {@link TableBuilder} to which to append this
	 *        {@link TableBuilder}'s rows.
	 * 
	 * @return The divider row joining the provided (top) {@link TableBuilder}'s
	 *         layout with this (bottom) {@link TableBuilder}'s layout.
	 */
	String toRowEnd( TableBuilder aTablePrinter );

	/**
	 * Prints the table's header with the content of the provided columns.
	 * 
	 * @param aColumns The columns to be used in the table's header.
	 * 
	 * @return The header {@link String} including line breaks as it most
	 *         probably will consist of more than one line.
	 */
	String toHeader( String... aColumns );

	/**
	 * Prints the table's header with the content of the provided columns.
	 * 
	 * @param aColumns The columns to be used in the table's header.
	 * 
	 * @return The header {@link String} including line breaks as it most
	 *         probably will consist of more than one line.
	 */
	default String toHeader( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		return toHeader( theRow );
	}

	/**
	 * Prints the table's (next) row with the content of the provided columns.
	 * Call this method for each row to print out.
	 * 
	 * @param aColumns The columns to be used in the (next) table's row.
	 * 
	 * @return The row {@link String} including line breaks as it most probably
	 *         will consist of more than one line.
	 */
	String toRow( String... aColumns );

	/**
	 * Prints the table's (next) row with the content of the provided columns.
	 * Call this method for each row to print out.
	 * 
	 * @param aColumns The columns to be used in the (next) table's row.
	 * 
	 * @return The row {@link String} including line breaks as it most probably
	 *         will consist of more than one line.
	 */
	default String toRow( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		return toRow( theRow );
	}

	/**
	 * Finishes off the table by closing it.
	 * 
	 * @return The tail of the table for finishing the table off.
	 */
	String toTail();

	/**
	 * Prints out a header begin to the {@link PrintStream} configured for this
	 * {@link PrintWriter}, for convenience reasons, this is being encapsulated
	 * by the {@link #printHeader(String...)} method. Use this method in case
	 * you need more control on the header construction than
	 * {@link #printHeader(String...)} can provide you.
	 */
	void printHeaderBegin();

	/**
	 * For more semantic clearness this method prints an end-of-header /
	 * begin-of-row line, though is equivalent to the method
	 * {@link #printHeaderBegin()}.
	 */
	default void printHeaderComplete() {
		printHeaderBegin();
	}

	/**
	 * Continues to print out a begun header to the {@link PrintStream}
	 * configured for this {@link PrintWriter}, for convenience reasons, this is
	 * being encapsulated by the {@link #printHeader(String...)} method. Use
	 * this method in case you need more control on the header construction than
	 * {@link #printHeader(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 */
	void printHeaderContinue( String... aColumns );

	/**
	 * Continues to print out a begun header to the {@link PrintStream}
	 * configured for this {@link PrintWriter}, for convenience reasons, this is
	 * being encapsulated by the {@link #printHeader(String...)} method. Use
	 * this method in case you need more control on the header construction than
	 * {@link #printHeader(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 */
	default void printHeaderContinue( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		printHeaderContinue( theRow );
	}

	/**
	 * Ends the headers of the provided {@link TableBuilder} for this table
	 * printer to continue to the {@link PrintStream} configured for this
	 * {@link PrintWriter}; different header widths and column widths are taken
	 * care of, so it is a pleasure to mix different {@link TableBuilder}s,
	 * ehttps://www.metacodes.proly when empty columns may result in better
	 * using another column layout making better use of the available width.
	 * 
	 * @param aTablePrinter The {@link TableBuilder} to which to append this
	 *        {@link TableBuilder}'s headers.
	 */
	void printHeaderEnd( TableBuilder aTablePrinter );

	/**
	 * Prints out a header end to the {@link PrintStream} configured for this
	 * {@link PrintWriter}, for convenience reasons, this is being encapsulated
	 * by the {@link #printHeader(String...)} method. Use this method in case
	 * you need more control on the header construction than
	 * {@link #printHeader(String...)} can provide you.
	 */
	void printHeaderEnd();

	/**
	 * Prints out a row begin to the {@link PrintStream} configured for this
	 * {@link PrintWriter}, for convenience reasons, this is being encapsulated
	 * by the {@link #printRow(String...)} method. Use this method in case you
	 * need more control on the row construction than
	 * {@link #printRow(String...)} can provide you.
	 */
	void printRowBegin();

	/**
	 * Ends a row, actually the same as {@link #printRowBegin()} (which begins a
	 * new row thereby ending the current row).
	 */
	default void printRowEnd() {
		printRowBegin();
	}

	/**
	 * Continues a print out begun row to the {@link PrintStream} configured for
	 * this {@link PrintWriter}, for convenience reasons, this is being
	 * encapsulated by the {@link #printRow(String...)} method. Use this method
	 * in case you need more control on the header construction than
	 * {@link #printRow(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 */
	void printRowContinue( String... aColumns );

	/**
	 * Continues a print out begun row to the {@link PrintStream} configured for
	 * this {@link PrintWriter}, for convenience reasons, this is being
	 * encapsulated by the {@link #printRow(String...)} method. Use this method
	 * in case you need more control on the header construction than
	 * {@link #printRow(String...)} can provide you.
	 *
	 * @param aColumns the columns
	 */
	default void printRowContinue( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		printRowContinue( theRow );
	}

	/**
	 * Ends the rows of the provided {@link TableBuilder} for this table printer
	 * to continue to the {@link PrintStream} configured for this
	 * {@link PrintWriter}; different row widths and column widths are taken
	 * care of, so it is a pleasure to mix different {@link TableBuilder}s,
	 * ehttps://www.metacodes.proly when empty columns may result in better
	 * using another column layout making better use of the available width.
	 * 
	 * @param aTablePrinter The {@link TableBuilder} to which to append this
	 *        {@link TableBuilder}'s rows.
	 */
	void printRowEnd( TableBuilder aTablePrinter );

	/**
	 * Prints the table's header to the {@link PrintStream} configured for this
	 * {@link PrintWriter} with the content of the provided columns.
	 * 
	 * @param aColumns The columns to be used in the table's header.
	 */
	void printHeader( String... aColumns );

	/**
	 * Prints the table's header to the {@link PrintStream} configured for this
	 * {@link PrintWriter} with the content of the provided columns.
	 * 
	 * @param aColumns The columns to be used in the table's header.
	 */
	default void printHeader( List<String> aColumns ) {
		String[] theHeader = null;
		if ( aColumns != null ) {
			theHeader = aColumns.toArray( new String[aColumns.size()] );
		}
		printHeader( theHeader );
	}

	/**
	 * Prints the table's (next) row to the {@link PrintStream} configured for
	 * this {@link PrintWriter} with the content of the provided columns. Call
	 * this method for each row to print out.
	 * 
	 * @param aColumns The columns to be used in the (next) table's row.
	 */
	void printRow( String... aColumns );

	/**
	 * Prints the table's (next) row to the {@link PrintStream} configured for
	 * this {@link PrintWriter} with the content of the provided columns. Call
	 * this method for each row to print out.
	 * 
	 * @param aColumns The columns to be used in the (next) table's row.
	 */
	default void printRow( List<String> aColumns ) {
		String[] theRow = null;
		if ( aColumns != null ) {
			theRow = aColumns.toArray( new String[aColumns.size()] );
		}
		printRow( theRow );
	}

	/**
	 * Finishes off the table by printing its closing to the {@link PrintStream}
	 * configured for this {@link PrintWriter}.
	 */
	void printTail();

	/**
	 * Retrieves the {@link TablePrinter} ({@link TableBuilder}) status. The
	 * {@link TableStatus} is required by a {@link TablePrinter} (
	 * {@link TableBuilder}) to determine whether to do additional table
	 * decoration for a print operation or to fail that operation as of an
	 * illegal state exception. As far as possible an illegal state exception is
	 * to be tried to be prevented by decorating the table automatically by an
	 * operation as of the current {@link TableStatus}.
	 * 
	 * @return The table's current {@link TableStatus}.
	 */
	TableStatus getTableStatus();

	/**
	 * Set the {@link TablePrinter} ({@link TableBuilder}) status. The
	 * {@link TableStatus} is required by a {@link TablePrinter} (
	 * {@link TableBuilder}) to determine whether to do additional table
	 * decoration for a print operation or to fail that operation as of an
	 * illegal state exception. As far as possible an illegal state exception is
	 * to be tried to be prevented by decorating the table automatically by an
	 * operation as of the current {@link TableStatus}. Setting the status
	 * manually, you can use differently configured {@link TablePrinter}
	 * interfaces to print their lines according to the status of a previous
	 * {@link TablePrinter}.
	 *
	 * @param aTableStatus The table's current {@link TableStatus} to be set.
	 * 
	 * @return the table printer
	 */
	TablePrinter setTableStatus( TableStatus aTableStatus );

}
