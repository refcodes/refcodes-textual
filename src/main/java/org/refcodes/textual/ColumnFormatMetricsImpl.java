// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Implementation of the {@link ColumnFormatMetrics} interface.
 */
public class ColumnFormatMetricsImpl extends ColumnWidthMetricsImpl implements ColumnFormatMetrics {

	// /////////////////////////////////////////////////////////////////////
	// ENUM:
	// /////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////

	private HorizAlignTextMode _headerHorizAlignTextMode = HorizAlignTextMode.LEFT;;

	private HorizAlignTextMode _rowHorizAlignTextMode = HorizAlignTextMode.LEFT;

	private String _headerEscapeCode = null;

	private String _rowEscapeCode = null;

	private MoreTextMode _headerMoreTextMode = MoreTextMode.NONE;

	private MoreTextMode _rowMoreTextMode = MoreTextMode.NONE;

	private TextFormatMode _headerTextFormatMode = TextFormatMode.TEXT;

	private TextFormatMode _rowTextFormatMode = TextFormatMode.TEXT;

	private SplitTextMode _headerSplitTextMode = SplitTextMode.AT_SPACE;

	private SplitTextMode _rowSplitTextMode = SplitTextMode.AT_SPACE;

	private EscapeCodeFactory _headerEscapeCodeFactory = null;

	private EscapeCodeFactory _rowEscapeCodeFactory = null;

	// /////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new column format metrics impl.
	 */
	public ColumnFormatMetricsImpl() {}

	/**
	 * Constructs a column's width, either in percent (%) or in number of chars.
	 * 
	 * @param aWidth The width for the column, either in percent (%) or in
	 *        number of chars, depending on the provided {@link ColumnWidthType}
	 *        .
	 * @param aWidthType The type of the width being provided, either percent
	 *        (%) or number of chars.
	 */
	public ColumnFormatMetricsImpl( int aWidth, ColumnWidthType aWidthType ) {
		super( aWidth, aWidthType );
	}

	// /////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnFormatMetrics withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCode( String aEscapeCode ) {
		_headerEscapeCode = aEscapeCode;
		_rowEscapeCode = aEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderEscapeCode( String aEscapeCode ) {
		_headerEscapeCode = aEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHeaderEscapeCode() {
		return _headerEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowEscapeCode( String aEscapeCode ) {
		_rowEscapeCode = aEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRowEscapeCode() {
		return _rowEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoreTextMode( MoreTextMode aMoreTextMode ) {
		_rowMoreTextMode = aMoreTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_headerHorizAlignTextMode = aHorizAlignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getHeaderHorizAlignTextMode() {
		return _headerHorizAlignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_rowHorizAlignTextMode = aHorizAlignTextMode;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getRowHorizAlignTextMode() {
		return _rowHorizAlignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderMoreTextMode( MoreTextMode aMoreTextMode ) {
		_headerMoreTextMode = aMoreTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoreTextMode getHeaderMoreTextMode() {
		return _headerMoreTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowMoreTextMode( MoreTextMode aMoreTextMode ) {
		_rowMoreTextMode = aMoreTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoreTextMode getRowMoreTextMode() {
		return _rowMoreTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderTextFormatMode( TextFormatMode aTextFormatMode ) {
		_headerTextFormatMode = aTextFormatMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextFormatMode getHeaderTextFormatMode() {
		return _headerTextFormatMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowTextFormatMode( TextFormatMode aTextFormatMode ) {
		_rowTextFormatMode = aTextFormatMode;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextFormatMode getRowTextFormatMode() {
		return _rowTextFormatMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderSplitTextMode( SplitTextMode aSplitTextMode ) {
		_headerSplitTextMode = aSplitTextMode;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SplitTextMode getHeaderSplitTextMode() {
		return _headerSplitTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowSplitTextMode( SplitTextMode aSplitTextMode ) {
		_rowSplitTextMode = aSplitTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SplitTextMode getRowSplitTextMode() {
		return _rowSplitTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHeaderEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		_headerEscapeCodeFactory = aEscapeCodeFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EscapeCodeFactory getHeaderEscapeCodeFactory() {
		return _headerEscapeCodeFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeaderEscapeCode( Object aIdentifier ) {
		final String theEscapeCode = _headerEscapeCodeFactory != null ? _headerEscapeCodeFactory.create( aIdentifier ) : null;
		return theEscapeCode != null ? theEscapeCode : getHeaderEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowEscapeCodeFactory( EscapeCodeFactory aEscapeCodeFactory ) {
		_rowEscapeCodeFactory = aEscapeCodeFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EscapeCodeFactory getRowEscapeCodeFactory() {
		return _rowEscapeCodeFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toRowEscapeCode( Object aIdentifier ) {
		final String theEscapeCode = _rowEscapeCodeFactory != null ? _rowEscapeCodeFactory.create( aIdentifier ) : null;
		return theEscapeCode != null ? theEscapeCode : getRowEscapeCode();
	}
}