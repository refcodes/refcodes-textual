// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a split-text mode property.
 */
public interface SplitTextModeAccessor {

	/**
	 * Retrieves the split-text mode from the split-text mode property.
	 * 
	 * @return The split-text mode stored by the split-text mode property.
	 */
	SplitTextMode getSplitTextMode();

	/**
	 * Provides a mutator for a split-text mode property.
	 */
	public interface SplitTextModeMutator {

		/**
		 * Sets the split-text mode for the split-text mode property.
		 * 
		 * @param aSplitTextMode The split-text mode to be stored by the
		 *        split-text mode property.
		 */
		void setSplitTextMode( SplitTextMode aSplitTextMode );
	}

	/**
	 * Provides a builder method for a split-text mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SplitTextModeBuilder<B extends SplitTextModeBuilder<B>> {

		/**
		 * Sets the split-text mode for the split-text mode property.
		 * 
		 * @param aSplitTextMode The split-text mode to be stored by the
		 *        split-text mode property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSplitTextMode( SplitTextMode aSplitTextMode );
	}

	/**
	 * Provides a split-text mode property.
	 */
	public interface SplitTextModeProperty extends SplitTextModeAccessor, SplitTextModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SplitTextMode}
		 * (setter) as of {@link #setSplitTextMode(SplitTextMode)} and returns
		 * the very same value (getter).
		 * 
		 * @param aSplitTextMode The {@link SplitTextMode} to set (via
		 *        {@link #setSplitTextMode(SplitTextMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SplitTextMode letSplitTextMode( SplitTextMode aSplitTextMode ) {
			setSplitTextMode( aSplitTextMode );
			return aSplitTextMode;
		}
	}
}
