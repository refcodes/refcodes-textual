// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Plain find/replace utility not(!) using regular expressions.
 */
public class ReplaceTextBuilder extends AbstractText<ReplaceTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _findText = null;

	private String _replaceText = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the replace text from the replace text property.
	 * 
	 * @return The replace text stored by the replace text property.
	 */
	public String getReplaceText() {
		return _replaceText;
	}

	/**
	 * Sets the replace text for the replace text property.
	 * 
	 * @param aReplaceText The replace text to be stored by the text align mode
	 *        property.
	 */
	public void setReplaceText( String aReplaceText ) {
		_replaceText = aReplaceText;
	}

	/**
	 * Retrieves the find text from the find text property.
	 * 
	 * @return The find text stored by the find text property.
	 */
	public String getFindText() {
		return _findText;
	}

	/**
	 * Sets the find text for the find text property.
	 * 
	 * @param aFindText The find text to be stored by the text align mode
	 *        property.
	 */
	public void setFindText( String aFindText ) {
		_findText = aFindText;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return asReplaced( getText(), _findText, _replaceText );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return asReplaced( aText, _findText, _replaceText );
	}

	/**
	 * Sets the replace text for the replace text property.
	 * 
	 * @param aReplaceText The replace text to be stored by the text align mode
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public ReplaceTextBuilder withReplaceText( String aReplaceText ) {
		setReplaceText( aReplaceText );
		return this;
	}

	/**
	 * Sets the find text for the find text property.
	 * 
	 * @param aFindText The find text to be stored by the text align mode
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public ReplaceTextBuilder withFindText( String aFindText ) {
		setFindText( aFindText );
		return this;
	}

	/**
	 * Replaces a text by an other text in a {@link String} array.
	 * 
	 * @param aText The text to be processed ('find-and-replace').
	 * @param aFindText The text which has to be replaced.
	 * @param aReplaceText The text that replaces the original.
	 * 
	 * @return The number of replacements done.
	 */
	public static String[] asReplaced( String[] aText, String aFindText, String aReplaceText ) {
		final String[] theResult = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theResult[i] = asReplaced( aText[i], aFindText, aReplaceText );
		}
		return theResult;
	}

	/**
	 * Replaces all occurrences of a find-{@link String} with a replace-
	 * {@link String} in a text and returns the find-and-replace {@link String}.
	 * 
	 * @param aText The text to be processed ('find-and-replace').
	 * @param aFindText The {@link String} to be searched and replaced.
	 * @param aReplaceText The {@link String} which will replace the searched
	 *        {@link String}.
	 * 
	 * @return Description is currently not available!
	 */
	public static String asReplaced( String aText, String aFindText, String aReplaceText ) {
		final StringBuilder theBuffer = new StringBuilder( aText );
		asReplaced( theBuffer, aFindText, aReplaceText );
		return theBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Finds the position in the given buffer of the given text beginning at the
	 * given index position.
	 * 
	 * @param aTextBuffer The buffer in which to search
	 * @param aFindText The text to be found
	 * @param aIndex The position in the buffer where the search process is to
	 *        be started.
	 * 
	 * @return Description is currently not available!
	 */
	// protected static int indexOf( StringBuffer aTextBuffer, String aFindText,
	// int aIndex ) {
	// if ( aIndex > aTextBuffer.length() ) return -1;
	// int theLength = aFindText.length();
	// boolean isFound;
	// for ( int i = aIndex; i < (aTextBuffer.length() - (theLength - 1)); i++ )
	// {
	// isFound = true;
	// for ( int l = 0; l < theLength; l++ )
	// if ( aTextBuffer.charAt( i + l ) != aFindText.charAt( l ) ) {
	// isFound = false;
	// break;
	// }
	// if ( isFound ) return i;
	// }
	// return -1;
	// }

	/**
	 * Finds the position in the given buffer of the given text beginning at the
	 * given index position.
	 * 
	 * @param aTextBuffer The buffer in which to search
	 * @param aFindText The text to be found
	 * @param aIndex The position in the buffer where the search process is to
	 *        be started.
	 * 
	 * @return Description is currently not available!
	 */
	// protected static boolean hasStringAt( StringBuffer aTextBuffer, String
	// aFindText, int aIndex ) {
	// int theLength = aFindText.length();
	// if ( (aIndex + theLength > aTextBuffer.length()) || (aIndex < 0) ) return
	// false;
	// int i = aIndex;
	// boolean isFound = true;
	// for ( int l = 0; l < theLength; l++ )
	// if ( aTextBuffer.charAt( i + l ) != aFindText.charAt( l ) ) {
	// isFound = false;
	// break;
	// }
	// return isFound;
	// }

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static int asReplaced( StringBuilder aTextBuffer, String aFindText, String aReplaceText ) {
		final int lengthOfFind = aFindText.length();
		int lengthOfSearchString = aTextBuffer.length() - lengthOfFind + 1;
		int theCount = 0;
		for ( int i = 0; i < lengthOfSearchString; i++ ) {
			if ( aFindText.equals( aTextBuffer.substring( i, ( i + lengthOfFind ) ) ) ) {
				theCount++;
				aTextBuffer.replace( i, ( i + lengthOfFind ), aReplaceText );
				lengthOfSearchString = aTextBuffer.length() - lengthOfFind + 1;
				i += aReplaceText.length() - 1;
			}
		}
		return theCount;
	}
}
