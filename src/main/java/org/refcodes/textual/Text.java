// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.textual.TextAccessor.TextBuilder;
import org.refcodes.textual.TextAccessor.TextProperty;
import org.refcodes.textual.TextAccessor.TextProvider;

/**
 * The Interface Text.
 *
 * @param <B> the generic type
 */
public interface Text<B extends Text<B>> extends TextProperty, TextBuilder<Text<B>>, TextProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	default B withText( String... aText ) {
		setText( aText );
		return (B) this;
	}

	/**
	 * Race condition safe shortcut for using {@link #withText(String...)}
	 * followed by {@link #toStrings()}. Implementation requirements: This
	 * method must not(!) be implemented by calling {@link #withText(String...)}
	 * followed by {@link #toStrings()} (do not change the text property) as
	 * this would not be thread safe!
	 * 
	 * @param aText The text to be processed.
	 * 
	 * @return The according resulting {@link String} array
	 */
	String[] toStrings( String... aText );

	/**
	 * Race condition safe shortcut for using {@link #withText(String...)}
	 * followed by {@link #toString()}. Implementation requirements: This method
	 * must not(!) be implemented by calling {@link #withText(String...)}
	 * followed by {@link #toString()} (do not change the text property) as this
	 * would not be thread safe!
	 * 
	 * @param aText The text to be processed.
	 * 
	 * @return The according resulting {@link String}
	 */
	String toString( String... aText );
}