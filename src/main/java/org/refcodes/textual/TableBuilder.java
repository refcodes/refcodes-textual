// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.MoreText;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.mixin.EscapeCodesStatusAccessor.EscapeCodeStatusBuilder;
import org.refcodes.mixin.EscapeCodesStatusAccessor.EscapeCodeStatusProperty;
import org.refcodes.mixin.PrintStreamAccessor.PrintStreamBuilder;
import org.refcodes.mixin.PrintStreamAccessor.PrintStreamProperty;
import org.refcodes.mixin.ResetEscapeCodeAccessor.ResetEscapeCodeBuilder;
import org.refcodes.mixin.ResetEscapeCodeAccessor.ResetEscapeCodeProperty;
import org.refcodes.mixin.RowWidthAccessor.RowWidthBuilder;
import org.refcodes.mixin.RowWidthAccessor.RowWidthProperty;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.runtime.Terminal;

/**
 * The {@link TableBuilder} has the Builder-Pattern applied to configure a text
 * (ASCII_HEADER_ASCII_BODY) table for later printing custom tables with a
 * title, the table rows and if required a table tail. As of the convention over
 * configuration paradigm, reasonable default values are preconfigured in order
 * to start right away; fine-tune your configuration with the provided builder
 * methods.
 */
public class TableBuilder implements TablePrinter, RowWidthProperty, RowWidthBuilder<TableBuilder>, PrintStreamProperty, PrintStreamBuilder<TableBuilder>, ResetEscapeCodeProperty, ResetEscapeCodeBuilder<TableBuilder>, EscapeCodeStatusProperty, EscapeCodeStatusBuilder<TableBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private enum TableSection {
		HEADER, BODY, TAIL;
	}

	/**
	 * The mode with which to initialize the {@link TableBuilder}.
	 */
	public enum RowWidthMode {
		/**
		 * The (absolute) column widths are used to calculate the overall row
		 * width.
		 */
		COLUMNS,

		/**
		 * The current terminal's width (as of
		 * {@link Terminal#toHeuristicWidth()}) is used as tow width.
		 */
		TERMINAL

	}

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TableStatus _tableStatus = TableStatus.NONE;
	private final List<ColumnRecord> _columnRecords = new ArrayList<>();
	private int[] _columnWidths = null;
	private int _rowWidth;
	private TableStyle _tableStyle = TableStyle.DOUBLE_SINGLE_HEADER_SINGLE_BODY;
	private String _lineBreak = Terminal.getLineBreak();
	private PrintStream _printStream = System.out;
	private HorizAlignTextMode _headerAlignMode = HorizAlignTextMode.CENTER;
	private HorizAlignTextMode _rowHorizAlignTextMode = HorizAlignTextMode.LEFT;
	private SplitTextMode _headerSplitMode = SplitTextMode.AT_SPACE;
	private SplitTextMode _rowSplitTextMode = SplitTextMode.AT_SPACE;
	private MoreTextMode _headerMoreMode = MoreTextMode.NONE;
	private MoreTextMode _rowMoreTextMode = MoreTextMode.NONE;
	private String _rowEscCode = null;
	private String _borderEscCode = null;
	private String _headerEscCode = null;
	private String _resetEscCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.RESET );
	private boolean _hasLeftBorder = true;
	private boolean _hasRightBorder = true;
	private boolean _hasDividerLine = true;
	private TextFormatMode _headerFormatMode = TextFormatMode.TEXT;
	private boolean _isEscCodesEnabled = true;
	private TextFormatMode _rowTextFormatMode = TextFormatMode.TEXT;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new table builder with the current terminal's width (as of
	 * {@link Terminal#toHeuristicWidth()}).
	 */
	public TableBuilder() {
		this( Terminal.toHeuristicWidth() );
	}

	/**
	 * Instantiates a new table builder using the provided {@link RowWidthMode}
	 * to determine the table's row width..
	 */
	public TableBuilder( RowWidthMode aMode ) {}

	/**
	 * Initializes the {@link TableBuilder} with the given row width.
	 * 
	 * @param aRowWidth The row width with which to initialize the
	 *        {@link TableBuilder}.
	 */
	public TableBuilder( int aRowWidth ) {
		_rowWidth = aRowWidth;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an array of {@link String} arrays where all the arrays are of the
	 * same length and where the arrays are all aligned as specified by the
	 * passed {@link VertAlignTextMode}.
	 *
	 * @param aColumns An array of {@link String} arrays to be equalized.
	 * @param aTextBlockMode The {@link VertAlignTextMode} to be used when
	 *        equalizing the columns.
	 * 
	 * @return The equalized columns.
	 */
	private String[][] toColumns( String[][] aColumns, VertAlignTextMode aTextBlockMode ) {
		final int theMaxLines = toMaxLength( aColumns );
		final String[][] theColumns = new String[aColumns.length][];
		for ( int i = 0; i < aColumns.length; i++ ) {
			theColumns[i] = new VertAlignTextBuilder().withText( aColumns[i] ).withRowHeight( theMaxLines ).withVertAlignTextMode( aTextBlockMode ).withEscapeCodesEnabled( _isEscCodesEnabled ).toStrings();
		}
		return theColumns;
	}

	/**
	 * Calculates the columns widths in "number of chars" according to the
	 * provided {@link ColumnWidthMetricsImpl} instances in relation to the
	 * provided total (available) width; the provided column widths can be a mix
	 * of "number of char" widths ({@link ColumnWidthType#ABSOLUTE}) and percent
	 * (%) widths ({@link ColumnWidthType#RELATIVE}). When the width is being
	 * relative, then the total sum of all of relative widths represents 100%.
	 * The resulting column widths add up to the total width. The column widths
	 * for the {@link ColumnWidthMetricsImpl} instances being specified in
	 * percent ({@link ColumnWidthType#RELATIVE}) are calculated from the
	 * remainder width after all "number of char" {@link ColumnWidthMetricsImpl}
	 * instances ( {@link ColumnWidthType#ABSOLUTE}) have been subtracted from
	 * the provided total width. *
	 * 
	 * @param aTotalWidth The total available width in number of chars.
	 * @param aColumnWidths The {@link ColumnWidthMetricsImpl} instances
	 *        providing width specifications either in percent (%) or in "number
	 *        of chars".
	 * 
	 * @return An array where each element represents one of the provided
	 *         {@link ColumnWidthMetricsImpl} instances in the order them
	 *         {@link ColumnWidthMetricsImpl} instances have been passed; the
	 *         array species the actual column widths in "number of chars",
	 *         adding up to the total width.
	 */
	static int[] toColumnWidths( int aTotalWidth, ColumnWidthMetrics... aColumnWidths ) {

		int theNumCharWidths = -1;
		int theRelativeWidths = -1;

		for ( ColumnWidthMetrics eColumnWidth : aColumnWidths ) {
			switch ( eColumnWidth.getColumnWidthType() ) {
			case ABSOLUTE -> {
				if ( theNumCharWidths == -1 ) {
					theNumCharWidths = eColumnWidth.getColumnWidth();
				}
				else {
					theNumCharWidths += eColumnWidth.getColumnWidth();
				}
			}
			case RELATIVE -> {
				if ( theRelativeWidths == -1 ) {
					theRelativeWidths = eColumnWidth.getColumnWidth();
				}
				else {
					theRelativeWidths += eColumnWidth.getColumnWidth();
				}
			}
			}
		}

		// -----------
		// Assertions:
		// -----------
		// @formatter:off
		/* 
			if ( theRelativeWidths != -1 && theRelativeWidths < 100 ) {
				throw new IllegalArgumentException( "You provided overall percent (%) widths <" + theRelativeWidths + "%> not adding up to <100%>." );
			}
			if ( theRelativeWidths > 100 ) {
				throw new IllegalArgumentException( "You provided overall percent (%) widths <" + theRelativeWidths + "%> exceeding <100%>." );
			}
		*/
		// @formatter:on

		if ( aTotalWidth == -1 ) {
			aTotalWidth = theNumCharWidths;
		}

		if ( theNumCharWidths < aTotalWidth && theRelativeWidths == -1 ) {
			throw new IllegalArgumentException( "Your provided overall number of char width <" + theNumCharWidths + "> does not add up to the total width available of <" + aTotalWidth + "> and you did not specify an percent (%) width to fill up the missing gap." );
		}
		if ( theNumCharWidths > aTotalWidth ) {
			throw new IllegalArgumentException( "The total width available <" + aTotalWidth + "> is exceeded by the provided overall number of char widths <" + theNumCharWidths + "> requested." );
		}

		final int[] theColumnWidths = new int[aColumnWidths.length];
		final int theRemainderWidth = aTotalWidth - theNumCharWidths;

		int theMaxRelativeWidth = 0;
		for ( ColumnWidthMetrics eColumnWidth : aColumnWidths ) {
			if ( eColumnWidth.getColumnWidthType() == ColumnWidthType.RELATIVE ) {
				theMaxRelativeWidth += eColumnWidth.getColumnWidth();
			}
		}
		ColumnWidthMetrics eColumnWidth;
		for ( int i = 0; i < aColumnWidths.length; i++ ) {
			eColumnWidth = aColumnWidths[i];
			switch ( eColumnWidth.getColumnWidthType() ) {
			case ABSOLUTE -> theColumnWidths[i] = eColumnWidth.getColumnWidth();
			case RELATIVE -> theColumnWidths[i] = (int) ( ( (double) theRemainderWidth ) / ( (double) theMaxRelativeWidth ) * ( eColumnWidth.getColumnWidth() ) );
			default -> throw new UnhandledEnumBugException( eColumnWidth.getColumnWidthType() );
			}
		}

		out: while ( NumericalUtility.sum( theColumnWidths ) < aTotalWidth ) {
			for ( int i = 0; i < theColumnWidths.length; i++ ) {
				if ( aColumnWidths[i].getColumnWidthType() == ColumnWidthType.RELATIVE ) {
					theColumnWidths[i] += 1;
					if ( NumericalUtility.sum( theColumnWidths ) == aTotalWidth ) {
						break out;
					}
				}
			}
		}
		gt: while ( NumericalUtility.sum( theColumnWidths ) > aTotalWidth ) {
			for ( int i = 0; i < theColumnWidths.length; i++ ) {
				theColumnWidths[i]--;
				if ( NumericalUtility.sum( theColumnWidths ) == aTotalWidth ) {
					break gt;
				}
			}
		}
		lt: while ( NumericalUtility.sum( theColumnWidths ) < aTotalWidth ) {
			for ( int i = 0; i < theColumnWidths.length; i++ ) {
				theColumnWidths[i]++;
				if ( NumericalUtility.sum( theColumnWidths ) == aTotalWidth ) {
					break lt;
				}
			}
		}
		return theColumnWidths;
	}

	/**
	 * Adds a column and returns this {@link TableBuilder} instance as of the
	 * Builder-Pattern.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder addColumn() {
		_columnRecords.add( new ColumnRecord() );
		_columnWidths = null;
		return this;
	}

	/**
	 * Adds a column and returns this {@link TableBuilder} instance as of the
	 * Builder-Pattern.
	 * 
	 * @param aWidth The width for the column.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder addColumn( int aWidth ) {
		final ColumnRecord theColumn = new ColumnRecord();
		theColumn.setColumnWidth( aWidth );
		_columnRecords.add( theColumn );
		_columnWidths = null;
		return this;
	}

	/**
	 * Adds a column and returns this {@link TableBuilder} instance as of the
	 * Builder-Pattern.
	 * 
	 * @param aWidth The width for the column.
	 * @param aWidthType The type the width is of, e.g.
	 *        {@link ColumnWidthType#RELATIVE} or
	 *        {@link ColumnWidthType#ABSOLUTE}
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder addColumn( int aWidth, ColumnWidthType aWidthType ) {
		final ColumnRecord theColumn = new ColumnRecord();
		theColumn.setColumnWidth( aWidth );
		theColumn.setColumnWidthType( aWidthType );
		_columnRecords.add( theColumn );
		_columnWidths = null;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream getPrintStream() {
		return _printStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getResetEscapeCode() {
		return _resetEscCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRowWidth() {
		return _rowWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TableStatus getTableStatus() {
		return _tableStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEscapeCodesEnabled() {
		return _isEscCodesEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPrintStream( PrintStream aPrintStream ) {
		_printStream = aPrintStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setResetEscapeCode( String aResetEscCode ) {
		_resetEscCode = aResetEscCode;
	}

	/**
	 * In case a value of -1 is provided, then the width is calculated
	 * dynamically from the individual columns' widths added together including
	 * any borders being specified. {@inheritDoc}
	 */
	@Override
	public void setRowWidth( int aRowWidth ) {
		_rowWidth = aRowWidth;
		updateColumnWidths();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TableBuilder setTableStatus( TableStatus aTableStatus ) {
		_tableStatus = aTableStatus;
		return this;
	}

	/**
	 * Returns the currently calculated or configured column widths. Changing
	 * the row width or adding a column will affect the result being returned.
	 * 
	 * @return The widths of the rows as currently calculated.
	 */
	public int[] toColumnWidths() {
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		return _columnWidths;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeader( String... aColumns ) {
		if ( _columnRecords.size() < aColumns.length ) {
			final int theAdd = aColumns.length - _columnRecords.size();
			for ( int i = 0; i < theAdd; i++ ) {
				addColumn();
			}
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		printlnTopBorder( _columnWidths, TableSection.HEADER, theBuilder );
		printlnLine( _columnWidths, TableSection.HEADER, theBuilder, aColumns );
		_tableStatus = TableStatus.HEADER_CONTINUE;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeaderBegin() {
		if ( _columnRecords.size() == 0 ) {
			throw new IllegalStateException( "You must add least a column before printing the header." );
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		printlnTopBorder( _columnWidths, TableSection.HEADER, theBuilder );
		_tableStatus = TableStatus.HEADER_BEGIN;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeaderContinue( String... aColumns ) {
		if ( _columnRecords.size() == 0 ) {
			for ( int i = 0; i < aColumns.length; i++ ) {
				addColumn();
			}
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		printlnLine( _columnWidths, TableSection.HEADER, theBuilder, aColumns );
		_tableStatus = TableStatus.HEADER_CONTINUE;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeaderEnd() {
		if ( _columnRecords.size() == 0 ) {
			throw new IllegalStateException( "You must add least a column before printing the header." );
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		printlnBottomBorder( _columnWidths, TableSection.HEADER, theBuilder );
		_tableStatus = TableStatus.HEADER_END;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toHeaderEnd( TableBuilder aTablePrinter ) {
		if ( _columnRecords.size() == 0 ) {
			throw new IllegalStateException( "You must add least a column to this table printer before joining the tables." );
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		if ( aTablePrinter.toColumnWidths().length == 0 ) {
			throw new IllegalStateException( "You must add least a column to the provided table printer before joining the tables." );
		}
		final StringBuilder theBuilder = new StringBuilder();
		// ---------------------------------------------------------------------
		// Set the status before the JOIN so that succeeding methods
		// know which characters to use when printing the "join" lines:
		// ---------------------------------------------------------------------
		joinRow( aTablePrinter, TableSection.HEADER, theBuilder );
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toRow( String... aColumns ) {
		if ( _columnRecords.size() == 0 ) {
			if ( _columnRecords.size() == 0 ) {
				for ( int i = 0; i < aColumns.length; i++ ) {
					addColumn();
				}
			}
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		if ( _tableStatus == TableStatus.NONE ) {
			printlnTopBorder( _columnWidths, TableSection.BODY, theBuilder );
		}
		else if ( _tableStatus == TableStatus.ROW_CONTINUE ) {
			printlnTopBorder( _columnWidths, TableSection.BODY, theBuilder );
		}
		else if ( _tableStatus == TableStatus.HEADER_CONTINUE ) {
			printlnTopBorder( _columnWidths, TableSection.HEADER, theBuilder );
		}

		printlnLine( _columnWidths, TableSection.BODY, theBuilder, aColumns );
		_tableStatus = TableStatus.ROW_CONTINUE;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toRowBegin() {
		if ( _columnRecords.size() == 0 ) {
			throw new IllegalStateException( "You must add least a column before printing the header." );
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		printlnTopBorder( _columnWidths, TableSection.BODY, theBuilder );
		_tableStatus = TableStatus.ROW_BEGIN;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toRowContinue( String... aColumns ) {
		if ( _columnRecords.size() == 0 ) {
			for ( int i = 0; i < aColumns.length; i++ ) {
				addColumn();
			}
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		final StringBuilder theBuilder = new StringBuilder();
		printlnLine( _columnWidths, TableSection.BODY, theBuilder, aColumns );
		_tableStatus = TableStatus.ROW_CONTINUE;
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toRowEnd( TableBuilder aTablePrinter ) {

		if ( _columnRecords.size() == 0 ) {
			throw new IllegalStateException( "You must add least a column to this table printer before joining the tables." );
		}
		if ( _columnWidths == null ) {
			updateColumnWidths();
		}
		if ( aTablePrinter.toColumnWidths().length == 0 ) {
			throw new IllegalStateException( "You must add least a column to the provided table printer before joining the tables." );
		}
		final StringBuilder theBuilder = new StringBuilder();
		// ---------------------------------------------------------------------
		// Set the status before the JOIN so that succeeding methods
		// know which characters to use when printing the "join" lines:
		// ---------------------------------------------------------------------
		joinRow( aTablePrinter, TableSection.BODY, theBuilder );
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toTail() {
		final StringBuilder theBuilder = new StringBuilder();
		printlnBottomBorder( _columnWidths, TableSection.TAIL, theBuilder );
		return theBuilder.toString();
	}

	/**
	 * Sets an overall ANSI Escape-Code for the borders and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. In case an ANSI
	 * Escape-Codes is set, then the ANSI Escape-Code is prepended and an ANSI
	 * Reset-Code is appended to the according text being printed. The latest
	 * ANSI Escape-Code set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withBorderEscapeCode( String aEscapeCode ) {
		_borderEscCode = aEscapeCode;
		return this;
	}

	/**
	 * Sets the current column's (the last added one with the
	 * {@link #addColumn()} method) format (including the width) and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern.
	 * 
	 * @param aColumnFormatMetrics The width and the {@link ColumnWidthType} (
	 *        {@link ColumnWidthType#RELATIVE} or
	 *        {@link ColumnWidthType#ABSOLUTE}) of the column as well as the
	 *        format.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 * 
	 * @throws IllegalStateException in case there is none column already added.
	 */
	public TableBuilder withColumnFormatMetrics( ColumnFormatMetrics aColumnFormatMetrics ) {
		final ColumnRecord theColumnRecord = currentColumnRecord();
		theColumnRecord.fromColumnFormatMetrics( aColumnFormatMetrics );
		_columnWidths = null;
		return this;
	}

	/**
	 * Sets the column's alignment mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * alignment mode being set wins.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withColumnHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		final ColumnRecord theColumnRecord = currentColumnRecord();
		theColumnRecord.setHeaderHorizAlignTextMode( aHorizAlignTextMode );
		theColumnRecord.setRowHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets the column's "more" mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * "more" mode being set wins.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for aligning the
	 *        text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withColumnMoreTextMode( MoreTextMode aMoreTextMode ) {
		final ColumnRecord theColumnRecord = currentColumnRecord();
		theColumnRecord.setHeaderMoreTextMode( aMoreTextMode );
		theColumnRecord.setRowMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets the column's line split mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * line split mode being set wins.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be used for aligning
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withColumnSplitTextMode( SplitTextMode aSplitTextMode ) {
		final ColumnRecord theColumnRecord = currentColumnRecord();
		theColumnRecord.setHeaderSplitTextMode( aSplitTextMode );
		theColumnRecord.setRowSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets the column's format mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * format mode being set wins.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be used for
	 *        formating the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withColumnTextFormatMode( TextFormatMode aTextFormatMode ) {
		final ColumnRecord theColumnRecord = currentColumnRecord();
		theColumnRecord.setHeaderTextFormatMode( aTextFormatMode );
		theColumnRecord.setRowTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * Sets the current column's (the last added one with the
	 * {@link #addColumn()} method) absolute width () @link
	 * ColumnWidthType#ABSOLUTE) and returns this {@link TableBuilder} instance
	 * as of the Builder-Pattern. Same as calling "withColumnWidth(int,
	 * ColumnWidthType.ABSOLUTE)}"
	 * 
	 * @param aWidth The width for the column.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 * 
	 * @throws IllegalStateException in case there is none column already added.
	 */
	public TableBuilder withColumnWidth( int aWidth ) {
		return withColumnWidth( aWidth, ColumnWidthType.ABSOLUTE );
	}

	/**
	 * Sets the current column's (the last added one with the
	 * {@link #addColumn()} method) width and returns this {@link TableBuilder}
	 * instance as of the Builder-Pattern.
	 * 
	 * @param aWidth The width for the column.
	 * @param aWidthType The type the width is of, e.g.
	 *        {@link ColumnWidthType#RELATIVE} or
	 *        {@link ColumnWidthType#ABSOLUTE}
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 * 
	 * @throws IllegalStateException in case there is none column already added.
	 */
	public TableBuilder withColumnWidth( int aWidth, ColumnWidthType aWidthType ) {
		currentColumnRecord().setColumnWidth( aWidth );
		currentColumnRecord().setColumnWidthType( aWidthType );
		_columnWidths = null;
		return this;
	}

	/**
	 * Sets the current column's (the last added one with the
	 * {@link #addColumn()} method) width and returns this {@link TableBuilder}
	 * instance as of the Builder-Pattern.
	 * 
	 * @param aColumnWidthMetrics The width and the {@link ColumnWidthType} (
	 *        {@link ColumnWidthType#RELATIVE} or
	 *        {@link ColumnWidthType#ABSOLUTE}) of the column.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 * 
	 * @throws IllegalStateException in case there is none column already added.
	 */
	public TableBuilder withColumnWidthMetrics( ColumnWidthMetrics aColumnWidthMetrics ) {
		currentColumnRecord().setColumnWidth( aColumnWidthMetrics.getColumnWidth() );
		currentColumnRecord().setColumnWidthType( aColumnWidthMetrics.getColumnWidthType() );
		_columnWidths = null;
		return this;
	}

	/**
	 * Sets an overall ANSI Escape-Code for the header, the rows as well as the
	 * borders; and returns this {@link TableBuilder} instance as of the builder
	 * pattern. In case an ANSI Escape-Codes is set, then an ANSI Reset-Code is
	 * prepended to the according text being printed. The latest ANSI escape
	 * code set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withEscapeCode( String aEscapeCode ) {
		withBorderEscapeCode( aEscapeCode );
		withTextEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCodesEnabled( boolean isEscCodesEnabled ) {
		_isEscCodesEnabled = isEscCodesEnabled;
	}

	/**
	 * Sets the column's header ANSI Escape-Code for the current header column
	 * and returns this {@link TableBuilder} instance as of the Builder-Pattern.
	 * The latest ANSI Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderColumnEscapeCode( String aEscapeCode ) {
		currentColumnRecord().setHeaderEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * Sets the column's header alignment mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest alignment mode being set wins.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderColumnHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		currentColumnRecord().setHeaderHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets the column's header "more" mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest "more" mode being set wins.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for aligning the
	 *        text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderColumnMoreTextMode( MoreTextMode aMoreTextMode ) {
		currentColumnRecord().setHeaderMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets the column's header line split mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest line split mode being set wins.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be used for aligning
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderColumnSplitTextMode( SplitTextMode aSplitTextMode ) {
		currentColumnRecord().setHeaderSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets the column's header format mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest format mode being set wins.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be used for
	 *        formating the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderColumnTextFormatMode( TextFormatMode aTextFormatMode ) {
		currentColumnRecord().setHeaderTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * Sets an overall ANSI Escape-Code for the header and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest ANSI
	 * Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderEscapeCode( String aEscapeCode ) {
		if ( aEscapeCode != null ) {
			if ( aEscapeCode.length() > 0 && !aEscapeCode.startsWith( AnsiEscapeCode.ESCAPE + "" ) ) {
				throw new IllegalArgumentException( "The provided ANSI Escape-Code does not start with \"" + AnsiEscapeCode.toEscapedSequence( AnsiEscapeCode.ESCAPE + "" ) + "\"." );
			}
			else if ( aEscapeCode.isEmpty() ) {
				throw new IllegalArgumentException( "Either use null to reset the ANSI Escape-Code or provide a valid ANSI Escape-Code with a length > 0." );
			}
		}
		_headerEscCode = aEscapeCode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setHeaderEscapeCode( aEscapeCode );
		}
		return this;
	}

	/**
	 * Sets an overall alignment mode for the header and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * alignment mode being set wins.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		currentColumnRecord().setHeaderHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets an overall "more" mode for the header and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * "more" mode being set wins.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for aligning the
	 *        text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderMoreTextMode( MoreTextMode aMoreTextMode ) {
		currentColumnRecord().setHeaderMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets an overall line split mode for the header and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest line
	 * split mode being set wins.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be used for aligning
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderSplitTextMode( SplitTextMode aSplitTextMode ) {
		currentColumnRecord().setHeaderSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets an overall format mode for the header and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * format mode being set wins.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be used for
	 *        formating the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHeaderTextFormatMode( TextFormatMode aTextFormatMode ) {
		currentColumnRecord().setHeaderTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * Sets an overall alignment mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * alignment mode being set wins.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_headerAlignMode = aHorizAlignTextMode;
		_rowHorizAlignTextMode = aHorizAlignTextMode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setHeaderHorizAlignTextMode( aHorizAlignTextMode );
			eRecord.setRowHorizAlignTextMode( aHorizAlignTextMode );
		}
		return this;
	}

	/**
	 * Sets whether to print the left border and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern.
	 * 
	 * @param hasLeftBorder True in case a left border is to be printed, elser
	 *        false
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withLeftBorder( boolean hasLeftBorder ) {
		_hasLeftBorder = hasLeftBorder;
		return this;
	}

	/**
	 * Sets the required line break and returns this {@link TableBuilder}
	 * instance as of the Builder-Pattern.
	 * 
	 * @param aLineBreak The line break character to use, by default the
	 *        platform specific one is used.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withLineBreak( String aLineBreak ) {
		_lineBreak = aLineBreak;
		return this;
	}

	/**
	 * Sets an overall "more" mode for the header and the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * "more" mode being set wins.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for aligning the
	 *        text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withMoreTextMode( MoreTextMode aMoreTextMode ) {
		_headerMoreMode = aMoreTextMode;
		_rowMoreTextMode = aMoreTextMode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setHeaderMoreTextMode( aMoreTextMode );
			eRecord.setRowMoreTextMode( aMoreTextMode );
		}
		return this;
	}

	/**
	 * Sets the ANSI reset Escape-Code for this {@link TableBuilder} instance as
	 * of the Builder-Pattern. In case an ANSI Escape-Codes is set, then this
	 * ANSI Reset-Code is prepended to the according text being printed.
	 * 
	 * @param aEscapeCode The {@link String} to be used for resetting ANSI
	 *        escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	@Override
	public TableBuilder withResetEscapeCode( String aEscapeCode ) {
		_resetEscCode = aEscapeCode;
		return this;
	}

	/**
	 * Sets whether to print the right border and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern.
	 * 
	 * @param hasRightBorder True in case a right border is to be printed, elser
	 *        false
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRightBorder( boolean hasRightBorder ) {
		_hasRightBorder = hasRightBorder;
		return this;
	}

	/**
	 * Sets whether to print the divider line between the columns of a row and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern.
	 * 
	 * @param hasDividerLine True in case the divider lines are to be printed,
	 *        elser false
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withDividerLine( boolean hasDividerLine ) {
		_hasDividerLine = hasDividerLine;
		return this;
	}

	/**
	 * Sets the column's header ANSI Escape-Code for the current row column and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest ANSI Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowColumnEscapeCode( String aEscapeCode ) {
		currentColumnRecord().setRowEscapeCode( aEscapeCode );
		return this;
	}

	// -------------------------------------------------------------------------
	// PRINT METHODS:
	// -------------------------------------------------------------------------

	/**
	 * Sets the column's header alignment mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest alignment mode being set wins.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowColumnHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		currentColumnRecord().setRowHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets the column's header "more" mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest "more" mode being set wins.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for aligning the
	 *        text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowColumnMoreTextMode( MoreTextMode aMoreTextMode ) {
		currentColumnRecord().setRowMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets the column's header line split mode for the header and the rows and
	 * returns this {@link TableBuilder} instance as of the Builder-Pattern. The
	 * latest line split mode being set wins.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be used for aligning
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowColumnSplitTextMode( SplitTextMode aSplitTextMode ) {
		currentColumnRecord().setRowSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets the column's row format mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * format mode being set wins.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be used for
	 *        formating the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowColumnTextFormatMode( TextFormatMode aTextFormatMode ) {
		currentColumnRecord().setRowTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * Sets an overall ANSI Escape-Code for the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest ANSI
	 * Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowEscapeCode( String aEscapeCode ) {
		if ( aEscapeCode != null ) {
			if ( aEscapeCode.length() > 0 && !aEscapeCode.startsWith( AnsiEscapeCode.ESCAPE + "" ) ) {
				throw new IllegalArgumentException( "The provided ANSI Escape-Code does not start with \"" + AnsiEscapeCode.toEscapedSequence( AnsiEscapeCode.ESCAPE + "" ) + "\"." );
			}
			else if ( aEscapeCode.isEmpty() ) {
				throw new IllegalArgumentException( "Either use null to reset the ANSI Escape-Code or provide a valid ANSI Escape-Code with a length > 0." );
			}
		}
		_rowEscCode = aEscapeCode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setRowEscapeCode( aEscapeCode );
		}
		return this;
	}

	/**
	 * Sets an overall alignment mode for the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * alignment mode being set wins.
	 * 
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} to be used for
	 *        aligning the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_rowHorizAlignTextMode = aHorizAlignTextMode;
		// currentColumnRecord().setRowHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets an overall "more" mode for the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * "more" mode being set wins.
	 * 
	 * @param aMoreTextMode The {@link MoreTextMode} to be used for aligning the
	 *        text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowMoreTextMode( MoreTextMode aMoreTextMode ) {
		_rowMoreTextMode = aMoreTextMode;
		// currentColumnRecord().setRowMoreTextMode( aMoreTextMode );
		return this;
	}

	/**
	 * Sets an overall line split mode for the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest line
	 * split mode being set wins.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be used for aligning
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowSplitTextMode( SplitTextMode aSplitTextMode ) {
		_rowSplitTextMode = aSplitTextMode;
		// currentColumnRecord().setRowSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Sets an overall format mode for the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * format mode being set wins.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be used for
	 *        formating the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withRowTextFormatMode( TextFormatMode aTextFormatMode ) {
		_rowTextFormatMode = aTextFormatMode;
		// currentColumnRecord().setRowTextFormatMode( aTextFormatMode );
		return this;
	}

	/**
	 * In case a value of -1 is provided, then the width is calculated
	 * dynamically from the individual columns' widths added together including
	 * any borders being specified. {@inheritDoc}
	 */
	@Override
	public TableBuilder withRowWidth( int aRowWidth ) {
		setRowWidth( aRowWidth );
		return this;
	}

	/**
	 * Sets an overall line split mode for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * line split mode being set wins.
	 * 
	 * @param aSplitTextMode The {@link SplitTextMode} to be used for aligning
	 *        the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withSplitTextMode( SplitTextMode aSplitTextMode ) {
		_headerSplitMode = aSplitTextMode;
		_rowSplitTextMode = aSplitTextMode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setHeaderSplitTextMode( aSplitTextMode );
			eRecord.setRowSplitTextMode( aSplitTextMode );
		}
		return this;
	}

	/**
	 * Sets the {@link TableStyle} to use when printing and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern.
	 * 
	 * @param aTableStyle The {@link TableStyle} defining the tables style to be
	 *        used when printing the table borders.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withTableStyle( TableStyle aTableStyle ) {
		setTableStyle( aTableStyle );
		return this;
	}

	/**
	 * Sets the {@link TableStyle} to use when printing.
	 * 
	 * @param aTableStyle The {@link TableStyle} defining the tables style to be
	 *        used when printing the table borders.
	 */
	public void setTableStyle( TableStyle aTableStyle ) {
		_tableStyle = aTableStyle;
	}

	/**
	 * Sets the column's ANSI Escape-Code for the current (header and row)
	 * column and returns this {@link TableBuilder} instance as of the builder
	 * pattern. The latest ANSI Escape-Code being set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withTextColumnEscapeCode( String aEscapeCode ) {
		final ColumnRecord theColumnRecord = currentColumnRecord();
		theColumnRecord.setHeaderEscapeCode( aEscapeCode );
		theColumnRecord.setRowEscapeCode( aEscapeCode );
		return this;
	}

	/**
	 * Sets an overall ANSI Escape-Code for the header and the rows and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. In case an
	 * ANSI Escape-Codes is set, then the ANSI Escape-Code is prepended and an
	 * ANSI Reset-Code is appended to the according text being printed. The
	 * latest ANSI Escape-Code set wins.
	 * 
	 * @param aEscapeCode The {@link String} to be used for ANSI escaping.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withTextEscapeCode( String aEscapeCode ) {
		if ( aEscapeCode != null ) {
			if ( aEscapeCode.length() > 0 && !aEscapeCode.startsWith( AnsiEscapeCode.ESCAPE + "" ) ) {
				throw new IllegalArgumentException( "The provided ANSI Escape-Code does not start with \"" + AnsiEscapeCode.toEscapedSequence( AnsiEscapeCode.ESCAPE + "" ) + "\"." );
			}
			else if ( aEscapeCode.isEmpty() ) {
				throw new IllegalArgumentException( "Either use null to reset the ANSI Escape-Code or provide a valid ANSI Escape-Code with a length > 0." );
			}
		}
		_headerEscCode = aEscapeCode;
		_rowEscCode = aEscapeCode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setHeaderEscapeCode( aEscapeCode );
			eRecord.setRowEscapeCode( aEscapeCode );
		}
		return this;
	}

	/**
	 * Sets an overall format mode for the header and the rows and returns this
	 * {@link TableBuilder} instance as of the Builder-Pattern. The latest
	 * format mode being set wins.
	 * 
	 * @param aTextFormatMode The {@link TextFormatMode} to be used for
	 *        formating the text.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	public TableBuilder withTextFormatMode( TextFormatMode aTextFormatMode ) {
		_headerFormatMode = aTextFormatMode;
		_rowTextFormatMode = aTextFormatMode;
		for ( ColumnRecord eRecord : _columnRecords ) {
			eRecord.setHeaderTextFormatMode( aTextFormatMode );
			eRecord.setRowTextFormatMode( aTextFormatMode );
		}
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHeader( String... aColumns ) {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toHeader( aColumns ) );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHeaderBegin() {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toHeaderBegin() );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHeaderContinue( String... aColumns ) {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toHeaderContinue( aColumns ) );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHeaderEnd() {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toHeaderEnd() );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHeaderEnd( TableBuilder aTablePrinter ) {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toHeaderEnd( aTablePrinter ) );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printRow( String... aColumns ) {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toRow( aColumns ) );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printRowBegin() {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toRowBegin() );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printRowContinue( String... aColumns ) {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toRowContinue( aColumns ) );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printRowEnd( TableBuilder aTablePrinter ) {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toRowEnd( aTablePrinter ) );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printTail() {
		synchronized ( getPrintStream() ) {
			getPrintStream().print( toTail() );
			getPrintStream().flush();
		}
	}

	/**
	 * {@inheritDoc} Sets an overall ANSI Escape-Code support for the
	 * {@link TableBuilder}, for the rows as well as the borders; and returns
	 * this {@link TableBuilder} instance as of the Builder-Pattern. In case of
	 * being set to {@link Boolean#FALSE}, then an ANSI support is disabled
	 * altogether, else it is enabled (again, if already configured). The latest
	 * setting wins.
	 * 
	 * @param isEscCodesEnabled True to enable, false to disable Escape-Code
	 *        support altogether.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	@Override
	public TableBuilder withEscapeCodesEnabled( boolean isEscCodesEnabled ) {
		setEscapeCodesEnabled( isEscCodesEnabled );
		return this;
	}

	/**
	 * Sets the print stream to be used when using the print methods of the
	 * {@link TableBuilder} and returns this {@link TableBuilder} instance as of
	 * the Builder-Pattern.
	 * 
	 * @param aPrintStream The {@link PrintStream} to be used when printing out
	 *        the table.
	 * 
	 * @return This {@link TableBuilder} instance to continue configuration.
	 */
	@Override
	public TableBuilder withPrintStream( PrintStream aPrintStream ) {
		setPrintStream( aPrintStream );
		return this;
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @return an instance (using a default implementation) of this builder
	 */
	static TableBuilder build() {
		return new TableBuilder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void appendEscapeBorder( StringBuilder aBuilder ) {
		if ( _isEscCodesEnabled ) {
			if ( _borderEscCode != null ) {
				aBuilder.append( _borderEscCode );
			}
		}
	}

	private void appendEscapeReset( StringBuilder aBuilder ) {
		if ( _isEscCodesEnabled ) {
			if ( _borderEscCode != null ) {
				aBuilder.append( _resetEscCode );
			}
		}
	}

	/**
	 * Same as {@link #printBottomBorder(int[], TableSection, StringBuilder)}
	 * without any ANSI Escape-Codes prepended and appended.
	 *
	 * @param aColumnWidths the column widths
	 * @param aTableSection the table section
	 * @param aBuilder the builder
	 */
	private void buildBottomBorder( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder ) {
		final TextBoxGrid theTableStyle = ( aTableSection == TableSection.HEADER ? _tableStyle.getHeader() : _tableStyle.getTail() );
		if ( _hasLeftBorder ) {
			aBuilder.append( theTableStyle.getBottomLeftEdge() );
		}
		for ( int i = 0; i < aColumnWidths.length; i++ ) {
			if ( i > 0 ) {
				aBuilder.append( theTableStyle.getBottomDividerEdge() );
			}
			aBuilder.append( new TextLineBuilder().withColumnWidth( aColumnWidths[i] ).withLineChar( theTableStyle.getBottomLine() ).toString() );
		}
		if ( _hasRightBorder ) {
			aBuilder.append( theTableStyle.getBottomRightEdge() );
		}
	}

	/**
	 * Same as {@link #printTopBorder(int[], TableSection, StringBuilder)}
	 * without any ANSI Escape-Codes prepended and appended.
	 *
	 * @param aColumnWidths the column widths
	 * @param aTableSection the table section
	 * @param aBuilder the builder
	 */
	private void buildTopBorder( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder ) {
		final TextBoxGrid theLineStyle = ( aTableSection == TableSection.HEADER ? _tableStyle.getHeader() : _tableStyle.getBody() );

		if ( _hasLeftBorder ) {
			if ( _tableStatus == TableStatus.NONE || _tableStatus == TableStatus.ROW_END ) {
				aBuilder.append( theLineStyle.getTopLeftEdge() );
			}
			else {
				aBuilder.append( theLineStyle.getLeftEdge() );
			}
		}
		for ( int i = 0; i < aColumnWidths.length; i++ ) {
			if ( i > 0 ) {
				if ( _tableStatus == TableStatus.NONE || _tableStatus == TableStatus.ROW_END ) {
					aBuilder.append( theLineStyle.getTopDividerEdge() );
				}
				else {
					aBuilder.append( theLineStyle.getDividerEdge() );
				}
			}
			aBuilder.append( new TextLineBuilder().withColumnWidth( aColumnWidths[i] ).withLineChar( theLineStyle.getTopLine() ).toString() );
		}
		if ( _hasRightBorder ) {
			if ( _tableStatus == TableStatus.NONE || _tableStatus == TableStatus.ROW_END ) {
				aBuilder.append( theLineStyle.getTopRightEdge() );
			}
			else {
				aBuilder.append( theLineStyle.getRightEdge() );
			}
		}
	}

	/**
	 * Retrieves the {@link ColumnRecord} currently being the one to be
	 * manipulated.
	 * 
	 * @return The current {@link ColumnRecord} being the one to be updated.
	 * 
	 * @throws IllegalStateException Thrown in case not one column has been
	 *         added with the {@link #addColumn()} method.
	 */
	private ColumnRecord currentColumnRecord() {
		int columnSize = _columnRecords.size();
		if ( columnSize == 0 ) {
			addColumn();
			columnSize++;
		}
		final ColumnRecord theColumnRecord = _columnRecords.get( columnSize - 1 );
		return theColumnRecord;
	}

	/**
	 * Joins this printer with the provided printer, this printer being the top
	 * printer which should end printing for the time being and the provided
	 * printer being the bottom printer which will continue printing.
	 *
	 * @param aTablePrinter The bottom {@link TableBuilder} which to join to the
	 *        {@link TableBuilder}.
	 * @param aTableSection The section of the table to operate on.
	 * @param aBuilder The {@link StringBuilder} to which to append the line.
	 */
	private void joinRow( TableBuilder aTablePrinter, TableSection aTableSection, StringBuilder aBuilder ) {
		_tableStatus = TableStatus.ROW_END;
		final TextBoxGrid theTableStyle = ( aTableSection == TableSection.HEADER ) ? _tableStyle.getHeader() : _tableStyle.getBody();
		final StringBuilder theTopBuilder = new StringBuilder();
		buildTopBorder( toColumnWidths(), aTableSection, theTopBuilder );
		final StringBuilder theBottomBuilder = new StringBuilder();
		buildTopBorder( aTablePrinter.toColumnWidths(), aTableSection, theBottomBuilder );

		// theTopBuilder.delete( theTopBuilder.length() - _lineBreak.length(),
		// theTopBuilder.length() );
		// theBottomBuilder.delete( theBottomBuilder.length() -
		// _lineBreak.length(), theBottomBuilder.length() );
		// ------------------------------------
		// Sort long printer and short printer:
		// ------------------------------------
		final StringBuilder theLongBuilder;
		final StringBuilder theShortBuilder;
		if ( theBottomBuilder.length() < theTopBuilder.length() ) {
			theLongBuilder = theTopBuilder;
			theShortBuilder = theBottomBuilder;
		}
		else {
			theShortBuilder = theTopBuilder;
			theLongBuilder = theBottomBuilder;
		}
		// --------------------------------------------------------------
		// Process all chars in short printer as well as in long printer:
		// --------------------------------------------------------------
		if ( _hasLeftBorder ) {
			theLongBuilder.replace( 0, 1, theTableStyle.getLeftEdge() + "" );
		}
		for ( int i = 1; i < theShortBuilder.length() - 1; i++ ) {
			if ( theShortBuilder.charAt( i ) != theLongBuilder.charAt( i ) ) {
				if ( theBottomBuilder.charAt( i ) == theTableStyle.getTopLine() ) {
					theLongBuilder.replace( i, i + 1, theTopBuilder.charAt( i ) + "" );
				}
				else if ( theTopBuilder.charAt( i ) == theTableStyle.getTopLine() ) {
					theLongBuilder.replace( i, i + 1, theTableStyle.getBottomDividerEdge() + "" );
				}
			}
			else {
				if ( theTopBuilder.charAt( i ) != theTableStyle.getTopLine() ) {
					theLongBuilder.replace( i, i + 1, theTableStyle.getDividerEdge() + "" );
				}
			}
		}
		// -----------------------------------------------------
		// Determine whether a printers are different in length:
		// -----------------------------------------------------
		if ( theShortBuilder.length() != theLongBuilder.length() ) {
			if ( theShortBuilder.charAt( theShortBuilder.length() - 1 ) != theLongBuilder.charAt( theShortBuilder.length() - 1 ) ) {
				if ( theShortBuilder == theTopBuilder ) {
					if ( theLongBuilder.charAt( theShortBuilder.length() - 1 ) == theTableStyle.getTopDividerEdge() ) {
						theLongBuilder.replace( theShortBuilder.length() - 1, theShortBuilder.length(), theTableStyle.getDividerEdge() + "" );
					}
					else {
						theLongBuilder.replace( theShortBuilder.length() - 1, theShortBuilder.length(), theTableStyle.getTopDividerEdge() + "" );
					}
					if ( _hasRightBorder ) {
						theLongBuilder.replace( theLongBuilder.length() - 1, theLongBuilder.length(), theTableStyle.getBottomRightEdge() + "" );
					}
				}
				else {
					if ( theLongBuilder.charAt( theShortBuilder.length() - 1 ) == theTableStyle.getTopDividerEdge() ) {
						theLongBuilder.replace( theShortBuilder.length() - 1, theShortBuilder.length(), theTableStyle.getDividerEdge() + "" );
					}
					else {
						theLongBuilder.replace( theShortBuilder.length() - 1, theShortBuilder.length(), theTableStyle.getBottomDividerEdge() + "" );
					}
				}
			}
			if ( theLongBuilder == theBottomBuilder ) {
				for ( int i = theShortBuilder.length(); i < theLongBuilder.length() - 1; i++ ) {
					if ( theLongBuilder.charAt( i ) == theTableStyle.getTopDividerEdge() ) {
						theLongBuilder.replace( i, i + 1, theTableStyle.getBottomDividerEdge() + "" );
					}
				}
			}
		}
		// ----------------------------------
		// The printers are of the same size:
		// ----------------------------------
		else {
			if ( _hasRightBorder ) {
				theLongBuilder.replace( theLongBuilder.length() - 1, theLongBuilder.length(), theTableStyle.getRightEdge() + "" );
			}
		}
		appendEscapeBorder( aBuilder );
		aBuilder.append( theLongBuilder.toString() );
		appendEscapeReset( aBuilder );
		aBuilder.append( _lineBreak );
	}

	/**
	 * Prints a bottom border without a line break with the given style.
	 * 
	 * @param aColumnWidths The columns widths to use.
	 * @param aTableSection The table section to print, either
	 *        {@link TableSection#HEADER} or {@link TableSection#TAIL}.
	 * @param aBuilder The {@link StringBuilder} to which to append the line.
	 */
	private void printBottomBorder( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder ) {
		appendEscapeBorder( aBuilder );
		buildBottomBorder( aColumnWidths, aTableSection, aBuilder );
		appendEscapeReset( aBuilder );
	}

	/**
	 * Prints a bottom border and a line break with the given style.
	 * 
	 * @param aColumnWidths The columns widths to use.
	 * @param aTableSection The table section to print, either
	 *        {@link TableSection#HEADER} or {@link TableSection#TAIL}.
	 * @param aBuilder The {@link StringBuilder} to which to append the line.
	 */
	private void printlnBottomBorder( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder ) {
		printBottomBorder( aColumnWidths, aTableSection, aBuilder );
		aBuilder.append( _lineBreak );
	}

	/**
	 * Prints a single line with the given style.
	 *
	 * @param aColumnWidths the column widths
	 * @param aTableSection The table section to print, either
	 *        {@link TableSection#HEADER} or {@link TableSection#BODY}.
	 * @param aBuilder The {@link StringBuilder} to which to append the line.
	 * @param aColumns The column texts to be printed.
	 */
	private void printlnLine( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder, String... aColumns ) {
		if ( _columnRecords.size() != aColumns.length ) {
			throw new IllegalStateException( "You have passed <" + aColumns.length + "> column text arguments, you have added <" + _columnRecords.size() + "> columns, though you must pass as many column text arguments as you have added columns. Hint: Maybe you modified a column's layout without adding a column beforehand; in such a case a column is added automatically which results in an unintended number of columns." );
		}
		final TextBoxGrid theLineStyle = ( aTableSection == TableSection.HEADER ? _tableStyle.getHeader() : _tableStyle.getBody() );
		String[][] theColumns = new String[aColumns.length][];
		for ( int i = 0; i < aColumns.length; i++ ) {
			// aColumns[i] = new MoreTextBuilderImpl().withText( aColumns[i] ).withColumnWidth( aColumnWidths[i] ).withMoreText( "" + MoreText.MORE_TEXT_BEFORE.getChar() ).withMoreTextMode( _columnRecords.get( i ).getMoreTextMode( aTableSection ) ).toString();
			aColumns[i] = MoreTextBuilder.asMoreText( aColumns[i], aColumnWidths[i], "" + MoreText.MORE_TEXT_BEFORE.getChar(), _columnRecords.get( i ).getMoreTextMode( aTableSection ), _isEscCodesEnabled );
			// theColumns[i] = new TextBlockBuilderImpl().withText( aColumns[i] ).withColumnWidth( aColumnWidths[i] ).withSplitTextMode( _columnRecords.get( i ).getSplitTextMode( aTableSection ) ).withHorizAlignTextMode( _columnRecords.get( i ).getHorizAlignTextMode( aTableSection ) ).toStrings();
			theColumns[i] = TextBlockBuilder.asTextBlock( aColumns[i], aColumnWidths[i], _columnRecords.get( i ).getHorizAlignTextMode( aTableSection ), _columnRecords.get( i ).getSplitTextMode( aTableSection ), ' ', _isEscCodesEnabled );

			if ( theColumns[i] == null ) {
				theColumns[i] = new String[] { "" };
			}
			for ( int j = 0; j < theColumns[i].length; j++ ) {
				theColumns[i][j] = HorizAlignTextBuilder.asAligned( theColumns[i][j], aColumnWidths[i], ' ', _columnRecords.get( i ).getHorizAlignTextMode( aTableSection ), _isEscCodesEnabled );
			}
		}
		theColumns = toColumns( theColumns, VertAlignTextMode.TOP );
		for ( int i = 0; i < theColumns.length; i++ ) {
			theColumns[i] = toEscapeColumn( _columnRecords.get( i ), aTableSection, theColumns[i], aColumns[i] );
		}
		for ( int i = 0; i < theColumns[0].length; i++ ) {
			if ( _hasLeftBorder ) {
				appendEscapeBorder( aBuilder );
				aBuilder.append( theLineStyle.getLeftLine() );
			}
			appendEscapeReset( aBuilder );

			for ( int j = 0; j < theColumns.length; j++ ) {
				if ( j > 0 ) {
					if ( _hasDividerLine ) {
						appendEscapeBorder( aBuilder );
						aBuilder.append( theLineStyle.getDividerLine() );
					}
					appendEscapeReset( aBuilder );
				}
				aBuilder.append( theColumns[j][i] );
			}

			if ( _hasRightBorder ) {
				appendEscapeBorder( aBuilder );
				aBuilder.append( theLineStyle.getRightLine() );
			}
			appendEscapeReset( aBuilder );

			aBuilder.append( _lineBreak );
		}
	}

	/**
	 * Prints a top border and a line break with the given style.
	 * 
	 * @param aColumnWidths The column widths to use when printing.
	 * @param aTableSection The table section to print, either
	 *        {@link TableSection#HEADER} or {@link TableSection#BODY}.
	 * @param aBuilder The {@link StringBuilder} to which to append the line.
	 */
	private void printlnTopBorder( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder ) {
		printTopBorder( aColumnWidths, aTableSection, aBuilder );
		aBuilder.append( _lineBreak );
	}

	/**
	 * Prints a top border without a line break with the given style.
	 * 
	 * @param aColumnWidths The column widths to use when printing.
	 * @param aTableSection The table section to print, either
	 *        {@link TableSection#HEADER} or {@link TableSection#BODY}.
	 * @param aBuilder The {@link StringBuilder} to which to append the line.
	 */
	private void printTopBorder( int[] aColumnWidths, TableSection aTableSection, StringBuilder aBuilder ) {
		appendEscapeBorder( aBuilder );
		buildTopBorder( aColumnWidths, aTableSection, aBuilder );
		appendEscapeReset( aBuilder );
	}

	private String[] toEscapeColumn( ColumnRecord aColumnRecord, TableSection aTableSection, String[] aColumns, String aIdentifier ) {
		if ( _isEscCodesEnabled ) {
			final String theEscCode = aColumnRecord.toEscapeCode( aTableSection, aIdentifier );
			if ( theEscCode != null ) {
				for ( int i = 0; i < aColumns.length; i++ ) {
					if ( aColumnRecord.getTextFormatMode( aTableSection ) == TextFormatMode.TEXT ) {

						// ESC-Code |-->
						if ( aColumns[i].length() > 0 ) {
							if ( aColumns[i].charAt( 0 ) != ' ' ) {
								aColumns[i] = theEscCode + aColumns[i];
							}
							else {
								for ( int j = 0; j < aColumns[i].length(); j++ ) {
									if ( aColumns[i].charAt( j ) != ' ' ) {
										aColumns[i] = aColumns[i].substring( 0, j ) + theEscCode + aColumns[i].substring( j );
										break;
									}
								}
							}
						}
						// <--| ESC-Code

						// ESC-Reset |-->
						if ( aColumns[i].charAt( aColumns[i].length() - 1 ) != ' ' ) {
							aColumns[i] = aColumns[i] + _resetEscCode;
						}
						else {
							for ( int j = aColumns[i].length() - 1; j >= 0; j-- ) {
								if ( aColumns[i].charAt( j ) != ' ' ) {
									aColumns[i] = aColumns[i].substring( 0, j + 1 ) + _resetEscCode + aColumns[i].substring( j + 1 );
									break;
								}
							}
						}
						// <--| ESC-Reset
					}
					else if ( aColumnRecord.getTextFormatMode( aTableSection ) == TextFormatMode.CELL ) {
						aColumns[i] = theEscCode + aColumns[i] + _resetEscCode;
					}

				}
			}
		}
		return aColumns;
	}

	/**
	 * Returns the max length of the arrays passed to this method.
	 *
	 * @param <T> the generic type
	 * @param aObjects The arrays of which the length of the one with the most
	 *        elements is returned.
	 * 
	 * @return The max length of elements found in the provided arrays or -1 if
	 *         all the arrays were null.
	 */
	@SafeVarargs
	private static <T extends Object> int toMaxLength( T[]... aObjects ) {
		int theMaxLength = -1;
		for ( Object[] eArray : aObjects ) {
			if ( eArray != null && eArray.length > theMaxLength ) {
				theMaxLength = eArray.length;
			}
		}
		return theMaxLength;
	}

	/**
	 * Gets the column width according to the currently added columns.
	 * 
	 * @return The column widths;
	 */
	private void updateColumnWidths() {
		if ( _columnRecords.isEmpty() ) {
			return;
		}

		int theWidth;
		if ( _rowWidth <= 0 ) {
			theWidth = -1;
		}
		else {
			theWidth = _rowWidth - ( _columnRecords.size() + 1 );
			if ( !_hasLeftBorder ) {
				theWidth++;
			}
			if ( !_hasRightBorder ) {
				theWidth++;
			}
			if ( !_hasDividerLine ) {
				theWidth += _columnRecords.size() - 1;
			}
		}
		_columnWidths = toColumnWidths( theWidth, _columnRecords.toArray( new ColumnRecord[_columnRecords.size()] ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class ColumnRecord extends ColumnFormatMetricsImpl {

		public ColumnRecord() {
			super( 1, ColumnWidthType.RELATIVE );
			setHeaderHorizAlignTextMode( _headerAlignMode );
			setRowHorizAlignTextMode( _rowHorizAlignTextMode );
			setHeaderSplitTextMode( _headerSplitMode );
			setRowSplitTextMode( _rowSplitTextMode );
			setHeaderEscapeCode( _headerEscCode );
			setRowEscapeCode( _rowEscCode );
			setHeaderTextFormatMode( _headerFormatMode );
			setRowTextFormatMode( _rowTextFormatMode );
			setHeaderMoreTextMode( _headerMoreMode );
			setRowMoreTextMode( _rowMoreTextMode );
		}

		private HorizAlignTextMode getHorizAlignTextMode( TableSection aTableSection ) {
			if ( aTableSection == TableSection.HEADER ) {
				return getHeaderHorizAlignTextMode();
			}
			if ( aTableSection == TableSection.BODY ) {
				return getRowHorizAlignTextMode();
			}
			throw new RuntimeException( "Unknown table section <" + aTableSection + ">, allowed values are <" + TableSection.HEADER + "> and <" + TableSection.BODY + ">." );
		}

		private MoreTextMode getMoreTextMode( TableSection aTableSection ) {
			if ( aTableSection == TableSection.HEADER ) {
				return getHeaderMoreTextMode();
			}
			if ( aTableSection == TableSection.BODY ) {
				return getRowMoreTextMode();
			}
			throw new RuntimeException( "Unknown table section <" + aTableSection + ">, allowed values are <" + TableSection.HEADER + "> and <" + TableSection.BODY + ">." );
		}

		private SplitTextMode getSplitTextMode( TableSection aTableSection ) {
			if ( aTableSection == TableSection.HEADER ) {
				return getHeaderSplitTextMode();
			}
			if ( aTableSection == TableSection.BODY ) {
				return getRowSplitTextMode();
			}
			throw new RuntimeException( "Unknown table section <" + aTableSection + ">, allowed values are <" + TableSection.HEADER + "> and <" + TableSection.BODY + ">." );
		}

		private TextFormatMode getTextFormatMode( TableSection aTableSection ) {
			if ( aTableSection == TableSection.HEADER ) {
				return getHeaderTextFormatMode();
			}
			if ( aTableSection == TableSection.BODY ) {
				return getRowTextFormatMode();
			}
			throw new RuntimeException( "Unknown table section <" + aTableSection + ">, allowed values are <" + TableSection.HEADER + "> and <" + TableSection.BODY + ">." );
		}

		private String toEscapeCode( TableSection aTableSection, String aIdentifier ) {
			if ( aTableSection == TableSection.HEADER ) {
				return toHeaderEscapeCode( aIdentifier );
			}
			if ( aTableSection == TableSection.BODY ) {
				return toRowEscapeCode( aIdentifier );
			}
			throw new RuntimeException( "Unknown table section <" + aTableSection + ">, allowed values are <" + TableSection.HEADER + "> and <" + TableSection.BODY + ">." );
		}
	}
}
