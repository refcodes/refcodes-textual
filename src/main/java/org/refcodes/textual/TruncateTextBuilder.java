// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.CharSet;
import org.refcodes.mixin.TruncateMode;
import org.refcodes.mixin.TruncateModeAccessor.TruncateModeBuilder;
import org.refcodes.mixin.TruncateModeAccessor.TruncateModeProperty;

/**
 * Strips given characters from text's left hand side or right hand side or
 * both.
 */
public class TruncateTextBuilder extends AbstractText<TruncateTextBuilder> implements TruncateModeProperty, TruncateModeBuilder<TruncateTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private TruncateMode _truncateTextMode = TruncateMode.TAIL;

	private char[] _truncateChars = CharSet.WHITE_SPACES.getCharSet();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the truncate text mode from the truncate text mode property.
	 * 
	 * @return The truncate text mode stored by the truncate text mode property.
	 */
	@Override
	public TruncateMode getTruncateMode() {
		return _truncateTextMode;
	}

	/**
	 * Sets the truncate text mode for the truncate text mode property.
	 * 
	 * @param aTruncateTextMode The truncate text mode to be stored by the
	 *        truncate text mode property.
	 */
	@Override
	public void setTruncateMode( TruncateMode aTruncateTextMode ) {
		_truncateTextMode = aTruncateTextMode;
	}

	/**
	 * Retrieves the truncate chars from the truncate chars property.
	 * 
	 * @return The truncate chars stored by the truncate chars property.
	 */
	public char[] getTruncateChars() {
		return _truncateChars;
	}

	/**
	 * Sets the truncate chars for the truncate chars property.
	 * 
	 * @param aTruncateChars The truncate chars to be stored by the text align
	 *        mode property.
	 */
	public void setTruncateChars( char... aTruncateChars ) {
		_truncateChars = aTruncateChars;
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String} array
	 */
	@Override
	public String[] toStrings() {
		return toStrings( getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		final String[] theResult = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theResult[i] = asTruncated( aText[i], _truncateTextMode, _truncateChars );
		}
		return theResult;
	}

	/**
	 * Sets the truncate text mode for the truncate text mode property.
	 * 
	 * @param aTruncateTextMode The truncate text mode to be stored by the
	 *        truncate text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	@Override
	public TruncateTextBuilder withTruncateMode( TruncateMode aTruncateTextMode ) {
		setTruncateMode( aTruncateTextMode );
		return this;
	}

	/**
	 * Sets the {@link String} array for the text property.
	 * 
	 * @param aTextLines The {@link String} array be stored by the text
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	@Override
	public TruncateTextBuilder withText( String... aTextLines ) {
		setText( aTextLines );
		return this;
	}

	/**
	 * Sets the truncate chars for the truncate chars property.
	 * 
	 * @param aStripChars The truncate chars to be stored by the text align mode
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public TruncateTextBuilder withStripChars( char... aStripChars ) {
		setTruncateChars( aStripChars );
		return this;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String}
	 * 
	 * @throws IllegalStateException Thrown in case more than one text line has
	 *         been set via the {@link #withText(String...)} or
	 *         {@link #setText(String...)} methods.
	 */
	@Override
	public String toString() {
		return super.toString();
	}

	/**
	 * Returns a new {@link String} without the according leading and/or
	 * trailing whitespaces as of {@link CharSet#WHITE_SPACES}.
	 *
	 * @param aText The text to truncate.
	 * @param aTruncateTextMode The {@link TruncateMode} describes how to
	 *        truncate (left, right or both).
	 * 
	 * @return The accordingly truncated {@link String}.
	 */
	public static String asTruncated( String aText, TruncateMode aTruncateTextMode ) {
		return asTruncated( aText, aTruncateTextMode, CharSet.WHITE_SPACES.getCharSet() );
	}

	/**
	 * Returns a new {@link String} without the according leading and/or
	 * trailing chars.
	 *
	 * @param aText The text to truncate.
	 * @param aTruncateTextMode The {@link TruncateMode} describes how to
	 *        truncate (left, right or both).
	 * @param aChars the chars to detect when truncating.
	 * 
	 * @return The accordingly truncated {@link String}.
	 */
	public static String asTruncated( String aText, TruncateMode aTruncateTextMode, char... aChars ) {
		switch ( aTruncateTextMode ) {
		case HEAD -> {
			return asTruncatedLeft( aText, aChars );
		}
		case HEAD_AND_TAIL -> {
			return toStrip( aText, aChars );
		}
		case TAIL -> {
			return asTruncatedRight( aText, aChars );
		}
		}
		throw new IllegalArgumentException( "You must pass a valid text strip mode, though you actually passed <" + aTruncateTextMode + ">!" );
	}

	/**
	 * Returns a new {@link String} without the according leading whitespaces as
	 * of {@link CharSet#WHITE_SPACES}.
	 *
	 * @param aText The text to truncate.
	 * 
	 * @return The accordingly truncated {@link String}.
	 */
	public static String asTruncatedLeft( String aText ) {
		return asTruncatedRight( aText, CharSet.WHITE_SPACES.getCharSet() );
	}

	/**
	 * Returns a new {@link String} without the according leading chars.
	 *
	 * @param aText The text to be stripped.
	 * @param aChars the chars
	 * 
	 * @return The stripped text.
	 */
	public static String asTruncatedLeft( String aText, char... aChars ) {
		final StringBuilder theStringBuffer = new StringBuilder( aText );
		for ( char aChar : aChars ) {
			doStripLeft( theStringBuffer, aChar );
		}
		return theStringBuffer.toString();
	}

	/**
	 * Returns a new {@link String} without the according trailing whitespaces
	 * as of {@link CharSet#WHITE_SPACES}.
	 *
	 * @param aText The text to truncate.
	 * 
	 * @return The accordingly truncated {@link String}.
	 */
	public static String asTruncatedRight( String aText ) {
		return asTruncatedRight( aText, CharSet.WHITE_SPACES.getCharSet() );
	}

	/**
	 * Returns a new {@link String} without the according trailing chars.
	 *
	 * @param aText The text to truncate.
	 * @param aChars the chars to detect when truncating.
	 * 
	 * @return The accordingly truncated {@link String}.
	 */
	public static String asTruncatedRight( String aText, char... aChars ) {
		final StringBuilder theStringBuffer = new StringBuilder( aText );
		for ( char aChar : aChars ) {
			doStripRight( theStringBuffer, aChar );
		}
		return theStringBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Removes all leading chars identical with specified char. E.g. if
	 * str=AAABBBAAABBB and ch=A the returned {@link String} will be BBBAAABBB.
	 * 
	 * @param aStringBuffer StringBuffer to modify.
	 * @param aChar Char to remove.
	 */
	protected static void doStripLeft( StringBuilder aStringBuffer, char aChar ) {
		for ( int i = 0; i < aStringBuffer.length(); i++ ) {
			if ( aStringBuffer.charAt( i ) == aChar ) {
				aStringBuffer.deleteCharAt( i );
				i--;
			}
			else {
				break;
			}
		}
	}

	/**
	 * Removes all rear chars identical with specified char ch. E.g. if
	 * str=AAABBBAAABBB and ch=B str will become AAABBBAAA.
	 * 
	 * @param aStringBuffer StringBuffer to modify.
	 * @param aChar Char to remove.
	 */
	protected static void doStripRight( StringBuilder aStringBuffer, char aChar ) {
		for ( int i = aStringBuffer.length() - 1; i >= 0; i-- ) {
			if ( aStringBuffer.charAt( i ) == aChar ) {
				aStringBuffer.deleteCharAt( i );
			}
			else {
				break;
			}
		}
	}

	/**
	 * Removes all leading and trailing chars identical with specified char ch.
	 * E.g. if str=BAABBBAAABBB and ch=B str will become AABBBAAA.
	 * 
	 * @param aStringBuffer StringBuffer to modify.
	 * @param aChar Char to remove.
	 */
	protected static void doStrip( StringBuilder aStringBuffer, char aChar ) {
		doStripLeft( aStringBuffer, aChar );
		doStripRight( aStringBuffer, aChar );
	}

	/**
	 * Returns a new String without leading nor trailing chars.
	 *
	 * @param aText The text to be stripped.
	 * @param aChars the chars
	 * 
	 * @return The stripped text.
	 */
	protected static String toStrip( String aText, char[] aChars ) {
		return asTruncatedRight( asTruncatedLeft( aText, aChars ), aChars );
	}
}
