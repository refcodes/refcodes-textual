// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a more-text mode property.
 */
public interface MoreTextModeAccessor {

	/**
	 * Retrieves the more-text mode from the more-text mode property.
	 * 
	 * @return The more-text mode stored by the more-text mode property.
	 */
	MoreTextMode getMoreTextMode();

	/**
	 * Provides a mutator for a more-text mode property.
	 */
	public interface MoreTextModeMutator {

		/**
		 * Sets the more-text mode for the more-text mode property.
		 * 
		 * @param aMoreTextMode The more-text mode to be stored by the more-text
		 *        mode property.
		 */
		void setMoreTextMode( MoreTextMode aMoreTextMode );
	}

	/**
	 * Provides a builder method for a more-text mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MoreTextModeBuilder<B extends MoreTextModeBuilder<B>> {

		/**
		 * Sets the more-text mode for the more-text mode property.
		 * 
		 * @param aMoreTextMode The more-text mode to be stored by the more-text
		 *        mode property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMoreTextMode( MoreTextMode aMoreTextMode );
	}

	/**
	 * Provides a more-text mode property.
	 */
	public interface MoreTextModeProperty extends MoreTextModeAccessor, MoreTextModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link MoreTextMode}
		 * (setter) as of {@link #setMoreTextMode(MoreTextMode)} and returns the
		 * very same value (getter).
		 * 
		 * @param aMoreTextMode The {@link MoreTextMode} to set (via
		 *        {@link #setMoreTextMode(MoreTextMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default MoreTextMode letMoreTextMode( MoreTextMode aMoreTextMode ) {
			setMoreTextMode( aMoreTextMode );
			return aMoreTextMode;
		}
	}
}
