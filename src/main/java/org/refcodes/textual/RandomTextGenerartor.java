// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.refcodes.generator.Generator;
import org.refcodes.mixin.CharSetAccessor.CharSetBuilder;
import org.refcodes.mixin.CharSetAccessor.CharSetProperty;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;

/**
 * Generates random text according to the {@link RandomTextMode} property and
 * other settings.
 */
public class RandomTextGenerartor implements ColumnWidthBuilder<RandomTextGenerartor>, ColumnWidthProperty, Generator<String>, CharSetProperty, CharSetBuilder<RandomTextGenerartor> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Random RND = new Random();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _columnWidth = 64;
	private RandomTextMode _randomTextMode = RandomTextMode.ASCII;
	private char[] _charSet = null;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RandomTextGenerartor withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String next() {
		if ( _charSet != null ) {
			return asRandom( _charSet, _columnWidth, RND );
		}
		return asRandom( _randomTextMode.getCharSet(), _columnWidth, RND );
	}

	/**
	 * Retrieves the random text mode from the random text mode property.
	 * 
	 * @return The random text mode stored by the random text mode property.
	 */
	public RandomTextMode getRandomTextMode() {
		return _randomTextMode;
	}

	/**
	 * Sets the random text mode for the random text mode property.
	 * 
	 * @param aRandomTextMode The random text mode to be stored by the random
	 *        text mode property.
	 */
	public void setRandomTextMode( RandomTextMode aRandomTextMode ) {
		_randomTextMode = aRandomTextMode;
		if ( aRandomTextMode != null ) {
			_charSet = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getCharSet() {
		return _charSet;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCharSet( char[] aCharSet ) {
		_charSet = aCharSet;
		if ( aCharSet != null ) {
			_randomTextMode = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RandomTextGenerartor withCharSet( char[] aCharSet ) {
		setCharSet( aCharSet );
		return this;
	}

	/**
	 * Sets the random text mode for the random text mode property.
	 * 
	 * @param aRandomTextMode The random text mode to be stored by the random
	 *        text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public RandomTextGenerartor withRandomTextMode( RandomTextMode aRandomTextMode ) {
		setRandomTextMode( aRandomTextMode );
		return this;
	}

	/**
	 * Creates a random text from the given parameters.
	 * 
	 * @param aColumnWidth The length of the text to generate.
	 * @param aRandomTextMode The chars to use for the random text.
	 * 
	 * @return The accordingly created random text.
	 */
	public static String asString( int aColumnWidth, RandomTextMode aRandomTextMode ) {
		return asRandom( aRandomTextMode.getCharSet(), aColumnWidth, RND );
	}

	/**
	 * Creates a random text with the given length consisting of the characters
	 * found in {@link RandomTextMode#ALPHANUMERIC}.
	 * 
	 * @param aColumnWidth The length of the text to generate.
	 * 
	 * @return The accordingly created random text.
	 */
	public static String asString( int aColumnWidth ) {
		return asRandom( RandomTextMode.ALPHANUMERIC.getCharSet(), aColumnWidth, RND );
	}

	/**
	 * Creates a random text from the given parameters.
	 * 
	 * @param aColumnWidth The length of the text to generate.
	 * @param aCharSet The chars to use for the random text.
	 * 
	 * @return The accordingly created random text.
	 */
	public static String asRandom( int aColumnWidth, char... aCharSet ) {
		return asRandom( aCharSet, aColumnWidth, RND );
	}

	/**
	 * Shuffles the provided characters to generate a text with the length of
	 * the number of provided characters containing each provided character
	 * though in a random order.
	 * 
	 * @param aCharSet The chars to use for the shuffled text.
	 * 
	 * @return The accordingly shuffled random text.
	 */
	public static String asShuffled( char... aCharSet ) {
		final List<Character> theChars = new ArrayList<>();
		for ( char eChar : aCharSet ) {
			theChars.add( eChar );
		}
		Collections.shuffle( theChars );
		final StringBuilder theBuilder = new StringBuilder();
		for ( char eChar : theChars ) {
			theBuilder.append( eChar );
		}
		return theBuilder.toString();
	}

	/**
	 * Shuffles the provided characters to generate a text with the length of
	 * the number of provided characters containing each provided character
	 * though in a random order.
	 * 
	 * @param aCharSet The chars to use for the shuffled text.
	 * 
	 * @return The accordingly shuffled random text.
	 */
	public static String asShuffled( String aCharSet ) {
		return asShuffled( aCharSet.toCharArray() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To random.
	 *
	 * @param aCharSet the char set
	 * @param aColumnWidth the column width
	 * @param aRndGenerator the rnd generator
	 * 
	 * @return the string
	 */
	private static String asRandom( char[] aCharSet, int aColumnWidth, Random aRndGenerator ) {
		final int theBound = aCharSet.length;
		final char[] theRandom = new char[aColumnWidth];
		for ( int i = 0; i < aColumnWidth; i++ ) {
			theRandom[i] = aCharSet[aRndGenerator.nextInt( theBound )];
		}
		return new String( theRandom );
	}
}
