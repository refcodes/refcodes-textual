// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.mixin.FamilyAccessor.FamilyBuilder;
import org.refcodes.mixin.FamilyAccessor.FamilyProperty;
import org.refcodes.mixin.NameAccessor.NameBuilder;
import org.refcodes.mixin.NameAccessor.NameProperty;
import org.refcodes.mixin.SizeAccessor.SizeBuilder;
import org.refcodes.mixin.SizeAccessor.SizeProperty;
import org.refcodes.mixin.StyleAccessor.StyleBuilder;
import org.refcodes.mixin.StyleAccessor.StyleProperty;

/**
 * The {@link Font} describes a more generic font.
 */
public class Font implements FamilyProperty<FontFamily>, FamilyBuilder<FontFamily, Font>, StyleProperty<FontStyle>, StyleBuilder<FontStyle, Font>, SizeProperty, SizeBuilder<Font>, NameProperty, NameBuilder<Font> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private FontFamily _family = FontFamily.SANS_SERIF;
	private int _size = 16;
	private FontStyle _style = FontStyle.PLAIN;
	private String _name = _family.getName();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new font impl.
	 */
	public Font() {}

	/**
	 * Instantiates a new font from a {@link java.awt.Font}.
	 * 
	 * @param aFont The {@link java.awt.Font} to use.
	 */
	public Font( java.awt.Font aFont ) {
		setSize( aFont.getSize() );
		setStyle( FontStyle.fromFontStyleCode( aFont.getStyle() ) );
		setFamily( FontFamily.fromName( aFont.getFamily() ) );

	}

	/**
	 * Instantiates a new font impl.
	 *
	 * @param aFontType the font type
	 * @param aFontStyle the font style
	 * @param aFontSize the font size
	 */
	public Font( FontFamily aFontType, FontStyle aFontStyle, int aFontSize ) {
		setFamily( aFontType );
		_style = aFontStyle;
		_size = aFontSize;
	}

	/**
	 * Instantiates a new font impl.
	 *
	 * @param aFontType the font type
	 * @param aFontStyle the font style
	 */
	public Font( FontFamily aFontType, FontStyle aFontStyle ) {
		this( aFontType, aFontStyle, -1 );
	}

	/**
	 * Instantiates a new font impl.
	 *
	 * @param aFontName the font name
	 * @param aFontStyle the font style
	 * @param aFontSize the font size
	 */
	public Font( String aFontName, FontStyle aFontStyle, int aFontSize ) {
		setName( aFontName );
		_style = aFontStyle;
		_size = aFontSize;
	}

	/**
	 * Instantiates a new font impl.
	 *
	 * @param aFontName the font name
	 * @param aFontStyle the font style
	 */
	public Font( String aFontName, FontStyle aFontStyle ) {
		this( aFontName, aFontStyle, -1 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the according {@link java.awt.Font}.
	 * 
	 * @return The according {@link java.awt.Font}.
	 */
	public java.awt.Font toAwtFont() {
		return new java.awt.Font( getName(), getStyle().getCode(), getSize() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FontFamily getFamily() {
		return _family;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFamily( FontFamily aFontCategory ) {
		_family = aFontCategory;
		_name = aFontCategory.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Font withFamily( FontFamily aFontName ) {
		setFamily( aFontName );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FontStyle getStyle() {
		return _style;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStyle( FontStyle aFontStyle ) {
		_style = aFontStyle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Font withStyle( FontStyle aFontStyle ) {
		setStyle( aFontStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getSize() {
		return _size;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSize( int aFontSize ) {
		_size = aFontSize;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Font withSize( int aFontSize ) {
		setSize( aFontSize );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + _family.getName() + ", " + _style + ", " + _size + ")@" + hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setName( String aFontName ) {
		_name = aFontName;
		_family = FontFamily.fromName( aFontName );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Font withName( String aFontName ) {
		setName( aFontName );
		return this;
	}
}
