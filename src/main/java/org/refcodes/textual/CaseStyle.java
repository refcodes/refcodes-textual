// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Predefined {@link CaseStyle} "implementations" (in terms of methods such as
 * {@link #toCaseStyle(String)}) making use of the {@link CaseStyleBuilder}.
 */
public enum CaseStyle {

	// @formatter:off
	NONE( null ),
	
	CAMEL_CASE(new CaseStyleBuilder() ),
	
	PASCAL_CASE(new CaseStyleBuilder() ),
	
	SNAKE_CASE(new CaseStyleBuilder().withCase( Case.LOWER ) ),
	
	KEBAB_CASE(new CaseStyleBuilder().withCase( Case.LOWER ) ),
	
	SNAKE_UPPER_CASE(new CaseStyleBuilder().withCase( Case.UPPER ) ),
	
	KEBAB_UPPER_CASE(new CaseStyleBuilder().withCase( Case.UPPER ) );
	// @formatter:on

	private CaseStyleBuilder _caseStyleBuilder;

	/**
	 * Instantiates a new case style.
	 *
	 * @param aCaseStyleBuilder the case style builder
	 */
	private CaseStyle( CaseStyleBuilder aCaseStyleBuilder ) {
		_caseStyleBuilder = aCaseStyleBuilder;
	}

	/**
	 * Converts the given text into the according {@link CaseStyle}.
	 *
	 * @param aText The text to be converted to the according case style.
	 * 
	 * @return the string
	 */
	public String toCaseStyle( String aText ) {
		switch ( this ) {
		case CAMEL_CASE:
			return _caseStyleBuilder.toCamelCase( aText );
		case KEBAB_CASE:
			return _caseStyleBuilder.toKebabCase( aText );
		case KEBAB_UPPER_CASE:
			return _caseStyleBuilder.toKebabCase( aText );
		case PASCAL_CASE:
			return _caseStyleBuilder.toPascalCase( aText );
		case SNAKE_CASE:
			return _caseStyleBuilder.toSnakeCase( aText );
		case SNAKE_UPPER_CASE:
			return _caseStyleBuilder.toSnakeCase( aText );
		case NONE:
		default:
			return aText;
		}
	}
}
