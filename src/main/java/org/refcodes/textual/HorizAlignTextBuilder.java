// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;
import org.refcodes.runtime.Terminal;
import org.refcodes.textual.FillCharAccessor.FillCharBuilder;
import org.refcodes.textual.FillCharAccessor.FillCharProperty;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeBuilder;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeProperty;

/**
 * Fills a text up on by appending the given char to the left or to the the
 * right or inbetween till the given length is reached. How the text is aligned
 * depends on the setting of the {@link HorizAlignTextMode} attribute.
 */
public class HorizAlignTextBuilder extends AbstractText<HorizAlignTextBuilder> implements FillCharProperty, FillCharBuilder<HorizAlignTextBuilder>, ColumnWidthBuilder<HorizAlignTextBuilder>, ColumnWidthProperty, HorizAlignTextModeProperty, HorizAlignTextModeBuilder<HorizAlignTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _columnWidth = Terminal.toHeuristicWidth();

	private HorizAlignTextMode _alignTextMode = HorizAlignTextMode.LEFT;

	private char _fillChar = ' ';

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextBuilder withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_alignTextMode = aHorizAlignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getHorizAlignTextMode() {
		return _alignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getFillChar() {
		return _fillChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFillChar( char aFillChar ) {
		_fillChar = aFillChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return asAligned( getText(), _columnWidth, _fillChar, _alignTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return asAligned( aText, _columnWidth, _fillChar, _alignTextMode );
	}

	/**
	 * Sets the align text mode for the align text mode property.
	 * 
	 * @param aHorizAlignTextMode The align text mode to be stored by the align
	 *        text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	@Override
	public HorizAlignTextBuilder withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextBuilder withFillChar( char aFillChar ) {
		setFillChar( aFillChar );
		return this;
	}

	/**
	 * Fills a text up as of the provided {@link HorizAlignTextMode} with a
	 * space " " till the given length is reached. It is assumed that the text
	 * does not contain any ANSI escape codes, else use the according method
	 * with the ANSI escape code flag.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aHorizAlignTextMode the horiz align text mode
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asAligned( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode ) {
		return asAligned( aText, aColumnWidth, ' ', aHorizAlignTextMode, false );
	}

	/**
	 * Fills a text up as of the provided {@link HorizAlignTextMode} with the
	 * give fill character till the given length is reached. It is assumed that
	 * the text does not contain any ANSI escape codes, else use the according
	 * method with the ANSI escape code flag.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param aHorizAlignTextMode the horiz align text mode
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asAligned( String aText, int aColumnWidth, char aFillChar, HorizAlignTextMode aHorizAlignTextMode ) {
		return asAligned( aText, aColumnWidth, aFillChar, aHorizAlignTextMode, false );
	}

	/**
	 * Fills a text up as of the provided {@link HorizAlignTextMode} with the
	 * give fill character till the given length is reached. It is assumed that
	 * the text does not contain any ANSI escape codes, else use the according
	 * method with the ANSI escape code flag.
	 *
	 * @param aText The text array to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param aHorizAlignTextMode the horiz align text mode
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String[] asAligned( String aText[], int aColumnWidth, char aFillChar, HorizAlignTextMode aHorizAlignTextMode ) {
		return asAligned( aText, aColumnWidth, aFillChar, aHorizAlignTextMode, false );
	}

	/**
	 * Fills a text up as of the provided {@link HorizAlignTextMode} with a
	 * space " " character till the given length is reached.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asAligned( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, boolean hasAnsiEscapeCodes ) {
		return asAligned( aText, aColumnWidth, ' ', aHorizAlignTextMode, hasAnsiEscapeCodes );
	}

	/**
	 * Fills a text up as of the provided {@link HorizAlignTextMode} with the
	 * give fill character till the given length is reached.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asAligned( String aText, int aColumnWidth, char aFillChar, HorizAlignTextMode aHorizAlignTextMode, boolean hasAnsiEscapeCodes ) {
		if ( aHorizAlignTextMode == null ) {
			return aText;
		}
		switch ( aHorizAlignTextMode ) {
		case BLOCK -> {
			return toAlignBlock( aText, aColumnWidth, aFillChar, hasAnsiEscapeCodes );
		}
		case CENTER -> {
			return toAlignCenter( aText, aColumnWidth, aFillChar, hasAnsiEscapeCodes );
		}
		case LEFT -> {
			return toAlignLeft( aText, aColumnWidth, aFillChar, hasAnsiEscapeCodes );
		}
		case RIGHT -> {
			return toAlignRight( aText, aColumnWidth, aFillChar, hasAnsiEscapeCodes );
		}
		}
		throw new IllegalArgumentException( "You must provide a supported value for the horizontal align mode, though you provided <" + aHorizAlignTextMode + ">. The code seems to be out of date, please participate in getting the code even better. See <http://www.refcodes.org> and <https://birbucket.org/refcodes>." );
	}

	/**
	 * Fills a text up as of the provided {@link HorizAlignTextMode} with the
	 * give fill character till the given length is reached.
	 *
	 * @param aText The text array to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String[] asAligned( String aText[], int aColumnWidth, char aFillChar, HorizAlignTextMode aHorizAlignTextMode, boolean hasAnsiEscapeCodes ) {
		final String[] theResult = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theResult[i] = asAligned( aText[i], aColumnWidth, aFillChar, aHorizAlignTextMode, hasAnsiEscapeCodes );
		}
		return theResult;
	}

	/**
	 * Fills a left aligned text with a space " " till the given length is
	 * reached. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asLeftAligned( String aText, int aColumnWidth ) {
		return asLeftAligned( aText, aColumnWidth, ' ', false );
	}

	/**
	 * Fills a left aligned text with the give fill character till the given
	 * length is reached. It is assumed that the text does not contain any ANSI
	 * escape codes, else use the according method with the ANSI escape code
	 * flag.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asLeftAligned( String aText, int aColumnWidth, char aFillChar ) {
		return asLeftAligned( aText, aColumnWidth, aFillChar, false );
	}

	/**
	 * Fills a left aligned text with the give fill character till the given
	 * length is reached. It is assumed that the text does not contain any ANSI
	 * escape codes, else use the according method with the ANSI escape code
	 * flag.
	 *
	 * @param aText The text array to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String[] asLeftAligned( String aText[], int aColumnWidth, char aFillChar ) {
		return asLeftAligned( aText, aColumnWidth, aFillChar, false );
	}

	/**
	 * Fills a left aligned text with a space " " character till the given
	 * length is reached.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * 
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asLeftAligned( String aText, int aColumnWidth, boolean hasAnsiEscapeCodes ) {
		return asLeftAligned( aText, aColumnWidth, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Fills a left aligned text with the give fill character till the given
	 * length is reached.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asLeftAligned( String aText, int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		return asAligned( aText, aColumnWidth, aFillChar, HorizAlignTextMode.LEFT, hasAnsiEscapeCodes );
	}

	/**
	 * Fills a left aligned text with the give fill character till the given
	 * length is reached.
	 *
	 * @param aText The text array to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String[] asLeftAligned( String aText[], int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		return asAligned( aText, aColumnWidth, aFillChar, HorizAlignTextMode.LEFT, hasAnsiEscapeCodes );
	}

	/**
	 * Fills a right aligned text with a space " " till the given length is
	 * reached. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asRightAligned( String aText, int aColumnWidth ) {
		return asRightAligned( aText, aColumnWidth, ' ', false );
	}

	/**
	 * Fills a right aligned text with the give fill character till the given
	 * length is reached. It is assumed that the text does not contain any ANSI
	 * escape codes, else use the according method with the ANSI escape code
	 * flag.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asRightAligned( String aText, int aColumnWidth, char aFillChar ) {
		return asRightAligned( aText, aColumnWidth, aFillChar, false );
	}

	/**
	 * Fills a right aligned text with the give fill character till the given
	 * length is reached. It is assumed that the text does not contain any ANSI
	 * escape codes, else use the according method with the ANSI escape code
	 * flag.
	 *
	 * @param aText The text array to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String[] asRightAligned( String aText[], int aColumnWidth, char aFillChar ) {
		return asRightAligned( aText, aColumnWidth, aFillChar, false );
	}

	/**
	 * Fills a right aligned text with a space " " character till the given
	 * length is reached.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * 
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asRightAligned( String aText, int aColumnWidth, boolean hasAnsiEscapeCodes ) {
		return asRightAligned( aText, aColumnWidth, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Fills a right aligned text with the give fill character till the given
	 * length is reached.
	 *
	 * @param aText The text to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String asRightAligned( String aText, int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		return asAligned( aText, aColumnWidth, aFillChar, HorizAlignTextMode.RIGHT, hasAnsiEscapeCodes );
	}

	/**
	 * Fills a right aligned text with the give fill character till the given
	 * length is reached.
	 *
	 * @param aText The text array to be filled up or aligned.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up with the given char till the width is
	 *         reached or the {@link String} untouched in case no
	 *         {@link HorizAlignTextMode} parameter has been provided..
	 */
	public static String[] asRightAligned( String aText[], int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		return asAligned( aText, aColumnWidth, aFillChar, HorizAlignTextMode.RIGHT, hasAnsiEscapeCodes );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Fills a text up on by appending the given char to the right till the
	 * given length is reached (align left).
	 * 
	 * @param aText The text to be filled up.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up on the right hand side with the given
	 *         char.
	 */
	private static String toAlignLeft( String aText, int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		if ( toLength( aText, hasAnsiEscapeCodes ) == aColumnWidth ) {
			return aText;
		}
		if ( toLength( aText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			return aText.substring( 0, aColumnWidth );
		}
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( aText );
		for ( int i = 0; i < aColumnWidth - toLength( aText, hasAnsiEscapeCodes ); i++ ) {
			theBuffer.append( aFillChar );
		}
		return theBuffer.toString();
	}

	/**
	 * Fills a text up by prepending the given char to the left till the given
	 * length is reached (align right).
	 * 
	 * @param aText The text to be filled up.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up on the left hand side with the given
	 *         char.
	 */
	private static String toAlignRight( String aText, int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		if ( toLength( aText, hasAnsiEscapeCodes ) == aColumnWidth ) {
			return aText;
		}
		if ( toLength( aText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			return aText.substring( toLength( aText, hasAnsiEscapeCodes ) - aColumnWidth );
		}
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aColumnWidth - toLength( aText, hasAnsiEscapeCodes ); i++ ) {
			theBuffer.append( aFillChar );
		}
		theBuffer.append( aText );
		return theBuffer.toString();
	}

	/**
	 * Fills a text up on by appending the given char to the left and the right
	 * till the given length is reached (align center).
	 * 
	 * @param aText The text to be filled up.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up on the left and right hand side with
	 *         the given char.
	 */
	private static String toAlignCenter( String aText, int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		if ( toLength( aText, hasAnsiEscapeCodes ) == aColumnWidth ) {
			return aText;
		}
		if ( toLength( aText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			final int theExceed = toLength( aText, hasAnsiEscapeCodes ) - aColumnWidth;
			final int theLeft = theExceed / 2;
			int theRight = theExceed / 2;
			theRight += theExceed % 2;
			return aText.substring( theLeft, toLength( aText, hasAnsiEscapeCodes ) - theRight );
		}
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( aText );
		while ( theBuffer.length() < aColumnWidth ) {
			theBuffer.append( aFillChar );
			if ( theBuffer.length() < aColumnWidth ) {
				theBuffer.insert( 0, aFillChar );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Fills a text up on by adding the given char to occurrences of the given
	 * char round robin till given length is reached (block mode).
	 * 
	 * @param aText The text to be filled up.
	 * @param aColumnWidth The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} filled up in block mode with the given char.
	 */
	private static String toAlignBlock( String aText, int aColumnWidth, char aFillChar, boolean hasAnsiEscapeCodes ) {
		if ( toLength( aText, hasAnsiEscapeCodes ) == aColumnWidth ) {
			return aText;
		}
		if ( toLength( aText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			final int theExceed = toLength( aText, hasAnsiEscapeCodes ) - aColumnWidth;
			final int theLeft = theExceed / 2;
			int theRight = theExceed / 2;
			theRight += theExceed % 2;
			return aText.substring( theLeft, toLength( aText, hasAnsiEscapeCodes ) - theRight );
		}
		if ( aText.indexOf( aFillChar ) == -1 ) {
			return toAlignLeft( aText, aColumnWidth, aFillChar, hasAnsiEscapeCodes );
		}
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( aText );
		final String theFillChar = new String( new char[] { aFillChar } );
		int eOffset = -1;
		while ( theBuffer.length() < aColumnWidth ) {
			eOffset = theBuffer.indexOf( theFillChar, eOffset + 1 );
			if ( eOffset == -1 ) {
				eOffset = 0;
			}
			else {
				theBuffer.insert( eOffset, theFillChar );
				while ( eOffset < theBuffer.length() - 1 && theBuffer.charAt( eOffset ) == aFillChar ) {
					eOffset++;
				}
				if ( eOffset > aColumnWidth ) {
					eOffset = 0;
				}
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Returns the length of the text, considering whether the text is expected
	 * to have ANSI escape codes not considered to be part of the length.
	 * 
	 * @param aText The text for which to get the length.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return The according print length.
	 */
	private static int toLength( String aText, boolean hasAnsiEscapeCodes ) {
		if ( hasAnsiEscapeCodes ) {
			return AnsiEscapeCode.toUnescapedLength( aText );
		}
		return aText.length();
	}
}
