// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.CharSet;
import org.refcodes.mixin.CharSetAccessor;

/**
 * The {@link RandomTextMode} specifies for according chars to be used when
 * generating random text.
 */
public enum RandomTextMode implements CharSetAccessor {

	/**
	 * Represents a charset consisting of only ASCII characters.
	 */
	ASCII(CharSet.ASCII.getCharSet()),

	/**
	 * Represents a charset consisting of only upper and lower case letters.
	 */
	ALPHABETIC(CharSet.ALPHABETIC.getCharSet()),

	/**
	 * Represents a charset consisting of only upper case letters.
	 */
	UPPER_CASE(CharSet.UPPER_CASE.getCharSet()),

	/**
	 * Represents a charset consisting of only lower case letters.
	 */
	LOWER_CASE(CharSet.LOWER_CASE.getCharSet()),

	/**
	 * Represents a charset consisting of only upper and lower case characters
	 * as well as of the digits 0 to 9.
	 */
	ALPHANUMERIC(CharSet.ALPHANUMERIC.getCharSet()),

	/**
	 * Represents a charset consisting of only upper case characters as well as
	 * of the digits 0 to 9.
	 */
	UPPER_CASE_ALPHANUMERIC(asConcat( CharSet.UPPER_CASE.getCharSet(), CharSet.NUMERIC.getCharSet() )),

	/**
	 * Represents a charset consisting of only lower case characters as well as
	 * of the digits 0 to 9.
	 */
	LOWER_CASE_ALPHANUMERIC(asConcat( CharSet.LOWER_CASE.getCharSet(), CharSet.NUMERIC.getCharSet() )),

	/**
	 * Represents a charset consisting of only digits 0 to 9.
	 */
	NUMERIC(CharSet.NUMERIC.getCharSet()),

	/**
	 * Represents a charset consisting of only digits 0 and 1.
	 */
	BINARY(CharSet.BINARY.getCharSet()),

	/**
	 * Represents a charset consisting of only digits 0 to 9 as well as the
	 * characters A to F.
	 */
	HEXADECIMAL(CharSet.HEXADECIMAL.getCharSet());

	private char[] _charSet;

	/**
	 * Instantiates a new random text mode.
	 *
	 * @param aCharSet the char set
	 */
	private RandomTextMode( char[] aCharSet ) {
		_charSet = aCharSet;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getCharSet() {
		return _charSet;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static char[] asConcat( char[] aHead, char[] aTail ) {
		final char[] theResult = new char[aHead.length + aTail.length];
		System.arraycopy( aHead, 0, theResult, 0, aHead.length );
		System.arraycopy( aTail, 0, theResult, aHead.length, aTail.length );
		return theResult;
	}
}
