// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;

/**
 * The {@link EscapeTextBuilder} provides means to "escape" and "unescape" text,
 * e.g. simple marshaling and unmarshaling.
 */
public class EscapeTextBuilder extends AbstractText<EscapeTextBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private List<Property> _properties;

	private EscapeTextMode _escapeTextMode = EscapeTextMode.ESCAPE;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the escape text mode from the escape text mode property.
	 * 
	 * @return The escape text mode stored by the escape text mode property.
	 */
	public EscapeTextMode getEscapeTextMode() {
		return _escapeTextMode;
	}

	/**
	 * Sets the escape text mode for the escape text mode property.
	 * 
	 * @param aEscapeTextMode The escape text mode to be stored by the align
	 *        text mode property.
	 */
	public void setEscapeTextMode( EscapeTextMode aEscapeTextMode ) {
		_escapeTextMode = aEscapeTextMode;
	}

	/**
	 * Gets the escape properties.
	 *
	 * @return the escape properties
	 */
	public Property[] getEscapeProperties() {
		return _properties.toArray( new Property[_properties.size()] );
	}

	/**
	 * Sets the escape properties.
	 *
	 * @param aProperties the new escape properties
	 */
	public void setEscapeProperties( Property... aProperties ) {
		_properties = new ArrayList<>();
		Collections.addAll( _properties, aProperties );
	}

	/**
	 * Adds the escape property.
	 *
	 * @param aProperty the property
	 * 
	 * @return the escape text builder
	 */
	public EscapeTextBuilder addEscapeProperty( Property aProperty ) {
		if ( _properties == null ) {
			_properties = new ArrayList<>();
		}
		_properties.add( aProperty );
		return this;
	}

	/**
	 * Adds the escape property.
	 *
	 * @param aKey the key
	 * @param aValue the value
	 * 
	 * @return the escape text builder
	 */
	public EscapeTextBuilder addEscapeProperty( String aKey, String aValue ) {
		return addEscapeProperty( new PropertyImpl( aKey, aValue ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return toEscaped( getText(), _properties, _escapeTextMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return toEscaped( aText, _properties, _escapeTextMode );
	}

	/**
	 * Sets the escape text mode for the escape text mode property.
	 * 
	 * @param aEscapeTextMode The escape text mode to be stored by the align
	 *        text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public EscapeTextBuilder withEscapeTextMode( EscapeTextMode aEscapeTextMode ) {
		setEscapeTextMode( aEscapeTextMode );
		return this;
	}

	/**
	 * With escape properties.
	 *
	 * @param aProperties the properties
	 * 
	 * @return the escape text builder
	 */
	public EscapeTextBuilder withEscapeProperties( Property... aProperties ) {
		setEscapeProperties( aProperties );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To escaped.
	 *
	 * @param aText the text
	 * @param aProperties the properties
	 * @param aEscapeTextMode the escape text mode
	 * 
	 * @return the string[]
	 */
	protected String[] toEscaped( String[] aText, List<Property> aProperties, EscapeTextMode aEscapeTextMode ) {
		final String[] theResult = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theResult[i] = toEscaped( aText[i], aProperties, aEscapeTextMode );
		}
		return theResult;
	}

	/**
	 * To escaped.
	 *
	 * @param aText the text
	 * @param aProperties the properties
	 * @param aEscapeTextMode the escape text mode
	 * 
	 * @return the string
	 */
	protected String toEscaped( String aText, List<Property> aProperties, EscapeTextMode aEscapeTextMode ) {
		switch ( aEscapeTextMode ) {
		case ESCAPE -> {
			return toEscaped( aText, toEscapeMatrix( aProperties ) );
		}
		case UNESCAPE -> {
			return toUnEscaped( aText, toEscapeMatrix( aProperties ) );
		}
		default -> {
		}
		}
		throw new IllegalArgumentException( "You must pass a valid escape text mode, though you actually passed <" + aEscapeTextMode + ">!" );
	}

	/**
	 * Escapes a text using the provided escape matrix which must be a two
	 * dimensional array with two elements per line and where the element at
	 * position 0 (of a line) represents the character and the element at
	 * position 1 (of a line) the escaped character.
	 * 
	 * @param aUnEscaped The text to be escaped.
	 * @param aEscapeMatrix The matrix to be used for escaping.
	 * 
	 * @return The escape text as of the escape matrix.
	 */
	public static String toEscaped( String aUnEscaped, String[][] aEscapeMatrix ) {
		if ( aEscapeMatrix != null ) {
			for ( String[] anAEscapeMatrix : aEscapeMatrix ) {
				aUnEscaped = aUnEscaped.replaceAll( toRegExEscaped( anAEscapeMatrix[0] ), anAEscapeMatrix[1] );
			}
			return aUnEscaped;
		}
		return EscapeTextBuilder.asEscaped( aUnEscaped );
	}

	/**
	 * Unescapes a text using the provided escape matrix which must be a two
	 * dimensional array with two elements per line and where the element at
	 * position 0 (of a line) represents the character and the element at
	 * position 1 (of a line) the escaped character.
	 * 
	 * @param aEscaped The text to be unescaped.
	 * @param aEscapeMatrix The matrix to be used for unescaping.
	 * 
	 * @return The unescape text as of the escape matrix.
	 */
	public static String toUnEscaped( String aEscaped, String[][] aEscapeMatrix ) {
		if ( aEscapeMatrix != null ) {
			for ( String[] anAEscapeMatrix : aEscapeMatrix ) {
				aEscaped = aEscaped.replaceAll( toRegExEscaped( anAEscapeMatrix[1] ), anAEscapeMatrix[0] );
			}
			return aEscaped;
		}
		return EscapeTextBuilder.asUnEscaped( aEscaped );
	}

	/**
	 * Default text escape implementation used by the {@link EscapeTextBuilder}
	 * in case no escape {@link Property} matrix has been supplied.
	 * 
	 * @param aUnEscapedText The text to be escaped.
	 * 
	 * @return The escaped text.
	 */
	public static String asEscaped( String aUnEscapedText ) {
		final StringBuilder theBuffer = new StringBuilder();
		char eChar;
		final int theLength = aUnEscapedText.length();
		for ( int i = 0; i < theLength; i++ ) {
			eChar = aUnEscapedText.charAt( i );
			if ( eChar > 0xfff ) {
				theBuffer.append( "\\u" + Integer.toHexString( eChar ).toUpperCase() );
			}
			else if ( eChar > 0xff ) {
				theBuffer.append( "\\u0" + Integer.toHexString( eChar ).toUpperCase() );
			}
			else if ( eChar > 0x7f ) {
				theBuffer.append( "\\u00" + Integer.toHexString( eChar ).toUpperCase() );
			}
			else if ( eChar < 32 ) {
				switch ( eChar ) {
				case '\b' -> {
					theBuffer.append( '\\' );
					theBuffer.append( 'b' );
				}
				case '\n' -> {
					theBuffer.append( '\\' );
					theBuffer.append( 'n' );
				}
				case '\t' -> {
					theBuffer.append( '\\' );
					theBuffer.append( 't' );
				}
				case '\f' -> {
					theBuffer.append( '\\' );
					theBuffer.append( 'f' );
				}
				case '\r' -> {
					theBuffer.append( '\\' );
					theBuffer.append( 'r' );
				}
				default -> {
					if ( eChar > 0xf ) {
						theBuffer.append( "\\u00" + Integer.toHexString( eChar ).toUpperCase() );
					}
					else {
						theBuffer.append( "\\u000" + Integer.toHexString( eChar ).toUpperCase() );
					}
				}
				}
			}
			else {
				switch ( eChar ) {
				case '\'' -> theBuffer.append( '\'' );
				case '"' -> {
					theBuffer.append( '\\' );
					theBuffer.append( '"' );
				}
				case '\\' -> {
					theBuffer.append( '\\' );
					theBuffer.append( '\\' );
				}
				case '/' -> theBuffer.append( '/' );
				default -> theBuffer.append( eChar );
				}
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Default text un-escape implementation used by the
	 * {@link EscapeTextBuilder} in case no escape {@link Property} matrix has
	 * been supplied.
	 * 
	 * @param aEscapedText The text to be un-escaped.
	 * 
	 * @return The un-escaped text.
	 */
	public static String asUnEscaped( String aEscapedText ) {
		final StringBuilder theBuffer = new StringBuilder();
		final int theLength = aEscapedText.length();
		final StringBuilder theTemp = new StringBuilder();
		boolean hasSlash = false;
		boolean isUnicode = false;
		char eChar;
		int eValue;
		for ( int i = 0; i < theLength; i++ ) {
			eChar = aEscapedText.charAt( i );
			if ( isUnicode ) {
				theTemp.append( eChar );
				if ( theTemp.length() == 4 ) {
					try {
						eValue = Integer.parseInt( theTemp.toString(), 16 );
						theBuffer.append( (char) eValue );
						theTemp.setLength( 0 );
						isUnicode = false;
						hasSlash = false;
					}
					catch ( NumberFormatException e ) {
						throw new IllegalArgumentException( "Unable to parse unicode: " + theTemp, e );
					}
				}
				continue;
			}
			if ( hasSlash ) {
				hasSlash = false;
				switch ( eChar ) {
				case '\\' -> theBuffer.append( '\\' );
				case '\'' -> theBuffer.append( '\'' );
				case '\"' -> theBuffer.append( '"' );
				case 'r' -> theBuffer.append( '\r' );
				case 'f' -> theBuffer.append( '\f' );
				case 't' -> theBuffer.append( '\t' );
				case 'n' -> theBuffer.append( '\n' );
				case 'b' -> theBuffer.append( '\b' );
				case 'u' -> isUnicode = true;
				default -> theBuffer.append( eChar );
				}
				continue;
			}
			else if ( eChar == '\\' ) {
				hasSlash = true;
				continue;
			}
			theBuffer.append( eChar );
		}
		if ( hasSlash ) {
			theBuffer.append( '\\' );
		}
		return theBuffer.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To escaped regexp.
	 *
	 * @param aDelimiter the delimiter
	 * 
	 * @return the string
	 */
	protected static String toEscapedRegexp( char aDelimiter ) {
		return ( "" + aDelimiter ).replaceAll( "\\.", "\\\\." ).replaceAll( "\\|", "\\\\|" );
	}

	/**
	 * To reg ex escaped.
	 *
	 * @param aText the text
	 * 
	 * @return the string
	 */
	protected static String toRegExEscaped( String aText ) {
		aText = aText.replaceAll( "\\+", "\\\\+" ).replaceAll( "\\*", "\\\\*" ).replaceAll( "\\.", "\\\\." );
		return aText;
	}

	/**
	 * To escape matrix.
	 *
	 * @param aProperties the properties
	 * 
	 * @return the string[][]
	 */
	private static String[][] toEscapeMatrix( List<Property> aProperties ) {
		if ( aProperties != null ) {
			final String[][] theStrings = new String[aProperties.size()][2];
			for ( int i = 0; i < aProperties.size(); i++ ) {
				theStrings[i][0] = aProperties.get( i ).getKey();
				theStrings[i][1] = aProperties.get( i ).getValue();
			}
			return theStrings;
		}
		return null;
	}
}
