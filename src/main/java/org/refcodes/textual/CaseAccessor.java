// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a {@link Case} property.
 */
public interface CaseAccessor {

	/**
	 * Gets the currently set {@link Case} being used.
	 * 
	 * @return The currently configured {@link Case}s.
	 */
	Case getCase();

	/**
	 * Provides a mutator for an {@link Case} property.
	 * 
	 * @param <B> The builder which implements the {@link CaseBuilder}.
	 */
	public interface CaseBuilder<B extends CaseBuilder<?>> {

		/**
		 * Sets the rows {@link Case} to use returns this builder as of the
		 * Builder-Pattern.
		 * 
		 * @param aCase The {@link Case} to be used when printing a row or the
		 *        header.
		 * 
		 * @return This {@link CaseBuilder} instance to continue configuration.
		 */
		B withCase( Case aCase );
	}

	/**
	 * Provides a mutator for an {@link Case} property.
	 */
	public interface CaseMutator {

		/**
		 * Sets the {@link Case} to be used.
		 * 
		 * @param aCase The {@link Case} to be stored by the {@link Case}
		 *        property.
		 */
		void setCase( Case aCase );
	}

	/**
	 * Provides a {@link Case} property.
	 */
	public interface CaseProperty extends CaseAccessor, CaseMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Case} (setter) as
		 * of {@link #setCase(Case)} and returns the very same value (getter).
		 * 
		 * @param aCase The {@link Case} to set (via {@link #setCase(Case)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Case letCase( Case aCase ) {
			setCase( aCase );
			return aCase;
		}
	}
}