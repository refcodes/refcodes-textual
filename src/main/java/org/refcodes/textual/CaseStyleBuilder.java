// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.Delimiter;
import org.refcodes.mixin.TextAccessor.TextBuilder;
import org.refcodes.mixin.TextAccessor.TextProperty;
import org.refcodes.runtime.Configuration;
import org.refcodes.textual.CaseAccessor.CaseBuilder;
import org.refcodes.textual.CaseAccessor.CaseProperty;

/**
 * Converts a text to the format of a {@link String} to the desired case. The
 * state of the {@link Case} accessible via {@link #getCase()} has not an effect
 * on some of the methods. For example it has no effect on
 * {@link #toCamelCase()}, but it has an effect in {@link #toSnakeCase(String)}.
 */
public class CaseStyleBuilder implements TextProperty, TextBuilder<CaseStyleBuilder>, CaseProperty, CaseBuilder<CaseStyleBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _text = null;

	private Case _case = Case.UPPER;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText() {
		return _text;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setText( String aText ) {
		_text = aText;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CaseStyleBuilder withText( String aText ) {
		setText( aText );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Case getCase() {
		return _case;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCase( Case aCase ) {
		_case = aCase;
	}

	/**
	 * Converts the text as of {@link #getText()} to camel-case ("camelCase").
	 * 
	 * @return The camel-case representation of the according text.
	 */
	public String toCamelCase() {
		return toCamelCase( getText() );
	}

	/**
	 * Converts the text as of {@link #getText()} to snake-case, either in upper
	 * case as of "SNAKE_CASE" or in lower-case as of "snake_case", depending on
	 * the {@link Case} property (as of {@link #setCase(Case)} and
	 * {@link #getCase()}).
	 * 
	 * @return The pascal-case representation of the according text.
	 */
	public String toSnakeCase() {
		return toSnakeCase( getText() );
	}

	/**
	 * Converts the text as of {@link #getText()} to pascal-case ("PascalCase").
	 * 
	 * @return The camel-case representation of the according text.
	 */
	public String toPascalCase() {
		return toPascalCase( getText() );
	}

	/**
	 * Converts the text as of {@link #getText()} to kebab-case, either in upper
	 * case as of "KEBAB-CASE" or in lower-case as of "kebab-case", depending on
	 * the {@link Case} property (as of {@link #setCase(Case)} and
	 * {@link #getCase()}).
	 * 
	 * @return The kebab-case representation of the according text.
	 */
	public String toKebabCase() {
		return toKebabCase( getText() );
	}

	/**
	 * Converts the provided text as of {@link #getText()} to camel-case
	 * ("camelCase").
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The camel-case representation of the according text.
	 */
	public String toCamelCase( String aText ) {
		return asCamelCase( aText );
	}

	/**
	 * Converts the provided text as of {@link #getText()} to snake-case, either
	 * in upper case as of "SNAKE_CASE" or in lower-case as of "snake_case",
	 * depending on the {@link Case} property (as of {@link #setCase(Case)} and
	 * {@link #getCase()}).
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The snake-case representation of the according text.
	 */
	public String toSnakeCase( String aText ) {
		final Case theCase = _case;
		return asSnakeCase( aText, theCase );
	}

	/**
	 * Converts the provided text as of {@link #getText()} to pascal-case
	 * ("PascalCase").
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The pascal-case representation of the according text.
	 */
	public String toPascalCase( String aText ) {
		return asPascalCase( aText );
	}

	/**
	 * Converts the provided text as of {@link #getText()} to kebab-case, either
	 * in upper case as of "KEBAB-CASE" or in lower-case as of "kebab-case",
	 * depending on the {@link Case} property (as of {@link #setCase(Case)} and
	 * {@link #getCase()}).
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The kebab-case representation of the according text.
	 */
	public String toKebabCase( String aText ) {
		final Case theCase = _case;
		return asKebabCase( aText, theCase );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CaseStyleBuilder withCase( Case aCase ) {
		setCase( aCase );
		return this;
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toCamelCase(String)} though with default settings.
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The camel-case representation of the according text.
	 */
	public static String asCamelCase( String aText ) {
		if ( aText == null || aText.isEmpty() ) {
			return aText;
		}
		final String theText = NormalizeCaseUtility.toNormalized( aText, Delimiter.SNAKE_CASE.getChar() ).
		// @formatter:off
				replaceAll( "_ä", "Ä" ).
				replaceAll( "_ö", "Ö" ).
				replaceAll( "_ü", "Ü" ).
				replaceAll( "_a", "A" ).
				replaceAll( "_b", "B" ).
				replaceAll( "_c", "C" ).
				replaceAll( "_d", "D" ).
				replaceAll( "_e", "E" ).
				replaceAll( "_f", "F" ).
				replaceAll( "_g", "G" ).
				replaceAll( "_h", "H" ).
				replaceAll( "_i", "I" ).
				replaceAll( "_j", "J" ).
				replaceAll( "_k", "K" ).
				replaceAll( "_l", "L" ).
				replaceAll( "_m", "M" ).
				replaceAll( "_n", "N" ).
				replaceAll( "_o", "O" ).
				replaceAll( "_p", "P" ).
				replaceAll( "_q", "Q" ).
				replaceAll( "_r", "R" ).
				replaceAll( "_s", "S" ).
				replaceAll( "_t", "T" ).
				replaceAll( "_u", "U" ).
				replaceAll( "_v", "V" ).
				replaceAll( "_w", "W" ).
				replaceAll( "_x", "X" ).
				replaceAll( "_y", "Y" ).
				replaceAll( "_z", "Z" ).
				replaceAll( "_0", "0" ).
				replaceAll( "_1", "1" ).
				replaceAll( "_2", "2" ).
				replaceAll( "_3", "3" ).
				replaceAll( "_4", "4" ).
				replaceAll( "_5", "5" ).
				replaceAll( "_6", "6" ).
				replaceAll( "_7", "7" ).
				replaceAll( "_8", "8" ).
				replaceAll( "_9", "9" );
				// @formatter:on
		return theText;
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toSnakeCase(String)} though with default settings.
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The snake-case representation of the according text.
	 */
	public static String asSnakeCase( String aText ) {
		return CaseStyleBuilder.asSnakeCase( aText, Case.UPPER );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toSnakeCase(String)}.
	 * 
	 * @param aText The text to be converted.
	 * @param aCase The case to use, either upper or lower.
	 * 
	 * @return The snake-case representation of the according text.
	 */
	public static String asSnakeCase( String aText, Case aCase ) {
		if ( aText == null || aText.isEmpty() ) {
			return aText;
		}
		return aCase.toCase( NormalizeCaseUtility.toNormalized( aText, Delimiter.SNAKE_CASE.getChar() ) );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toPascalCase(String)} though with default settings.
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The pascal-case representation of the according text.
	 */
	public static String asPascalCase( String aText ) {
		if ( aText == null || aText.isEmpty() ) {
			return aText;
		}
		aText = asCamelCase( aText );
		aText = Character.toUpperCase( aText.charAt( 0 ) ) + aText.substring( 1 );
		return aText;
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toKebabCase(String)} though with default settings.
	 * 
	 * @param aText The text to be converted.
	 * 
	 * @return The "kebab-case" representation of the according text.
	 */
	public static String asKebabCase( String aText ) {
		return CaseStyleBuilder.asKebabCase( aText, Case.UPPER );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toKebabCase(String)}.
	 * 
	 * @param aText The text to be converted.
	 * @param aCase The case to use, either upper or lower.
	 * 
	 * @return The "kebab-case" representation of the according text.
	 */
	public static String asKebabCase( String aText, Case aCase ) {
		if ( aText == null || aText.isEmpty() ) {
			return aText;
		}
		return aCase.toCase( NormalizeCaseUtility.toNormalized( aText, Delimiter.KEBAB_CASE.getChar() ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class NormalizeCaseUtility.
	 */
	static class NormalizeCaseUtility extends Configuration {
		/**
		 * To normalized.
		 *
		 * @param aText the text
		 * @param aSeparator the separator
		 * 
		 * @return the string
		 */
		protected static String toNormalized( String aText, char aSeparator ) {
			return Configuration.asNormalized( aText, aSeparator );
		}
	}
}
