// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.mixin.BoxGridImpl;

/**
 * Implementation of the {@link TextBoxGrid} interface.
 */
public class TextBoxGridImpl extends BoxGridImpl<Character> implements TextBoxGrid {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * For easy of use this constructor uses a {@link String} array representing
	 * the box grid characters to be used. The array may look as follows: <code>
	 * {
	 *   "┌─┬─┐",
	 *   "│ │ │",
	 *   "├─┼─┤",
	 *   "│ │ │",
	 *   "└─┴─┘"
	 * }
	 *</code>
	 * 
	 * @param aTextBoxGrid The {@link String} array "painting" the grid.
	 */
	public TextBoxGridImpl( String[] aTextBoxGrid ) {
		_topLeftEdge = aTextBoxGrid[0].charAt( 0 );
		_topLine = aTextBoxGrid[0].charAt( 1 );
		_topDividerEdge = aTextBoxGrid[0].charAt( 2 );
		_topRightEdge = aTextBoxGrid[0].charAt( 4 );
		_rightLine = aTextBoxGrid[1].charAt( 4 );
		_rightEdge = aTextBoxGrid[2].charAt( 4 );
		_bottomRightEdge = aTextBoxGrid[4].charAt( 4 );
		_bottomLine = aTextBoxGrid[4].charAt( 3 );
		_bottomDividerEdge = aTextBoxGrid[4].charAt( 2 );
		_bottomLeftEdge = aTextBoxGrid[4].charAt( 0 );
		_leftLine = aTextBoxGrid[1].charAt( 0 );
		_leftEdge = aTextBoxGrid[2].charAt( 0 );
		_dividerLine = aTextBoxGrid[1].charAt( 2 );
		_dividerEdge = aTextBoxGrid[2].charAt( 2 );
		_innerLine = aTextBoxGrid[2].charAt( 1 );
	}
}
