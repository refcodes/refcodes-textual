// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.graphical.Pixmap;
import org.refcodes.graphical.RgbPixmap;

/**
 * The pixmap scale mode specifies how an algorithm will scale a {@link Pixmap}
 * for proper display with text (character) e.g. when printing an
 * {@link RgbPixmap} on the console with ASCII_HEADER_ASCII_BODY art using the
 * {@link AsciiArtBuilder}.
 */
public enum PixmapRatioMode {
	/**
	 * The pixmap will not be scaled at all.
	 */
	NONE(1),
	/** The width to height ration is one to one half (1:1/2). */
	ONE_HALF((float) 1 / (float) 2),

	/** The width to height ration is one to two thirds (1:2/3). */
	TWO_THIRDS((float) 2 / (float) 3),

	/** The width to height ration is one to three quarters (1:3/4). */
	THREE_QUARTERS((float) 3 / (float) 4),

	/**
	 * The width to height ration is adjusted to look good on a console /
	 * terminal.
	 */
	CONSOLE(ONE_HALF.getRatio());

	private float _ratio;

	/**
	 * Instantiates a new pixmap ratio mode.
	 *
	 * @param aRatio the ratio
	 */
	private PixmapRatioMode( float aRatio ) {
		_ratio = aRatio;
	}

	/**
	 * Gets the ratio.
	 *
	 * @return the ratio
	 */
	public float getRatio() {
		return _ratio;
	}
}
