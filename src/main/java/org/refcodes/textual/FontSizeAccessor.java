// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

/**
 * Provides an accessor for a font style property.
 */
public interface FontSizeAccessor {

	/**
	 * Retrieves the font style from the font style property.
	 * 
	 * @return The font style stored by the font style property.
	 */
	int getFontSize();

	/**
	 * Provides a mutator for a font style property.
	 */
	public interface FontSizeMutator {

		/**
		 * Sets the font style for the font style property.
		 * 
		 * @param aFontSize The font style to be stored by the font style
		 *        property.
		 */
		void setFontSize( int aFontSize );
	}

	/**
	 * Provides a builder method for a font style property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FontSizeBuilder<B extends FontSizeBuilder<B>> {

		/**
		 * Sets the font style for the font style property.
		 * 
		 * @param aFontSize The font style to be stored by the font style
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFontSize( int aFontSize );
	}

	/**
	 * Provides a font style property.
	 */
	public interface FontSizeProperty extends FontSizeAccessor, FontSizeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setFontSize(int)} and returns the very same value (getter).
		 * 
		 * @param aFontSize The integer to set (via {@link #setFontSize(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letFontSize( int aFontSize ) {
			setFontSize( aFontSize );
			return aFontSize;
		}
	}
}
