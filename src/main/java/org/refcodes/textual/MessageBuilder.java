// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.text.MessageFormat;
import java.util.Arrays;

import org.refcodes.exception.Trap;
import org.refcodes.mixin.ArgumentsAccessor.ArgumentsBuilder;
import org.refcodes.mixin.ArgumentsAccessor.ArgumentsProperty;
import org.refcodes.mixin.MessageAccessor.MessageProperty;

/**
 * Wraps {@link MessageFormat#format(String, Object...)} to prevent failing when
 * message cannot be formatted. Also expands any arrays passed inside the
 * arguments to a comma separated {@link String} of the array's elements as of
 * {@link Arrays#toString(Object[])} without the square braces: In case such an
 * expanded array's length is 0, then the according placeholder (such as "{0}",
 * "{1}" and "{2}" and so on) is preserved in the resulting {@link String}!
 */
public class MessageBuilder implements MessageProperty, org.refcodes.mixin.MessageAccessor.MessageBuilder<org.refcodes.textual.MessageBuilder>, ArgumentsProperty<Object>, ArgumentsBuilder<Object, org.refcodes.textual.MessageBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _message;
	private Object[] _arguments;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMessage() {
		return _message;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMessage( String aMessage ) {
		_message = aMessage;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getArguments() {
		return _arguments;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setArguments( Object[] aArguments ) {
		_arguments = aArguments;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.refcodes.textual.MessageBuilder withArguments( Object[] aArguments ) {
		_arguments = aArguments;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.refcodes.textual.MessageBuilder withMessage( String aMessage ) {
		_message = aMessage;
		return this;
	}

	/**
	 * Wraps {@link MessageFormat#format(String, Object...)} to prevent failing
	 * when message cannot be formatted. Also expands any arrays passed inside
	 * the arguments to a comma separated {@link String} of the array's elements
	 * as of {@link Arrays#toString(Object[])} without the square braces: In
	 * case such an expanded array's length is 0, then the according placeholder
	 * (such as "{0}", "{1}" and "{2}" and so on) is preserved in the resulting
	 * {@link String}!
	 * 
	 * @return The substituted message.
	 */
	public String toMessage() {
		return asMessage( _message, _arguments );
	}

	/**
	 * Calls {@link #toMessage()} and returns its result.
	 * 
	 * The according {@link String}.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return toMessage();
	}

	/**
	 * Wraps {@link MessageFormat#format(String, Object...)} to prevent failing
	 * when message cannot be formatted. Also expands any arrays passed inside
	 * the arguments to a comma separated {@link String} of the array's elements
	 * as of {@link Arrays#toString(Object[])} without the square braces: In
	 * case such an expanded array's length is 0, then the according placeholder
	 * (such as "{0}", "{1}" and "{2}" and so on) is preserved in the resulting
	 * {@link String}!
	 *
	 * @param aMessage The message to be formatted by substituting the
	 *        placeholder ("{0}", "{1}" and "{2}" and so on) with the according
	 *        arguemtns's elements.
	 * @param aArguments the arguments The arguments which are used for
	 *        substitution.
	 * 
	 * @return The substituted message.
	 */
	public static String asMessage( String aMessage, Object... aArguments ) {
		return Trap.asMessage( aMessage, aArguments );
	}
}
