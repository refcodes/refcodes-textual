// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.refcodes.data.Text;

/**
 * The {@link VerboseTextBuilder} us a utility-builder for creating human
 * readable {@link String} objects from arrays, collections or maps (instead of
 * solely printing out the object references of those collections). The arrays,
 * collections or maps may be nested in any way still producing accordingly
 * indented and parenthesized text down to the single elements in those
 * collections whose {@link Object#toString()} method is invoked . This
 * utility-builder is very useful when printing debug or error logs or
 * inspecting such the like data structures "on the fly".
 */
public class VerboseTextBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final char DEFAULT_COLLECTION_HEAD = '{';
	public static final char DEFAULT_COLLECTION_TAIL = '}';

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Collection<Object> _collection = null;
	private Map<Object, Object> _map = null;
	private Character _collectionHead = DEFAULT_COLLECTION_HEAD;
	private Character _collectionTail = DEFAULT_COLLECTION_TAIL;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the {@link String} to be used for the begin of a collection.
	 * 
	 * @param aHead The begin of a collection.
	 */
	public void setCollectionHead( Character aHead ) {
		_collectionHead = aHead;

	}

	/**
	 * Gets the {@link String} to be used for the begin of a collection.
	 * 
	 * @return The begin of a collection.
	 */
	public Character getCollectionHead() {
		return _collectionHead;
	}

	/**
	 * Sets the {@link String} to be used for the begin of a collection.
	 * 
	 * @param aTail The begin of a collection.
	 */
	public void setCollectionTail( Character aTail ) {
		_collectionTail = aTail;
	}

	/**
	 * gets the {@link String} to be used for the begin of a collection.
	 * 
	 * @return The begin of a collection.
	 */
	public Character getCollectionTail() {
		return _collectionTail;
	}

	/**
	 * Gets the elements for the elements property.
	 * 
	 * @return The elements being stored by the elements property.
	 */
	public Object[] getElements() {
		return _collection.toArray();
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 */
	public void setElements( Object[] aElements ) {
		_collection = new ArrayList<>();
		_map = null;
		for ( Object eElement : aElements ) {
			_collection.add( eElement );
		}
	}

	/**
	 * Sets the elements.
	 *
	 * @param aElements the new elements
	 */
	@SuppressWarnings("unchecked")
	public void setElements( Collection<?> aElements ) {
		_collection = ( (Collection<Object>) aElements );
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 */
	@SuppressWarnings("unchecked")
	public void setElements( Map<?, ?> aElements ) {
		_collection = null;
		_map = (Map<Object, Object>) aElements;

	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( boolean[] aElements ) {
		final Boolean[] theElements = new Boolean[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( byte[] aElements ) {
		final Byte[] theElements = new Byte[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( short[] aElements ) {
		final Short[] theElements = new Short[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( int[] aElements ) {
		final Integer[] theElements = new Integer[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( long[] aElements ) {
		final Long[] theElements = new Long[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( float[] aElements ) {
		final Float[] theElements = new Float[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( double[] aElements ) {
		final Double[] theElements = new Double[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( char[] aElements ) {
		final Character[] theElements = new Character[aElements.length];
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = aElements[i];
		}
		setElements( (Object[]) theElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( boolean[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( byte[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( short[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( int[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( long[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( float[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( double[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( char[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Boolean[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Byte[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Short[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Integer[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Long[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Float[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Double[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( Character[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the CSV elements for the CSV elements property.
	 * 
	 * @param aElements The CSV elements to be stored by the CSV elements
	 *        property.
	 */
	public void setElements( String[] aElements ) {
		setElements( (Object[]) aElements );
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( Object[] aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( Collection<?> aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 */
	@SuppressWarnings("unchecked")
	public void setElements( Object aElements ) {
		if ( aElements == null || aElements.getClass().isArray() ) {
			setElements( (Object[]) aElements );
		}
		else if ( aElements instanceof Collection<?> ) {
			setElements( (Collection<Object>) aElements );
		}
		else if ( aElements instanceof Map<?, ?> ) {
			setElements( (Map<?, ?>) aElements );
		}
		else {
			setElements( new Object[] { aElements } );
		}
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( Object aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * Sets the elements for the elements property.
	 * 
	 * @param aElements The elements to be stored by the elements property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public VerboseTextBuilder withElements( Map<?, ?> aElements ) {
		setElements( aElements );
		return this;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String}
	 */
	@Override
	public String toString() {
		if ( _collection != null ) {
			return asVerboseString( _collectionHead, _collection, _collectionTail );
		}
		if ( _map != null ) {
			return asVerboseString( _collectionHead, _map, _collectionTail );
		}
		return Text.NULL_VALUE.getText();
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( boolean[] aElements ) {
		final List<Boolean> theElements = new ArrayList<>();
		for ( boolean aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( byte[] aElements ) {
		final List<Byte> theElements = new ArrayList<>();
		for ( byte aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( short[] aElements ) {
		final List<Short> theElements = new ArrayList<>();
		for ( short aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( int[] aElements ) {
		final List<Integer> theElements = new ArrayList<>();
		for ( int aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( long[] aElements ) {
		final List<Long> theElements = new ArrayList<>();
		for ( long aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( float[] aElements ) {
		final List<Float> theElements = new ArrayList<>();
		for ( float aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( double[] aElements ) {
		final List<Double> theElements = new ArrayList<>();
		for ( double aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( char[] aElements ) {
		final List<Character> theElements = new ArrayList<>();
		for ( char aElement : aElements ) {
			theElements.add( aElement );
		}
		return toString( theElements );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Boolean[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Byte[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Short[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Integer[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Long[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Float[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Double[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Character[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( String[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Object[] aElements ) {
		return asVerboseString( _collectionHead, Arrays.asList( aElements ), _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Collection<?> aElements ) {
		return asVerboseString( _collectionHead, aElements, _collectionTail );
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Map<?, ?> aElements ) {
		return asVerboseString( _collectionHead, aElements, _collectionTail );
	}

	/**
	 * Sets the {@link String} to be used for the begin of a collection.
	 * 
	 * @param aHead The begin of a collection.
	 * 
	 * @return This instance as of the builder pattern.
	 */
	public VerboseTextBuilder withCollectionHead( Character aHead ) {
		setCollectionHead( aHead );
		return this;
	}

	/**
	 * The {@link String} being build by the builder upon the settings of the
	 * attributes and the provided argument. This method is to be side effect
	 * free in terms of the elements (and the verbose result) are not part of
	 * the state for this instance (from the point of view of this method).
	 * Still changing for example the elements via
	 * {@link #withElements(Collection)} can cause side effects! For avoiding
	 * thread race conditions / side effects regarding the elements text (and
	 * the verbose result), use this method instead of the combination of
	 * {@link #withElements(Collection)} with {@link #toString()}.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public String toString( Object aElements ) {
		if ( aElements.getClass().isArray() ) {
			return toString( (Object[]) aElements );
		}
		if ( aElements instanceof Collection ) {
			return toString( (Collection<?>) aElements );
		}
		if ( aElements instanceof Map ) {
			return toString( (Map<?, ?>) aElements );
		}
		return toString( new Object[] { aElements } );
	}

	// -------------------------------------------------------------------------

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(boolean[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( boolean[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Boolean> theList = new ArrayList<>();
		for ( boolean eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(byte[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( byte[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		//	List<String> theList = new ArrayList<>();
		//	for ( byte eValue : aElements ) {
		//		theList.add( NumericalUtility.toHexString( eValue ) );
		//	}
		final List<Byte> theList = new ArrayList<>();
		for ( byte eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(short[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( short[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Short> theList = new ArrayList<>();
		for ( short eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(int[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( int[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Integer> theList = new ArrayList<>();
		for ( int eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(long[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( long[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Long> theList = new ArrayList<>();
		for ( long eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(float[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( float[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Float> theList = new ArrayList<>();
		for ( float eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(double[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( double[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Double> theList = new ArrayList<>();
		for ( double eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(char[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( char[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Character> theList = new ArrayList<>();
		for ( char eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Boolean[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Boolean[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		final List<Boolean> theList = new ArrayList<>();
		for ( boolean eValue : aElements ) {
			theList.add( eValue );
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, theList, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Byte[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Byte[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Short[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Short[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Integer[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Integer[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Long[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Long[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Float[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Float[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Double[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Double[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Character[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Character[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(String[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( String[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Boolean[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Object[] aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, Arrays.asList( aElements ), DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Boolean[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Collection<?> aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, aElements, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Boolean[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Map<?, ?> aElements ) {
		if ( aElements == null ) {
			return null;
		}
		return VerboseTextBuilder.asVerboseString( DEFAULT_COLLECTION_HEAD, aElements, DEFAULT_COLLECTION_TAIL );
	}

	/**
	 * This method represents a shortcut for the instance method
	 * {@link #toString(Boolean[])} though with public settings.
	 * 
	 * @param aElements The elements to be transformed to a human readable
	 *        {@link String},
	 * 
	 * @return The according resulting {@link String}
	 */
	public static String asString( Object aElements ) {
		if ( aElements == null ) {
			return null;
		}
		if ( aElements instanceof boolean[] ) {
			return asString( (boolean[]) aElements );
		}
		if ( aElements instanceof byte[] ) {
			return asString( (byte[]) aElements );
		}
		if ( aElements instanceof char[] ) {
			return asString( (char[]) aElements );
		}
		if ( aElements instanceof short[] ) {
			return asString( (short[]) aElements );
		}
		if ( aElements instanceof int[] ) {
			return asString( (int[]) aElements );
		}
		if ( aElements instanceof long[] ) {
			return asString( (long[]) aElements );
		}
		if ( aElements instanceof float[] ) {
			return asString( (float[]) aElements );
		}
		if ( aElements instanceof double[] ) {
			return asString( (double[]) aElements );
		}
		if ( aElements.getClass().isArray() ) {
			return asString( (Object[]) aElements );
		}
		if ( aElements instanceof Collection ) {
			return asString( (Collection<?>) aElements );
		}
		if ( aElements instanceof Map ) {
			return asString( (Map<?, ?>) aElements );
		}
		return asString( new Object[] { aElements } );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To verbose string.
	 * 
	 * @param aCollectionHead The head sequence for the collection.
	 * @param aCollection the collection
	 * @param aCollectionTail The tail sequence for the collection.
	 * 
	 * @return the string
	 */
	protected static String asVerboseString( Character aCollectionHead, Collection<?> aCollection, Character aCollectionTail ) {

		String theCollectionHead = "";
		if ( aCollectionHead != null ) {
			theCollectionHead = aCollectionHead + "";
		}
		String theCollectionTail = "";
		if ( aCollectionTail != null ) {
			theCollectionTail = aCollectionTail + "";
		}

		if ( aCollection.isEmpty() ) {
			return theCollectionHead + theCollectionTail;
		}
		final StringBuilder theBuffer = new StringBuilder();
		if ( aCollectionHead != null ) {
			theBuffer.append( aCollectionHead + " " );
		}
		final Iterator<?> e = aCollection.iterator();
		Object eValue;
		while ( e.hasNext() ) {
			eValue = e.next();
			if ( eValue != null && eValue.getClass().isArray() ) {
				if ( eValue instanceof byte[] ) {
					eValue = asString( (byte[]) eValue );
				}
				else if ( eValue instanceof short[] ) {
					eValue = asString( (short[]) eValue );
				}
				else if ( eValue instanceof int[] ) {
					eValue = asString( (int[]) eValue );
				}
				else if ( eValue instanceof long[] ) {
					eValue = asString( (long[]) eValue );
				}
				else if ( eValue instanceof float[] ) {
					eValue = asString( (float[]) eValue );
				}
				else if ( eValue instanceof double[] ) {
					eValue = asString( (double[]) eValue );
				}
				else if ( eValue instanceof boolean[] ) {
					eValue = asString( (boolean[]) eValue );
				}
				else if ( eValue instanceof char[] ) {
					eValue = asString( (char[]) eValue );
				}
				else {
					eValue = asString( (Object[]) eValue );
				}
				theBuffer.append( eValue );
			}
			else {
				if ( eValue instanceof String ) {
					theBuffer.append( "\"" + eValue + "\"" );
				}
				else if ( eValue instanceof Character ) {
					theBuffer.append( "\'" + eValue + "\'" );
				}
				else if ( eValue instanceof Enum<?> ) {
					theBuffer.append( ( (Enum<?>) eValue ).name() );
				}
				else {
					theBuffer.append( eValue );
				}
			}
			if ( e.hasNext() ) {
				theBuffer.append( ", " );
			}
		}
		if ( aCollectionTail != null ) {
			theBuffer.append( " " + aCollectionTail );
		}
		return theBuffer.toString();
	}

	/**
	 * To verbose string.
	 * 
	 * @param aCollectionHead The head sequence for the collection.
	 * @param aMap the map
	 * @param aCollectionTail The tail sequence for the collection.
	 *
	 * @return the string
	 */
	protected static String asVerboseString( Character aCollectionHead, Map<?, ?> aMap, Character aCollectionTail ) {
		String theCollectionHead = "";
		if ( aCollectionHead != null ) {
			theCollectionHead = aCollectionHead + "";
		}
		String theCollectionTail = "";
		if ( aCollectionTail != null ) {
			theCollectionTail = aCollectionTail + "";
		}
		if ( aMap.isEmpty() ) {
			return theCollectionHead + theCollectionTail;
		}
		final StringBuilder theBuffer = new StringBuilder();
		if ( aCollectionHead != null ) {
			theBuffer.append( aCollectionHead );
		}
		theBuffer.append( '\n' );
		final Iterator<?> e = aMap.keySet().iterator();
		Object eKey;
		Object eValue;
		while ( e.hasNext() ) {
			eKey = e.next();
			eValue = aMap.get( eKey );
			if ( eValue != null && eValue.getClass().isArray() ) {
				if ( eValue instanceof byte[] ) {
					eValue = asString( (byte[]) eValue );
				}
				else if ( eValue instanceof short[] ) {
					eValue = asString( (short[]) eValue );
				}
				else if ( eValue instanceof int[] ) {
					eValue = asString( (int[]) eValue );
				}
				else if ( eValue instanceof long[] ) {
					eValue = asString( (long[]) eValue );
				}
				else if ( eValue instanceof float[] ) {
					eValue = asString( (float[]) eValue );
				}
				else if ( eValue instanceof double[] ) {
					eValue = asString( (double[]) eValue );
				}
				else if ( eValue instanceof boolean[] ) {
					eValue = asString( (boolean[]) eValue );
				}
				else if ( eValue instanceof char[] ) {
					eValue = asString( (char[]) eValue );
				}
				else {
					eValue = asString( (Object[]) eValue );
				}
				theBuffer.append( "  " + eKey + " = " + eValue );
			}
			else {
				if ( eValue instanceof String ) {
					theBuffer.append( "  " + eKey + " = \"" + eValue + "\"" );
				}
				else if ( eValue instanceof Enum<?> ) {
					theBuffer.append( "  " + eKey + " = \"" + ( (Enum<?>) eValue ).name() + "\"" );
				}
				else {
					theBuffer.append( "  " + eKey + " = " + eValue );
				}
			}
			if ( e.hasNext() ) {
				theBuffer.append( ',' );
			}
			theBuffer.append( '\n' );
		}
		if ( aCollectionTail != null ) {
			theBuffer.append( aCollectionTail );
		}
		return theBuffer.toString();
	}
}
