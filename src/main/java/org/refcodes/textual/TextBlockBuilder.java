// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.CharSet;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthBuilder;
import org.refcodes.mixin.ColumnWidthAccessor.ColumnWidthProperty;
import org.refcodes.runtime.Terminal;
import org.refcodes.textual.FillCharAccessor.FillCharBuilder;
import org.refcodes.textual.FillCharAccessor.FillCharProperty;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeBuilder;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeProperty;

/**
 * Builds a text block according to the configuration. E.g. the
 * {@link HorizAlignTextMode} or the {@link SplitTextMode} can be configured as
 * well as the desired column width (as of {@link #setColumnWidth(int)}).
 */
public class TextBlockBuilder extends AbstractText<TextBlockBuilder> implements FillCharProperty, FillCharBuilder<TextBlockBuilder>, ColumnWidthBuilder<TextBlockBuilder>, ColumnWidthProperty, HorizAlignTextModeProperty, HorizAlignTextModeBuilder<TextBlockBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Characters indicating a line break when found inside a {@link String}.
	 */
	public static final String LINE_BREAKS = new String( new char[] { 9, 10, 13 } );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _columnWidth = Terminal.toHeuristicWidth();
	private HorizAlignTextMode _alignTextMode = HorizAlignTextMode.LEFT;
	private SplitTextMode _splitTextMode = SplitTextMode.AT_SPACE;
	private char _fillChar = ' ';

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getFillChar() {
		return _fillChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFillChar( char aFillChar ) {
		_fillChar = aFillChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBlockBuilder withColumnWidth( int aColumnWidth ) {
		setColumnWidth( aColumnWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnWidth( int aColumnWidth ) {
		_columnWidth = aColumnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnWidth() {
		return _columnWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_alignTextMode = aHorizAlignTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getHorizAlignTextMode() {
		return _alignTextMode;
	}

	/**
	 * Retrieves the split text mode from the split text mode property.
	 * 
	 * @return The split text mode stored by the split text mode property.
	 */
	public SplitTextMode getSplitTextMode() {
		return _splitTextMode;
	}

	/**
	 * Sets the split text mode for the split text mode property.
	 * 
	 * @param aSplitTextMode The split text mode to be stored by the align text
	 *        mode property.
	 */
	public void setSplitTextMode( SplitTextMode aSplitTextMode ) {
		_splitTextMode = aSplitTextMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings() {
		return toStrings( getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		return TextBlockBuilder.asTextBlock( aText, getColumnWidth(), getHorizAlignTextMode(), getSplitTextMode(), getFillChar() );
	}

	/**
	 * Sets the align text mode for the align text mode property.
	 * 
	 * @param aHorizAlignTextMode The align text mode to be stored by the align
	 *        text mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	@Override
	public TextBlockBuilder withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBlockBuilder withFillChar( char aFillChar ) {
		setFillChar( aFillChar );
		return this;
	}

	/**
	 * Sets the split text mode for the split text mode property.
	 * 
	 * @param aSplitTextMode The split text mode to be stored by the align text
	 *        mode property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public TextBlockBuilder withSplitTextMode( SplitTextMode aSplitTextMode ) {
		setSplitTextMode( aSplitTextMode );
		return this;
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth ) {
		return asTextBlock( aText, aColumnWidth, false );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, SplitTextMode aSplitTextMode ) {
		return asTextBlock( aText, aColumnWidth, aSplitTextMode, false );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode ) {
		return asTextBlock( aText, aColumnWidth, aHorizAlignTextMode, false );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, SplitTextMode aSplitTextMode, char aFillChar ) {
		return asTextBlock( aText, aColumnWidth, aHorizAlignTextMode, aSplitTextMode, aFillChar, false );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth ) {
		return asTextBlock( aText, aColumnWidth, false );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, SplitTextMode aSplitTextMode ) {
		return asTextBlock( aText, aColumnWidth, aSplitTextMode, false );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode ) {
		return asTextBlock( aText, aColumnWidth, aHorizAlignTextMode, false );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned. It is assumed that the text does not contain any ANSI escape
	 * codes, else use the according method with the ANSI escape code flag.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, SplitTextMode aSplitTextMode, char aFillChar ) {
		return asTextBlock( aText, aColumnWidth, aHorizAlignTextMode, aSplitTextMode, aFillChar, false );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, boolean hasAnsiEscapeCodes ) {
		return asTextBlock( aText, aColumnWidth, null, null, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, SplitTextMode aSplitTextMode, boolean hasAnsiEscapeCodes ) {
		return asTextBlock( aText, aColumnWidth, null, aSplitTextMode, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, boolean hasAnsiEscapeCodes ) {
		return asTextBlock( aText, aColumnWidth, aHorizAlignTextMode, null, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Cuts the {@link String} into sub{@link String} instances with the maximum
	 * given length. The array of the resulting {@link String} instances is
	 * returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, SplitTextMode aSplitTextMode, char aFillChar, boolean hasAnsiEscapeCodes ) {
		if ( aText == null ) {
			return null;
		}
		if ( aSplitTextMode == null ) {
			aSplitTextMode = SplitTextMode.AT_FIXED_WIDTH;
		}
		switch ( aSplitTextMode ) {
		case AT_END_OF_LINE:
			return toSplitAtEndOfLineTextBlock( aText, aColumnWidth, aHorizAlignTextMode, aFillChar, hasAnsiEscapeCodes );
		case AT_FIRST_END_OF_LINE:
			return toSplitAtFirstEndOfLineTextBlock( aText, aColumnWidth, aHorizAlignTextMode, aFillChar, hasAnsiEscapeCodes );
		case AT_SPACE:
			return toSplitAtSpaceTextBlock( aText, aColumnWidth, aHorizAlignTextMode, aFillChar, hasAnsiEscapeCodes );
		case AT_FIXED_WIDTH:
		default:
			return toSplitAtFixedWidthTextBlock( aText, aColumnWidth, aHorizAlignTextMode, aFillChar, hasAnsiEscapeCodes );
		}
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, boolean hasAnsiEscapeCodes ) {
		return asTextBlock( aText, aColumnWidth, null, null, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, SplitTextMode aSplitTextMode, boolean hasAnsiEscapeCodes ) {
		return asTextBlock( aText, aColumnWidth, null, aSplitTextMode, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, boolean hasAnsiEscapeCodes ) {
		return asTextBlock( aText, aColumnWidth, aHorizAlignTextMode, null, ' ', hasAnsiEscapeCodes );
	}

	/**
	 * Cuts the {@link String} array into sub{@link String} instances with the
	 * maximum given length. The array of the resulting {@link String} instances
	 * is returned.
	 * 
	 * @param aText The {@link String} text line to be cut into sub
	 *        {@link String} instances ( {@link String} array).
	 * @param aColumnWidth The maximum length of a line returned in the
	 *        {@link String} array.
	 * @param aHorizAlignTextMode The {@link HorizAlignTextMode} specifies on
	 *        how a line is to be aligned in the result.
	 * @param aSplitTextMode Depending on which value is passed, a line is split
	 *        into sub{@link String} instances preferably reckoning an end of a
	 *        line ( {@link SplitTextMode#AT_END_OF_LINE}), preferably reckoning
	 *        the spaces ({@link SplitTextMode#AT_SPACE}) or exactly reckoning a
	 *        given width ({@link SplitTextMode#AT_FIXED_WIDTH}).
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return A {@link String} array with the sub{@link String} instances.
	 */
	public static String[] asTextBlock( String[] aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, SplitTextMode aSplitTextMode, char aFillChar, boolean hasAnsiEscapeCodes ) {
		final List<String> theText = new ArrayList<>();
		String eLine;
		for ( int i = 0; i < aText.length; i++ ) {
			eLine = aText[i];
			Collections.addAll( theText, TextBlockBuilder.asTextBlock( eLine, aColumnWidth, aHorizAlignTextMode, aSplitTextMode, aFillChar, hasAnsiEscapeCodes ) );
			if ( i < aText.length - 1 ) {
				theText.add( new TextLineBuilder().withLineChar( ' ' ).withColumnWidth( theText.get( 0 ).length() ).toString() );
			}
		}
		return theText.toArray( new String[theText.size()] );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a single line by replacing all new lines, line feeds and
	 * tabulators from the given {@link String} by full stops or spaces
	 * (heuristic) so that it is displayed in one single line.
	 * 
	 * @param aText The {@link String} from which to replace all characters
	 *        responsible for line wrapping and stuff.
	 * 
	 * @return A {@link String} which will render in a single line.
	 */
	private static String toNonBreakingLine( String aText ) {
		if ( aText == null ) {
			return null;
		}
		final StringBuilder theStringBuffer = new StringBuilder();
		final StringTokenizer e = new StringTokenizer( aText, LINE_BREAKS );
		String eText;
		while ( e.hasMoreTokens() ) {
			eText = e.nextToken();
			theStringBuffer.append( eText );
			if ( e.hasMoreTokens() && ( theStringBuffer.length() > 0 ) && ( theStringBuffer.charAt( theStringBuffer.length() - 1 ) != ' ' ) ) {
				// ----------------
				// Has an EOL char?
				// ----------------
				if ( ( !hasAtEndOfLine( theStringBuffer, CharSet.LINE_BREAK_MARKERS ) ) && ( !hasAtEndOfLine( theStringBuffer, CharSet.OPENING_BRACES ) ) && ( theStringBuffer.charAt( theStringBuffer.length() - 1 ) != '.' ) ) {
					theStringBuffer.append( '.' );
				}
				theStringBuffer.append( ' ' );
			}
		}
		return theStringBuffer.toString();
	}

	/**
	 * Tests whether the given text ends with one of the provided characters at
	 * the end of the line.
	 * 
	 * @param aBuffer The buffer which's end is to be inspected.
	 * @param aEolChars The EOL chars for which to test.
	 * 
	 * @return True in case the buffer ends with one of the provided characters,
	 *         else false.
	 */
	private static boolean hasAtEndOfLine( StringBuilder aBuffer, CharSet aEolChars ) {
		boolean hasEndOfLineChar;
		hasEndOfLineChar = false;
		out: for ( int i = 0; i < aEolChars.getCharSet().length; i++ ) {
			if ( aBuffer.charAt( aBuffer.length() - 1 ) == aEolChars.getCharSet()[i] ) {
				hasEndOfLineChar = true;
				break out;
			}
		}
		return hasEndOfLineChar;
	}

	/**
	 * To split at space text block.
	 *
	 * @param aText the text
	 * @param aColumnWidth the length
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string[]
	 */
	private static String[] toSplitAtSpaceTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, char aFillChar, boolean hasAnsiEscapeCodes ) {
		final List<String> theStringList = new ArrayList<>();
		String eSubstring;
		String eText = toNonBreakingLine( aText );
		while ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			eText = toTruncatePrefixedSpaces( eText, hasAnsiEscapeCodes );
			eSubstring = toLastSpaceIndex( eText, aColumnWidth, hasAnsiEscapeCodes );
			eSubstring = toTruncateTrailingSpaces( eSubstring, hasAnsiEscapeCodes );
			theStringList.add( HorizAlignTextBuilder.asAligned( eSubstring, aColumnWidth, aFillChar, aHorizAlignTextMode, hasAnsiEscapeCodes ) );
			eText = eText.substring( eSubstring.length() );
		}
		if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > 0 ) {
			eText = toTruncatePrefixedSpaces( eText, hasAnsiEscapeCodes );
			eText = toTruncateTrailingSpaces( eText, hasAnsiEscapeCodes );
			if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > 0 ) {
				theStringList.add( HorizAlignTextBuilder.asAligned( eText, aColumnWidth, aFillChar, aHorizAlignTextMode, hasAnsiEscapeCodes ) );
			}
		}
		final String[] theSubstringArray;
		if ( theStringList.size() == 0 ) {
			theSubstringArray = new String[] { aText };
		}
		else {
			theSubstringArray = new String[theStringList.size()];
			for ( int i = 0; i < theSubstringArray.length; i++ ) {
				theSubstringArray[i] = theStringList.get( i );
			}
		}
		return theSubstringArray;
	}

	/**
	 * To last space index.
	 *
	 * @param aText the text
	 * @param aColumnWidth the length
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string
	 */
	private static String toLastSpaceIndex( String aText, int aColumnWidth, boolean hasAnsiEscapeCodes ) {

		String eText = aText;
		int index;
		while ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			index = lastIndexOf( eText, CharSet.SPACE_MARKERS.getCharSet() );
			if ( index == -1 ) {
				return aText.substring( 0, aColumnWidth );
			}
			eText = eText.substring( 0, index );
		}
		return eText;
	}

	private static int lastIndexOf( String aText, char[] aCharSet ) {
		for ( int i = aText.length() - 1; i > 0; i-- ) {
			for ( int l = 0; l < aCharSet.length; l++ ) {
				if ( aText.charAt( i ) == CharSet.SPACE_MARKERS.getCharSet()[l] ) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * To split at fixed width text block.
	 *
	 * @param aText the text
	 * @param aColumnWidth the length
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string[]
	 */
	private static String[] toSplitAtFixedWidthTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, char aFillChar, boolean hasAnsiEscapeCodes ) {
		final List<String> theStringList = new ArrayList<>();
		String eSubstring;
		String eText = aText;
		while ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			eSubstring = eText.substring( 0, aColumnWidth );
			eText = eText.substring( aColumnWidth );
			theStringList.add( new HorizAlignTextBuilder().withText( eSubstring ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
		}
		if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > 0 ) {
			theStringList.add( new HorizAlignTextBuilder().withText( eText ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
		}
		final String[] theSubstringArray;
		if ( theStringList.size() == 0 ) {
			theSubstringArray = new String[] { aText };
		}
		else {
			theSubstringArray = new String[theStringList.size()];
			for ( int i = 0; i < theSubstringArray.length; i++ ) {
				theSubstringArray[i] = theStringList.get( i );
			}
		}
		return theSubstringArray;
	}

	/**
	 * To split at first end of line text block.
	 *
	 * @param aText the text
	 * @param aColumnWidth the length
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string[]
	 */
	private static String[] toSplitAtFirstEndOfLineTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, char aFillChar, boolean hasAnsiEscapeCodes ) {
		final List<String> theStringList = new ArrayList<>();
		String eSubstring;
		String eText = aText;
		int eLineBreakIndex;
		int eTempIndex;
		while ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			eSubstring = eText.substring( 0, aColumnWidth + 1 );
			eSubstring = toTruncateTrailingSpaces( eSubstring, hasAnsiEscapeCodes );
			eLineBreakIndex = -1;

			for ( int i = 0; i < CharSet.END_OF_LINE.getCharSet().length; i++ ) {
				eTempIndex = eSubstring.indexOf( CharSet.END_OF_LINE.getCharSet()[i] );
				if ( ( eLineBreakIndex == -1 ) || ( ( eTempIndex != -1 ) && ( eTempIndex < eLineBreakIndex ) ) ) {
					eLineBreakIndex = eTempIndex;
				}
			}

			if ( eLineBreakIndex > aColumnWidth ) {
				for ( int i = 0; i < CharSet.LINE_BREAK_MARKERS.getCharSet().length; i++ ) {
					eTempIndex = eSubstring.lastIndexOf( CharSet.LINE_BREAK_MARKERS.getCharSet()[i] );
					if ( ( eLineBreakIndex == -1 ) || ( ( eTempIndex != -1 ) && ( eTempIndex > eLineBreakIndex ) ) ) {
						eLineBreakIndex = eTempIndex;
					}
				}
			}

			if ( eLineBreakIndex != -1 ) {
				eSubstring = eText.substring( 0, eLineBreakIndex + 1 );
				eSubstring = toTruncateTrailingSpaces( eSubstring, hasAnsiEscapeCodes );
				if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > eLineBreakIndex ) { // eText.length() - 1
					eText = eText.substring( eLineBreakIndex + 1 );
					eText = toTruncatePrefixedSpaces( eText, hasAnsiEscapeCodes );
				}
				else {
					eText = "";
				}
			}
			else {
				if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) { // eText.length() - 1
					eSubstring = eText.substring( 0, aColumnWidth );
					eText = eText.substring( aColumnWidth );
				}
				else {
					eText = "";
				}
			}
			theStringList.add( new HorizAlignTextBuilder().withText( toNonBreakingLine( eSubstring ) ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
		}
		if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > 0 ) {
			do {
				eLineBreakIndex = -1;
				for ( int i = 0; i < CharSet.END_OF_LINE.getCharSet().length; i++ ) {
					eTempIndex = eText.indexOf( CharSet.END_OF_LINE.getCharSet()[i] );
					if ( ( eLineBreakIndex == -1 ) || ( ( eTempIndex != -1 ) && ( eTempIndex < eLineBreakIndex ) ) ) {
						eLineBreakIndex = eTempIndex;
					}
				}
				if ( eLineBreakIndex != -1 ) {
					eSubstring = eText.substring( 0, eLineBreakIndex + 1 );
					eSubstring = toTruncateTrailingSpaces( eSubstring, hasAnsiEscapeCodes );
					if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > eLineBreakIndex ) { // eText.length() - 1
						eText = eText.substring( eLineBreakIndex + 1 );
						eText = toTruncatePrefixedSpaces( eText, hasAnsiEscapeCodes );
						theStringList.add( new HorizAlignTextBuilder().withText( toNonBreakingLine( eSubstring ) ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
					}
					else {
						eText = "";
					}
				}

			} while ( eLineBreakIndex != -1 );

		}

		if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > 0 ) {
			theStringList.add( new HorizAlignTextBuilder().withText( toNonBreakingLine( eText ) ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
		}

		final String[] theSubstringArray;
		if ( theStringList.size() == 0 ) {
			theSubstringArray = new String[] { aText };
		}
		else {
			theSubstringArray = new String[theStringList.size()];
			for ( int i = 0; i < theSubstringArray.length; i++ ) {
				theSubstringArray[i] = theStringList.get( i );
			}
		}
		return theSubstringArray;
	}

	/**
	 * To split at end of line text block.
	 *
	 * @param aText the text
	 * @param aColumnWidth the length
	 * @param aHorizAlignTextMode the horiz align text mode
	 * @param aFillChar The fill char to be used when filling up the
	 *        {@link String}.
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string[]
	 */
	private static String[] toSplitAtEndOfLineTextBlock( String aText, int aColumnWidth, HorizAlignTextMode aHorizAlignTextMode, char aFillChar, boolean hasAnsiEscapeCodes ) {
		final List<String> theStringList = new ArrayList<>();

		String eSubstring;
		String eText = aText;
		int eLineBreakIndex;
		int eTempIndex;
		while ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) {
			eSubstring = eText.substring( 0, aColumnWidth + 1 );
			eSubstring = toTruncateTrailingSpaces( eSubstring, hasAnsiEscapeCodes );
			eLineBreakIndex = -1;
			for ( int i = 0; i < CharSet.LINE_BREAK_MARKERS.getCharSet().length; i++ ) {
				eTempIndex = eSubstring.lastIndexOf( CharSet.LINE_BREAK_MARKERS.getCharSet()[i] );
				if ( ( eLineBreakIndex == -1 ) || ( ( eTempIndex != -1 ) && ( eTempIndex > eLineBreakIndex ) ) ) {
					eLineBreakIndex = eTempIndex;
				}
			}

			if ( eLineBreakIndex != -1 ) {
				eSubstring = eText.substring( 0, eLineBreakIndex + 1 );
				eSubstring = toTruncateTrailingSpaces( eSubstring, hasAnsiEscapeCodes );
				if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > eLineBreakIndex ) { // eText.length() - 1
					eText = eText.substring( eLineBreakIndex + 1 );
					eText = toTruncatePrefixedSpaces( eText, hasAnsiEscapeCodes );
				}
				else {
					eText = "";
				}
			}
			else {
				if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > aColumnWidth ) { // eText.length() - 1
					eSubstring = eText.substring( 0, aColumnWidth );
					eText = eText.substring( aColumnWidth );
				}
				else {
					eText = "";
				}
			}
			theStringList.add( new HorizAlignTextBuilder().withText( toNonBreakingLine( eSubstring ) ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
		}
		if ( AnsiEscapeCode.toLength( eText, hasAnsiEscapeCodes ) > 0 ) {
			theStringList.add( new HorizAlignTextBuilder().withText( toNonBreakingLine( eText ) ).withColumnWidth( aColumnWidth ).withFillChar( aFillChar ).withHorizAlignTextMode( aHorizAlignTextMode ).toString() );
		}

		final String[] theSubstringArray;
		if ( theStringList.size() == 0 ) {
			theSubstringArray = new String[] { aText };
		}
		else {
			theSubstringArray = new String[theStringList.size()];
			for ( int i = 0; i < theSubstringArray.length; i++ ) {
				theSubstringArray[i] = theStringList.get( i );
			}
		}
		return theSubstringArray;
	}

	/**
	 * To truncate trailing spaces.
	 *
	 * @param aText the text
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string
	 */
	private static String toTruncateTrailingSpaces( String aText, boolean hasAnsiEscapeCodes ) {
		while ( AnsiEscapeCode.toLength( aText, hasAnsiEscapeCodes ) > 0 && aText.charAt( aText.length() - 1 ) == ' ' ) {
			aText = aText.substring( 0, AnsiEscapeCode.toLength( aText, hasAnsiEscapeCodes ) - 1 );
		}
		return aText;
	}

	/**
	 * To truncate prefixed spaces.
	 *
	 * @param aText the text
	 * @param hasAnsiEscapeCodes Whether to take any (non printable) ANSI escape
	 *        codes into account which would, when not being considered, cause
	 *        wrong results.
	 * 
	 * @return the string
	 */
	private static String toTruncatePrefixedSpaces( String aText, boolean hasAnsiEscapeCodes ) {
		if ( AnsiEscapeCode.toLength( aText, hasAnsiEscapeCodes ) > 1 ) {
			while ( aText.charAt( 0 ) == ' ' ) {
				aText = aText.substring( 1 );
			}
		}
		return aText;
	}
}
