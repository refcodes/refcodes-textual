// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.refcodes.data.TextBorder;
import org.refcodes.graphical.BoxBorderMode;
import org.refcodes.graphical.BoxBorderModeAccessor.BoxBorderModeBuilder;
import org.refcodes.graphical.BoxBorderModeAccessor.BoxBorderModeProperty;
import org.refcodes.textual.TextBoxGridAccessor.TextBoxGridBuilder;
import org.refcodes.textual.TextBoxGridAccessor.TextBoxGridProperty;

/**
 * "Draws" a border around a text. The text border can be provided by adjusting
 * properties such as the {@link TableStyle} or the {@link BoxBorderMode}. When
 * providing a {@link TableStyle}, then the {@link TableStyle#getBody()}
 * characters are used to draw the border.
 */
public class TextBorderBuilder extends AbstractText<TextBorderBuilder> implements BoxBorderModeProperty, BoxBorderModeBuilder<TextBorderBuilder>, TextBoxGridProperty, TextBoxGridBuilder<TextBorderBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char _borderChar = '#';
	private BoxBorderMode _boxBorderMode = BoxBorderMode.ALL;
	private TextBoxGrid _textBoxGrid = TextBoxStyle.ASCII;
	private int _borderWidth = 1;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Convenience method for {@link #setTextBoxGrid(TextBoxGrid)} to set the
	 * {@link TextBoxGrid} from a {@link TextBoxStyle} value
	 * 
	 * @param aTextBoxStyle The {@link TextBoxStyle} which's {@link TextBoxGrid}
	 *        is to be set.
	 */
	public void setTextBoxStyle( TextBoxStyle aTextBoxStyle ) {
		setTextBoxGrid( aTextBoxStyle );
	}

	/**
	 * Convenience method for {@link #withTextBoxGrid(TextBoxGrid)} to set the
	 * {@link TextBoxGrid} from a {@link TextBoxStyle} value
	 * 
	 * @param aTextBoxStyle The {@link TextBoxStyle} which's {@link TextBoxGrid}
	 *        is to be set.
	 * 
	 * @return This instance as of the builder pattern,
	 */
	public TextBorderBuilder withTextBoxStyle( TextBoxStyle aTextBoxStyle ) {
		setTextBoxGrid( aTextBoxStyle );
		return this;
	}

	/**
	 * Sets the border char for the border char property.
	 * 
	 * @param aBorderChar The border char to be stored by the text align mode
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public TextBorderBuilder withBorderChar( char aBorderChar ) {
		setBorderChar( aBorderChar );
		return this;
	}

	/**
	 * Sets the border width for the border width property.
	 * 
	 * @param aBorderWidth The border width to be stored by the text align mode
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public TextBorderBuilder withBorderWidth( int aBorderWidth ) {
		setBorderWidth( aBorderWidth );
		return this;
	}

	/**
	 * This is a convenience method for easily instantiating the according
	 * builder.
	 * 
	 * @return an instance (using a default implementation) of this builder
	 */
	static TextBorderBuilder build() {
		return new TextBorderBuilder();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BoxBorderMode getBoxBorderMode() {
		return _boxBorderMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBoxBorderMode( BoxBorderMode aBoxBorderMode ) {
		_boxBorderMode = aBoxBorderMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBorderBuilder withBoxBorderMode( BoxBorderMode aBoxBorderMode ) {
		setBoxBorderMode( aBoxBorderMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBoxGrid getTextBoxGrid() {
		return _textBoxGrid;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
		_textBoxGrid = aTextBoxGrid;
		_borderChar = Character.UNASSIGNED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBorderBuilder withTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
		setTextBoxGrid( aTextBoxGrid );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBorderBuilder withTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
		setTextBoxGrid( aTextBoxStyle );
		return this;
	}

	/**
	 * Retrieves the border char from the border char property.
	 * 
	 * @return The border char stored by the border char property.
	 */
	public char getBorderChar() {
		return _borderChar;
	}

	/**
	 * Sets the border char for the border char property.
	 * 
	 * @param aBorderChar The border char to be stored by the text align mode
	 *        property.
	 */
	public void setBorderChar( char aBorderChar ) {
		_borderChar = aBorderChar;
		_textBoxGrid = null;
	}

	/**
	 * Retrieves the border width from the border width property.
	 * 
	 * @return The border width stored by the border width property.
	 */
	public int getBorderWidth() {
		return _borderWidth;
	}

	/**
	 * Sets the border width for the border width property.
	 * 
	 * @param aBorderWidth The border width to be stored by the text align mode
	 *        property.
	 */
	public void setBorderWidth( int aBorderWidth ) {
		_borderWidth = aBorderWidth;
	}

	/**
	 * The {@link String}s being build by the builder upon the settings of the
	 * attributes.
	 * 
	 * @return The according resulting {@link String} array
	 */
	@Override
	public String[] toStrings() {
		return toStrings( getText() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toStrings( String... aText ) {
		// @formatter:off
		if ( _textBoxGrid != null ) { 
			return toBorder( aText, 
				_textBoxGrid.getTopLeftEdge(), 
				_textBoxGrid.getTopRightEdge(), 
				_textBoxGrid.getBottomLeftEdge(), 
				_textBoxGrid.getBottomRightEdge(), 
				_textBoxGrid.getTopLine(), 
				_textBoxGrid.getRightLine(), 
				_textBoxGrid.getTopLine(), 
				_textBoxGrid.getLeftLine(), 
				_boxBorderMode ); }
		// @formatter:on
		return toBorder( aText, _borderWidth, _borderChar, _boxBorderMode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Adds the according border with the given char and the provided witdh to
	 * the provided text.
	 *
	 * @param aText The text to which the border is to be applied.
	 * @param aBorderWidth The width of the border to be drawn.
	 * @param aBorderChar The character of the border to be drawn.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toLeftBorder( String[] aText, int aBorderWidth, char aBorderChar ) {
		final String theBorder = toLine( aBorderWidth, aBorderChar );
		final String[] theText = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theText[i] = theBorder + aText[i];
		}
		return theText;
	}

	/**
	 * Adds the according border with the given char and the provided width to
	 * the provided text.
	 *
	 * @param aText The text to which the border is to be applied.
	 * @param aBorderWidth The width of the border to be drawn.
	 * @param aBorderChar The character of the border to be drawn.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toRightBorder( String[] aText, int aBorderWidth, char aBorderChar ) {
		final String theBorder = toLine( aBorderWidth, aBorderChar );
		final String[] theText = new String[aText.length];
		for ( int i = 0; i < aText.length; i++ ) {
			theText[i] = aText[i] + theBorder;
		}
		return theText;
	}

	/**
	 * Adds the according border with the given char and the provided width to
	 * the provided text.
	 *
	 * @param aText The text to which the border is to be applied.
	 * @param aBorderWidth The width of the border to be drawn.
	 * @param aBorderChar The character of the border to be drawn.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toTopBorder( String[] aText, int aBorderWidth, char aBorderChar ) {
		final String[] theText = new String[aText.length + aBorderWidth];
		final String theBorder = toLine( aText[0].length(), aBorderChar );
		for ( int i = 0; i < aBorderWidth; i++ ) {
			theText[i] = theBorder;
		}
		for ( int i = 0; i < aText.length; i++ ) {
			theText[i + aBorderWidth] = aText[i];
		}
		return theText;
	}

	/**
	 * Adds the according border with the given char and the provided width to
	 * the provided text.
	 *
	 * @param aText The text to which the border is to be applied.
	 * @param aBorderWidth The width of the border to be drawn.
	 * @param aBorderChar The character of the border to be drawn.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toBottomBorder( String[] aText, int aBorderWidth, char aBorderChar ) {
		final String[] theText = new String[aText.length + aBorderWidth];
		for ( int i = 0; i < aText.length; i++ ) {
			theText[i] = aText[i];
		}
		final String theBorder = toLine( aText[aText.length - 1].length(), aBorderChar );
		for ( int i = 0; i < aBorderWidth; i++ ) {
			theText[aText.length + i] = theBorder;
		}
		return theText;
	}

	/**
	 * Adds the according border with the given char and the provided width to
	 * the provided text.
	 *
	 * @param aText The text to which the border is to be applied.
	 * @param aBorderWidth The width of the border to be drawn.
	 * @param aBorderChar The character of the border to be drawn.
	 * @param aBoxBorderMode the box border mode
	 * 
	 * @return The text with the according border being applied.
	 */
	public static String[] toBorder( String[] aText, int aBorderWidth, char aBorderChar, BoxBorderMode aBoxBorderMode ) {
		if ( aBoxBorderMode.isLeftBorder() ) {
			aText = toLeftBorder( aText, aBorderWidth, aBorderChar );
		}
		if ( aBoxBorderMode.isRightBorder() ) {
			aText = toRightBorder( aText, aBorderWidth, aBorderChar );
		}
		if ( aBoxBorderMode.isTopBorder() ) {
			aText = toTopBorder( aText, aBorderWidth, aBorderChar );
		}
		if ( aBoxBorderMode.isBottomBorder() ) {
			aText = toBottomBorder( aText, aBorderWidth, aBorderChar );
		}
		return aText;
	}

	/**
	 * Adds a predefined border according border to the provided text.
	 *
	 * @param aText The text to which the border is to be applied.
	 * @param aTopLeftBorderChar The top left border's character.
	 * @param aTopRightBorderChar The top right border's character.
	 * @param aBottomLeftBorderChar The bottom left border's character.
	 * @param aBottomRightBorderChar The bottom right border's character.
	 * @param aTopBorderChar the top border char
	 * @param aRightBorderChar the right border char
	 * @param aBottomBorderChar the bottom border char
	 * @param aLeftBorderChar the left border char
	 * @param aBoxBorderMode the box border mode
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toBorder( String[] aText, char aTopLeftBorderChar, char aTopRightBorderChar, char aBottomLeftBorderChar, char aBottomRightBorderChar, char aTopBorderChar, char aRightBorderChar, char aBottomBorderChar, char aLeftBorderChar, BoxBorderMode aBoxBorderMode ) {
		if ( aBoxBorderMode.isLeftBorder() ) {
			aText = toLeftBorder( aText, 1, aLeftBorderChar );
		}
		if ( aBoxBorderMode.isRightBorder() ) {
			aText = toRightBorder( aText, 1, aRightBorderChar );
		}
		if ( aBoxBorderMode.isTopBorder() ) {
			aText = toTopBorder( aText, 1, aTopBorderChar );
		}
		if ( aBoxBorderMode.isBottomBorder() ) {
			aText = toBottomBorder( aText, 1, aBottomBorderChar );
		}
		if ( aBoxBorderMode.isLeftBorder() && aBoxBorderMode.isTopBorder() ) {
			aText[0] = aTopLeftBorderChar + aText[0].substring( 1 );
		}
		if ( aBoxBorderMode.isLeftBorder() && aBoxBorderMode.isBottomBorder() ) {
			aText[aText.length - 1] = aBottomLeftBorderChar + aText[aText.length - 1].substring( 1 );
		}
		if ( aBoxBorderMode.isRightBorder() && aBoxBorderMode.isTopBorder() ) {
			aText[0] = aText[0].substring( 0, aText[0].length() - 1 ) + aTopRightBorderChar;
		}
		if ( aBoxBorderMode.isRightBorder() && aBoxBorderMode.isBottomBorder() ) {
			aText[aText.length - 1] = aText[aText.length - 1].substring( 0, aText[aText.length - 1].length() - 1 ) + aBottomRightBorderChar;
		}
		return aText;
	}

	/**
	 * Adds a predefined "single-line" border to the provided text.
	 * 
	 * @param aText The text to which the border is to be applied.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toSingleBorder( String[] aText ) {
		return toBorder( aText, TextBorder.SINGLE_TOP_LEFT_EDGE.getChar(), TextBorder.SINGLE_TOP_RIGHT_EDGE.getChar(), TextBorder.SINGLE_BOTTOM_LEFT_EDGE.getChar(), TextBorder.SINGLE_BOTTOM_RIGHT_EDGE.getChar(), TextBorder.SINGLE_HORIZONTAL_LINE.getChar(), TextBorder.SINGLE_VERTICAL_LINE.getChar(), TextBorder.SINGLE_HORIZONTAL_LINE.getChar(), TextBorder.SINGLE_VERTICAL_LINE.getChar(), BoxBorderMode.ALL );
	}

	/**
	 * Adds a predefined "double-line" border to the provided text.
	 * 
	 * @param aText The text to which the border is to be applied.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toDoubleBorder( String[] aText ) {
		return toBorder( aText, TextBorder.DOUBLE_TOP_LEFT_EDGE.getChar(), TextBorder.DOUBLE_TOP_RIGHT_EDGE.getChar(), TextBorder.DOUBLE_BOTTOM_LEFT_EDGE.getChar(), TextBorder.DOUBLE_BOTTOM_RIGHT_EDGE.getChar(), TextBorder.DOUBLE_HORIZONTAL_LINE.getChar(), TextBorder.DOUBLE_VERTICAL_LINE.getChar(), TextBorder.DOUBLE_HORIZONTAL_LINE.getChar(), TextBorder.DOUBLE_VERTICAL_LINE.getChar(), BoxBorderMode.ALL );
	}

	/**
	 * Adds a predefined "single-line" border to the provided text.
	 * 
	 * @param aText The text to which the border is to be applied.
	 * 
	 * @return The text with the according border being applied.
	 */
	protected static String[] toAsciiBorder( String[] aText ) {
		return toBorder( aText, TextBorder.ASCII_TOP_LEFT_EDGE.getChar(), TextBorder.ASCII_TOP_RIGHT_EDGE.getChar(), TextBorder.ASCII_BOTTOM_LEFT_EDGE.getChar(), TextBorder.ASCII_BOTTOM_RIGHT_EDGE.getChar(), TextBorder.ASCII_HORIZONTAL_LINE.getChar(), TextBorder.ASCII_VERTICAL_LINE.getChar(), TextBorder.ASCII_HORIZONTAL_LINE.getChar(), TextBorder.ASCII_VERTICAL_LINE.getChar(), BoxBorderMode.ALL );
	}

	/**
	 * Returns a {@link String} with the given length and containing only the
	 * provided fill character.
	 * 
	 * @param aLength The length to be reached.
	 * @param aFillChar The char to be used for filling up
	 * 
	 * @return The {@link String} filled with the fill character till the
	 *         provided length.
	 */
	protected static String toLine( int aLength, char aFillChar ) {
		if ( aLength <= 0 ) {
			return "";
		}
		final StringBuilder theBuffer = new StringBuilder();
		while ( theBuffer.length() < aLength ) {
			theBuffer.append( aFillChar );
		}
		return theBuffer.toString();
	}
}
