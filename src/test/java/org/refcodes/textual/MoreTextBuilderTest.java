// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class MoreTextBuilderTest.
 */
public class MoreTextBuilderTest {
	/**
	 * Test to more.
	 */
	@Test
	public void testToMore() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "1" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "1" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123" ).withColumnWidth( 2 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123" ).withColumnWidth( 2 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "1234" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "1234" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123456" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123456" ).withColumnWidth( 3 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123456" ).withColumnWidth( 5 ).withMoreText( "..." ).toString() );
			System.out.println( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123456" ).withColumnWidth( 5 ).withMoreText( "..." ).toString() );
		}
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "1" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "1" );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "1" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "1" );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "123" );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "123" );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123" ).withColumnWidth( 2 ).withMoreText( "..." ).toString(), ".." );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123" ).withColumnWidth( 2 ).withMoreText( "..." ).toString(), ".." );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "1234" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "..." );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "1234" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "..." );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123456" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "..." );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123456" ).withColumnWidth( 3 ).withMoreText( "..." ).toString(), "..." );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.LEFT ).withText( "123456" ).withColumnWidth( 5 ).withMoreText( "..." ).toString(), "...56" );
		assertEquals( new MoreTextBuilder().withMoreTextMode( MoreTextMode.RIGHT ).withText( "123456" ).withColumnWidth( 5 ).withMoreText( "..." ).toString(), "12..." );
	}
}
