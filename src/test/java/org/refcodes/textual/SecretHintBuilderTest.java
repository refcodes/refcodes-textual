// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class SecretHintBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSecretHintText() {
		final String thePassword1 = new SecretHintBuilder().withText( "123456" ).toString();
		final String thePassword2 = new SecretHintBuilder().withText( "12345678" ).toString();
		final String thePassword3 = new SecretHintBuilder().withText( "1234567890" ).toString();
		final String thePassword4 = new SecretHintBuilder().withText( "123456789012" ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePassword1 );
			System.out.println( thePassword2 );
			System.out.println( thePassword3 );
			System.out.println( thePassword4 );
		}
		assertEquals( "" + thePassword1.charAt( 0 ), "*" );
		assertEquals( 6, thePassword1.length() );
		assertEquals( "" + thePassword1.charAt( thePassword1.length() - 1 ), "*" );
		assertEquals( "" + thePassword2.charAt( 0 ), "*" );
		assertEquals( 8, thePassword2.length() );
		assertEquals( "" + thePassword2.charAt( thePassword2.length() - 1 ), "*" );
		assertEquals( "" + thePassword3.charAt( 0 ), "1" );
		assertEquals( 10, thePassword3.length() );
		assertEquals( "" + thePassword3.charAt( thePassword3.length() - 1 ), "0" );
		assertEquals( "" + thePassword4.charAt( 0 ), "1" );
		assertEquals( 12, thePassword4.length() );
		assertEquals( "" + thePassword4.charAt( thePassword4.length() - 1 ), "2" );
	}

	@Test
	public void testSecretHintTexts() {
		String thePasswords = new SecretHintBuilder().withText( "1", "12", "123", "1234", "12345", "123456", "1234567", "12345678", "123456789", "1234567890" ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( thePasswords );
		}
		thePasswords = thePasswords.replaceAll( "\r", "" ); // Fuck DOS
		assertEquals( thePasswords, "*\n" + "**\n" + "***\n" + "****\n" + "*****\n" + "******\n" + "*******\n" + "********\n" + "*********\n" + "1********0" );
	}
}
