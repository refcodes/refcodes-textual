// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class OverwriteTextBuilderTest.
 */
public class OverwriteTextBuilderTest {
	/**
	 * Test text overwrite builder.
	 */
	@Test
	public void testTextOverwriteBuilder() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "__________" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "__________" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "____" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "____" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "__" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "__" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "" ).withOverwritingText( "Text" ).toString() );
			System.out.println( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "" ).withOverwritingText( "Text" ).toString() );
		}
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "__________" ).withOverwritingText( "Text" ).toString(), "Text______" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "__________" ).withOverwritingText( "Text" ).toString(), "______Text" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "____" ).withOverwritingText( "Text" ).toString(), "Text" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "____" ).withOverwritingText( "Text" ).toString(), "Text" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "__" ).withOverwritingText( "Text" ).toString(), "Te" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "__" ).withOverwritingText( "Text" ).toString(), "xt" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.LEFT ).withText( "" ).withOverwritingText( "Text" ).toString(), "" );
		assertEquals( new OverwriteTextBuilder().withOverwriteTextMode( OverwriteTextMode.RIGHT ).withText( "" ).withOverwritingText( "Text" ).toString(), "" );
	}
}
