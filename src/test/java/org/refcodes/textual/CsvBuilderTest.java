// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class CsvBuilderTest.
 */
public class CsvBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] CSV_VALUES_1 = new String[] { "aaa", "\"bbb", "ccc\"", "\"ddd\"", "e\"ee", "\"f\"ff", "g\"gg\"", "h,hh", "\"i,ii", "j,jj\"", "\"k,kk\"", "l,l\"l", "m\"\"\"m,\"\"m" };
	private static final String CSV_TEXT_1 = "aaa,\"\"\"bbb\",\"ccc\"\"\",\"\"\"ddd\"\"\",\"e\"\"ee\",\"\"\"f\"\"ff\",\"g\"\"gg\"\"\",\"h,hh\",\"\"\"i,ii\",\"j,jj\"\"\",\"\"\"k,kk\"\"\",\"l,l\"\"l\",\"m\"\"\"\"\"\"m,\"\"\"\"m\"";
	// "12088860108"|""|"12088860108"|""
	private static final String CSV_TEXT_2_IN = "\"12088860108\"|\"\"|\"12088860108\"|\"\"";
	private static final String CSV_TEXT_2_OUT = "12088860108||12088860108|";
	private static final String CSV_TEXT_3_IN = "\"|\"|\".\"|\",\"|\"-\"";
	private static final String CSV_TEXT_3_OUT = "";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testCsvNulls1() {
		final CsvBuilder theBuilder = new CsvBuilder().withDelimiter( ',' );
		final String[] theFields = theBuilder.toFields( ",1,2,3,4,5" );
		assertEquals( null, theFields[0] );
		assertEquals( "1", theFields[1] );
		assertEquals( "2", theFields[2] );
		assertEquals( "3", theFields[3] );
		assertEquals( "4", theFields[4] );
		assertEquals( "5", theFields[5] );
		assertEquals( 6, theFields.length );
	}

	@Test
	public void testCsvNulls2() {
		final CsvBuilder theBuilder = new CsvBuilder().withDelimiter( ',' );
		final String[] theFields = theBuilder.toFields( ",,2,3,4,5" );
		assertEquals( null, theFields[0] );
		assertEquals( null, theFields[1] );
		assertEquals( "2", theFields[2] );
		assertEquals( "3", theFields[3] );
		assertEquals( "4", theFields[4] );
		assertEquals( "5", theFields[5] );
		assertEquals( 6, theFields.length );
	}

	@Test
	public void testCsvNulls3() {
		final CsvBuilder theBuilder = new CsvBuilder().withDelimiter( ',' );
		final String[] theFields = theBuilder.toFields( "0,1,,3,4,5" );
		assertEquals( "0", theFields[0] );
		assertEquals( "1", theFields[1] );
		assertEquals( null, theFields[2] );
		assertEquals( "3", theFields[3] );
		assertEquals( "4", theFields[4] );
		assertEquals( "5", theFields[5] );
		assertEquals( 6, theFields.length );
	}

	@Test
	public void testCsvNulls4() {
		final CsvBuilder theBuilder = new CsvBuilder().withDelimiter( ',' );
		final String[] theFields = theBuilder.toFields( "0,1,,,4,5" );
		assertEquals( "0", theFields[0] );
		assertEquals( "1", theFields[1] );
		assertEquals( null, theFields[2] );
		assertEquals( null, theFields[3] );
		assertEquals( "4", theFields[4] );
		assertEquals( "5", theFields[5] );
		assertEquals( 6, theFields.length );
	}

	@Test
	public void testCsvNulls5() {
		final CsvBuilder theBuilder = new CsvBuilder().withDelimiter( ',' );
		final String[] theFields = theBuilder.toFields( "0,1,2,3,4," );
		assertEquals( "0", theFields[0] );
		assertEquals( "1", theFields[1] );
		assertEquals( "2", theFields[2] );
		assertEquals( "3", theFields[3] );
		assertEquals( "4", theFields[4] );
		assertEquals( null, theFields[5] );
		assertEquals( 6, theFields.length );
	}

	@Test
	public void testCsvNulls6() {
		final CsvBuilder theBuilder = new CsvBuilder().withDelimiter( ',' );
		final String[] theFields = theBuilder.toFields( ",1,,,4," );
		assertEquals( null, theFields[0] );
		assertEquals( "1", theFields[1] );
		assertEquals( null, theFields[2] );
		assertEquals( null, theFields[3] );
		assertEquals( "4", theFields[4] );
		assertEquals( null, theFields[5] );
		assertEquals( 6, theFields.length );
	}

	@Test
	public void testToSeparatedValues1() {
		// Reference:
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "EXPECTED = " + CSV_TEXT_1 );
		}
		final List<String> theList1 = Arrays.asList( CSV_VALUES_1 );
		Iterator<String> e = theList1.iterator();
		StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( "LIST     = " );
		while ( e.hasNext() ) {
			theBuffer.append( e.next() );
			if ( e.hasNext() ) {
				theBuffer.append( ';' );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBuffer.toString() );
		}
		// String 1 from List 1:
		final String theString1 = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theList1 ).withDelimiter( ',' ).toRecord();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theString1 );
		}
		assertEquals( CSV_TEXT_1, theString1 );
		// List 2 from String 1:
		theBuffer = new StringBuilder();
		theBuffer.append( "TO CSV   = " );
		final List<String> theList2 = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withRecord( theString1 ).withDelimiter( ',' ).toFields();
		e = theList2.iterator();
		while ( e.hasNext() ) {
			theBuffer.append( e.next() );
			if ( e.hasNext() ) {
				theBuffer.append( ';' );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBuffer.toString() );
		}
		assertEquals( theList1, theList2 );
		// String 2 from List 2:
		theBuffer = new StringBuilder();
		theBuffer.append( "STRING 2  = " );
		final String theString2 = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theList2 ).withDelimiter( ',' ).toRecord();
		theBuffer.append( theString2 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBuffer.toString() );
		}
		assertEquals( theString1, theString2 );
	}

	@Test
	public void testToSeparatedValues2() {
		// List 2 from String 1:
		final List<String> theList = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withRecord( CSV_TEXT_2_IN ).withDelimiter( '|' ).toFields();
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( "LIST 2    = " );
		final Iterator<String> e = theList.iterator();
		while ( e.hasNext() ) {
			theBuffer.append( e.next() );
			if ( e.hasNext() ) {
				theBuffer.append( ';' );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBuffer.toString() );
		}
		final String theOut = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theList ).withDelimiter( '|' ).toRecord();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "EXPECTED  = " + CSV_TEXT_2_OUT );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "CSV 2     = " + theOut );
		}
		assertEquals( CSV_TEXT_2_OUT, theOut );
	}

	@Disabled("Bug when CSV line begins with a separator in quotation marks")
	@Test
	public void testToSeparatedValues3() {
		// List 2 from String 1:
		final List<String> theList = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withRecord( CSV_TEXT_3_IN ).withDelimiter( '|' ).toFields();
		final StringBuilder theBuffer = new StringBuilder();
		theBuffer.append( "LIST 3    = " );
		final Iterator<String> e = theList.iterator();
		while ( e.hasNext() ) {
			theBuffer.append( e.next() );
			if ( e.hasNext() ) {
				theBuffer.append( ';' );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theBuffer.toString() );
		}
		final String theOut = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theList ).withDelimiter( '|' ).toRecord();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "EXPECTED  = " + CSV_TEXT_3_OUT );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "CSV 3     = " + theOut );
		}
		assertEquals( CSV_TEXT_3_OUT, theOut );
	}

	@Test
	public void testTextManipulation() {
		final String stringList = "element1, element2, element3";
		final CsvBuilder theBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED );
		final String[] stringArray = theBuilder.toFields( stringList );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Testing 'toStringArray()' and back 'toString()':" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String list  = <" + stringList + ">" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String list --> array --> list = <" + new VerboseTextBuilder().toString( stringArray ) + ">" );
		}
		assertEquals( stringList, theBuilder.toRecord( stringArray ) );
	}

	@Test
	public void testThreadSafeTextManipulation() {
		final String stringList = "element1, element2, element3";
		final CsvBuilder theBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED );
		final String[] stringArray = theBuilder.toFields( stringList );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Testing 'toStringArray()' and back 'toString()':" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String list  = <" + stringList + ">" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "String list --> array --> list = <" + new VerboseTextBuilder().toString( stringArray ) + ">" );
		}
		assertEquals( stringList, theBuilder.toRecord( stringArray ) );
	}

	@Test
	public void testEmptyHeader() {
		final CsvBuilder theBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED );
		final String[] theHeaderKeys = theBuilder.toFields( "" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theHeaderKeys ) );
		}
		assertEquals( 0, theHeaderKeys.length );
	}

	@Test
	public void testNullHeader() {
		final CsvBuilder theBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED );
		final String[] theHeaderKeys = theBuilder.toFields( null );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theHeaderKeys ) );
		}
		assertEquals( 0, theHeaderKeys.length );
	}

	@Test
	public void testWhiteSpaceHeader() {
		final CsvBuilder theBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withTrim( true );
		final String[] theHeaderKeys = theBuilder.toFields( "\n" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( VerboseTextBuilder.asString( theHeaderKeys ) );
		}
		assertEquals( 0, theHeaderKeys.length );
	}
}
