// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.InputStream;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.ext.corporate.LogoPixmap;
import org.refcodes.data.ext.corporate.LogoPixmapInputStreamFactory;
import org.refcodes.data.ext.symbols.SymbolPixmap;
import org.refcodes.data.ext.symbols.SymbolPixmapInputStreamFactory;
import org.refcodes.graphical.RgbPixmap;
import org.refcodes.graphical.RgbPixmapImageBuilder;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.runtime.Terminal;

public class AsciiArtBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToAsciiArt() {
		Font theFont = new Font( FontFamily.DIALOG_INPUT, FontStyle.ITALIC, 24 );
		String[] theLines = new AsciiArtBuilder().withText( "DEBUG" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
		theFont = new Font( FontFamily.SERIF, FontStyle.BOLD, 24 );
		theLines = new AsciiArtBuilder().withText( "FATAL!" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
		theFont = new Font( FontFamily.SERIF, FontStyle.BOLD, 24 );
		theLines = new AsciiArtBuilder().withText( "ONE-TWO-THREE" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).withAsciiColors( new char[] { ' ', '1', '2', '3' } ).toStrings();
		toOut( theLines );
		theFont = new Font( FontFamily.SERIF, FontStyle.BOLD, 22 );
		theLines = new AsciiArtBuilder().withText( "ABC123 MINIMUM" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).withAsciiColorPalette( AsciiColorPalette.MIN_LEVEL_GRAY ).toStrings();
		toOut( theLines );
		theFont = new Font( FontFamily.SERIF, FontStyle.BOLD, 22 );
		theLines = new AsciiArtBuilder().withText( "ABC123 NORMAL" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).withAsciiColorPalette( AsciiColorPalette.NORM_LEVEL_GRAY ).toStrings();
		toOut( theLines );
		theFont = new Font( FontFamily.SERIF, FontStyle.BOLD, 22 );
		theLines = new AsciiArtBuilder().withText( "ABC123 MAXIMUM" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).toStrings();
		toOut( theLines );
		theFont = new Font( FontFamily.SANS_SERIF, FontStyle.BOLD, -1 );
		theLines = new AsciiArtBuilder().withText( "Bag off!" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).toStrings();
		toOut( theLines );
		for ( String eString : new String[] { "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "COMMENT", "OFF" } ) {
			theFont = new Font( FontFamily.SANS_SERIF, FontStyle.BOLD, 24 );
			theLines = new AsciiArtBuilder().withText( eString ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
			toOut( theLines );
		}
	}

	@Test
	public void testToInverseAsciiArt() {
		final Font theFont = new Font( FontFamily.MONOSPACED, FontStyle.BOLD_ITALIC, -1 );
		String[] theLines = new AsciiArtBuilder().withText( "REFCODES" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
		theLines = new AsciiArtBuilder().withText( "FUNCODES" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
		theLines = new AsciiArtBuilder().withText( "COMCODES" ).withFont( theFont ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
	}

	@Test
	public void testToAsciiArtArray() {
		final Font theFont = new Font( FontFamily.MONOSPACED, FontStyle.BOLD, 26 );
		final String[] theLines = new AsciiArtBuilder().withFont( theFont ).withText( "Hallo", "JAKOB", ":-) :-)" ).withColumnWidth( 80 ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
	}

	@Test
	public void testSymbolPixmap() throws IOException {
		final InputStream in = new SymbolPixmapInputStreamFactory().create( SymbolPixmap.SKULL );
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder();
		theBuilder.withImageInputStream( in );
		final RgbPixmap thePixmap = theBuilder.toPixmap();
		final String[] theLines = new AsciiArtBuilder().withPixmapRatioMode( PixmapRatioMode.CONSOLE ).withColumnWidth( Terminal.toHeuristicWidth() ).withRgbPixmap( thePixmap ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
	}

	@Test
	public void testScaledSymbolPixmap() throws IOException {
		final InputStream in = new SymbolPixmapInputStreamFactory().create( SymbolPixmap.SKULL );
		final RgbPixmap thePixmap = new RgbPixmapImageBuilder().withImageInputStream( in ).toPixmap();
		final String[] theLines = new AsciiArtBuilder().withPixmapRatioMode( PixmapRatioMode.CONSOLE ).withColumnWidth( 48 ).withRgbPixmap( thePixmap ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
	}

	@Test
	public void testRefcodesPixmap() throws IOException {
		final InputStream in = new LogoPixmapInputStreamFactory().create( LogoPixmap.FUNCODES );
		final RgbPixmapImageBuilder theBuilder = new RgbPixmapImageBuilder();
		theBuilder.withImageInputStream( in );
		final RgbPixmap thePixmap = theBuilder.toPixmap();
		final String[] theLines = new AsciiArtBuilder().withPixmapRatioMode( PixmapRatioMode.CONSOLE ).withColumnWidth( Terminal.toHeuristicWidth() ).withRgbPixmap( thePixmap ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
	}

	@Test
	public void testScaledRefcodesPixmap() throws IOException {
		final InputStream in = new LogoPixmapInputStreamFactory().create( LogoPixmap.FUNCODES );
		final RgbPixmap thePixmap = new RgbPixmapImageBuilder().withImageInputStream( in ).toPixmap();
		final String[] theLines = new AsciiArtBuilder().withPixmapRatioMode( PixmapRatioMode.CONSOLE ).withColumnWidth( 48 ).withRgbPixmap( thePixmap ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).toStrings();
		toOut( theLines );
	}

	@Test
	public void testRefcodesPixmapAsString() throws IOException {
		final InputStream in = new LogoPixmapInputStreamFactory().create( LogoPixmap.FUNCODES );
		final RgbPixmap thePixmap = new RgbPixmapImageBuilder().withImageInputStream( in ).toPixmap();
		final String theLine = new AsciiArtBuilder().withPixmapRatioMode( PixmapRatioMode.CONSOLE ).withColumnWidth( Terminal.toHeuristicWidth() ).withRgbPixmap( thePixmap ).withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\n" + theLine );
		}
	}

	@Disabled("Referred to by the blog")
	@Test
	public void testRefcodesExample1() {
		// @formatter:off
		final AsciiArtBuilder theBuilder = new AsciiArtBuilder().
			withFontStyle( FontStyle.BOLD ).withFontFamily( FontFamily.SANS_SERIF ).
				withAsciiColorPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withAsciiArtMode( AsciiArtMode.NORMAL ).
					withColumnWidth( 120 );
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\n" + theBuilder.toString( "DON'T PANIC" ) );
		}
	}

	@Disabled("Referred to by the blog")
	@Test
	public void testRefcodesExample2() {
		// @formatter:off
		final AsciiArtBuilder theBuilder = new AsciiArtBuilder().
			withFontStyle( FontStyle.BOLD_ITALIC ).withFontFamily( FontFamily.SANS_SERIF ).
				withAsciiColorPalette( AsciiColorPalette.HALFTONE_GRAY ).
					withColumnWidth( 120 );
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\n" + theBuilder.toString( "HELLO WORLD!" ) );
		}
	}

	@Disabled("Referred to by the blog")
	@Test
	public void testRefcodesExample3() {
		// @formatter:off
		final AsciiArtBuilder theBuilder = new AsciiArtBuilder().
			withFontStyle( FontStyle.BOLD_ITALIC ).withFontFamily( FontFamily.SANS_SERIF ).
				withAsciiColorPalette( AsciiColorPalette.HALFTONE_GRAY ).
					withColumnWidth( 120 ).withAsciiArtMode( AsciiArtMode.INVERSE );
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\n" + theBuilder.toString( "HELLO WORLD!" ) );
		}
	}

	@Disabled("Referred to by the blog")
	@Test
	public void testRefcodesExample4() {
		// @formatter:off
		final AsciiArtBuilder theBuilder = new AsciiArtBuilder().
			withFontStyle( FontStyle.BOLD_ITALIC ).withFontFamily( FontFamily.SANS_SERIF ).
				withAsciiColors( new char[] {'.', '/', '#'} ).
					withColumnWidth( 120 );
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\n" + theBuilder.toString( "HELLO WORLD!" ) );
		}
	}

	@Test
	public void testSimpleBanner1() {

		// ---------------------------------------------------------------------
		final String aText = "Tiny-Banner";
		final int aWidth = 80;
		final AsciiArtMode aAsciiArtMode = AsciiArtMode.NORMAL;
		final char[] aPalette = AsciiColorPalette.HALFTONE_GRAY.getPalette();
		// ---------------------------------------------------------------------
		final String[] theResult = AsciiArtBuilder.asSimpleBanner( aText, aWidth, aAsciiArtMode, aPalette );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eLine : theResult ) {
				System.out.println( eLine );
			}
		}
		for ( String eLine : theResult ) {
			assertEquals( aWidth, eLine.length() );
		}
	}

	@Test
	public void testSimpleBanner2() {

		// ---------------------------------------------------------------------
		final String aText = "InverseBanner";
		final int aWidth = 80;
		final AsciiArtMode aAsciiArtMode = AsciiArtMode.INVERSE;
		final char[] aPalette = AsciiColorPalette.HALFTONE_GRAY.getPalette();
		// ---------------------------------------------------------------------
		final String[] theResult = AsciiArtBuilder.asSimpleBanner( aText, aWidth, aAsciiArtMode, aPalette );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eLine : theResult ) {
				System.out.println( eLine );
			}
		}
		for ( String eLine : theResult ) {
			assertEquals( aWidth, eLine.length() );
		}
	}

	@Test
	public void testSimpleBanner3() {

		// ---------------------------------------------------------------------
		final String aText = "NormalBanner";
		final int aWidth = 29;
		final AsciiArtMode aAsciiArtMode = AsciiArtMode.NORMAL;
		final char[] aPalette = AsciiColorPalette.HALFTONE_GRAY.getPalette();
		// ---------------------------------------------------------------------
		final String[] theResult = AsciiArtBuilder.asSimpleBanner( aText, aWidth, aAsciiArtMode, aPalette );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eLine : theResult ) {
				System.out.println( eLine );
			}
		}
		for ( String eLine : theResult ) {
			assertEquals( aWidth, eLine.length() );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void toOut( String[] aLines ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eLine : aLines ) {
				System.out.println( "\"" + eLine + "\"" + " [" + eLine.length() + "]" );
			}
			System.out.println( "\"" + new TextLineBuilder().withColumnWidth( aLines[0].length() ).withLineChar( '-' ).toString() + "\"" );
		}
	}
}
