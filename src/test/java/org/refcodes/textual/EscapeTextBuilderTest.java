// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;

public class EscapeTextBuilderTest {

	private static final String UNESCAPED_TEXT_1 = "12345-abcde+12345/abcde";
	private static final String ESCAPED_TEXT_1 = "12345XXXMINUSXXXabcdeXXXPLUSXXX12345XXXSLASHXXXabcde";
	private static final Property[] ESCAPE_MATRIX = new Property[] { new PropertyImpl( "-", "XXXMINUSXXX" ), new PropertyImpl( "+", "XXXPLUSXXX" ), new PropertyImpl( "*", "XXXASTERISKXXX" ), new PropertyImpl( "/", "XXXSLASHXXX" ), new PropertyImpl( "~", "XXXTILDEXXX" ), new PropertyImpl( "#", "XXXHASHXXX" ) };

	@Test
	public void testFromEscapedToUnescaped() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "UNESCAPED_TEXT   = " + UNESCAPED_TEXT_1 );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ESCAPED_TEXT     = " + ESCAPED_TEXT_1 );
		}
		final String theEscapedText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.ESCAPE ).withText( UNESCAPED_TEXT_1 ).withEscapeProperties( ESCAPE_MATRIX ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "theEscapedText   = " + theEscapedText );
		}
		assertEquals( ESCAPED_TEXT_1, theEscapedText );
		final String theUnEscapedText = new EscapeTextBuilder().withEscapeTextMode( EscapeTextMode.UNESCAPE ).withText( theEscapedText ).withEscapeProperties( ESCAPE_MATRIX ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "theUnEscapedText = " + theUnEscapedText );
		}
		assertEquals( UNESCAPED_TEXT_1, theUnEscapedText );
	}
}
