// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class NumberSuffixComparatorTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNumberPrefixArray1() {
		final String[] theArray = { "ttyUSB1", "ttyUSB10", "ttyUSB2", "ttyUSB" };
		final String[] theExpected = { "ttyUSB", "ttyUSB1", "ttyUSB2", "ttyUSB10" };
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Before: " + Arrays.toString( theArray ) );
		}
		Arrays.sort( theArray, new NumberSuffixComparator() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sorted: " + Arrays.toString( theArray ) );
		}
		assertArrayEquals( theExpected, theArray );
	}

	@Test
	public void testNumberPrefixArray2() {
		final String[] theArray = { "ttyUSB22", "tty0", "com19", "ttyUSB1", "com7", "ttyUSB10", "ttyUSB2", "com", "ttyUSB", "tty10", "tty3", "com7", "com0" };
		final String[] theExpected = { "com", "com0", "com7", "com7", "com19", "tty0", "tty3", "tty10", "ttyUSB", "ttyUSB1", "ttyUSB2", "ttyUSB10", "ttyUSB22" };
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Before: " + Arrays.toString( theArray ) );
		}
		Arrays.sort( theArray, new NumberSuffixComparator() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Sorted: " + Arrays.toString( theArray ) );
		}
		assertArrayEquals( theExpected, theArray );
	}
}
