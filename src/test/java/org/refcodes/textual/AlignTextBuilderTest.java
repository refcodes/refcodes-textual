// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class AlignTextBuilderTest.
 */
public class AlignTextBuilderTest {
	/**
	 * Test text align builder.
	 */
	@Test
	public void testTextAlignBuilder() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 10 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 10 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 10 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 4 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 4 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 4 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 2 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 2 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 2 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 0 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 0 ).withFillChar( '_' ).toString() );
			System.out.println( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 0 ).withFillChar( '_' ).toString() );
		}
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 10 ).withFillChar( '_' ).toString(), "___Text___" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 10 ).withFillChar( '_' ).toString(), "Text______" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 10 ).withFillChar( '_' ).toString(), "______Text" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 4 ).withFillChar( '_' ).toString(), "Text" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 4 ).withFillChar( '_' ).toString(), "Text" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 4 ).withFillChar( '_' ).toString(), "Text" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 2 ).withFillChar( '_' ).toString(), "ex" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 2 ).withFillChar( '_' ).toString(), "Te" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 2 ).withFillChar( '_' ).toString(), "xt" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( "Text" ).withColumnWidth( 0 ).withFillChar( '_' ).toString(), "" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( "Text" ).withColumnWidth( 0 ).withFillChar( '_' ).toString(), "" );
		assertEquals( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( "Text" ).withColumnWidth( 0 ).withFillChar( '_' ).toString(), "" );
	}

	@Test
	public void testTextAlignBuilderWithFixedWidth() {
		final String notFixedLengthText = "123456789012345";
		final String leftFixedLength10Text = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( notFixedLengthText ).withColumnWidth( 10 ).withFillChar( '*' ).toString();
		final String leftFixedLength15Text = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( notFixedLengthText ).withColumnWidth( 15 ).withFillChar( '*' ).toString();
		final String leftFixedLength20Text = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.LEFT ).withText( notFixedLengthText ).withColumnWidth( 20 ).withFillChar( '*' ).toString();
		System.out.println( "TextUtility.toFixedLengthString(): notFixedLengthText = <" + notFixedLengthText + ">, leftFixedLength10Text = <" + leftFixedLength10Text + ">." );
		System.out.println( "TextUtility.toFixedLengthString(): notFixedLengthText = <" + notFixedLengthText + ">, leftFixedLength15Text = <" + leftFixedLength15Text + ">." );
		System.out.println( "TextUtility.toFixedLengthString(): notFixedLengthText = <" + notFixedLengthText + ">, leftFixedLength20Text = <" + leftFixedLength20Text + ">." );
		// Right fixed fill:
		final String rightFixedLength10Text = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( notFixedLengthText ).withColumnWidth( 10 ).withFillChar( '*' ).toString();
		final String rightFixedLength15Text = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( notFixedLengthText ).withColumnWidth( 15 ).withFillChar( '*' ).toString();
		final String rightFixedLength20Text = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( notFixedLengthText ).withColumnWidth( 20 ).withFillChar( '*' ).toString();
		System.out.println( "TextUtility.toFixedLengthString(): notFixedLengthText = <" + notFixedLengthText + ">, rightFixedLength10Text = <" + rightFixedLength10Text + ">." );
		System.out.println( "TextUtility.toFixedLengthString(): notFixedLengthText = <" + notFixedLengthText + ">, rightFixedLength15Text = <" + rightFixedLength15Text + ">." );
		System.out.println( "TextUtility.toFixedLengthString(): notFixedLengthText = <" + notFixedLengthText + ">, rightFixedLength20Text = <" + rightFixedLength20Text + ">." );
		// Set the return value:
		if ( !"1234567890".equals( leftFixedLength10Text ) ) {
			fail( "ERROR: notFixedLengthText = <" + notFixedLengthText + ">, leftFixedLength10Text = <" + leftFixedLength10Text + ">, expected = <6789012345>." );
		}
		if ( !"123456789012345".equals( leftFixedLength15Text ) ) {
			fail( "TextUtility.toFixedLengthString(): ERROR: notFixedLengthText = <" + notFixedLengthText + ">, leftFixedLength10Text = <" + leftFixedLength15Text + ">, expected = <123456789012345>." );
		}
		if ( !"123456789012345*****".equals( leftFixedLength20Text ) ) {
			fail( "TextUtility.toFixedLengthString(): ERROR: notFixedLengthText = <" + notFixedLengthText + ">, leftFixedLength10Text = <" + leftFixedLength20Text + ">, expected = <*****123456789012345>." );
		}
		if ( !"6789012345".equals( rightFixedLength10Text ) ) {
			fail( "TextUtility.toFixedLengthString(): ERROR: notFixedLengthText = <" + notFixedLengthText + ">, rightFixedLength10Text = <" + rightFixedLength10Text + ">, expected = <1234567890>." );
		}
		if ( !"123456789012345".equals( rightFixedLength15Text ) ) {
			fail( "TextUtility.toFixedLengthString(): ERROR: notFixedLengthText = <" + notFixedLengthText + ">, rightFixedLength10Text = <" + rightFixedLength15Text + ">, expected = <123456789012345>." );
		}
		if ( !"*****123456789012345".equals( rightFixedLength20Text ) ) {
			fail( "TextUtility.toFixedLengthString(): ERROR: notFixedLengthText = <" + notFixedLengthText + ">, rightFixedLength10Text = <" + rightFixedLength20Text + ">, expected = <123456789012345*****>." );
		}
	}
}
