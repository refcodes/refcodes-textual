// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.ConsoleDimension;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;

public class TableBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final DateTimeFormatter NORM_DATE_FORMAT = DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm:ss" ).withZone( ZoneId.systemDefault() );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToHeader() {
		final TableBuilder theTablePrinter = new TableBuilder();
		theTablePrinter.addColumn().withColumnWidth( 25, ColumnWidthType.RELATIVE );
		theTablePrinter.addColumn().withColumnWidth( 33, ColumnWidthType.RELATIVE );
		theTablePrinter.addColumn().withColumnWidth( 50, ColumnWidthType.RELATIVE ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTablePrinter.addColumn().withColumnWidth( 33, ColumnWidthType.RELATIVE );
		System.out.print( theTablePrinter.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinter.withTableStyle( TableStyle.ASCII_HEADER_ASCII_BODY );
		System.out.print( theTablePrinter.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinter.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		System.out.print( theTablePrinter.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinter.withTableStyle( TableStyle.DOUBLE_SINGLE_HEADER_SINGLE_BODY );
		System.out.print( theTablePrinter.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinter.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_BODY );
		System.out.print( theTablePrinter.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinter.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		System.out.print( theTablePrinter.toHeaderBegin() );
		System.out.print( theTablePrinter.toHeaderContinue( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toHeaderContinue( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toHeaderContinue( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRowContinue( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRowContinue( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRowContinue( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinter.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY ).withTextEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_WHITE, AnsiEscapeCode.BG_BRIGHT_RED ) );
		System.out.print( theTablePrinter.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinter.toRowBegin() );
		System.out.print( theTablePrinter.toRowContinue( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRowContinue( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toRowContinue( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinter.toTail() );
	}

	@Test
	public void testJoinTables() {
		final TableBuilder theTablePrinterA = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterB = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() );
		theTablePrinterB.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterC = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() );
		theTablePrinterC.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterA.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		theTablePrinterB.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		theTablePrinterC.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		System.out.print( theTablePrinterA.toHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" ) );
		System.out.print( theTablePrinterA.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterB.toRowEnd( theTablePrinterA ) );
		System.out.print( theTablePrinterB.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterC.toRowEnd( theTablePrinterB ) );
		System.out.print( theTablePrinterC.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinterB.withRowWidth( ConsoleDimension.MAX_WIDTH.getValue() ).addColumn().withColumnWidth( 79, ColumnWidthType.ABSOLUTE ).withRowHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		System.out.print( theTablePrinterB.toRowEnd( theTablePrinterC ) );
		System.out.print( theTablePrinterB.toRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterB.toRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterC.toRowEnd( theTablePrinterB ) );
		System.out.print( theTablePrinterC.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterC.toRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() );
		System.out.print( theTablePrinterB.toRowEnd( theTablePrinterC ) );
		System.out.print( theTablePrinterB.toRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterB.toRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" ) );
		System.out.print( theTablePrinterB.toTail() );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
	}

	@Test
	public void testPrintTables() {
		final TableBuilder theTablePrinterA = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterB = new TableBuilder().withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() );
		theTablePrinterB.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterC = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterA.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		theTablePrinterB.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		theTablePrinterC.withTableStyle( TableStyle.DOUBLE_HEADER_DOUBLE_SINGLE_BODY );
		theTablePrinterA.printHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" );
		theTablePrinterA.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRowEnd( theTablePrinterA );
		theTablePrinterB.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() ).addColumn().withColumnWidth( 30, ColumnWidthType.ABSOLUTE );
		theTablePrinterB.printRowEnd( theTablePrinterC );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() / 2 );
		theTablePrinterB.printRowEnd( theTablePrinterC );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.withBorderEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) );
		theTablePrinterC.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() / 2 );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printTail();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
	}

	@Test
	public void testPrintSingleBlankTables() {
		final TableBuilder theTablePrinterA = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() ).withLeftBorder( false ).withRightBorder( false ).withTableStyle( TableStyle.SINGLE_BLANK_HEADER_SINGLE_BLANK_BODY );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterB = new TableBuilder().withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() ).withLeftBorder( false ).withRightBorder( false ).withTableStyle( TableStyle.SINGLE_BLANK_HEADER_SINGLE_BLANK_BODY );
		theTablePrinterB.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterC = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() ).withLeftBorder( false ).withRightBorder( false ).withTableStyle( TableStyle.SINGLE_BLANK_HEADER_SINGLE_BLANK_BODY );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterA.printHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" );
		theTablePrinterA.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRowEnd( theTablePrinterA );
		theTablePrinterB.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() ).addColumn().withColumnWidth( 30, ColumnWidthType.ABSOLUTE );
		theTablePrinterB.printRowEnd( theTablePrinterC );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() / 2 );
		theTablePrinterB.printRowEnd( theTablePrinterC );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.withBorderEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) );
		theTablePrinterC.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() / 2 );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printTail();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
	}

	@Test
	public void testPrintAsciiBlankTables() {
		final TableBuilder theTablePrinterA = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() ).withLeftBorder( false ).withRightBorder( false ).withTableStyle( TableStyle.ASCII_BLANK_HEADER_ASCII_BLANK_BODY );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterA.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterB = new TableBuilder().withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() ).withLeftBorder( false ).withRightBorder( false ).withTableStyle( TableStyle.ASCII_BLANK_HEADER_ASCII_BLANK_BODY );
		theTablePrinterB.addColumn().withColumnWidth( 1, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterB.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		final TableBuilder theTablePrinterC = new TableBuilder().withRowWidth( ConsoleDimension.MIN_WIDTH.getValue() ).withLeftBorder( false ).withRightBorder( false ).withTableStyle( TableStyle.ASCII_BLANK_HEADER_ASCII_BLANK_BODY );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 2, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 4, ColumnWidthType.RELATIVE );
		theTablePrinterC.addColumn().withColumnWidth( 3, ColumnWidthType.RELATIVE );
		theTablePrinterA.printHeader( "AAAAA", "BBBBB", "CCCCC", "DDDDD" );
		theTablePrinterA.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRowEnd( theTablePrinterA );
		theTablePrinterB.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() ).addColumn().withColumnWidth( 30, ColumnWidthType.ABSOLUTE );
		theTablePrinterB.printRowEnd( theTablePrinterC );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() / 2 );
		theTablePrinterB.printRowEnd( theTablePrinterC );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterB.printRow( "ATARI", "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.withBorderEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) );
		theTablePrinterC.withRowWidth( ConsoleDimension.NORM_WIDTH.getValue() / 2 );
		theTablePrinterC.printRowEnd( theTablePrinterB );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printRow( "XX XX XX", "YY YY YY", "ZZ ZZ ZZ", "QQ QQ QQ" );
		theTablePrinterC.printTail();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println();
		}
	}

	@Test
	public void testAnsiLoggerTable() {
		final RandomTextGenerartor theRndTextGenerator = new RandomTextGenerartor().withColumnWidth( 128 ).withRandomTextMode( RandomTextMode.ALPHANUMERIC );
		final TableBuilder theTablePrinter = new TableBuilder();
		theTablePrinter.addColumn().withColumnWidth( 7, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_DEFAULT ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTablePrinter.addColumn().withColumnWidth( 7, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_GREEN ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.CENTER );
		theTablePrinter.addColumn().withColumnWidth( 10, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_WHITE, AnsiEscapeCode.BG_BRIGHT_RED ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 19, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_DEFAULT ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTablePrinter.addColumn().withColumnWidth( 16, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 22, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 45, ColumnWidthType.RELATIVE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_GREEN ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 55, ColumnWidthType.RELATIVE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		if ( theTablePrinter.getRowWidth() < 160 ) {
			theTablePrinter.withRowWidth( 160 );
		}
		for ( int i = 0; i < 25; i++ ) {
			theTablePrinter.printRow( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( Integer.toString( i ) ).withColumnWidth( 7 ).withFillChar( '0' ).toString(), "INFO", "Thread-" + i, NORM_DATE_FORMAT.format( Instant.now() ), Execution.getCallerStackTraceElement().getMethodName(), Execution.getCallerStackTraceElement().getClassName(), theRndTextGenerator.next(), "" );
		}
		theTablePrinter.printTail();
	}

	@Test
	public void testLoggerTable() {
		final RandomTextGenerartor theRndTextGenerator = new RandomTextGenerartor().withColumnWidth( 128 ).withRandomTextMode( RandomTextMode.ALPHANUMERIC );
		final TableBuilder theTablePrinter = new TableBuilder();
		theTablePrinter.addColumn().withColumnWidth( 7, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.CENTER );
		theTablePrinter.addColumn().withColumnWidth( 7, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.CENTER );
		theTablePrinter.addColumn().withColumnWidth( 10, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.CENTER );
		theTablePrinter.addColumn().withColumnWidth( 19, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTablePrinter.addColumn().withColumnWidth( 16, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 22, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 45, ColumnWidthType.RELATIVE ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 55, ColumnWidthType.RELATIVE ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		if ( theTablePrinter.getRowWidth() < 160 ) {
			theTablePrinter.withRowWidth( 160 );
		}
		for ( int i = 0; i < 25; i++ ) {
			theTablePrinter.printRow( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( Integer.toString( i ) ).withColumnWidth( 7 ).withFillChar( '0' ).toString(), "INFO", "Thread-" + i, NORM_DATE_FORMAT.format( Instant.now() ), Execution.getCallerStackTraceElement().getMethodName(), Execution.getCallerStackTraceElement().getClassName(), theRndTextGenerator.next(), "" );
		}
		theTablePrinter.printTail();
	}

	@Test
	public void testMoreLoggerTable() {
		final RandomTextGenerartor theRndTextGenerator = new RandomTextGenerartor().withColumnWidth( 128 ).withRandomTextMode( RandomTextMode.ALPHANUMERIC );
		final TableBuilder theTablePrinter = new TableBuilder().withMoreTextMode( MoreTextMode.CENTER );
		theTablePrinter.addColumn().withColumnWidth( 7, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_DEFAULT ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTablePrinter.addColumn().withColumnWidth( 7, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_GREEN ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.CENTER );
		theTablePrinter.addColumn().withColumnWidth( 10, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_WHITE, AnsiEscapeCode.BG_BRIGHT_RED ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 19, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_DEFAULT ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTablePrinter.addColumn().withColumnWidth( 16, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 22, ColumnWidthType.ABSOLUTE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 45, ColumnWidthType.RELATIVE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_GREEN ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		theTablePrinter.addColumn().withColumnWidth( 55, ColumnWidthType.RELATIVE ).withTextColumnEscapeCode( AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.FG_BRIGHT_BLUE ) ).withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT );
		if ( theTablePrinter.getRowWidth() < 160 ) {
			theTablePrinter.withRowWidth( 160 );
		}
		theTablePrinter.printRowBegin();
		for ( int i = 0; i < 25; i++ ) {
			theTablePrinter.printRowContinue( new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withText( Integer.toString( i ) ).withColumnWidth( 7 ).withFillChar( '0' ).toString(), "INFO", "Thread-" + i, NORM_DATE_FORMAT.format( Instant.now() ), Execution.getCallerStackTraceElement().getMethodName(), Execution.getCallerStackTraceElement().getClassName(), theRndTextGenerator.next(), "" );
		}
		theTablePrinter.printTail();
	}

	@Test
	public void toGoodColumnNumCharWidths() {
		// @formatter:off
		final ColumnWidthMetricsImpl[] theColumnWidths = new ColumnWidthMetricsImpl[] {
			new ColumnWidthMetricsImpl( 1, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 50, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 2, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 25, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 4, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 8, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 25, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 16, ColumnWidthType.ABSOLUTE )
		};
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Provided column widths = " + new VerboseTextBuilder().withElements( theColumnWidths ).toString() );
		}
		int[] theNumChars = testColumnWidths( 63, theColumnWidths );
		assertEquals( theNumChars[0], 1 );
		assertEquals( theNumChars[1], 16 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 8 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 8 );
		assertEquals( theNumChars[7], 16 );
		theNumChars = testColumnWidths( 64, theColumnWidths );
		assertEquals( theNumChars[0], 1 );
		assertEquals( theNumChars[1], 17 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 8 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 8 );
		assertEquals( theNumChars[7], 16 );
		theNumChars = testColumnWidths( 65, theColumnWidths );
		assertEquals( theNumChars[0], 1 );
		assertEquals( theNumChars[1], 18 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 8 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 8 );
		assertEquals( theNumChars[7], 16 );
		theNumChars = testColumnWidths( 66, theColumnWidths );
		assertEquals( theNumChars[0], 1 );
		assertEquals( theNumChars[1], 18 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 9 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 8 );
		assertEquals( theNumChars[7], 16 );
		theNumChars = testColumnWidths( 67, theColumnWidths );
		assertEquals( theNumChars[0], 1 );
		assertEquals( theNumChars[1], 18 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 9 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 9 );
		assertEquals( theNumChars[7], 16 );
	}

	@Test
	public void toOddColumnNumCharWidths() {
		// @formatter:off
		ColumnWidthMetricsImpl[] theColumnWidths = new ColumnWidthMetricsImpl[] {
			new ColumnWidthMetricsImpl( 1, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 50, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 2, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 25, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 4, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 8, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 26, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 16, ColumnWidthType.ABSOLUTE )
		};
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Provided column widths = " + new VerboseTextBuilder().withElements( theColumnWidths ).toString() );
		}
		try {
			TableBuilder.toColumnWidths( 63, theColumnWidths );
		}
		catch ( IllegalArgumentException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Expected exception: " + e.getMessage() );
			}
		}
		// @formatter:off
		theColumnWidths = new ColumnWidthMetricsImpl[] {
			new ColumnWidthMetricsImpl( 1, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 2, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 4, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 8, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 16, ColumnWidthType.ABSOLUTE )
					};
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Provided column widths = " + new VerboseTextBuilder().withElements( theColumnWidths ).toString() );
		}
		try {
			TableBuilder.toColumnWidths( 62, theColumnWidths );
			fail( "Expected this test to fail as we provided more than 63 chars width" );
		}
		catch ( IllegalArgumentException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Expected exception: " + e.getMessage() );
			}
		}
		// @formatter:off
		theColumnWidths = new ColumnWidthMetricsImpl[] {
			new ColumnWidthMetricsImpl( 1, ColumnWidthType.ABSOLUTE ), 
			new ColumnWidthMetricsImpl( 2, ColumnWidthType.ABSOLUTE ), 
			new ColumnWidthMetricsImpl( 4, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 8, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 16, ColumnWidthType.ABSOLUTE )
		};
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Provided column widths = " + new VerboseTextBuilder().withElements( theColumnWidths ).toString() );
		}
		try {
			TableBuilder.toColumnWidths( 30, theColumnWidths );
			fail( "Expected this test to fail as we provided more than 63 chars width" );
		}
		catch ( IllegalArgumentException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Expected exception: " + e.getMessage() );
			}
		}
	}

	@Test
	public void toUglyColumnNumCharWidths() {
		// @formatter:off
		ColumnWidthMetricsImpl[] theColumnWidths = new ColumnWidthMetricsImpl[] {
			new ColumnWidthMetricsImpl( 1, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 0, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 2, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 50, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 4, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 8, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 50, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 16, ColumnWidthType.ABSOLUTE )
		};
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Provided column widths = " + new VerboseTextBuilder().withElements( theColumnWidths ).toString() );
		}
		int[] theNumChars = TableBuilder.toColumnWidths( 63, theColumnWidths );
		toOut( theNumChars );
		assertEquals( theNumChars[0], 1 );
		assertEquals( theNumChars[1], 0 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 16 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 16 );
		assertEquals( theNumChars[7], 16 );
		// @formatter:off
		theColumnWidths = new ColumnWidthMetricsImpl[] {
			new ColumnWidthMetricsImpl( 0, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 50, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 2, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 25, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 4, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 8, ColumnWidthType.ABSOLUTE ),
			new ColumnWidthMetricsImpl( 25, ColumnWidthType.RELATIVE ),
			new ColumnWidthMetricsImpl( 16, ColumnWidthType.ABSOLUTE )
		};
		// @formatter:on
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Provided column widths = " + new VerboseTextBuilder().withElements( theColumnWidths ).toString() );
		}
		theNumChars = TableBuilder.toColumnWidths( 63, theColumnWidths );
		toOut( theNumChars );
		assertEquals( theNumChars[0], 0 );
		assertEquals( theNumChars[1], 17 );
		assertEquals( theNumChars[2], 2 );
		assertEquals( theNumChars[3], 8 );
		assertEquals( theNumChars[4], 4 );
		assertEquals( theNumChars[5], 8 );
		assertEquals( theNumChars[6], 8 );
		assertEquals( theNumChars[7], 16 );
	}

	@Test
	public void testExampleBuilder() {
		// @formatter:off
		final SysRecord[] someSysRecords = new SysRecord[] {
				toRandomSysRecord(), toRandomSysRecord(), toRandomSysRecord(), 
				toRandomSysRecord(), toRandomSysRecord()
		};
		final TableBuilder theTableBuilder = new TableBuilder().
			withRowWidth( 80 ). withTableStyle( TableStyle.DOUBLE_SINGLE_HEADER_SINGLE_DASHED_BODY ).
			addColumn(). // SYSTEM-FILE
			addColumn().withRowColumnHorizAlignTextMode( HorizAlignTextMode.CENTER ).withColumnWidth( 6 ). // TID
			addColumn().withColumnWidth( 14 ). // LOCATION
			addColumn().withColumnWidth( 10 ).withRowColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT ). // LOAD
			addColumn().withColumnWidth( 4 ).withRowColumnHorizAlignTextMode( HorizAlignTextMode.CENTER ). // T
			addColumn().withColumnWidth( 9 ).withRowColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT ); // MEM
		theTableBuilder.printHeader( "SYSTEM-FILE", "TID", "LOCATION", "LOAD", "T", "MEM" );
		for ( SysRecord eSysRecord : someSysRecords ) {
			theTableBuilder.printRow( 
				eSysRecord.getName(), 
				eSysRecord.getId(), 
				eSysRecord.getLocation(), 
				eSysRecord.getLoadKbS() + " KB/s", 
				eSysRecord.getThreads(), 
				eSysRecord.getMemoryMb() + " MB"
			);
		}
		theTableBuilder.printTail();
		// @formatter:on
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private SysRecord toRandomSysRecord() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor();
		// @formatter:off
		return new SysRecord( 
			theTextGenerator.withColumnWidth( 8 ).withRandomTextMode( RandomTextMode.UPPER_CASE ).next(),
			theTextGenerator.withColumnWidth( 4 ).withRandomTextMode( RandomTextMode.NUMERIC ).next(),
			theTextGenerator.withColumnWidth( 12 ).withRandomTextMode( RandomTextMode.LOWER_CASE ).next(),
			theTextGenerator.withColumnWidth( 3 ).withRandomTextMode( RandomTextMode.NUMERIC ).next(),
			theTextGenerator.withColumnWidth( 2 ).withRandomTextMode( RandomTextMode.NUMERIC ).next(),
			theTextGenerator.withColumnWidth( 4 ).withRandomTextMode( RandomTextMode.NUMERIC ).next()
		);
		// @formatter:on
	}

	private int[] testColumnWidths( int aTotalWidth, ColumnWidthMetricsImpl[] aColumnWidths ) {
		final int[] theNumChars = TableBuilder.toColumnWidths( aTotalWidth, aColumnWidths );
		toOut( theNumChars );
		assertEquals( aTotalWidth, NumericalUtility.sum( theNumChars ) );
		assertEquals( aColumnWidths.length, theNumChars.length );
		for ( int i = 0; i < theNumChars.length; i++ ) {
			if ( aColumnWidths[i].getColumnWidthType() == ColumnWidthType.ABSOLUTE ) {
				assertEquals( aColumnWidths[i].getColumnWidth(), theNumChars[i] );
			}
		}
		return theNumChars;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class SysRecord {
		private final String name;
		private final String id;
		private final String location;
		private final String loadKbS;
		private final String threads;
		private final String memoryMb;

		public SysRecord( String name, String id, String location, String loadKbS, String threads, String memoryMb ) {
			this.name = name;
			this.id = id;
			this.location = location;
			this.loadKbS = loadKbS;
			this.threads = threads;
			this.memoryMb = memoryMb;
		}

		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public String getLocation() {
			return location;
		}

		public String getLoadKbS() {
			return loadKbS;
		}

		public String getThreads() {
			return threads;
		}

		public String getMemoryMb() {
			return memoryMb;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected void toOut( int[] aIntValues ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final Integer[] theNumCharObjs = new Integer[aIntValues.length];
			for ( int i = 0; i < aIntValues.length; i++ ) {
				theNumCharObjs[i] = aIntValues[i];
			}
			System.out.println( "Calculated column widths = " + new VerboseTextBuilder().withElements( theNumCharObjs ).toString() );
		}
	}
}
