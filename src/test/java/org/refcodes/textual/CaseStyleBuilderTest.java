// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class CaseStyleBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	private static final String[] NAMES = new String[]{
		"mySystemProperty",
		"MY_SYSTEM_PROPERTY",
		"my_system_property",
		"My_System_Property",
		"My-System-Property",
		"_My-System-Property",
		"mySystemProperty__",
		"my___System--Property__",
	};
	// @formatter:on

	private static final String SNAKE_CASE = "MY_SYSTEM_PROPERTY";
	private static final String SNAKE_LOWER = "my_system_property";
	private static final String KEBAB_CASE = "MY-SYSTEM-PROPERTY";
	private static final String KEBAB_LOWER = "my-system-property";
	private static final String CAMEL_CASE = "mySystemProperty";
	private static final String PASCAL_CASE = "MySystemProperty";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPascalEdgeCase() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		String thePascalCase = theBuilder.toPascalCase( "a" );
		assertEquals( "A", thePascalCase );
		thePascalCase = theBuilder.toPascalCase( "a_a" );
		assertEquals( "AA", thePascalCase );
		thePascalCase = theBuilder.toPascalCase( "aa_aa_aa" );
		assertEquals( "AaAaAa", thePascalCase );
		thePascalCase = theBuilder.toPascalCase( "a_____" );
		assertEquals( "A", thePascalCase );
		thePascalCase = theBuilder.toPascalCase( "aA" );
		assertEquals( "AA", thePascalCase );
		thePascalCase = theBuilder.toPascalCase( "aa" );
		assertEquals( "Aa", thePascalCase );
	}

	@Test
	public void testSnakeCase() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toSnakeCase( eText ) );
			}
			assertEquals( SNAKE_CASE, theBuilder.toSnakeCase( eText ) );
		}
	}

	@Test
	public void testSnakeCaseWithState() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withText( eText ).toSnakeCase() );
			}
			assertEquals( SNAKE_CASE, theBuilder.withText( eText ).toSnakeCase() );
		}
	}

	@Test
	public void testSnakeLower() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder().withCase( Case.LOWER );
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toSnakeCase( eText ) );
			}
			assertEquals( SNAKE_LOWER, theBuilder.toSnakeCase( eText ) );
		}
	}

	@Test
	public void testSnakeLowerWithState() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder().withCase( Case.LOWER );
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withText( eText ).toSnakeCase() );
			}
			assertEquals( SNAKE_LOWER, theBuilder.withText( eText ).toSnakeCase() );
		}
	}

	@Test
	public void testKebabCase() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toKebabCase( eText ) );
			}
			assertEquals( KEBAB_CASE, theBuilder.toKebabCase( eText ) );
		}
	}

	@Test
	public void testKebabCaseWithState() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withText( eText ).toKebabCase() );
			}
			assertEquals( KEBAB_CASE, theBuilder.withText( eText ).toKebabCase() );
		}
	}

	@Test
	public void testKebabLower() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder().withCase( Case.LOWER );
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toKebabCase( eText ) );
			}
			assertEquals( KEBAB_LOWER, theBuilder.toKebabCase( eText ) );
		}
	}

	@Test
	public void testKebabLowerWithState() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder().withCase( Case.LOWER );
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withText( eText ).toKebabCase() );
			}
			assertEquals( KEBAB_LOWER, theBuilder.withText( eText ).toKebabCase() );
		}
	}

	@Test
	public void testCamelCase() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toCamelCase( eText ) );
			}
			assertEquals( CAMEL_CASE, theBuilder.toCamelCase( eText ) );
		}
	}

	@Test
	public void testCamelCaseWithState() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withText( eText ).toCamelCase() );
			}
			assertEquals( CAMEL_CASE, theBuilder.withText( eText ).toCamelCase() );
		}
	}

	@Test
	public void testPascalCase() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.toPascalCase( eText ) );
			}
			assertEquals( PASCAL_CASE, theBuilder.toPascalCase( eText ) );
		}
	}

	@Test
	public void testPascalCaseWithState() {
		final CaseStyleBuilder theBuilder = new CaseStyleBuilder();
		for ( String eText : NAMES ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theBuilder.withText( eText ).toPascalCase() );
			}
			assertEquals( PASCAL_CASE, theBuilder.withText( eText ).toPascalCase() );
		}
	}
}
