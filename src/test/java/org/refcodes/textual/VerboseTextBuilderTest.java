// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class VerboseTextBuilderTest.
 */
public class VerboseTextBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final byte[] PRIMITIVE_BYTES = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	private static final Byte[] OBJECT_BYTES = new Byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	private static final String[] STRINGS = new String[] { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "zero" };
	private static final char[] CHARS = new char[] { 'a', 'b', 'c', 'd', 'e', 'f' };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSingleElement() {
		final String theVerbose = new VerboseTextBuilder().withElements( "Hallo" ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ \"Hallo\" }", theVerbose );
	}

	@Test
	public void testSingleElementThreadSafe() {
		final String theVerbose = new VerboseTextBuilder().toString( "Hallo" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ \"Hallo\" }", theVerbose );
	}

	@Test
	public void testPrimitiveByteArray() {
		final String theVerbose = new VerboseTextBuilder().withElements( PRIMITIVE_BYTES ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }", theVerbose );
	}

	@Test
	public void testPrimitiveByteArrayThreadSafe() {
		final String theVerbose = new VerboseTextBuilder().toString( PRIMITIVE_BYTES );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }", theVerbose );
	}

	@Test
	public void testObjectByteArray() {
		final String theVerbose = new VerboseTextBuilder().withElements( OBJECT_BYTES ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }", theVerbose );
	}

	@Test
	public void testObjectByteArrayThreadSafe() {
		final String theVerbose = new VerboseTextBuilder().toString( OBJECT_BYTES );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }", theVerbose );
	}

	@Test
	public void testStringArray() {
		final String theVerbose = new VerboseTextBuilder().withElements( STRINGS ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ \"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"zero\" }", theVerbose );
	}

	@Test
	public void testCharArray() {
		final String theVerbose = new VerboseTextBuilder().withElements( CHARS ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 'a', 'b', 'c', 'd', 'e', 'f' }", theVerbose );
	}

	@Test
	public void testStringArrayThreadSafe() {
		final String theVerbose = new VerboseTextBuilder().toString( STRINGS );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ \"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"zero\" }", theVerbose );
	}

	@Test
	public void testObjectByteCollection() {
		final String theVerbose = new VerboseTextBuilder().withElements( Arrays.asList( OBJECT_BYTES ) ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }", theVerbose );
	}

	@Test
	public void testObjectByteCollectionThreadSafe() {
		final String theVerbose = new VerboseTextBuilder().toString( Arrays.asList( OBJECT_BYTES ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }", theVerbose );
	}

	@Test
	public void testStringCollection() {
		final String theVerbose = new VerboseTextBuilder().withElements( Arrays.asList( STRINGS ) ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ \"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"zero\" }", theVerbose );
	}

	@Test
	public void testStringCollectionThreadSafe() {
		final String theVerbose = new VerboseTextBuilder().toString( Arrays.asList( STRINGS ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{ \"one\", \"two\", \"three\", \"four\", \"five\", \"six\", \"seven\", \"eight\", \"nine\", \"zero\" }", theVerbose );
	}

	@Test
	public void testStringMap() {
		final Map<String, Integer> theMap = new HashMap<>();
		theMap.put( "one", 1 );
		theMap.put( "two", 2 );
		theMap.put( "three", 3 );
		theMap.put( "four", 4 );
		theMap.put( "five", 5 );
		theMap.put( "six", 6 );
		theMap.put( "seven", 7 );
		theMap.put( "eight", 8 );
		theMap.put( "nine", 9 );
		theMap.put( "zero", 0 );
		final String theVerbose = new VerboseTextBuilder().withElements( theMap ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{\n" + "  zero = 0,\n" + "  nine = 9,\n" + "  six = 6,\n" + "  four = 4,\n" + "  one = 1,\n" + "  seven = 7,\n" + "  two = 2,\n" + "  three = 3,\n" + "  five = 5,\n" + "  eight = 8\n" + "}", theVerbose );
	}

	@Test
	public void testStringMapTreadSafe() {
		final Map<String, Integer> theMap = new HashMap<>();
		theMap.put( "one", 1 );
		theMap.put( "two", 2 );
		theMap.put( "three", 3 );
		theMap.put( "four", 4 );
		theMap.put( "five", 5 );
		theMap.put( "six", 6 );
		theMap.put( "seven", 7 );
		theMap.put( "eight", 8 );
		theMap.put( "nine", 9 );
		theMap.put( "zero", 0 );
		final String theVerbose = new VerboseTextBuilder().toString( theMap );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{\n" + "  zero = 0,\n" + "  nine = 9,\n" + "  six = 6,\n" + "  four = 4,\n" + "  one = 1,\n" + "  seven = 7,\n" + "  two = 2,\n" + "  three = 3,\n" + "  five = 5,\n" + "  eight = 8\n" + "}", theVerbose );
	}

	@Test
	public void testAnotherStringMap() {
		final Map<String, int[]> theMap = new HashMap<>();
		theMap.put( "one", new int[] { 1 } );
		theMap.put( "two", new int[] { 2, 2 } );
		theMap.put( "three", new int[] { 3, 3, 3 } );
		theMap.put( "four", new int[] { 4, 4, 4, 4 } );
		theMap.put( "five", new int[] { 5, 5, 5, 5, 5 } );
		theMap.put( "six", new int[] { 6, 6, 6, 6, 6, 6 } );
		theMap.put( "seven", new int[] { 7, 7, 7, 7, 7, 7, 7 } );
		theMap.put( "eight", new int[] { 8, 8, 8, 8, 8, 8, 8, 8 } );
		theMap.put( "nine", new int[] { 9, 9, 9, 9, 9, 9, 9, 9, 9 } );
		theMap.put( "zero", new int[] {} );
		final String theVerbose = new VerboseTextBuilder().withElements( theMap ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{\n" + "  zero = {},\n" + "  nine = { 9, 9, 9, 9, 9, 9, 9, 9, 9 },\n" + "  six = { 6, 6, 6, 6, 6, 6 },\n" + "  four = { 4, 4, 4, 4 },\n" + "  one = { 1 },\n" + "  seven = { 7, 7, 7, 7, 7, 7, 7 },\n" + "  two = { 2, 2 },\n" + "  three = { 3, 3, 3 },\n" + "  five = { 5, 5, 5, 5, 5 },\n" + "  eight = { 8, 8, 8, 8, 8, 8, 8, 8 }\n" + "}", theVerbose );
	}

	@Test
	public void testAnotherStringMapThreadSafe() {
		final Map<String, int[]> theMap = new HashMap<>();
		theMap.put( "one", new int[] { 1 } );
		theMap.put( "two", new int[] { 2, 2 } );
		theMap.put( "three", new int[] { 3, 3, 3 } );
		theMap.put( "four", new int[] { 4, 4, 4, 4 } );
		theMap.put( "five", new int[] { 5, 5, 5, 5, 5 } );
		theMap.put( "six", new int[] { 6, 6, 6, 6, 6, 6 } );
		theMap.put( "seven", new int[] { 7, 7, 7, 7, 7, 7, 7 } );
		theMap.put( "eight", new int[] { 8, 8, 8, 8, 8, 8, 8, 8 } );
		theMap.put( "nine", new int[] { 9, 9, 9, 9, 9, 9, 9, 9, 9 } );
		theMap.put( "zero", new int[] {} );
		final String theVerbose = new VerboseTextBuilder().toString( theMap );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theVerbose );
		}
		assertEquals( "{\n" + "  zero = {},\n" + "  nine = { 9, 9, 9, 9, 9, 9, 9, 9, 9 },\n" + "  six = { 6, 6, 6, 6, 6, 6 },\n" + "  four = { 4, 4, 4, 4 },\n" + "  one = { 1 },\n" + "  seven = { 7, 7, 7, 7, 7, 7, 7 },\n" + "  two = { 2, 2 },\n" + "  three = { 3, 3, 3 },\n" + "  five = { 5, 5, 5, 5, 5 },\n" + "  eight = { 8, 8, 8, 8, 8, 8, 8, 8 }\n" + "}", theVerbose );
	}
}
