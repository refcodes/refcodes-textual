// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.CharSet;
import org.refcodes.mixin.TruncateMode;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class TruncateTextBuilderTest.
 */
public class TruncateTextBuilderTest {
	/**
	 * Tests the string utility class.
	 */
	@Test
	public void testTextStripBuilder() {
		final String text = "Rock'n'Roll";
		final String find = "'";
		final String replace = "\\'";
		final String out;
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "starting test()..." );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "text = '" + text + "', find = '" + find + "', replace = '" + replace + "'." );
		}
		out = new ReplaceTextBuilder().withText( text ).withFindText( find ).withReplaceText( replace ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "result = '" + out + "'." );
		}
		String str = "AAAAAA  BBBBB AAAAABBBAAABBBAAA";
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\"" + str + "\"" );
		}
		str = new TruncateTextBuilder().withTruncateMode( TruncateMode.HEAD ).withText( str ).withStripChars( 'A' ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Strip left all 'A' chars: \"" + str + "\"" );
		}
		str = new TruncateTextBuilder().withTruncateMode( TruncateMode.TAIL ).withText( str ).withStripChars( 'A' ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Strip right all 'A' chars: \"" + str + "\"" );
		}
		if ( !"  BBBBB AAAAABBBAAABBB".equals( str ) ) {
			fail( "ERROR [100]" );
		}
		final String str2 = " \t\t\t\t\t\t\tHallo wie gehts?         ";
		final String ret = new TruncateTextBuilder().withTruncateMode( TruncateMode.HEAD_AND_TAIL ).withText( str2 ).withStripChars( CharSet.WHITE_SPACES.getCharSet() ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "\"" + str2 + "\"" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Strip all left and all right WHITESPACE chars: \"" + ret + "\"" );
		}
		if ( !"Hallo wie gehts?".equals( ret ) ) {
			fail( "ERROR [200]" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Prints out the provided array of int values.
	 * 
	 * @param aIntValues The int values to be printed out.
	 */
	protected void toOut( int[] aIntValues ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final Integer[] theNumCharObjs = new Integer[aIntValues.length];
			for ( int i = 0; i < aIntValues.length; i++ ) {
				theNumCharObjs[i] = aIntValues[i];
			}
			System.out.println( "Calculated column widths = " + new VerboseTextBuilder().withElements( theNumCharObjs ).toString() );
		}
	}
}
