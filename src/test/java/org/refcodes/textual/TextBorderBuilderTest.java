// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.graphical.BoxBorderMode;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class TextBorderBuilderTest.
 */
public class TextBorderBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] BLOCK = new String[] { "          ", "          ", "          ", "          ", "          ", "          ", "          ", "          ", "          ", "          " };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTextBorderBuilder() {
		final String[] theBlock = new TextBorderBuilder().withText( BLOCK ).withBoxBorderMode( BoxBorderMode.ALL ).toStrings();
		toOut( theBlock );
		assertEquals( BLOCK[0].length() + 2, theBlock[0].length() );
		assertEquals( BLOCK.length + 2, theBlock.length );
	}

	@Test
	public void testTextBorderBuilderWidth1() {
		final String[] theBlock = new TextBorderBuilder().withText( BLOCK ).withBorderChar( '#' ).withBoxBorderMode( BoxBorderMode.ALL ).withBorderWidth( 1 ).toStrings();
		toOut( theBlock );
		assertEquals( BLOCK[0].length() + 2, theBlock[0].length() );
		assertEquals( BLOCK.length + 2, theBlock.length );
	}

	@Test
	public void testTextBorderBuilderWidth2() {
		final String[] theBlock = new TextBorderBuilder().withText( BLOCK ).withBorderChar( '#' ).withBoxBorderMode( BoxBorderMode.ALL ).withBorderWidth( 2 ).toStrings();
		toOut( theBlock );
		assertEquals( BLOCK[0].length() + 4, theBlock[0].length() );
		assertEquals( BLOCK.length + 4, theBlock.length );
	}

	@Test
	public void testTextBorderBuilderMutation() {
		String[] eBlock;
		int eHorizontal;
		int eVertical;
		for ( BoxBorderMode eMode : BoxBorderMode.values() ) {
			eBlock = new TextBorderBuilder().withText( BLOCK ).withBoxBorderMode( eMode ).withTextBoxGrid( TextBoxStyle.DOUBLE ).toStrings();
			toOut( eBlock );
			eHorizontal = 0;
			if ( eMode.isLeftBorder() ) {
				eHorizontal++;
			}
			if ( eMode.isRightBorder() ) {
				eHorizontal++;
			}
			eVertical = 0;
			if ( eMode.isTopBorder() ) {
				eVertical++;
			}
			if ( eMode.isBottomBorder() ) {
				eVertical++;
			}
			assertEquals( BLOCK[0].length() + eHorizontal, eBlock[0].length() );
			assertEquals( BLOCK.length + eVertical, eBlock.length );
		}
	}

	@Test
	public void testTextBorderBuilderMutationWidth1() {
		String[] eBlock;
		int eHorizontal;
		int eVertical;
		for ( BoxBorderMode eMode : BoxBorderMode.values() ) {
			eBlock = new TextBorderBuilder().withText( BLOCK ).withBorderChar( '#' ).withBoxBorderMode( eMode ).withBorderWidth( 1 ).toStrings();
			toOut( eBlock );
			eHorizontal = 0;
			if ( eMode.isLeftBorder() ) {
				eHorizontal++;
			}
			if ( eMode.isRightBorder() ) {
				eHorizontal++;
			}
			eVertical = 0;
			if ( eMode.isTopBorder() ) {
				eVertical++;
			}
			if ( eMode.isBottomBorder() ) {
				eVertical++;
			}
			assertEquals( BLOCK[0].length() + eHorizontal, eBlock[0].length() );
			assertEquals( BLOCK.length + eVertical, eBlock.length );
		}
	}

	@Test
	public void testTextBorderBuilderMutationWidth2() {
		String[] eBlock;
		int eHorizontal;
		int eVertical;
		for ( BoxBorderMode eMode : BoxBorderMode.values() ) {
			eBlock = new TextBorderBuilder().withText( BLOCK ).withBorderChar( '#' ).withBoxBorderMode( eMode ).withBorderWidth( 2 ).toStrings();
			toOut( eBlock );
			eHorizontal = 0;
			if ( eMode.isLeftBorder() ) {
				eHorizontal += 2;
			}
			if ( eMode.isRightBorder() ) {
				eHorizontal += 2;
			}
			eVertical = 0;
			if ( eMode.isTopBorder() ) {
				eVertical += 2;
			}
			if ( eMode.isBottomBorder() ) {
				eVertical += 2;
			}
			assertEquals( BLOCK[0].length() + eHorizontal, eBlock[0].length() );
			assertEquals( BLOCK.length + eVertical, eBlock.length );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void toOut( String[] aLines ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eLine : aLines ) {
				System.out.println( "\"" + eLine + "\"" + " [" + eLine.length() + "]" );
			}
			System.out.println( "\"" + new TextLineBuilder().withColumnWidth( aLines[0].length() ).withLineChar( '-' ).toString() + "\"" );
		}
	}
}
