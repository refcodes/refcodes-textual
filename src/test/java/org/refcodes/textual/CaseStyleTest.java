// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class CaseStyleTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] INPUT = new String[] { "_caseStyle", "CASE_STYLE", "CASE#STYLE", "case style", "CASE _style" };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNone() {
		for ( String eName : INPUT ) {
			final CaseStyle theStyle = CaseStyle.NONE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + theStyle.toCaseStyle( eName ) );
			}
			assertEquals( eName, theStyle.toCaseStyle( eName ) );
		}
	}

	@Test
	public void testCamelCase() {
		for ( String eName : INPUT ) {
			final CaseStyle theStyle = CaseStyle.CAMEL_CASE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + theStyle.toCaseStyle( eName ) );
			}
			assertEquals( "caseStyle", theStyle.toCaseStyle( eName ) );
		}
	}

	@Test
	public void testPascalCase() {
		for ( String eName : INPUT ) {
			final CaseStyle theStyle = CaseStyle.PASCAL_CASE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + theStyle.toCaseStyle( eName ) );
			}
			assertEquals( "CaseStyle", theStyle.toCaseStyle( eName ) );
		}
	}

	@Test
	public void testSnakeCase() {
		for ( String eName : INPUT ) {
			final CaseStyle thsStyle = CaseStyle.SNAKE_CASE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + thsStyle.toCaseStyle( eName ) );
			}
			assertEquals( "case_style", thsStyle.toCaseStyle( eName ) );
		}
	}

	@Test
	public void testSnakeUpperCase() {
		for ( String eName : INPUT ) {
			final CaseStyle thsStyle = CaseStyle.SNAKE_UPPER_CASE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + thsStyle.toCaseStyle( eName ) );
			}
			assertEquals( "CASE_STYLE", thsStyle.toCaseStyle( eName ) );
		}
	}

	@Test
	public void testKebabCase() {
		for ( String eName : INPUT ) {
			final CaseStyle thsStyle = CaseStyle.KEBAB_CASE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + thsStyle.toCaseStyle( eName ) );
			}
			assertEquals( "case-style", thsStyle.toCaseStyle( eName ) );
		}
	}

	@Test
	public void testKebabUpperCase() {
		for ( String eName : INPUT ) {
			final CaseStyle thsStyle = CaseStyle.KEBAB_UPPER_CASE;
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eName + " := " + thsStyle.toCaseStyle( eName ) );
			}
			assertEquals( "CASE-STYLE", thsStyle.toCaseStyle( eName ) );
		}
	}
}
