// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class ReplaceTextBuilderTest.
 */
public class ReplaceTextBuilderTest {
	/**
	 * Test to replaced.
	 */
	@Test
	public void testToReplaced() {
		final String theTest = "FILE != NULL";
		final String theResult = new ReplaceTextBuilder().withText( theTest ).withFindText( "!=" ).withReplaceText( "IS NOT" ).toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ORIGINAL := " + theTest );
			System.out.println( "REPLACED := " + theResult );
		}
		assertEquals( "FILE IS NOT NULL", theResult );
	}
}
