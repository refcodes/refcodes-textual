// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * The Class RandomTextGeneratorTest.
 *
 * @author steiner
 */
public class RandomTextGeneratorTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("Just used for debugging")
	@Test
	public void testAsciiText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.ASCII );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testAlphabeticText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.ALPHABETIC );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testUpperCaseText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.UPPER_CASE );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testLowerCaseText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.LOWER_CASE );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testNumericText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.NUMERIC );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testBinaryText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.BINARY );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testHexadecimalText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withRandomTextMode( RandomTextMode.HEXADECIMAL );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}

	@Disabled("Just used for debugging")
	@Test
	public void testCustomText() {
		final RandomTextGenerartor theTextGenerator = new RandomTextGenerartor().withColumnWidth( 80 ).withCharSet( new char[] { 'J', 'a', 'k', 'o', 'b', ' ' } );
		for ( int i = 0; i < 25; i++ ) {
			System.out.println( theTextGenerator.next() );
		}
	}
}
