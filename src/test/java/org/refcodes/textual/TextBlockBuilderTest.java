// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.textual;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class TextBlockBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String TEST_TEXT = "=== ÜBERSCHRIFT ===\nIn ei alsbald ubrigen achtete. Oben eins steg gern kopf auf nur.\n=== ÜBERSCHRIFT ===\n Es an lohgruben hochstens gepfiffen wo arbeitete tanzmusik. So gelegen es du spuckte mageren je. Regnet kommen kleine spinat la schale ei du fallen. Behutsam schuppen zu um schlafen funkelte reinlich erzahlte. Ihr beschlo und kriegen lockere offnung konnten wie ten. Um ob so dann mund weit je. Konntest ku blattern ansprach kraftlos mu. === TEST === Uberlegt horchend burschen zu du so kollegen da. Pa ja zueinander dachkammer da dazwischen du knabenhaft. Dem sie klein drauf unter waren lange ihr lie wesen. Warm mein ort hut mag fein ists gut filz. === HEADLINE === Kugeln da da pa stelle leeren. Schlich breiten ach hut dem argerte. === ENDE === Wer kein ward zur die art dame tief also. ";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTextBlockBuilder() {
		final String[] theStrings = new TextBlockBuilder().withText( TEST_TEXT ).withColumnWidth( 90 ).withHorizAlignTextMode( HorizAlignTextMode.BLOCK ).withSplitTextMode( SplitTextMode.AT_SPACE ).toStrings();
		toOut( theStrings );
	}

	@Test
	public void testEdgeCase1() {
		final String[] theStrings = new TextBlockBuilder().withText( "12345678:abcdefgh:ABCDEFGH:1" ).withColumnWidth( 9 ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withSplitTextMode( SplitTextMode.AT_SPACE ).toStrings();
		toOut( theStrings );
		assertEquals( 4, theStrings.length );
		assertEquals( "12345678:", theStrings[0] );
		assertEquals( "abcdefgh:", theStrings[1] );
		assertEquals( "ABCDEFGH:", theStrings[2] );
		assertEquals( "        1", theStrings[3] );
	}

	@Test
	public void testEdgeCase2() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withColumnWidth( 9 ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "0        ", theStrings[1] );
	}

	@Test
	public void testEdgeCase2Right() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_FIRST_END_OF_LINE ).withColumnWidth( 9 ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "        0", theStrings[1] );
	}

	@Test
	public void testEdgeCase3() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_END_OF_LINE ).withColumnWidth( 9 ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "0        ", theStrings[1] );
	}

	@Test
	public void testEdgeCase3Right() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_END_OF_LINE ).withColumnWidth( 9 ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "        0", theStrings[1] );
	}

	@Test
	public void testEdgeCase4() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withColumnWidth( 9 ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "0        ", theStrings[1] );
	}

	@Test
	public void testEdgeCase4Right() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_FIXED_WIDTH ).withColumnWidth( 9 ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "        0", theStrings[1] );
	}

	@Test
	public void testEdgeCase5() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_SPACE ).withColumnWidth( 9 ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "0        ", theStrings[1] );
	}

	@Test
	public void testEdgeCase5Right() {
		final String[] theStrings = new TextBlockBuilder().withText( "1234567890" ).withSplitTextMode( SplitTextMode.AT_SPACE ).withColumnWidth( 9 ).withHorizAlignTextMode( HorizAlignTextMode.RIGHT ).toStrings();
		toOut( theStrings );
		assertEquals( 2, theStrings.length );
		assertEquals( "123456789", theStrings[0] );
		assertEquals( "        0", theStrings[1] );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void toOut( String[] aLines ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( String eLine : aLines ) {
				System.out.println( "\"" + eLine + "\"" + " [" + eLine.length() + "]" );
			}
			System.out.println( "\"" + new TextLineBuilder().withColumnWidth( aLines[0].length() ).withLineChar( '-' ).toString() + "\"" );
		}
	}
}
